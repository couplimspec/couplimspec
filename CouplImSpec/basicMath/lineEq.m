function [a,b]=lineEq(x,y)
%% compute linear equation parameters between two points : tools for interpolation between points
%   y = ax+b
%% input
%
%   x = x axis values : vector or matrix with two colomns
%   y : y axis values : vector or matrix with two columns - same dimension than x
%
%
%% output
%   a : value or vector of the slope(s)
%   b : value or vector of constant
%
%% principle
%   simple computing !

%% Use : 
%   [a,b]=lineEq(x,y)
%   newvalue = a * xnew +b
%% auteurs : 
%   MF Devaux 
%   BIA PVPP
%   21 janvier  2020
%% context
% 


% x axis values or vectors
x1=x(:,1);
x2=x(:,2);

% y-axis values or vectors
y1=y(:,1);
y2=y(:,2);

% compute slope value or vector a
a=(y2-y1)./(x2-x1);

% compute constant value or vector b
b=y2-a*x2;
        
end
    