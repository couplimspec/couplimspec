function R=rotationMatd(ang)
% return the rotation matrix of a given angle in degree

% author : 

% MF Devaux
% BIA-PVPP
%
%  17 spetmebre 2013


R=[cosd(ang) -sind(ang) ;sind(ang) cosd(ang)];


