function [T,sfolder]=invMatTransform(Ti,sfolder)
 %% description
    % invert a affine "register parameter file"
    
%% input

    %  Ti : transformation matrix
    %  save folder
    % or nothing : interactive function
    
%% output
    
    %   T : resulting transformation matrix
    %   sfolder : save folder
   
%% principe
    % ask for register parameter file if nothing is input
   

%% use
    % [T]=invMatTransform(Ti);
    
%% Comments
    % written for Upssala project
    % updated for Dhivyaa project
    
    
%% Author
    % MF Devaux
    % INRA Nantes
    % BIA - PVPP
    
%% date
    % 15 avril 2014 for save folder
    % 27 mars 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >2
    error('use: [T,sfolder]=invMatTransform(Ti,sfolder) or [T,sfolder]=invMatTransform ');
end


if nargin==0
    [nom,rep]=uigetfile({'*.txt'},'registration parameter file','*.register.txt');
    cd (rep)

    [angr,scar,tr]=readRegister(nom,'.');
    Ti=matTransform(angr,scar,tr);
    
    sfolder=uigetdir('.','select save directory');
  
end;

%% treatment
T=inv(Ti);

%% save
if nargin==0

    nom=strrep(nom,'.register.txt','');

    listenom=decoderRegister(nom);

    nb=length(listenom);
    nom=listenom{end};
    for i=(nb-1):-1:1
        nom=strcat(nom,'.TO.',listenom{i});
    end;
    % ligne image=colonne matrice matlab et vice versa
    cd(sfolder)
    writeRegister(T,nom);
    
 %% matlab function tracking  
    name=strrep(nom,'.txt','');
    fid=fopen(strcat(name,'.track.txt'),'w');

    if fid==0
        errordlg('enable to open track file');
    end;

    fprintf(fid,'\r\n%s\n\n',datestr(now,0));
    fprintf(fid,'Invert register parameter files \r\n');
    fprintf(fid,'_________________________________\r\n');

    fprintf(fid,'\r\n register parameter file name: %s.register.txt\r\n',nom);
    fprintf(fid,'data folder: %s\r\n',rep);
    

    fprintf(fid,'\r\n\r\n');
    fprintf(fid,'\r\nsaved resulting register parameter file name : %s.register.txt \r\n',name);
    fprintf(fid,'data folder: %s\r\n',sfolder);

    % save of function used
    fprintf(fid,'__________________________________________________________________________\r\n');
    info=which (mfilename);
    os=computer;        % return the type of computer used : windows, mac...
    switch os(1)
        case 'P'                        % for windows
            ind=strfind(info,'\');                          
        case 'M'                        % for Mac
            ind=strfind(info,'/');
        otherwise
            ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
    end;

    repprog=info(1:(ind(length(ind))-1));
    fprintf(fid,'function name: %s ',mfilename);
    res=dir(info);
    fprintf(fid,'on %s \r\n',res.date);
    fprintf(fid,'function folder: %s \r\n',repprog);
    %fprintf(fid,'__________________________________________________________________________\r\n');

    fclose(fid);
    
end;

%%end
cd(orig)
