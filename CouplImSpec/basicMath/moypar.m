function   [don,n]=moypar(tab,n)
% moyenne des lignes d'un tableau par n lignes � la fois
% les lignes � moyenner doivent �tre contigues
%
% param�tres d'entr�e
%       tab : struture .d .i .v contenant les valeurs � moyenner
%       n : nombre de lignes contigues � moyenner ensemble
%   les param�tres sont optionnels, s'ils ne sont pas pass�s dans la fonction
%       ils sont demand�s int�ractivement
%
% parm�tres de sortie
%       don : structure r�sultat
%       n : renvoi du param�tre nombre de lignes � moyennner
%
% USage : 
%       moypar
%       [don,n]=moypar(tab,n);
%
%   AUteur : Marie-Fran�oise Devaux
%               URPOI-Parois
%               4 octobre 2004
%

porig=pwd;

if (nargin~=0) & (nargin~=2)
    error('Usage : moypar ou [don,n]=moypar(tab,n)');
end;

% entr�e des donn�es si pas de param�tres pass�s � la fonction
if nargin==0
    [nomfic,nomrep]=uigetfile({'*.txt';'all files'},'nom du fichier � moyenner');
    nomfic=strrep(nomfic,'.txt','');
    n=-1;
    while n<1
        rep=inputdlg('ATTENTION : les observations � moyenner doivent se suivre dans le fichier','nombre d''observation � moyenner',1,{'2'});
        n=str2num(char(rep));
    end;
    cd(nomrep);
    tab=readDIV(nomfic);
end;

% si pas de param�tres de sortie, le fichier est sauvegard�
if nargout==0
    [nomsauve,repsauve]=uiputfile({sprintf('*.m%d.txt',n)},'nom du fichier de sauvegarde des r�sultats',sprintf('%s.m%d.txt',nomfic,n));
end;

% test sur le nombre d'observations et n
nind=size(tab.i,1);
if ((nind/n)-floor(nind/n))~=0
    error(sprintf('le nombre d''individus n''est pas un multiple de %d',n));
end;

nb=nind/n;

for i=1:nb
    don.d(i,:)=mean(tab.d(((i-1)*n+1):(i*n),:));
    if i==1
        don.i=tab.i(1,:);
    else
        don.i=char(don.i,tab.i((i-1)*n+1,:));
    end;
end;

don.v=tab.v;

if nargout==0;
    cd(repsauve);
    writeDIV(don,nomsauve);
end;

cd(porig)
    
    
