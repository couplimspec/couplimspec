function [T]=multMatTransform(T1,T2)

 %% description
    % multiplication of an affine transformation = rotation, scale and
    % translation
    
%% input

    %  T1, T2 : the two matrix to multiply
    % or nothin
    
%% output
    
    %   T : resulting transformation matrix

%% principe
    % ask for register parameter file if nothing is input
    % multiply the matrices and saved as a register parameter file if
    % interactive input

%% use
    % [T]=multMatTransform(T1,T2);
    
%% Comments
    % written for Upssala project
    % updated for Dhivyaa project
    
    
%% Author
    % MF Devaux
    % INRA Nantes
    % BIA - PVPP
    
%% date
    % 27 mars 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >2
    error('use: [T]=multMatTransform(T1,T2) or T=multMatTransform ');
end


%% input
if nargin==0
    [nom1,rep1]=uigetfile({'*.txt'},'first register parameter file','*.register.txt');
    cd (rep1)

    [angr1,scar1,tr1]=readRegister(nom1,'.');

    nom1=strrep(nom1,'.register.txt','');
    listenom1=decoderRegister(nom1);
    
    
    T1=matTransform(angr1,scar1,tr1);

    cd(orig);
    
    [nom2,rep2]=uigetfile({'*.txt'},'second register parameter file','*.register.txt');
    cd (rep2)

    [angr2,scar2,tr2]=readRegister(nom2,'.');
    %sens2=ouinon('sens inverse ?');
    nom2=strrep(nom2,'.register.txt','');
    listenom2=decoderRegister(nom2);
    
    if ~strcmp(listenom1{1},strrep(listenom2{end},'.register.txt',''))
        error('not sure that it is a good idea to multiply the two files %s et %s ',nom1,nom2);
    else
        nom1=listenom1{2};
        for i=3:length(listenom1)
            nom1=strcat(nom1,'.TO.',listenom1{i});
        end;
        
        nom2=strrep(nom2,'.register.txt','');
    end;
   T2=matTransform(angr2,scar2,tr2);

    
end;


%% treatement

T=T1*T2;

%% save 
if nargin==0
    name=strcat(listenom2{1},'.TO.',listenom1{end});
    writeRegister(T,strcat(listenom2{1},'.TO.',listenom1{end}));


    %% matlab function tracking  
    name=strrep(name,'.txt','');
    fid=fopen(strcat(name,'.track.txt'),'w');

    if fid==0
        errordlg('enable to open track file');
    end;

    fprintf(fid,'\r\n%s\t',datestr(now,0));
    fprintf(fid,'multiply register parameter files \r\n');
    fprintf(fid,'__________________________________\r\n');

    fprintf(fid,'\r\nfirst register parameter file name: %s.register.txt\r\n',nom1);
    fprintf(fid,'data folder: %s\r\n',rep1);
    fprintf(fid,'\r\nsecond register parameter file name: %s.register.txt\r\n',nom2);
    fprintf(fid,'data folder: %s\r\n',rep2);

    fprintf(fid,'\r\n\r\n');
    fprintf(fid,'\r\nsaved resulting register parameter file name : %s.register.txt \r\n',name);
    fprintf(fid,'data folder: %s\r\n',rep2);

    % save of function used
    fprintf(fid,'__________________________________________________________________________\r\n');
    info=which (mfilename);
    os=computer;        % return the type of computer used : windows, mac...
    switch os(1)
        case 'P'                        % for windows
            ind=strfind(info,'\');                          
        case 'M'                        % for Mac
            ind=strfind(info,'/');
        otherwise
            ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
    end;

    repprog=info(1:(ind(length(ind))-1));
    fprintf(fid,'function name: %s ',mfilename);
    res=dir(info);
    fprintf(fid,'on %s \r\n',res.date);
    fprintf(fid,'function folder: %s \r\n',repprog);
    %fprintf(fid,'__________________________________________________________________________\r\n');

    fclose(fid);
    
end;

%% end

cd (orig)





 







