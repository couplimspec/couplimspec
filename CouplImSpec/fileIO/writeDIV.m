function writeDIV(don,nom)

%% description
% sauvegarde de donn�es matlab de la forme de structure .i .d et .v
%                     dans un fichier texte de type 'txt' 
%
% Par d�faut, l'extension est 'txt'
%
    
%% input

% don : fichier de donn�es
% nom : nom g�n�rique de sauvegarde
    
%% output

% nothing

%% principle

% write a DIV matlab structure as a txt file with
%   - first line = nom var1 var2 var3... 
%   - other lines : name value1 value2 value 3...
% see : 
%%
    % <https://www.dropbox.com/s/586gsiy39wueptm/DIV.docx>
%
%% USAGE : 
%		ecrire(don,'tableau');
%
%% comment
% updated for couplImSpec toolbox
% from function ecrire
% TODO : translate comments in english


%% AUTHOR
% Auteur : 	MF Devaux
%           BIA-PVPP
%           INRA Nantes

%% date
% 25 mars 2014
% 10 f�vrier 2014
%
% Version 2 du 28/8/2000
% Version 2.1 du 18 mars 2004 (/r/n pour les fins de ligne)
% version 3 du 29 novembre 2005 : fichier saisir : �criture rapide des
% fichiers avec la fonction save
% 14 d�cembre 2010 : remplacement des espaces par des tabulations
% suppression des vieux formats...
% 22 janvier 2013 : majuscule du TXT
%

%% context variables 
orig=pwd;           % returns the current directory

%% start
if (nargin~=2) 
   error('use: writeDIV(DIV,filename)');
end;

%% treatement
% �limination de l'extension .txt si elle est pass�e en param�tre dans le nom'
nom=strrep(nom,'.txt','');
nom=strrep(nom,'.TXT','');


%test de l'existence de la structure don.d, don;i et don.v n�cessaire � la sauvegarde
if isstruct(don)
   % test de l'existence des 3 tableaux .d .i et .v
   if isfield(don,'v') && isfield(don,'i') && isfield(don,'d')
      nomfic=strcat(nom,'.txt');
      fic=fopen(nomfic,'w');
      if fic==-1
          error('unable to open file %s', nomfic);
      end;

      % ecriture des variables
      chaine=sprintf('nom');                 % premi�re case 'excel' remplie par la chaine 'nom'
      nv=size(don.v,1);
      if isnumeric(don.v)
          don.v=num2str(don.v);
      end;
      for i=1:nv
          chaine=strcat(chaine,sprintf('\t%s',don.v(i,:)));
      end;
      fwrite(fic,chaine);
      fprintf(fic,'\r\n');

      % �criture des individus et des donn�es
      if isnumeric(don.i)
          don.i=num2str(don.i);
      end;
      ni=size(don.i,1);                % nombre d'individus
      % une colonne de tab....
      tt=num2str(repmat(0:1,ni,1),'\t%d') ;
      tt=tt(:,2);
      tmp=[don.i,tt];
      ifin=size(tmp,2);
      dtmp=num2str(don.d,'%-d\t');
      ifd=size(dtmp,2);
%      dtmp(:,(ifd-1):ifd)='\r\n';
      tmp(:,(ifin+1):(ifin+ifd))=dtmp;
      clear dtmp;
      for i=1:ni
          fwrite(fic,tmp(i,:));
          fprintf(fic,'\r\n');
      end;
      clear tmp;

      fclose(fic);
   end;
else
   error('structure does not contain fields .d .i et .v');
end;

%% matlab function tracking  
% no function tracking

%% end
cd(orig)
