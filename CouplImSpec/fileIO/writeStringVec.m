function writeStringVec(vecteur,nom)

%% description
% ecriture d'un fichier texte sous la forme d'un vecteur de chaines de caracteres


%% input

%   vecteur : tableu de chaine de caharctere
%   nom : nom du fichier : aucune extension n'est rajoutee
%
%% output

%   pas de parmetres de sortie
%

%% Principle

% NB : cette procedure ne necessite pas que toutes les chaines de caracteres
%      aient la meme longueur
%

%% comment
% review for couplimspec toolbox

% adapted from ecrire_vec_char function
% TODO : translate comments in english

%% AUTHOR
% Auteurs : M-F Devaux 
% BIA PVPP

%% date
% 25 mars 2014

% 14 fevrier 2014
% version du 18/06/2010
%

%% start
% test parametres
if (nargin~=2)
	error('USE : writeStringVec(<vector of string>, <filename>)')
end;

if isempty(strfind(nom,'.txt'))
    nom=strcat(nom,'.txt');
end

%% write
% ecriture du fichier
fic=fopen(nom,'w');
if fic==0
    error('cannot write file %s',nom);
end;
nb=size(vecteur,1);
for i=1:nb
    fprintf(fic,'%s\r\n',vecteur(i,:));
end;
fclose(fic);

%% end






				