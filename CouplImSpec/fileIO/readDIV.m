function [don,filename,rfolder]=readDIV(nomfic)

%% description
% lecture d'un fichier de type <txt> sous la forme du format DIV
% CouplImSpec


%% input

%  nothing
% ou
% nomfic : name of file to read with or without the txt extension

%% output

%   don : data table as a DIV structure

%% principe

% 		la premiere ligne doit �tre constituee du nom des variables
% 		separes par des espaces : ATTENTION : ne pas oublier la variable
%       <nom des individus> qui correspond � la premiere colonne du fichier txt
%		les lignes suivantes sont constituees par le nom de l'individu
%		suivi des donnees numeriques
%
% constitution de la structure           don.d      : donnees
%										 don.i      : nom des individus
%										 don.v      : nom des variables
%                                        don.n      : nom du fichier sans
%                                        l'extension txt
%
%
%% USE :
%	 don=readDIV('filename')
%    plot(don.d')       % pour tracer des courbes par exemple
%

%% Comments
% updated for couplimspec toolbox

%% AUTEUR : MF Devaux
%           BIA - PVPP - INRA Nantes
%% date
% 3/10/2018 : return filemane in the don structure
% 22/06/2018 : return filename and rfolder
% 25/03/2014
%
% VERSION 1 du 6/5/2002 URPOI MicMAc
% Version 2 du 3/2/2014
%


%% context variables
orig=pwd;           % returns the current directory


%% start
%
if (nargin>1)
    error('USE : 	 don=readDIV(''filename'')');
end

%% input
if nargin == 0
    [filename,rfolder]=uigetfile({'*.txt'},'select file ');
    
    [~,~,ext] = fileparts(filename);
    
    if ~strcmp(ext,'.txt')
        error('txt file expected');
    end
    nomfic=filename;
else
    nomfic=strrep(nomfic,'.txt','');
    nomfic=strcat(nomfic,'.txt');
    if ~exist(nomfic,'file')
        error('%s file does not exist in folder %s',nomfic,orig);
    end
    rfolder=orig;
end




%% treatment
% ouverture du fichier
cd(rfolder)
fic=fopen(nomfic,'r');
if (fic==-1)
    error('cannot read file %s',nomfic);
end

%lecture de la premiere ligne qui contient les noms des variables
premiereligne=fgetl(fic);
% extraction de la premiere chaine de caractere
[val, ~, ~, next]=sscanf(premiereligne,'%s',1);
% la premiere chaine de caractere est eliminee
premiereligne=premiereligne(next:length(premiereligne));
% constitution du vecteur variable
variable=char(val);	% val='nom'
if strcmp(variable, 'nom')                                      % a priori fichier de donnees !
    % premiere chaine suivante
    [val, ~, ~, next]=sscanf(premiereligne,'%s',1);
    % boucle pour extraire tous les noms de variables jusqu'�
    % val=''
    while (~isempty(val))
        variable=char(variable,char(val));
        premiereligne=premiereligne(next:length(premiereligne));
        [val, ~, ~, next]=sscanf(premiereligne,'%s',1);
    end
    
    
    % premiere partie de la structure  : vecteur des noms de variables
    don.v=variable(2:size(variable,1),:);
    
    % initialisation
    ind=1;
    
    % boucle jusqu'� la fin du fichier
    while ~feof(fic)
        % lecture des lignes suivantes : constituees du nom des individus et des valeurs associees
        ligne{ind}=fgetl(fic); %#ok<AGROW>
        ind=ind+1;
    end
    
    [tok,ligne]=strtok(ligne);
    don.i=char(tok);
    
    don.d=str2num(char(ligne)); %#ok<*ST2NM>
    
else
    errordlg(['file <' nomfic '> is not a DIV file: ''nom'' expected as first word of the file']);
    don=0;
end


fclose(fic);
nomfic=strrep(nomfic,'.txt','');
don.n=nomfic;

%% save

%% track
% no tracking for this fucntion

%% end
cd(orig)

