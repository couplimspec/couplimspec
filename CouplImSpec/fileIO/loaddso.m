  function [dso,rfolder]=loaddso(fname,rfolder)
 %% description
    % load data set object from disk 
        % dso is a dataset object from eigenvector research see
        % http://www.eigenvector.com/software/dataset.htm
    % dso is supposed to be organised as described in file
    % dso.docx (folder help of couplImSpec toolbox)
 %%
    % <https://www.dropbox.com/s/8r9upsgx5j12c9m/DSO.docx>
       
%% input

    % fname : filename
    % rfolder : folder where to read the dso
    
%% output
    
    % the data set object
    % the folder where it has been read

%% principe
    % the filename is a.dso.mat matalb file that only contain a datasetobject
    % from CouplimSpec toolbox
    % load matalb function is used to load the data in the matalb memory
    % the filename is modify to dso and can be fixed to another name
    % depending on the function
    
    
%% use
    % dso=loaddso(fname,rfolder);
    
    
%% Comments
    % written for couplImSpec Toolbox
    
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 27 mars 2014
    % 20 novembre 2015 for retruning rfolder
    

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >2
    error('use: dso=loaddso(fname,rfolder); or dso=loaddso(fname) or dso=loaddso;');
end

if nargin==2
    cd(rfolder)
else
    rfolder=orig;
end
    

if nargin>=1
    if ~exist(fname,'file')
        error('file %s does not exist in folder %d',fname,rfolder);
    end
end

if nargin == 0
    [fname,rfolder]=uigetfile({'*.dso.mat'},'name of dso file','*.dso.mat');
end;

%% treatment 
cd(rfolder)
% load dso file as a tmp variable using matlab load function
tmp=load(fname);
% recover fieldname = name of the dso when it was saved
finame=char(fieldnames(tmp));
% recover the data set object of name finame (use the dynamic fieldnames =
% generate fieldnames from variables
dso=tmp.(finame);


%% no function tracking
%% end

cd (orig)
    