function [vec_char,nom,rep]=readStringVec(nom)
%%description
% lecture d'un fichier texte sous la forme d'un vecteur de chaines de caracteres

%% input

%   nom : nom du fichier a lire (optionnel) : sinon entree interactive    

%% output
    
    %   vec_char : vecteur de chaine de caractere renvoye par la fonction
    %   nom : nom du fihier
    %   rep : repertoire de lecture du fichier
    %


%% principe

    % NB : cette procedure ne necessite pas que toutes les chaines de caracteres
%      aient la m�me longueur

%% use
    % [vec_char,nom,rep]=readStringVec(nom)
    % 
    
%% Comments
% review for CouplImSpec toolbox
    % original function = function lire_vec_char from utile folder
    
%% AUTHOR
    % M-F Devaux 
    % BIA-PVPP -INRA Nantes

%% date
% 25 mars 2014
%   24 mars 2014 : version couplImSpec

% version du 30/06/2003
% BIA-PV
% version du 7 fevrier 2008 pour le renvoi du nom du fichier et du
% repertoire de lecture du fichier
%
% version du 10 septembre 2019 : bug solved : case of one single char array in the file

%% global and context variables
orig=pwd;

%% start

% test parametres
if (nargin~=0)&&(nargin~=1)
	error('USE : [vec_char,nom,rep]=readStringVec(<filename>) or [vec_char,nom,rep]=readStringVec;')
end

%% input

if nargin==0
    [nom,rep]=uigetfile({'*.txt'},'input string vector file');
end

% ajout pour le cas o� le nom sans extension .txt est passe'
% test sur l'existence d'un fichier de nom <nom>.txt
if exist(strcat(nom,'.txt'),'file')
    nom=strcat(nom,'.txt');
end

%% treatment
% lecture du fichier
if exist(nom,'file')
    fid=fopen(nom,'r');
    if (fid==-1)
        error('cannot read file %s',nom);
    end

    res=textscan(fid,'%s',1);
    vec_char=char(res{1});
    if strcmp(vec_char,'nom')
        warndlg(['File <' nom '> maybe a data file'],'********** ATTENTION **********' )
    end
    res=textscan(fid,'%s');
    if ~isempty(res{1})
        vec_char=char(vec_char,char(res{1}));
    end
    
    fclose(fid);
else
    error(['File <' nom '> does not exist'])
end

%%tracking
% no tracking for this function

%% end
cd(orig)



				