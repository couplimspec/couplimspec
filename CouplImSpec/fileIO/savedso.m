  function savedso(dso,sname,sfolder)
 %% description
    % save dataset object on disk 
        % dso is a dataset object from eigenvector research see
        % http://www.eigenvector.com/software/dataset.htm
    % dso is supposed to be organised as described in file
    % dso.docx (folder help of couplImSpec toolbox)
 %%
    % <https://www.dropbox.com/s/8r9upsgx5j12c9m/DSO.docx>
       
%% input

    % dso : data set object
    % sname : save filename
    % sfolder : save folder
    
%% output
    
    % no output 

%% principe
    % save dso as a mat file
    % save the sum of intensities if data set is an image
    
%% use
    % savedso(dso);
    
    
%% Comments
    % updated for couplImSpec Toolbox
    % writen for F.Allouche Phd Thesis
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    %  F. Allouche, MF Devaux   INRA Nantes BIA - PVPP
    
%% date
    % 13 f�vrier 2014
    % 03/03/2011    
    % 29/03/2019 for option '-v7.3' to be able to store large image >2Go

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >3
    error('use: savedso(dso,name,folder)');
end

if nargin==1
    sname=strrep(dso.name,'.dso.mat','');
end;

if nargin<3
    sfolder=pwd;
end;
    
% test that input is a datadet
if ~strcmp(class(dso),'dataset')
    error('input data is not a data set object');
end;


%% save dso
cd(sfolder);
sname=strrep(sname,'.dso.mat','');
save(strcat(sname,'.dso.mat'),'dso','-v7.3');

%% if dso is an image 
% save sum of wavenumbers as a grey level image
% compute imagedata sum 
if dso.imagemode
    area=sum(dso.imagedata,3);

    % adjust values to 0-255 grey levels
    mini=min(area(:));
    maxi=max(area(:));

    a=255/(maxi-mini);
    b=-a*mini;

    imaff=uint8(area*a+b);

    imwrite(imaff,strcat(dso.name,'.sum.tif'),'tif','compression','none');
end;

%% matlab function tracking  

% no function tracking
%% end

cd (orig)
    