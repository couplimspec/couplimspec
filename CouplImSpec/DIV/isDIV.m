function [ok,message]=isDIV(d)

%% description
% test if d is a DIV structure

%% input

%  d : data table

%% output

% ok logical variable =1 if d is a IV structure
% message : if ok==0, message give an indication of the problem

%% principe
% check if d i and v fileds exist
% check consistency between the number of row and column of d.d
% compared to the number of row of i and v fileds.


%% use
% if isDIV(d) ....

%% Comments
%


%% Author
% MF Devaux
% BIA - PVPP
% INRA Nantes

%% date
% 17 aout 2015


%% start

if nargin ~=1
    error('use: ok=isDIV(d);');
end

message='';

%% treatement
% test structure and fields
if isstruct(d)
    % fields
    if isfield(d,'v') && isfield(d,'i') && isfield(d,'d')
        ok=1;
        
    else
        if ~isfield(d,'v')
            ok=0;
            message='missing v field';
        end
        if ~isfield(d,'i')
            ok=0;
            message=strcat(messsage, ' - missing i field');
        end
        if ~isfield(d,'d')
            ok=0;
            message=strcat(messsage, ' - missing d field');
        end
        
    end
else
    ok=0;
    message='not a structure';
end;


% check sonsistency between number of row and clumns of each field
if size(d.i,1)~=size(d.d,1)
    ok=0;
    message='number of i label differ from the number of row in data d.d';
end;

if size(d.v,1)~=size(d.d,2)
    ok=0;
    message=strcat(message,' - number of v label differ from the number of variables in data d.d');
end;




%% matlab function tracking

% no function tracking

%% end

