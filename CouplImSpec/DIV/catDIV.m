  function d=catDIV(d1,d2,dim)

 %% description
    % concatenate DIV matlab data table
    
%% input

    %  d1 and d2 : DIV tablee table to be concatenated
    % dim : dimension of concatenation
    
%% output
    
    %   d : resulting DIV file

%% principe
    % check the number of line or column according to the dimension to be
    % concatenated
    % concatenate d, i or v fileds


%% use
    % d=cat(d1,d2,1);
    
%% Comments
    % 
    
    
%% Author
    % MF Devaux
    % BIA - PVPP
    % INRA Nantes
    
%% date
    % 14 aout 2015

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=2 && nargin ~=3
    error('use: d=cat(d1,d2,dim);');
end

if ~isDIV(d1) && isDIV(d2)
    error('input data table must be DIV tables')
end;

if nargin == 2
    dim=0;
    if size(d1.i,1)==size(d2.i,1)
        dim=2;
    end;
    if size(d1.v,1)==size(d2.v,1) 
        if dim==0
            warning('concatenation according to lines');
        else
            dim=1;
        end;
    end;
end;


%% input


%% treatement
if dim==1;
    d.d=[d1.d;d2.d];
    d.i=char(d1.i,d2.i);
    d.v=d1.v;
else
    d.d=[d1.d d2.d];
    d.v=char(d1.v,d2.v);
    d.i=d1.i;
end;

% %% save 
% 
% 
% %% matlab function tracking  
% 
% fid=fopen(strcat(name,'.track.txt'),'w');
% 
% if fid==0
%     errordlg('enable to open track file');
% end;
% 
% fprintf(fid,'\r\n%s\t',datestr(now,0));
% fprintf(fid,'Import data set object from LAbspec *.spc map file \r\n');
% fprintf(fid,'__________________________________________________________________________\r\n');
% 
% % fprintf(fid,'\r\ninput file name: %s%s\r\n',name,ext);
% % fprintf(fid,'data folder: %s\r\n',pathname);
% % 
% % fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
% % fprintf(fid,'data folder: %s\r\n',sfolder);
% 
% % save of function used
% fprintf(fid,'__________________________________________________________________________\r\n');
% info=which (mfilename);
% os=computer;        % return the type of computer used : windows, mac...
% switch os(1)
%     case 'P'                        % for windows
%         ind=strfind(info,'\');                          
%     case 'M'                        % for Mac
%         ind=strfind(info,'/');
%     otherwise
%         ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
% end;
% 
% repprog=info(1:(ind(length(ind))-1));
% fprintf(fid,'function name: %s ',mfilename);
% res=dir(info);
% fprintf(fid,'on %s \r\n',res.date);
% fprintf(fid,'function folder: %s \r\n',repprog);
% %fprintf(fid,'__________________________________________________________________________\r\n');
% 
% fclose(fid);

%% end

cd (orig)
    