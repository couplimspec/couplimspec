  function decodeSpectralConfocalNantesMetadata
 %% description
    % decode headers of txt files exported from NIS (NIKON) software 
    % from properties of nd2 or tif images
    
%% input
    %  no input 
        
%% output
    %  spectral information  of image are recorded in a DIV file
    % column 1 : excitation wavelength
    % column 2 : emission wavelength
     
    
%% principe
    % it is supposed that one multiTiff file was recorded for each excitation
    % wavelength.
    %
    % for each exciatation wavelength, metadata are supposed to have been
    % exported from NIS software and saved in a txt file with a common
    % generic name
    %
    % all txt files with the common generic name <genname> are decoded
    % excitation and emission wavlengths are saved in one file that
    % concatenate the information from each file
    %
    % resulting file name is <genname.ConfocalNantes.ExcEm.txt>
    


%% use
    % decodeSpectralConfocalMetadata
 
    
%% Comments
    % review and updated for couplImSpec toolbox
    % previous version were removed
    
    
%% Author
    % MF Devaux
    % INRA Nantes
    % BIA - PVPP
    
%% date
    % 08/11/2018
    
    
%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: decodeSpectralConfocalMetadata');
end


%% input
% open file from folder
[nom,pathname]=uigetfile({'*.txt'},'first file of properties of the multispectral image','*.txt');
cd(pathname)
% generic name
ln=length(nom);
for i=1:min(10, ln-6)
    listenom{i}=nom(1:(ln-(i+3))); %#ok<AGROW>
end
listenom{i+1}='other';
[selection ]=listdlg('liststring',listenom,'selectionmode','single','initialvalue',3,'name','nom g�n�rique','promptstring','nom g�n�rique');
fnamegen=char(listenom(selection));
if strcmp(fnamegen,'other')
    fnamegen=char(inputdlg('enter generic filename','generic name',1,{nom(1:(ln-4))}));
end
    

% list of file with generic name fnamegen
liste=dir(strcat(fnamegen,'*.txt'));
nb=length(liste);

% sauvegarde
[noms,reps]=uiputfile({'*.txt'},'filename to save spectral variable names ',strcat(fnamegen,'.ConfocalNantes.ExcEm.txt'));

    

%% treatement
for i=1:nb
    cd(pathname)
    display(liste(i).name);
    
    fic=fopen(liste(i).name,'r');
    if fic==0
        error('enable to open %s file',liste(i).name);
    end
    
    % read first 3 lines (2 fgetl per line ?)
    % Nikon A1
	% Nom OC: CONFOCAL
	% Nom du Microscope: N/A
    for j=1:6
        ligne=fgetl(fic); %#ok<NASGU>
    end
    
    j=1;
    %% read emission wavelengths
    ligne=fgetl(fic);
    % loop until end of file
    while ~feof(fic) && length(ligne)>5
        em(j)=str2double(ligne([4 6 8]));
        ligne=fgetl(fic); %#ok<NASGU>
        % next line type : 
        % 		Emission de longueur d'onde: 500.2 - 508.2
        for k=1:2
            ligne=fgetl(fic); %#ok<NASGU>
        end
        % next line with excitation wavelength
        %   Excitation de longueur d'onde: 405.0
        ligne=fgetl(fic);
        ligne=strrep(ligne,ligne(1),'');
        ligne=strrep(ligne,ligne(1),'');
        ligne=strrep(ligne,' ','');
        ind=strfind(ligne,':');
        %exc(j)=str2double(ligne([68 70 72]));
        exc(j)=str2double(ligne((ind+1):end));
        if exc(j)==405                         % replace 405 by actual value for UV
            exc(j)=375;
        end
        ligne=fgetl(fic); %#ok<NASGU>
        % read next 2 lines
		%   Radian du st�nop�: 39,04
        %   Modalit� du canal: Scan Laser Confocal, SD
        for k=1:4
            ligne=fgetl(fic); %#ok<NASGU>
        end
        j=j+1;
        % then ready to read next emission wavelength ...
        ligne=fgetl(fic);
    end
    
    %% build resulting file
    if i==1
        lexc=exc';
        lem=em';
    else
       lexc=[lexc;exc'];
       lem=[lem;em'];
    end        
   
    clear em
    clear exc
end

d.d(:,1)=lexc;
d.d(:,2)=lem;
d.v=char('loexc','loem');
d.i=(1:size(d.d,1))';

%% save
cd (reps)
writeDIV(d,noms);
clear d

            
%% build char vector to be included in dso files
% for j=1:size(d.d,1)
%      vsim(j,:)=strcat(num2str(d.d(:,1)),'-',num2str(d.d(:,2))); %#ok<AGROW>
% end    


%% matlab function tracking  

fid=fopen(strcat(strrep(noms,'.txt',''),'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'extract emission and excitation wavelength from confocal metadata export files \r\n');
fprintf(fid,'________________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput file name:\r\n');
for i=1:nb
    fprintf(fid,'\t - input file name: %s\r\n',liste(i).name);
end
fprintf(fid,'data folder: %s\r\n',pathname);
 
fprintf(fid,'\r\n\r\nsaved excitation and emission wavelengths in file name :\r\n');
fprintf(fid,'\t - %s \r\n',noms);

fprintf(fid,'data folder: %s\r\n',reps);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    