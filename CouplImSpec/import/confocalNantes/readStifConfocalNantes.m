  function [dso,liste]=readStifConfocalNantes(method,rfolder,fnamegen,code,codebf,pixSize,codevar)

 %% description
    % Read series of confocal tif images acquired for the same sample 
    % to form a multispectral or multivariate dso structure
    
    
%% input

    % nothing 
    % or
    % method : method of image acquisition 
    % rfolder : read folder for tiff images
    % fnamegen : generic name of images 
    % code : series of extension to be added to generic name to read the file
    % codebf : code of visible image, if there is none : codebf must be ''
    % pixSize : pixel size as a structure with 2 fields : value and unit
    % codevar : code of spectral variables to be included in dso file if exists

    
%% output
    
    % dso: datasetobject of multivariate image 
    % liste : liste of files included in the multispectral serie

%% principe

    % all the file with the generic name are considered
    % those for which the extension is in code are put in the multivariate
    % image
    % data is organised to build the dso structure as described in file
    % dso.docx (folder help of couplImSpec toolbox)
    %
    %%
    % <https://www.dropbox.com/s/6kdns5ajqrkdaxj/DSO.docx>
        
%% use

    % [dso]=readStifConfocalNantes(method,rfolder,fnamegen,code,codebf,pixSize,codevar)
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % updated for couplImSpec Toolbox
  
    
%% Author
    % Mf Devaux 
    % INRA Nantes BIA - PVPP
    
%% date
    % 8 novembre 2018
    
%% context and global variables 

orig=pwd;           % returns the current directory

ext='tif';          % image extension


%% specific devices
%  confocal microscope Nantes : correspondence bteween confocal images and excitation wavelengths
% codeconfocalNantes={375,'UV'; 488,'BL';561,'VE'};

%% start
if nargin ~=7 
    error('use: [dso]=readStifConfocalNantes(method,rfolder,fnamegen,code,codebf,pixSize,codevar);');
end


cd(rfolder)
liste=dir(strcat(fnamegen,'*.tif'));
if isempty(liste)
    error('no file of generic name %s in folder %s',fnamegen,rfolder)
end

%% take into account brightfield image 

cd(rfolder)
if ~isempty(codebf)
    nombfim=strcat(fnamegen,codebf,'.',ext);
    if exist(nombfim,'file')
        bfimName=nombfim;
        bfimPathname=pwd;
    end
end
   
%% sort excitation wavelength in order to read files: 
% from shorter to higher wavelength of excitation and emission
lo=codevar{1};
[~,ordre]=sort(lo);

 % recover the emission wavelength of the file
 vEm=codevar{2};
 emLo=cell(3,1);
 for i=1:length(lo)
     emLo{i}=vEm(vEm(:,1)==lo(i),2);
 end

%% load files to workspace

for i=1:length(code)
    nom=strcat(fnamegen,code{ordre(i)},'.',ext);
    disp(nom);
    info=imfinfo(nom);
    nb=length(info);
    
    nim=nb;
    
    %% read the tiff image

    for j=1:nim
        disp(j);
        tmp=imread(nom,j);
        if j==1
            im=tmp;
            x=size(im,1);
            y=size(im,2);
        else
            im(:,:,j)=tmp;
        end
    end
    
    % recover the codevar part of the file
    vExcEm=emLo{ordre(i)};
    
    if size(vExcEm,1)~=size(im,3)
        error('the number of image read %d differ from the expected number %d',size(im,3),size(vExcEm,1));
    end
    
    % variable label for the dso
    vsim=strcat(num2str(lo(ordre(i))),'-',num2str(round(vExcEm)));
    
    if i==1
        data=reshape(im,x*y,nim);
        vs=vsim;
        liste=nom;
        x1=x;
        y1=y;
    else
        if x~=x1 || y~=y1
            error('les tailles des images diff�rent')
        end
        data=[data , reshape(im,x*y,nim)];
        vs=char(vs,vsim);
        liste=char(liste,nom);
    end
    clear vsim
    
end

 
%% dso: data set object
dso=dataset;
dso.name=fnamegen;
dso.author='Microscope confocal Nantes';
dso.data=data;
dso.axisscale{1}=1:x*y;
dso.title{1}='pixel';
dso.title{2}='images';
dso.type='image';
dso.imagemode=1;
dso.imagesize=[x y];
dso.label{2}=vs;
nbim=size(dso.label{2},1);    
dso.axisscale{2}=1:nbim;

% dso.userdata
% acquisition field
dso.userdata.acquisition.unit='';               % unit of wavenumber
dso.userdata.acquisition.method=method;         %  image acquisition method
dso.userdata.acquisition.pixSize=pixSize;       % pixel size
% no coordstage data available a priori....

% visible image
if ~isempty(codebf)
    dso.userdata.visim.name=bfimName; 
    dso.userdata.visim.pathname=bfimPathname; 
    dso.userdata.visim.scale=1;
    dso.userdata.visim.angle=0;
    dso.userdata.visim.trow=0;
    dso.userdata.visim.tcol=0;
end
 
 
%% matlab function tracking  
% no tracking


%% end

cd (orig)
    