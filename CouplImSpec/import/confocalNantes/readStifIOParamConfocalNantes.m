  function [rfolder,fnamegen,code,codebf,pixSize,codevar]=readStifIOParamConfocalNantes
  %% description
  
    % input parameters for readStif fucntion for the specific case of the
    % confocal microscope of nantes
    
%% input

    %  nothing
    
%% output
    
    % rfolder : read folder for tiff images
    % fnamegen : generic name of images 
    % code : series of extension to be added to generic name to read the file
    % codebf : code of visible image, if there is none : codebf must be ''
    % pixSize : pixel size as a structure with 2 fields : value and unit
    % codevar : code of spectral variables: first cell : excitation
    % wavelength, second : emission wavelengths


%% principe

    % entree interactive des parametres pour les fonctions readStif : 
    % specific case of the confocal microscope of Nantes 


%% use

    % [rfolder,fnamegen,code,pixSize,codevar]=readStifIOParamConfocalNantes
    
%% Comments

%   review for Couplimspec toolbox
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 08 novembre 2018 

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use:   function [rfolder,fnamegen,code,pixSize,codevar]=readStifIOParamConfocalNantes;');
end


%% input

%% open file from folder 
[nom,rfolder]=uigetfile({'*.tif'},'first confocal image of the multispectral serie','*.tif');
cd(rfolder)

% generic name
ln=length(nom);
for i=1:min(10, ln-6)
    listenom{i}=nom(1:(ln-(i+3)));
end
listenom{i+1}='other';
[selection ]=listdlg('liststring',listenom,'selectionmode','single','initialvalue',3,'name','generic name','promptstring','file generic name');
fnamegen=char(listenom(selection));
if strcmp(fnamegen,'other')
    fnamegen=char(inputdlg('file generic name','generic name',1,{nom(1:(ln-4))}));
end
ext=nom((ln-2):ln);

%% selection of code and brightfield image
cd(rfolder);
liste=dir(strcat(fnamegen,'*.',ext));
for i=1:length(liste)
    codei=strrep(liste(i).name,fnamegen,'');
    codei=strrep(codei,strcat('.',ext),'');
    code{i}=codei; %#ok<*AGROW>
end

if length(code)>1
    % image brighfield
    [selection ]=listdlg('liststring',['none',code],'selectionmode','multiple','name','visible image','promptstring','code of visible image');
    if selection==1
        codebf='';
    else
        codebf=code{selection-1};
        code=cellstr(char(code{[1:(selection-2) selection:end]}));
    end

    % les images de la s�rie
    if length(code)>1
        [selection ]=listdlg('liststring',code,'selectionmode','multiple','initialvalue',1:length(code),'name','code of image to include ','promptstring','select codes');
        code=code(selection);
    end
else
    codebf='';
end

%% pixel size

fileInfo=imfinfo(nom);
pixSize.value=fileInfo(1).UnknownTags(2).Value;
pixSize.unit='microns';
        
        
%% excitation-emission wavelengths values

[nomlo,pathlo]=uigetfile({'*.txt'},'files of excitation-emission values','*.ConfocalNantes.ExcEm.txt');
cd(pathlo)
cv=readDIV(nomlo);
codevar{1}=unique(cv.d(:,1),'stable');
codevar{2}=cv.d;


%% matlab function tracking  
% no tracking for this function

%% end

cd (orig)
    