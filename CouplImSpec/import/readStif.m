  function [dso]=readStif(method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar,sfolder)

 %% description
    % Read series of tif images acquired for the same sample to form a multispectral or multivariate dso structure
    
    
%% input

    % nothing 
    % or
    % method : method of image acquisition 
    % rfolder : read folder for tiff images
    % fnamegen : generic name of images 
    % code : series of extension to be added to generic name to read the file
    % codebf : code of visible image, if there is none : codebf must be ''
    % regbf : flag indicating if the spectral image is registrated or not
    %           to the visible image
    % pixSize : pixel size as a structure with 2 fields : value and unit
    % codevar : code of spectral variables to be included in dso file if exists
    % sfolder : save folder for dso.mat file

    
%% output
    
    % dso: datasetobject of multivariate image 

%% principe

    % all the file with the generic name are considered
    % those for which the extension is in code are put in the multivariate
    % image
    % data is organised to build the dso structure as described in file
    % dso.docx (folder help of couplImSpec toolbox)
    %
    %%
    % <https://www.dropbox.com/s/6kdns5ajqrkdaxj/DSO.docx>
        
%% use

    % code={'_B',_G','_UV'};
    % codebf='FC;
    % regbf=1;
    % fnamegen='FF'
    % rfolder = '.'
    % sfolder='imported'
    % method='macrofluo'
    % tpix.value=4.3;
    % tpix.unit='microns';
    % codevar={'UBR','UBV','UBB','UAR','UAV','UAB'}
    % [dso]=readStif(fnamegen,sfolder,code,codebf,regbf,method,codevar,tpix)
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % updated for couplImSpec Toolbox
   
    
%% Author
    % Mf Devaux 
    % INRA Nantes BIA - PVPP
    
%% date
    % 26/03/2014
    
    
%% context and global variables 

orig=pwd;           % returns the current directory

ext='tif';          % image extension


%% start
if nargin ~=9 && nargin ~=0 
    error('use: [dso]=readStif(method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar,sfolder); or [dso_read]= readStif;');
end

if nargin ==0;
    [method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar,sfolder]=readStifIOParam;
end;


%% treatment    
switch method
    case {'MACROFLUO-Nantes'}
        [dso,liste]=readStifMacrofluoNantes(method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar);
    
    case {'CONFOCAL-Nantes'}
        [dso,liste]=readStifConfocalNantes(method,rfolder,fnamegen,code,codebf,pixSize,codevar);
        
    otherwise
        [dso,liste]=readStifGen(method,rfolder,fnamegen,code,codebf,regbf,pixSize);
        
end;

%% save dso
disp('sauvegarde....')
cd(sfolder);
sname=strcat(fnamegen,'.dso.mat');
savedso(dso,sname);
 
%% matlab function tracking  

fid=fopen(strcat(fnamegen,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,'Build a dso structure of multivariate image from a set of tif files \r\n');
fprintf(fid,'__________________________________________________________________\r\n');

fprintf(fid,'\r\ninput generic file name: %s%s\r\n',fnamegen,ext);
fprintf(fid,'data folder: %s\r\n',rfolder);

fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

fprintf(fid,'\r\nfiles included in the multivariate image\r\n');
for i=1:length(code)
    fprintf(fid,'\t- %s\r\n',liste(i,:));
end;
fprintf(fid,'\r\n');

fprintf(fid,'\r\nname of images in the multivariate image\r\n');
vs=dso.label{2};
for i=1:size(vs,1)
    fprintf(fid,'\t- %s\r\n',vs(i,:));
end;
fprintf(fid,'\r\n');

if pixSize.value~=0
    fprintf(fid,'\r\nPixel size : %6.2f %s\r\n',pixSize.value, pixSize.unit);
else
    fprintf(fid,'\r\nPixel size : unknown\r\n');
end;
fprintf(fid,'\r\n');

if ~isempty(codebf)
    nombfim=strcat(fnamegen,codebf,'.',ext);
    fprintf(fid,'Visible image : %s\r\n',nombfim);
    if regbf
        fprintf(fid,'Spectral image registrated to brightfield image\r\n');
    end
    fprintf(fid,'\r\n');
else
    fprintf(fid,'No visible image\r\n');
    fprintf(fid,'\r\n');
end;

fprintf(fid,'\r\nmethode d''acquisition des images : %s\r\n',method);
fprintf(fid,'\r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);


%% end

cd (orig)
    