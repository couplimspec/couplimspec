  function [dso,liste]=readStifGen(method,rfolder,fnamegen,code,codebf,regbf,pixSize)

 %% description
    % Read series of tif images acquired for the same sample to form a multispectral or multivariate dso structure
    
    
%% input

    % nothing 
    % or
    % method : method of image acquisition 
    % rfolder : read folder for tiff images
    % fnamegen : generic name of images 
    % code : series of extension to be added to generic name to read the file
    % codebf : code of visible image, if there is none : codebf must be ''
    % regbf : flag indicating if the spectral image is registrated or not
    %           to the visible image
    % pixSize : pixel size as a structure with 2 fields : value and unit

    
%% output
    
    % dso: datasetobject of multivariate image 
    % liste : liste of files included in the multispectral serie

%% principe

    % all the file with the generic name are considered
    % those for which the extension is in code are put in the multivariate
    % image
    % data is organised to build the dso structure as described in file
    % dso.docx (folder help of couplImSpec toolbox)
    %
    %%
    % <https://www.dropbox.com/s/6kdns5ajqrkdaxj/DSO.docx>
        
%% use

    % [dso]=readStifGen(method,rfolder,fnamegen,code,codebf,regbf,pixSize);
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % updated for couplImSpec Toolbox
   
    
%% Author
    % Mf Devaux 
    % INRA Nantes BIA - PVPP
    
%% date
    % 26/03/2014
    % 30/06/2014    : bug : deux fois aller � rfolder
    
    
%% context and global variables 

orig=pwd;           % returns the current directory

ext='tif';          % image extension



%% start
if nargin ~=7 
    error('use: [dso]=readStifGen(method,rfolder,fnamegen,code,codebf,regbf,pixSize);');
end

cd(rfolder)
liste=dir(strcat(fnamegen,'*.tif'));
if isempty(liste)
    error('no file of generic name %s in folder %s',fnamegen,rfolder)
end;


%% take into account brightfield image 

if ~isempty(codebf)
    nombfim=strcat(fnamegen,codebf,'.',ext);
    if exist(nombfim,'file')
        bfimName=nombfim;
        bfimPathname=pwd;
    end;
end
   

    
%% load files to workspace

for i=1:length(code)
    nom=strcat(fnamegen,code{i},'.',ext);
    disp(nom);
    info=imfinfo(nom);
    nb=length(info);
    
    % number of images in the current tif file 
    if nb==1
        if strcmp(info.PhotometricInterpretation,'RGB')     % color
            nim=3;
        else                                                   % monochromatic
            nim=1;
        end;
    else                                                     % multichannel
        nim=nb;
    end;
    
    %% read the tiff image
    if nb==1
        im=imread(nom);
        x=size(im,1);
        y=size(im,2);
    else
        for j=1:nim
            disp(j);
            tmp=imread(nom,j);
            if j==1
                im=tmp;
                x=size(im,1);
                y=size(im,2);
            else
                im(:,:,j)=tmp;
            end;
        end
           
    end
                
        
    %% variable de l'image en cours de lecture 
    if nim==1
        vsim=code(i);
    else if nim==3
        vsim=strcat(code(i),['R';'G';'B']);
        else if nim<10 
            vsim=strcat(code{i},num2str((1:nim)'));
            else
                vsim=strcat(code{i},'0',num2str((1:9)'));
                vsim=[vsim;strcat(code{i},num2str((10:nim)'))];
            end;
        end;
    end;
        
    %% build data table
    if i==1
        data=reshape(im,x*y,nim);
        vs=vsim;
        liste=nom;
        x1=x;
        y1=y;
    else
        if x~=x1 || y~=y1
            error('les tailles des images diff�rent')
        end
        vs=[vs;vsim];
        data=[data , reshape(im,x*y,nim)];
        liste=char(liste,nom);
    end;
    clear vsim
    
end;

vs=char(vs);

%% dso: data set object
dso=dataset;
dso.name=fnamegen;
dso.author=method;
dso.data=data;
dso.axisscale{1}=1:x*y;
dso.title{1}='pixel';
dso.title{2}='images';
dso.type='image';
dso.imagemode=1;
dso.imagesize=[x y];
dso.label{2}=vs;
nbim=size(dso.label{2},1);    
dso.axisscale{2}=1:nbim;

% dso.userdata
% acquisition field
dso.userdata.acquisition.unit='';               % unit of wavenumber
dso.userdata.acquisition.method=method;         %  image acquisition method
dso.userdata.acquisition.pixSize=pixSize;       % pixel size
% no coordstage data available a priori....

% visible image
if ~isempty(codebf)
    dso.userdata.visim.name=bfimName; 
    dso.userdata.visim.pathname=bfimPathname; 
    if regbf
        dso.userdata.visim.scale=1;
        dso.userdata.visim.angle=0;
        dso.userdata.visim.trow=0;
        dso.userdata.visim.tcol=0;
    end;
end
 

%% matlab function tracking  

%no tracking


%% end

cd (orig)
    