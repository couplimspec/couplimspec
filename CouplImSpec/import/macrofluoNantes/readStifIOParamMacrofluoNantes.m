  function [rfolder,fnamegen,code,codebf,pixSize,codevar]=readStifIOParamMacrofluoNantes
  %% description
  
    % input parameters for readStif fucntion for the specific case of the
    % macrofluo of nantes
    
    
%% input

    %  nothing
    
%% output
    
    % rfolder : read folder for tiff images
    % fnamegen : generic name of images 
    % code : series of extension to be added to generic name to read the file
    % codebf : code of visible image, if there is none : codebf must be ''
    % pixSize : pixel size as a structure with 2 fields : value and unit
    % codevar : code of spectral variables to be included in dso file if exists



%% principe

    % entree interactive des parametres pour les fonctions readStif : 
    % specific case of the macrofluo of Nantes 


%% use

    % [rfolder,fnamegen,code,codebf,pixSize,codevar]=readStifIOParamMacrofluoNantes
    
    
%% Comments

%   review for CouplimsPEC tOOLBOX
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 26 mars 2014
    % 20 aout 2019 : point virgules and check. shoart names


%% context variables 

orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: [rfolder,fnamegen,code,codebf,pixSize,codevar]=readStifIOParamMacrofluoNantes;');
end


%% input
%% open file from folder 
mprompt='Input first macrofluo image of the multispectral serie';

[nom,rfolder]=uigetfile({'*.tif'},mprompt,'*.tif');
cd(rfolder)

% generic name
ln=length(nom);
for i=1:min(10, ln-5)
    listenom{i}=nom(1:(ln-(i+3)));
end
listenom{i+1}='other';
[selection ]=listdlg('liststring',listenom,'selectionmode','single','initialvalue',3,'name','generic name','promptstring','file generic name');
fnamegen=char(listenom(selection));
if strcmp(fnamegen,'other')
    fnamegen=char(inputdlg('file generic name','generic name',1,{nom(1:(ln-4))}));
end
ext=nom((ln-2):ln);

%% selection of code and brightfield image
cd(rfolder);
liste=dir(strcat(fnamegen,'*.',ext));
for i=1:length(liste)
    codei=strrep(liste(i).name,fnamegen,'');
    codei=strrep(codei,strcat('.',ext),'');
    code{i}=codei; %#ok<*AGROW>
end

if length(code)>1
    % image brighfield
    [selection ]=listdlg('liststring',['none',code],'selectionmode','multiple','name','visible image','promptstring','code of visible image');
    if selection==1
        codebf='';
    else
        codebf=code{selection-1};
        code=cellstr(char(code{[1:(selection-2) selection:end]}));
    end
    
    % les images de la s�rie
    if length(code)>1
        [selection ]=listdlg('liststring',code,'selectionmode','multiple','initialvalue',1:length(code),'name','code of image to include ','promptstring','select codes');
        code=code(selection);
    end
else
    codebf='';
end

%% pixel size

fileInfo=imfinfo(nom);
if isfield(fileInfo(1),'UnknownTags')
    pixSize.value=fileInfo(1).UnknownTags(2).Value;
else
    tpix=-1;
    while tpix<0
        answer = inputdlg('Pixel size (0 if unknown): ','Pixel Size',1,{'0'});
        tpix=str2num(char(answer)); %#ok<*ST2NM>
    end
    pixSize.value=tpix;
end

pixSize.unit='microns';
        

%% spectral variables  
[nomlo,pathlo]=uigetfile({'*.txt'},'file of spectral variable','*MacrofluoNantes.Filter.txt');
cd(pathlo)
codevar=readStringVec(nomlo);


%% matlab function tracking  
% no tracking for this function

%% end

cd (orig)
    