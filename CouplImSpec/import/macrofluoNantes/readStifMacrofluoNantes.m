  function [dso,liste]=readStifMacrofluoNantes(method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar)

 %% description
    % Read macrofluo tif images acquired for the same sample 
    % to form a multispectral or multivariate dso structure
    
    
%% input

    % nothing 
    % or
    % method : method of image acquisition 
    % rfolder : read folder for tiff images
    % fnamegen : generic name of images 
    % code : series of extension to be added to generic name to read the file
    % codebf : code of visible image, if there is none : codebf must be ''
    % regbf : flag indicating if the spectral image is registrated or not
    %           to the visible image
    % pixSize : pixel size as a structure with 2 fields : value and unit
    % codevar : code of spectral variables to be included in dso file if exists

    
%% output
    
    % dso: datasetobject of multivariate image 
    % liste : liste of files included in the multispectral serie

%% principle

    % all the file with the generic name are considered
    % those for which the extension is in code are put in the multivariate
    % image
    % data is organised to build the dso structure as described in file
    % dso.docx (folder help of couplImSpec toolbox)
    %
    %%
    % <https://www.dropbox.com/s/6kdns5ajqrkdaxj/DSO.docx>
        
%% use

    % 
    %  [dso]=readStifMacrofluoNantes(method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar)
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % updated for couplImSpec Toolbox
    % writen ro read images acquired with a fluorescence macroscope (C.
    % Barron) histochem project
    % modified to take into account any multi page tiff image
    
    
%% Author
    % Mf Devaux 
    % INRA Nantes BIA - PVPP
    
%% date
    % 26/03/2014
    % 21/12/2018 : change code of filters
 
    
%% context and global variables 

orig=pwd;           % returns the current directory

ext='tif';          % image extension


%% specific devices
% macrofluo Nantes : code of macrofluo filters in Nantes
codemacroNantes={'U1','U2','BL','GR'};

%% start
if nargin ~=8 
    error('use:[dso]=readStifMacrofluoNantes(method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar);');
end

cd(rfolder)
liste=dir(strcat(fnamegen,'*.tif'));
if isempty(liste)
    error('no file of generic name %s in folder %s',fnamegen,rfolder)
end;


%% take into account brightfield image 

cd(rfolder)
if ~isempty(codebf)
    nombfim=strcat(fnamegen,codebf,'.',ext);
    if exist(nombfim,'file')
        bfimName=nombfim;
        bfimPathname=pwd;
    end;
end
   

    
%% load files to workspace

for i=1:length(code)
    nom=strcat(fnamegen,code{i},'.',ext);
    disp(nom);
    info=imfinfo(nom);
    nb=length(info);
    
    % number of images in the current tif file 
    if nb==1
        if strcmp(info.PhotometricInterpretation,'RGB')     % color
            nim=3;
        else                                                   % monochromatic
            nim=1;
        end;
    else                                                     % multichannel
        nim=nb;
    end;
    
    %% read the tiff image
    if nb==1
        im=imread(nom);
        x=size(im,1);
        y=size(im,2);
    else
        for j=1:nim
            disp(j);
            tmp=imread(nom,j);
            if j==1
                im=tmp;
                x=size(im,1);
                y=size(im,2);
            else
                im(:,:,j)=tmp;
            end;
        end
           
    end
                
        
    %% variable de l'image en cours de lecture 
    if nim==1
        vsim=code(i);
    end
    if nim==3
        vsim=strcat(code(i),['R';'G';'B']);
    else
        % in this case, it is not expected to have several multitiff image
        vsim=codevar;  
        if length(code)>1
            error('variable label will be wrong: TODO: modify function');
        end;
    end;
    
    if i==1
        data=reshape(im,x*y,nim);
        vs=vsim;
        liste=nom;
        x1=x;
        y1=y;
    else
        if x~=x1 || y~=y1
            error('les tailles des images diff�rent')
        end
        vs=[vs;vsim];
        data=[data , reshape(im,x*y,nim)];
        liste=char(liste,nom);
    end;
    clear vsim
    
end;
  
%% sort excitation wavelength to put in a spectral order 
% renversement des mesures pour mettre les images dans le sens
% des longueurs d'onde : sont consid�r�s : UA, UB, BL et VE
% on consid�re �galement que les images couleurs sont enregistr�es dans le sens BGR et non pas
% RGB

% les deux premi�res lettres du code variables sont test�es : 
vsim=vs;
vs=vsim(:,1:2);
pos=zeros(1,size(vs,1));
% comparaison avec les codes macrofluo and defined order
for k=1:size(vs,1)
    trouve=0;
    j=1;
    while ~strcmp(vs(k,:),codemacroNantes{j}) && j<=4;
        j=j+1;
    end;
    if strcmp(vs(k,:),codemacroNantes{j})
       pos(k)=j;
       trouve=1;
    end;

    if ~trouve
        error('code %s not identified for macroFluo Nantes',vs(i,:));
    end;
end
vs=vsim;

% sort the data matrix taking into account the fact that macrofluo color images
% are recorded in BGR order
tmp=data;
vtmp=vs;
id=1;
for k=1:4
    ind=(pos==k);
    tmp(:,id:(id+2))=data(:,ind);
    vtmp(id:(id+2),:)=vs(ind,:);
    id=id+3;
end;
data=tmp;
vs=vtmp;

%% dso: data set object
dso=dataset;
dso.name=fnamegen;
dso.author='MacroFluo Nantes : Nikon AZ100';
dso.data=data;
dso.axisscale{1}=1:x*y;
dso.title{1}='pixel';
dso.title{2}='images';
dso.type='image';
dso.imagemode=1;
dso.imagesize=[x y];
dso.label{2}=vs;
nbim=size(dso.label{2},1);    
dso.axisscale{2}=1:nbim;

% dso.userdata
% acquisition field
dso.userdata.acquisition.unit='';               % unit of wavenumber
dso.userdata.acquisition.method=method;         %  image acquisition method
dso.userdata.acquisition.pixSize=pixSize;       % pixel size
% no coordstage data available a priori....

% visible image
if ~isempty(codebf)
    dso.userdata.visim.name=bfimName; 
    dso.userdata.visim.pathname=bfimPathname; 
    if regbf
        dso.userdata.visim.scale=1;
        dso.userdata.visim.angle=0;
        dso.userdata.visim.trow=0;
        dso.userdata.visim.tcol=0;
    end;
end
 

%% matlab function tracking  
% no tracking


%% end

cd (orig)
    