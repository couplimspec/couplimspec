  function [dso]=readomnic(fname,sfolder)

 %% description
    % Read Omnic v8 *.map file (IR and Raman) 
    
%% input

    % fname: 'filename.map' 
    % sfolder : save folder for dso.mat file
    
%% output
    
    %dso: datasetobject of map 

%% principe
    % Adapted from nicolet function of CytoSpec by P.Lash.
    % line 84 and 127 have been modified to read *. map from Thermo Raman
    % dXR microscope. Work from both infrared and Raman spectra images.
    % Does not work with points map 

%% use
    % [dso]= readomnic('filename.map'); return dataset "dso" of omnic map.
    % dso= readomnic;
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % writen for F.Allouche Phd Thesis
    
    
%% Author
    % F.Jamme INRA/SOLEIL 
    % modified par F. Allouche, MF Devaux   INRA Nantes BIA - PVPP
    % modified by MF Devaux CouplImSpec projetc
    
%% date
    % 21/01/2011
    % 28/01/2011
    % 31/01/2011
    % 01/02/2011
    % 01/03/2011    small corrections + sauve sum of spectral intensities as a grey level image
    %               import visible image
    % 24/10/2011    put numbers as pixel labels
    % 06/3/2014     CouplImSpec

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >2
    error('use: [dso_read]= readomnic(filename)');
end

if nargin >0;
    if ~exist(fname,'file')
        error('no file %s in folder %s',fname,orig)
    end;
    [pathname,name,ext]=fileparts(fname);
    pathname=pwd;
    sname=strcat(name,'.dso.mat');
end

if nargin ==1
     % file.dso.mat and track file will be saved in the same folder than the
    % original spc file
    sfolder=pathname;
end;

if nargin ==0;
    % open file from folder 
    [fname,pathname]=uigetfile({'*.map'},'select *.map file');
    [path, name, ext] = fileparts(fname);
    cd(pathname);
    % name and folder to save results
    [sname,sfolder] = uiputfile({'*.mat'},'result file name',strcat(name,'.dso.mat'));
end


%% load file to workspace

% getting the parameters encoded as floating point 32/ integer32 values.
% searching first for the file pointer of the parameter block
% The pointer is named 'indicblock';

    fid     = fopen(fname);
    fseek(fid,0,'bof');
    Mfloat32  = fread(fid,inf,'float32');
    fseek(fid,93*4,'bof');
    tmp = fread(fid,1,'uint32');
    indicblock = (tmp-204)/4;
    fseek(fid,(indicblock+249)*4+2,'bof');
    mapprm = fread(fid,6,'float32');
   

%% load visible 

visimname=strcat(name,'.tif');
if exist(visimname,'file') 
    visim = imread(visimname);
end;

%% reading now the parameters:

UWN = Mfloat32(indicblock+4);   % upper wavenumber limit
LWN = Mfloat32(indicblock+5);   % lower wavenumber limit
BGG = Mfloat32(indicblock+14);  % gain background
IdF = Mfloat32(indicblock+17);  % identifier for start indices of spectra modified by FJ 
%IdF = Mfloat32(indicblock+18);  % identifier for start indices of spectra 
LAS = Mfloat32(indicblock+20);  % Laser wavenumber
APT = Mfloat32(indicblock+23);  % aperture size of spectrometer
RLAS= Mfloat32(indicblock+24);  % Raman Laser Frequency added by FJ 
HPF = Mfloat32(indicblock+40);  % high pass filter (only for IR)
LPF = Mfloat32(indicblock+41);  % low pass filter (only for IR)
VEL = Mfloat32(indicblock+47);  % mirror velocity (only for IR)

%% getting parameter from uint32 values:
fseek(fid,indicblock*4,'bof');
Mint32  = fread(fid,13,'uint32');
NOd = Mint32(1);             %  NOd - number of data points
NMP = Mint32(7);             %  NMP - number of measurement points
IPP = Mint32(8);             %  IPP - interferogram peak position (only for IR)
NSS = Mint32(9);             %  NSS - number of sample scans
NPF = Mint32(11);            %  NPF - number of points for FFT (only for IR)
NGS = Mint32(13);            %  NGS - number of background scans
WVS  = (UWN-LWN)/(NOd-1);    %  WVS - wavenumber step
WN = (LWN:WVS:UWN);          %  WN  - wavenumber
clear('Mint32');

%% getting parameter from float32 values:

Ystart = mapprm(1);
Ystop  = mapprm(2);
STY    = mapprm(3);           % STY - step size in y direction
Xstart = mapprm(4);
Xstop  = mapprm(5);
STX    = mapprm(6);           % STX - step size in x direction


SZY    = Ystop-Ystart;        % SZY - map size in y-direction
SZX    = Xstop-Xstart;        % SZX - map size in x-direction
YdI    = round(SZY/STY+1);    % YdI - number of spectra in y-direction
XdI    = round(SZX/STX+1);    % XdI - number of spectra in x-direction
XxY    = XdI * YdI;           % XxY - number of spectra (equals xdim  * ydim)

%% build image matrix

% finding the start position for the block of spectral data: the value 'IdF' can be found
% at the starting position of the spectral data block
temp = find(Mfloat32==IdF);
% spcstart = (temp(2)+31); % original value +31 
spcstart = (temp(2)+32);  % modified by F.J for Raman 
%fseek(fid,spcstart*4,'bof');
C(NOd,XxY) = 0;
for i=1:XxY
%               opsA(1:1,i)    = freadso(fid,1,'uint32');
%               opsB(1:1,i)    = freadso(fid,1,'float32');
%               lstr  = fread(fid,70,'char');
%               disp([num2str(i) ': ' char(permute(lstr,[2,1]))]);
%               opsC(1:7,i)    = fread(fid,7,'int16');
%               opsd(1:2,i)    = fread(fid,2,'float32');
    ctr(1)         = spcstart-NOd + i*(NOd+25);
    ctr(2)         = ctr(1)+ NOd-1;
    C(:,i)         = Mfloat32(ctr(1):ctr(2));
    %          fseek(fid,NOd*4,'cof');
end
clear('Mfloat32');
fclose(fid);

%% matrix cosmetics

C = permute(C,[2,1]);
dX=[Xstart:STX:Xstop];
dY=[Ystart:STY:Ystop];

%% dso: data set object

dso=dataset;
dso.name=name;
dso.author='';
dso.data=C;
dso.axisscale{1}=1:XxY;
dso.axisscale{2}=WN;
%dso.labelname{1,1}=num2str(dsoX);
dso.title{1}='pixel';
dso.label{1}=num2str(dso.axisscale{1}');
dso.label{2}=num2str(WN');
dso.title{2}='Wavenumber';
dso.type='image';
dso.imagemode=1;
dso.imagesize=[XdI YdI];
%dso.imageaxisscale{1,1}=dX;
%dso.imageaxisscale{2,1}=dY;

description={['upper wavenumber value:     ' num2str(UWN)] ; ['lower wavenumber value:     ' num2str(LWN)]...
    ; ['wavenumber step:              ' num2str(WVS)] ...
    ; ['number of data points:        ' num2str(NOd)]...
    ; ['number of sample scans:       ' num2str(NSS)] ...
    ; ['number of background scans:   ' num2str(NGS)] ; ['number of spectra:            ' num2str(XxY)]...
    ; ['number of spectra in x-direction:  ' num2str(XdI)] ; ['number of spectra in y-direction:  ' num2str(YdI)]...
    ; ['step size in x-direction:     ' num2str(STX)] ; ['step size in y-direction:     ' num2str(STY)]};

if VEL>0                    % case infrared
    description{12}=['mirror velocity:              ' num2str(VEL)];
    method='INFRARED';
end;
if RLAS>0                   % case Raman
    description{12}=['Raman Laser frequency:    ' num2str(RLAS)];
    method='RAMAN';
end

dso.description=description;

% dso.userdata.image
dso.userdata.image.coord.row=1:dso.imagesize(1);
dso.userdata.image.coord.col=1:dso.imagesize(2);
dso.userdata.image.size.nbRow=dso.imagesize(1);
dso.userdata.image.size.nbCol=dso.imagesize(2);

% dso.userdata.acquisition
dso.userdata.acquisition.method=method;         % spectral method
dso.userdata.acquisition.unit='cm-1';           % unit of wavenumber
dso.userdata.acquisition.coordStage.row=dX;                 % position in �m in X
dso.userdata.acquisition.coordStage.col=dY;                 % position in �m in Y
pixS=dX(2)-dX(1);
dso.userdata.acquisition.pixSize.value=pixS;         % pixel size
dso.userdata.acquisition.pixSize.unit='micron';  % pixel size unit : fix to micron for omnic map

% visible image
if exist('visim','var')
    dso.userdata.visim.name=visimname;           % image visible
    dso.userdata.visim.pathname=pathname;           % image visible
    dso.userdata.visim.scale=0;                       % scale
    dso.userdata.visim.angle=0;                       % scale
    dso.userdata.visim.trow=0;                       % scale
    dso.userdata.visim.tcol=0;                       % scale
end;


%% plot imagedata sum 

h=figure(27);
set(h,'windowstyle','docked');
area=sum(dso.imagedata,3);

% works with %the PLS - toolbox
%imagesc(dso.imageaxisscale{2,1},dso.imageaxisscale{1,1},area); 

imagesc(dY,dX,area);
colormap(jet)
title(name,'Fontsize',12);
set(gca,'dataAspectRatio',[1 1 1])




%% save dso
cd(sfolder);
save(sname,'dso');

%% save sum of wavenumbers as a grey level image

% adjust values to 0-255 grey levels
mini=min(area(:));
maxi=max(area(:));

a=255/(maxi-mini);
b=-a*mini;

imaff=uint8(area*a+b);

imwrite(imaff,strcat(name,'.sum.tif'),'tif','compression','none');

%% matlab function tracking  

fid=fopen(strcat(name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Import data set object from OMNIC *.map file \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput file name: %s%s\r\n',name,ext);
fprintf(fid,'data folder: %s\r\n',pathname);

fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    