  function importSpa

 %% description
    % import a set of spa spectra and extract stage coordinates as dso couplimspec file
    % https://www.dropbox.com/s/6kdns5ajqrkdaxj/DSO.docx
    
%% input

    %  nothing 
    
%% output
    
    %   dso file of spectra and coordinates values saved on the disk

%% principe
    % SPA spectra from OMNIC software are found a a single folder which
    % should be a subfolder of the folder where the map has been recorded
    %
    % use the spareadr function from the PLS toolbox to read files one by
    % one to ensure that all files of the folder are taken into account.
    % spareadr return a dso object
    % the dso are combined to form a single dso
    %
    % the resulting dso is saved on disk

    % extract the coordintaes from field description of dso structure
    % 
    % dso structure : 
    %   Extrait de: D:\mfdevaux\projets\SOLEIL2014\SMIS\20140308Devaux\20141212\eau2f13mb5L6a\faisceau.map
    %   Position (X,Y): -682.09, 203.39
    %   Ven D�c 12 22:20:13 2014 (GMT+01:00)
    
    % copy video image if it exist in the hieracrchy of folder

%% use
    % importSpa
    
%% Comments
    %  initially written for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % analysing SMIS data
    %
    
    
%% Author
    % MF Devaux
    % BIA PVPP
    
%% date
    % 16/4/15
    % 22 janvier 2018 : adaptation to full import of SMIS data proposal 20150929
    % 6 d�cembre 2018 : import from the two different SOLEIL machine : C2 and
    % continum : the metadata differ and recovering the X Y coordinate is
    % not similar
    % also recover date of acquisition and optionnal video image

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: importSpa');
end


%% input
[~,rfolder]=uigetfile({'spa'},'name of the first spa file','*.spa');
cd(rfolder)
files=dir('*.spa');

if isempty(files)
    error('no spa files in folder %s',rfolder);
end

listspectro={'INFRARED','RAMAN'};
sel = listdlg('PromptString','Choose spectroscopy','listString',listspectro,'selectionMOde','single');
method=listspectro{sel};

% pixel / point size in micron
pixSize = inputNumber('size of pixel in �m');

% extract reference name of hierarchy of folders
% spa files are supposed to be found in splitmap folder
% the OMNIC map files are in upper folder map(n)
% the folder map(n) are found in a root folder with the sample name
% recover map folder name and sample name
%splitmap
[mapFolder,~,~]=fileparts(rfolder(1:(end-1)));
% map folder
[sampleFolder,mapName,~]=fileparts(mapFolder);
% sample folder
[rootFolder,sampleName,~]=fileparts(sampleFolder);

% video image
cd(mapFolder);
ltif=dir('*.tif');
if isempty(ltif)
    video=0;
else
    if length(ltif)>=2
        ltif={ltif.name};
        selec=listdlg('liststring',ltif,'selectionmode','single','PromptString','choose video image file');
        videoName=ltif{selec};
    else
        videoName=ltif.name;
    end
    videoIm=imread(videoName);
    figure
    imshow(videoIm,[]);
    ok=yesno('ok for this associated video images');
    if ok
        video=1;
    else
        video=0;
    end
end

if ~video
        warning('no video image selected in %s folder. Please export video from OMNIC');
end

% save dso resulting file :
cd(rootFolder)
cd ..
sname=[sampleName '.' mapName '.dso.mat'];
[sname,sfolder]=uiputfile({'*.dso.mat'},'save resulting dso file as ',sname);


%% treatement
cd(rfolder)
% number of spa files
nb=length(files);
ilue=0;

X=zeros(nb,1);
Y=zeros(nb,1);
year=zeros(nb,1);
mois=zeros(nb,1);
day=zeros(nb,1);
hour=zeros(nb,1);
minute=zeros(nb,1);
second=zeros(nb,1);
tnum=zeros(nb,1);
fcoord=1;                   % flag for searching coordinates
fdate=1;

% for each file
for i=1:nb
    
    if isempty(strfind(files(i).name,'bkg'))        %  bkacground files should not be imported
        display(files(i).name);
        ilue=ilue+1;
        % read spa file using PLStoolbox function 
        dso=spareadr(files(i).name);
        
        % name of sample
        ns=strrep(files(i).name,'.spa','');
    
    
        % recover the description where X and Y coordinates are saved
        descr=dso.description;
        
        
        if fcoord
            % extract X and Y stage coordinates from the char descr
            iPos=strfind(descr,'(X,Y)');                            % case C2 microscope
            if ~isempty(iPos)
                iPos=iPos+length('(X,Y):');
                
                % beginning of X value
                descr=descr(iPos:end);
                
                iPos=strfind(descr,',')-1;
                
                X(ilue)=str2double(descr(1:iPos));
                
                %move after comma
                descr=descr((iPos+2):end);
                
                % find Y value befoe starting date
                tr=10;
                
                while isnan(str2double(descr(1:tr)))
                    tr=tr-1;
                end
                
                Y(ilue)=str2double(descr(1:tr));
            else
                iPos=strfind(descr,'X=');                           % case continuum microscope
                
                if ~isempty(iPos)
                    iPos=iPos+length('X=');
                    
                    % beginning of X value
                    descr=descr(iPos:end);
                    
                    iPos=strfind(descr,',')-1;
                    
                    X(ilue)=str2double(descr(1:iPos));
                    
                    iPos=strfind(descr,'Y=');
                    iPos=iPos+length('Y=');
                    
                    % beginning of Y value
                    descr=descr(iPos:end);
                    
                    iPos=strfind(descr,',')-1;
                    
                    Y(ilue)=str2double(descr(1:iPos));
                else
                    if ilue==1
                        warning('no coordinates found in spa file %s, coordinates ignored for the rest of the data',files(i).name);
                        fcoord=0;
                    end
                end
            end
        end
        
        if fdate
            
            charEndLine=descr(end);
            
           
            if contains(descr,'(GMT+0')
                
                iPos=strfind(descr,charEndLine);
                % search the line with the date
                deb=1;
                fin=iPos(1)-1;
                nip=1;
                ligne=descr(deb:fin);
                while ~contains(ligne,'(GMT+0')
                        deb=iPos(nip)+1;
                    fin=iPos(nip+1)-1;
                    ligne=descr(deb:fin);
                    nip=nip+1;
                end
                    
              %  iPos=iPos(end-1)+1;
                
               % ldate=descr(iPos:end);
                ldate=ligne;
                ip=strfind(ldate,'(GMT+0');
                nbCharDate=25;                      % format of date from OMNIC
                ldate=ldate((ip-nbCharDate):end);
                
                month=ldate(5:7);
                switch upper(month)
                    case 'JAN'
                        nm=1;
                    case 'FEB'
                        nm=2;
                    case 'MAR'
                        nm=3;
                    case 'APR'
                        nm=4;
                    case 'MAI'
                        nm=5;
                    case 'JUN'
                        nm=6;
                    case 'JUL'
                        nm=7;
                    case 'AUG'
                        nm=8;
                    case 'SEP'
                        nm=9;
                    case 'OCT'
                        nm=10;
                    case 'NOV'
                        nm=11;
                    case 'DEC'
                        nm=12;
                    otherwise
                        warning('month unknown')
                        nm=0;
                end
                mois(ilue)=nm;
                day(ilue)=str2double(ldate(9:10));
                
                hour(ilue)=str2double(ldate(12:13));
                minute(ilue)=str2double(ldate(15:16));
                second(ilue)=str2double(ldate(18:19));
                year(ilue)=str2double(ldate(21:24));
                tnum(ilue)=datenum(year(ilue),mois(ilue),day(ilue),hour(ilue),minute(ilue),second(ilue));
            else
                fdate=0;
                warning('no date and time information found in spa file %s, time ignored',files(i).name);
                
            end
        end
            
                    
            
        
        % build dso file
        if ilue==1
            data=zeros(nb,size(dso.data,2));
            data(1,:)=dso.data;
            lname=ns;
        else
            data(ilue,:)=dso.data;
            lname=char(lname,ns);
        end
    end
    
    
end

if fcoord
    % finalise X and Y coordinate values
    X=X(1:ilue);
    Y=Y(1:ilue);
end

if fdate
    % finalise time measure
    tnum=tnum-tnum(1);              % relate time to first measure
    [~,~,~,h,n,s]=datevec(tnum);
    tnum=3600*h+60*n+s;             % time in second
    
    temps.d=[year(1:ilue) mois(1:ilue) day(1:ilue) hour(1:ilue) minute(1:ilue) second(1:ilue) tnum(1:ilue)];
end

% fnal constitution of dso file
data=data(1:ilue,:);

dsoT=dataset;
dsoT.name=strrep(sname,'.dso.mat','');
dsoT.author='importSPA-MFD-SD';

dsoT.data=data;

dsoT.title{1}='OMNIC spectra';
dsoT.title{2}='Wavenumbers (cm-1)';

dsoT.label{1}=lname;
dsoT.label{2}=num2str(dso.axisscale{2}');

dsoT.axisscale{2}=dso.axisscale{2};

dsoT.userdata.acquisition.method=method;
dsoT.userdata.acquisition.unit='cm-1';
dsoT.userdata.acquisition.pixSize.value=pixSize;
dsoT.userdata.acquisition.pixSize.unit='�m';

dsoT.userdata.acquisition.coordStage.row=X;
dsoT.userdata.acquisition.coordStage.col=Y;



%% save
cd(sfolder)
savedso(dsoT);

if fcoord
    % save coordinate
    c.d=[X Y];
    c.i=dsoT.label{1};
    c.v=char('X','Y');
    snamecoord=strcat(dsoT.name,'.coord');

    writeDIV(c,snamecoord);
end

if fdate
    temps.i=dsoT.label{1};
    temps.v=char('year','month','day','hour','min','second','trel(s)');
    
    snamedate=strcat(dsoT.name,'.time');
    writeDIV(temps,snamedate);
end

% video image
if video
    imwrite(videoIm,strcat(dsoT.name,'.imVideo.tif'),'tif','compression','none');
end

%% matlab function tracking  

fid=fopen(strcat(sname,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t\r\n',datestr(now,0));
fprintf(fid,'import spa files found after a splitmap and extract coordinates -  OMNIC files \r\n');
fprintf(fid,'___________________________________________________________\r\n');

fprintf(fid,'\r\ninput spa folder: %s\r\n',rfolder);

fprintf(fid,'\r\n\r\n');
fprintf(fid,'dso resulting file: %s\r\n',sname);

if fcoord
    fprintf(fid,'\r\nFile coordinates: %s.txt \r\n',snamecoord);
else
    fprintf(fid,'\r\nNo coordinates\r\n');
end

if fdate
    fprintf(fid,'\r\nDate and time of acquisition: %s.txt \r\n',snamedate);
else
    fprintf(fid,'\r\nNo information about date and time\r\n');
end

if video
    fprintf(fid,'\r\nVideo image: %s\r\n  found in folder: %s\r\n', videoName,mapFolder);
    fprintf(fid,'saved in file : %s.imVideo.tif \r\n',dsoT.name);
else
   fprintf(fid,'\r\nNo video image\r\n');

end

fprintf(fid,'\r\n\r\n');

fprintf(fid,'\r\nAll files saved in folder: %s\r\n',sfolder');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    