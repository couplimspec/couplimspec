  function readSpaCoord

 %% description
    % read the coordinates of the stage of a set of spa spectra
    
%% input

    %  nothing 
    
%% output
    
    %   coordinates are saved on the disk

%% principe
    % all spa files of a folder are considered
    % use spareadr of PLSToolbox of eigenvector
    % extract the coordintaes from field description of dso structure
    % 
    % dso structure : 
    %   Extrait de: D:\mfdevaux\projets\SOLEIL2014\SMIS\20140308Devaux\20141212\eau2f13mb5L6a\faisceau.map
    %   Position (X,Y): -682.09, 203.39
    %   Ven D�c 12 22:20:13 2014 (GMT+01:00)

%% use
    % readspaCoord
    
%% Comments
    %  written for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % analysisng SMIS data
    
    
%% Author
    % MF Devaux
    % BIA PVPP
    
%% date
    % 16/4/15

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: readSpaCoord');
end


%% input
[~,rfolder]=uigetfile({'spa'},'name of the first spa file','*.spa');
cd(rfolder)
files=dir('*.spa');

if isempty(files)
    error('no spa files in folder %s',rfolder);
end;

files=files(1:end-1);

[sparfolder,spafolder,~]=fileparts(rfolder(1:(end-1)));

[sname,sfolder]=uiputfile({'*.txt'},'save coordinate as ',strcat(spafolder,'.coord.txt'));
sname=strrep(sname,'.coord','');
sname=strrep(sname,'.txt','');
sname=strcat(sname,'.coord');

%% treatement

% number of spa files
nb=length(files);

X=zeros(nb,1);
Y=zeros(nb,1);

% for each file
for i=1:nb
    display(files(i).name);
    % read spa file using PLStoolbox function 
    dso=spareadr(files(i).name);
    
    % recover the description where X and Y coordinates are saved
    descr=dso.description;
    
    % extract X and Y from the char descr
    iPos=strfind(descr,'(X,Y)');
    iPos=iPos+length('(X,Y):');
    
    % beginning of X value
    descr=descr(iPos:end);
    
    iPos=strfind(descr,',')-1;
    
    X(i)=str2double(descr(1:iPos));
    
    %move after comma
    descr=descr((iPos+2):end);
    
    % find Y value befoe starting date
    tr=10;
    
    while isnan(str2double(descr(1:tr)))
        tr=tr-1;
    end;
    
    Y(i)=str2double(descr(1:tr));
    
end;


%% save
% save coordinate
c.d=[X Y];
c.i=char(strrep(cellstr(char(files.name)),'.spa',''));
c.v=char('X','Y');

cd(sfolder)

writeDIV(c,sname);
    
    
    
%% matlab function tracking  

fid=fopen(strcat(sname,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t\r\n',datestr(now,0));
fprintf(fid,'extract stage coordinate from spa OMNIC files \r\n');
fprintf(fid,'_______________________________________________\r\n');

fprintf(fid,'\r\ninput spa folder: %s\r\n',spafolder);
fprintf(fid,'data folder: %s\r\n',sparfolder);


fprintf(fid,'\r\ncoordinates saved in file : %s.txt \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    