  function [dso]=readTxtMapRamanNantes(fname,sfolder,visible)

 %% description
    % Read txt map file exported from Raman RENISHAW INRAE NANTES
    % DEDICATED to RAMAN acquisition in snake mode 
    
%% input

    % fname: 'filename.txt' 
    % sfolder : save folder for dso.mat file
    % visible : flag indicating if visible image is associated to the RAMAN
    % map

    
%% output
    
    % dso: datasetobject of map 

%% principe
    % txt RAMAN map exported from the Renishaw device INRAE NANTES
    % the txt map contain four columns : 
    % X and Y stage coordinates
    % wavenumbers 
    % Raman intensity
    %
    % the size of the map is deduced from the coordinate stage
    %
    % the function was written for snake acquisition in the order fisrt
    % line from left to right and second line from right to left
    % etc...
    

%% use
    % [dso]= readTxtMapRamanNantes(fname,sfolder,visible); return dataset "dso" of spc map.
    % dso= readomnic;
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % written for RAMAN map acquisition in INRAE Nantes
    
    
    
%% Author
    % MF Devaux and Angelina D'Orlando
    % INRAE nantes
    
    
%% date
    % 20/02/2020 
    % 12/01/2021 : track for visible image

%% context variables 
orig=pwd;           % returns the current directory



%% start

if nargin >3
    error('use: [dso_read]= readTxtMapRamanNantes(fname,sfolder,visible)');
else
    if nargin ==0
        
        % open file from folder
        [fname,pathname]=uigetfile({'*.txt'},'select *.txt file');
        [~, name, ext] = fileparts(fname);
        visible=yesno('associated visible image?');
        if visible
            visimname=strcat(name,'.tif');
            if ~exist(visimname,'file')
                [visimname,pathnameVis]=uigetfile({'*.tif'},'select visible image *.tif file','*.tif');
            end
        end
        
        listUnit={'micron','nm'};
        [indx] = listdlg('PromptString','pixel size unit','SelectionMode','single','ListString',listUnit);
        pixUnit=listUnit(indx);
        cd(pathname);
        % name and folder to save results
        [sname,sfolder] = uiputfile({'*.mat'},'result file name',strcat(name,'.dso.mat'));
    else
        if nargin==3
            pathname=pwd;
            pathnameVis=pwd;
            visimname=strrep(fname,'.txt','.tif');
        end
    end
end


%% load file to workspace
cd(pathname)
sprintf('read txt map...');
data=importTxtMap(fname);

% identify map size
X=data(:,1);
Y=data(:,2);

vx=unique(X);
vy=unique(Y);

nx=length(vx);
ny=length(vy);

% identify number of wavenumbers
nblo=length(X)/nx/ny;

lo=data(1:nblo,3);

% build the map taking into account the snake acquisition
i=1;
ideb=1;
ifin=nx*nblo;
ids=1;
ifs=nx;
spectres(1:nblo,ids:ifs)=reshape(data(ideb:ifin,4),nblo,nx);

sens=2;
while i<ny
    i=i+1;
    ideb=ifin+1;
    ifin=nx*nblo*i;
    ids=ifs+1;
    ifs=nx*i;
    if sens==2
        spectres(1:nblo,ids:ifs)=fliplr(reshape(data(ideb:ifin,4),nblo,nx));
        sens=1;
    else
        spectres(1:nblo,ids:ifs)=reshape(data(ideb:ifin,4),nblo,nx);
        sens=2;
    end
end
    
% transpose
spectres=spectres';

% build dataset
dso=dataset(spectres);  %#ok<DTSET>
dso.name=strrep(sname,'.dso.mat','');

dso.type='image';
dso.imagesize=[nx ny];
dso.imagemode=1;

% check every attribute of dataset obejct
dso.author='RAMAN-Renishaw-INRAE-Nantes';

dso.axisscale{2}=lo;


% identifyer
dso.title{1}='pixel';
dso.label{2}=num2str(dso.axisscale{2}');
dso.title{2}='Wavenumbers (cm-1)';


% userdata
% acquisition field
dso.userdata.acquisition.unit='nm';               % unit of wavenumber
dso.userdata.acquisition.method='RAMAN';         %  image acquisition method
pixSizeX=diff(vx);
pixSizeX=pixSizeX(1);
pixSizeY=diff(vy);
pixSizeY=pixSizeY(1);

if abs(pixSizeX-pixSizeY)>0.5
    error('check pixel size');
end
dso.userdata.acquisition.pixSize.value=pixSizeX;       % pixel size
dso.userdata.acquisition.pixSize.unit=pixUnit;       % pixel size


dso.userdata.acquisition.coordStage.col=vx;  % unique value for the map
dso.userdata.acquisition.coordStage.row=vy;  % idem

if exist('visim','var')
    dso.userdata.visim.name=visimname;           % image visible
    dso.userdata.visim.pathname=pathnameVis;
end


%% plot imagedata sum 

h=figure(27);
set(h,'windowstyle','docked');
area=sum(dso.imagedata,3);

% works with %the PLS - toolbox
%imagesc(dso.imageaxisscale{2,1},dso.imageaxisscale{1,1},area); 

imagesc(vx,vy,area);
colormap(jet)
title(name,'Fontsize',12);
set(gca,'dataAspectRatio',[1 1 1])


%% save dso
cd(sfolder);
sprintf('save as dso file...');
savedso(dso);


%% matlab function tracking  

fid=fopen(strcat(name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Import data set object from Raman txt map recorded in snake mode and exported from RENISHAW \r\n');
fprintf(fid,'_____________________________________________________________________________________________\r\n');
fprintf(fid,'\r\ninput file name: %s%s\r\n',name,ext);
fprintf(fid,'data folder: %s\r\n',pathname);

if visible
    fprintf(fid,'\r\associated visible image: %s\r\n',visimname);
    fprintf(fid,'\r\nfound in folder: %s\r\n',pathnameVis);
else
    fprintf(fid,'\r\no  visible image associated to the spectral image: \r\n');
end



fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);


%% end

cd (orig)


function [data]=importTxtMap(nomfic)

%% description
% lecture d'un fichier de type <txt> map Raman renishaw 

%% input

% 
% nomfic : name of file to read with or without the txt extension

%% output
%  data : four column matrix
%   X and Y : stage coordinates
%   lo : wavenumbers
%   Raman intenisty

%% principe

% 		la premiere ligne doit �tre constituee du nom des variables
% 		
%		les lignes suivantes sont constituees par les valeurs

%       renvoi la matrice des valeurs
%
%
%% USE :
%	 [data]=importTxtMap(nomfic)
%    
%

%% Comments
%      adataped from readDIV
%% AUTEUR : MF Devaux
%           A D'Orlando
%           BIA - PVPP - BIBS - INRAE Nantes
%% date
% 11/02/2020
%


%% context variables
orig=pwd;           % returns the current directory


%% start
%
if (nargin>1)
    error('USE : 	 don=readDIV(''filename'')');
end



%% treatment
% ouverture du fichier
fic=fopen(nomfic,'r');
if (fic==-1)
    error('cannot read file %s',nomfic);
end

%lecture de la premiere ligne qui contient les noms des variables
premiereligne=fgetl(fic);
% extraction de la premiere chaine de caractere
[val, ~, ~, next]=sscanf(premiereligne,'%s',1);
% la premiere chaine de caractere est eliminee
premiereligne=premiereligne(next:length(premiereligne));
% constitution du vecteur variable
variable=char(val);	% val='#X'
if strcmp(variable, '#X')                                      % a priori fichier de donnees !
    % premiere chaine suivante
    [val, ~, ~, next]=sscanf(premiereligne,'%s',1);
    % boucle pour extraire tous les noms de variables jusqu'�
    % val=''
    while (~isempty(val))
        variable=char(variable,char(val));
        premiereligne=premiereligne(next:length(premiereligne));
        [val, ~, ~, next]=sscanf(premiereligne,'%s',1);
    end
    
    % initialisation
    ind=1;
    
    % boucle jusqu'� la fin du fichier
    while ~feof(fic)
        % lecture des lignes suivantes : constituees du nom des individus et des valeurs associees
        ligne{ind}=fgetl(fic); %#ok<AGROW>
        ind=ind+1;
    end
    data=str2num(char(ligne)); %#ok<*ST2NM>
    
else
    errordlg(['file <' nomfic '> dosed not seem to be a Raman txt Map exported from RENISHAW  : ''#X'' expected as first word of the file']);
    data=0;
end


fclose(fic);


%% save

%% track
% no tracking for this function

%% end
cd(orig)


    