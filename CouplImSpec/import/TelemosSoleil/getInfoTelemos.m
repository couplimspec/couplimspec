  function [nameFolder,rootFolder,listDir,listChannel,Objective]=getInfoTelemos(message,rootFolder)

 %% description
 
    % Extraction of metadata of all images in a micromanager root folder obtained  using the TELEMOS
    % microscope at synchrotron SOLEIL
    %       - Name of experiment, 
    %       - list of folders
    %       - list of Channels 
    %       - Objective used
    % 
    % 
    % this function is specific of telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    
    
    
%% input
    %  optional parameter :
    % message : question asked to user
    %  nameFolder = root folder name

    
%% output
    
    % nameFolder : root folder name
    % rootFolder: absolute root folder name
    % listDir : list of folders in the root folder
    % listChannel : list of channels of the acquisition
    %  Objective: Objective used

%% principle
    % 
    % micro manager save  images in a structured file folder
    % architecture :
    % root folder : name given by the user
    %   subfolders 
    %       - roi(n)_tile1 : n folders for each selected roi
    %    or
    %       - Pos(n) : n folders for each selected position
    %   display_and_comments.txt : file describing the channels acquired in
    %   
    %   in each subfolder roi(n)_tile1 or Pos(n)
    %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
    %               n = number of time
    %               p = z focal plane
    %               DM300_327-353 = name of the channel
    %       metadata.txt : files describing all metadata associated with the
    %       acquisition : x, y, z position, camera settings .... etc.
    %
    %
    % read the <display_and_comment.txt> file in order to find the information 
    % of the list of channels
    % go into the root folder and find the subfolders wich name is starting 
    %   either wih roi or pos
    % subfolder names are returned as relative name folders
    % go into the first roior pos subfolders, read the metadat.txt file to
    % extract the objective

%% use
    % [nameFolder,rootFolder,listDir,listChannel,Objective]=getInfoTelemos(message,nameFolder)
    % 
    
%% Comments
    % adapted for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % from getInfoTelemos function 
    % written in project build mosaic images for telemos microscope of the DISCO line At SOLEIL
    % generalisation of the function written for proposal 20130978
    
%% Author
    % MF Devaux
    % INRA- BIA-PVPP

    
%% date
    % 3 decembre 2014
    % 15 decembre 2017
    %  3 septembre 2018 : comment and check
    %  11 septembre 2018 : extract the objective
    %     4 juin 2019 : replace exist by isfolder or isfile

    

%% context variables 
% folder separator depends on os 
os=computer; % returns the Operating system 
switch os(1)
    
    case'P'
       foldSep='\'; %  for Windows OS
    case'M'
        foldSep='/'; %  for Mac OS
    otherwise
        foldSep='/'; %  UNIX (to be checked)
end

orig=pwd;           % returns the current directory

%% start

if nargin >2
    error('use: [nameFolder,rootFolder,listDir,listChannel,Objective]=getInfoTelemos(opt<message>,<nameFolder>)')
end

%% input
    
if nargin ==0
    % Enter name of root folder 
    rootFolder=uigetdir(orig,'Select root folder where subfolders have been recorded');
else
    if nargin==1
        if ischar(message)
            rootFolder=uigetdir(orig,message);
         else
            error('invalid parameter <message>. text expected');
        end
    else
        if ~isfolder(rootFolder)
            error('folder %s does not exist',rootFolder);
        end
    end
end

ind=strfind(rootFolder,foldSep);
nameFolder=rootFolder((ind(end)+1):end);

%% treatment
% ListDir
cd (rootFolder)
% test for roi subfolders
list=dir('Roi*');
if ~isempty(list)
    roi=1;
    listDir=list([list.isdir]); 
    % list of subfolders
    listDir=char(listDir.name);

    % put rois in order from 1 to n 
    % 0 char is missing for managing change of 10th
    %TODO: g�rer les roi et pas seulement les tile....
    pos=strfind(listDir(1,:),'tile');
    pos=pos+length('tile');

    index=str2num(listDir(:,pos:end)); %#ok<ST2NM>
    [~,I]=sort(index);

    listDir=listDir(I,:);
else
    roi=0;
end

list=dir('Pos*');
if ~isempty(list)
    pos=1;
    listDir=list([list.isdir]); 
    % list of subfolders
    listDir=char(listDir.name);
else
    pos=0;
end

if roi && pos
    error('<roi> AND <pos> subfolders found in root folder %s. Check the file folders',nameFolder);
end

if ~roi && ~pos
    error('no <roi> OR <pos> subfolders found in root folder %s. Check the file folders',nameFolder);
end


% read display_and_comments file
fid=fopen('display_and_comments.txt','r');
if fid<=0
    error('could not open <display_and_comments.txt> file')
end

while ~feof(fid)
    rline=fgetl(fid);
    
    if contains(rline,'"Name":')
        ind=strfind(rline,'"');
        nameChannel=rline((ind(end-1)+1):(ind(end)-1));
        if ~exist('listChannel','var')
            listChannel=nameChannel;
        else
            listChannel=char(listChannel,nameChannel);
        end
    end
end


% read the first metadata file
cd(listDir(1,:));

fid=fopen('metadata.txt','r');
if fid==0
    error('could not open <metadata.txt> file')
end

% read until "ZeissObjectiveTurret-Label" found
% 
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    if contains(rline,'"ZeissObjectiveTurret-Label":')
        rline=strrep(rline,'"ZeissObjectiveTurret-Label": ','');
        rline=strrep(rline,',','');
        Objective=rline;
        ok=1;
    end
end

cd (orig)
    