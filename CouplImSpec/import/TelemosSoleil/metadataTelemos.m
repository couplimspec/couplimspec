  function [nameFolder,rootFolder,listDir,listChannel]=metadataTelemos(message)

 %% description
    % Extraction of metadata of all images in a micromanager root folder obtained  using the TELEMOS
    % microscope at synchrotron SOLEIL
    %       - Folder structure : list of subfolder
    %       - Channels
    %       - x, y and z a priori coordinates given by the moving stage 
    %       - the size of the image in pixel 
    %       - Pixel size + 
    %       - time of acquisition
    % 
    % this function is specific of telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    
    
%% input
    %  nothing 
    % interactive function
    % optionnal : message to send to function  getinfotelemos
    
%% output
    % nameFolder : root folder name
    % rootFolder: absolute root folder name
    % listDir : list of folders in the root folder
    % listChannel : list of channels of the acquisition
    % +
    % on disk : metadata = DIV file that contain the metadata extracted
    
%% principe
    % micro manager save  images in a structured file folder
    % architecture :
    % root folder : name given by the user
    %   subfolders roi(n)_tile1 : n folders for each selected roi
    %           or pos(n) : n folders for each selected position
    %   display_and_comments.txt : file describing the channels acquired in
    %   
    %   in each subfolder roi(n)_tile1 or Pos(n) : 
    %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
    %               n = number of time
    %               p = z focal plane
    %               DM300_327-353 = name of the channel
    %       metadata.txt : files describing all metadata associated with the
    %       acquisition : x, y, z position, camera settings .... etc.
    % 
    % read the <metadata.txt> file in order to find the information of the
    % fields :
    %  in the first summary :
    %       Slices : number of z plane
    %       Interval_ms : time between two frames in time series
    %       z-step-um : interval between two slices
    %       Width and Height of the image in pixel unit
    %       Frames : number of time
    %       PixelSize
    
    %  for each "Framekey"
    %       exposure-ms : time of exposure for each image
    %       Time : get the acquisition time
    %       XpositionUm
    %       YpositionUm
    %       ZpositionUm

    %   the channels used for acquisition are saved in a txt file called
    %       <rootFolderName>.listChannels.txt
    
    %   the position and size of the ROI defined for the acquisition are
    %   saved in a DIV txt file called <rootFolderName>.roiAcq.txt
    
    %   the objective name used for acquisition is saved in a txt file called
    %       <rootFolderName>.Objective.txt
        
    %   the metadata concerning the folders are saved in a DIV txt file
    %        called <rootFolderName>.<pos or roi>.metadata.txt
    %       the following metadata are saved for each folder : 
    %           'nTime','timeStep(s)','nZ','zStep(�m)','pixelSize(�m)','nline','ncol'
    
    %   the metadata concerning the files found in the folder are saved in
    %   a DIV txt file called <rootFolderName>.<foldername>.metadata.txt
    %       in this file, metadata are sorted according to the acquisition
    %       time in increassing order
    %       the following metatdata are saved for each file : 
    %           'xPos','yPos','zPos','Channel','tAcqu(s)','matlabDateNumber'
    %   the matlab datenumber can be converted into date and hour by the
    %   datevec matlab function
    %           so year, month, day, hour, second are saved as well as
    %           time relative to the first image
    % 
    % use the getInfoTelemos and readMedataTelemos function

%% use
    %  [nameFolder,rootFolder,listDir,listChannel]=metadataTelemos(message)

    % 
    
%% Comments
    %  written for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % making films of the degradation
    
    
%% Author
    % MF Devaux
    % INRA- BIA-PVPP

    
%% date
    %  23 f�vrier 2015
    % 15 d�cembre 2017 : ROIs + message
    % 3 septembre 2018: comments and checking
    % 11 septembre : find objective in metadata
%     4 juin 2019 : replace exist by isfolder or isfile

 
%% context variables 

orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('Use:  [nameFolder,rootFolder,listDir,listChannel]=metadataTelemos(opt:<message>);');
end

if nargin==0
    message='select Micromanager root folder to open';
end

%% input
% find the root folder and the subfolders
[nameFolder,rootFolder,listDir,listChannel,Objective]=getInfoTelemos(message);
nbf=size(listDir,1);

%% treatment
cd(rootFolder)
metadatafolder.i=listDir;
metadatafolder.v=char('nTime','timeStep(s)','nZ','zStep(�m)','pixelSize(�m)','nline','ncol');
metadatafile.v=char('xPos','yPos','zPos','Channel','tAcqu(ms)','yearAcq','monthAcq','dayAcq','hourAcq','minAcq','secAcq','relTime');

roiInfo.i=nameFolder;
roiInfo.v=char('xPos','ypos','width','height');

% for each subfolder
warn{1}='';
for i=1:nbf
    if ~isfile(strcat(nameFolder,'.',listDir(i,:),'.metadata.txt'))
        [nTime,timeStep,timeTelemos,nSlice,zStep,spixel,width,height,ROIpos,xPos,yPos,zPos,tAcq,Chan,listim,warn]=readMetadataTelemos(listDir(i,:),listChannel);
        if i==1
            roiInfo.d=[ROIpos width height];
        end
        metadatafolder.d(i,1)=nTime;
        metadatafolder.d(i,2)=timeStep;
        metadatafolder.d(i,3)=nSlice;
        metadatafolder.d(i,4)=zStep;
        metadatafolder.d(i,5)=spixel;
        metadatafolder.d(i,6)=height;
        metadatafolder.d(i,7)=width;
        
        timacq=datevec(timeTelemos);
        
        metadatafile.i=char(strrep(listim,'.tif',''));
        metadatafile.d=xPos;
        metadatafile.d(:,2)=yPos;
        metadatafile.d(:,3)=zPos;
        metadatafile.d(:,4)=Chan;
        metadatafile.d(:,5)=tAcq;
        metadatafile.d(:,6)=timacq(:,1);
        metadatafile.d(:,7)=timacq(:,2);
        metadatafile.d(:,8)=timacq(:,3);
        metadatafile.d(:,9)=timacq(:,4);
        metadatafile.d(:,10)=timacq(:,5);
        metadatafile.d(:,11)=timacq(:,6);
        
        % time from the first image
        time0=timacq(1,1:6);
        res=datevec(datenum(timacq(:,1:6))-datenum(time0));
        % time in second taking into account hour, minutes and second
        ttime=(res(:,4)*3600+ res(:,5)*60+res(:,6));
        
        metadatafile.d(:,12)=ttime;
        
        writeDIV(metadatafile,strcat(nameFolder,'.',listDir(i,:),'.metadata.txt'));
    end
end

%% save

if strcmp(listDir(1,1),'P')
    ngenfold='pos';
else
    ngenfold='roi';
end
writeDIV(metadatafolder,strcat(nameFolder,'.',ngenfold,'.metadata.txt'));

writeDIV(roiInfo,strcat(nameFolder,'.roiAcq.txt'));

writeStringVec(listChannel,strcat(nameFolder,'.listChannels.txt'));

writeStringVec(Objective,strcat(nameFolder,'.Objective.txt'));

%% matlab function tracking  

fid=fopen(strcat(nameFolder,'.',ngenfold,'.metadata.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,'Read and extract metadata acquired using TELEMOS fluorescence microscope SOLEIL\r\n');
fprintf(fid,'_______________________________________________________________________________\r\n');

fprintf(fid,'\r\nroot folder name: %s\r\n',nameFolder);
fprintf(fid,'root folder pathname: %s\r\n',strrep(rootFolder,nameFolder,''));

fprintf(fid,'\r\n');
fprintf(fid,'\r\nObjective: %s \r\n',Objective);

fprintf(fid,'\r\n');
fprintf(fid,'\r\nChannels: \r\n');
for i=1:size(listChannel,1)
    fprintf(fid,'\t - %s\r\n',listChannel(i,:));
end

fprintf(fid,'\r\n');
fprintf(fid,'\r\nSubfolders: \r\n');
for i=1:size(listDir,1)
    fprintf(fid,'\t - %s\r\n',listDir(i,:));
end

if ~strcmp(warn{1},'')
    fprintf(fid,'\r\nWARNING:');
    for i=1:length(warn)
        fprintf(fid,'\r\n\t-%s',warn{i});
    end
end

fprintf(fid,'\r\n');
fprintf(fid,'\r\n=> Channels name saved in file %s.listChannels.txt\r\n',nameFolder);
fprintf(fid,'\r\n=> Objective name saved in file %s.Objective.txt\r\n',nameFolder);
fprintf(fid,'\r\n=> Roi defined for acquisition: position and size saved in file %s.roiAcq.txt\r\n',nameFolder);
fprintf(fid,'\r\n=> Folders metadata saved in file: %s\r\n',strcat(nameFolder,'.',ngenfold,'.metadata.txt'));
fprintf(fid,'\r\n=> Files metadata saved in files %s.%s*.metadata.txt\r\n',nameFolder,ngenfold);



% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
info=which('getInfoTelemos.m');
res=dir(info);
fprintf(fid,'use getInfoTelemos.m %s \r\n',res.date);
info=which('readMetadataTelemos.m');
res=dir(info);
fprintf(fid,'use readMetadataTelemos.m %s \r\n',res.date);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);      
%% end

cd (orig)
    