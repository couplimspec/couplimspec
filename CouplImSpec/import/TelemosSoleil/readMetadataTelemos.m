  function [nTime,timeStep,timeTelemos,nSlice,zStep,spixel,width,height,ROIPos,xPos,yPos,zPos,tAcq,Chan,listim,warn]=readMetadataTelemos(dirTelemos,listChannel)

 %% description
     % Extraction of metadata of all images in a micromanager subfolder dirTelemosobtained  using the TELEMOS
    % microscope at synchrotron SOLEIL
    %       - Time lapse information
    %       - Z stack acquisition
    %       - list of image in the folder
    %       - X, y and z coordinates of images given by the moving stage 
    %       - the size of the image in pixel 
    %       - Pixel size
    %       - time of acquisition
    
    % 
    % this function is specific of telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    
%% input
    %  optional parameter :
    %  dirTelemos = subfolder name
    %  listChannel = channels for which acqusiition has been performed
    
%% output
    % nTime: number of time
    % timeStep: time betwwen two frames in time series in second
    % timeTelemos : char array of the time at which individual images are
     % nSlice : number of z slices
    % zstep : z step between two slice in s stacks
    % spixel : pixel size in um
    % width higth : size of the image in pixel
    %  ROIPos : Roi position definied for the acquisition in full frame image of the camera
    % x  y z coordinates in pixel unit
    % tAcq : exposure time
    % acquired
    % listim : file names found in the subfolder dirTelemos
    % warn : warning of post control of metadata
    
%% principe
    % micro manager save  images in a structured file folder
    % architecture :
    % root folder : name given by the user
    %   subfolders roi(n)_tile1 : n folders for each selected roi
    %           or pos(n) : n folders for each selected position
    %   display_and_comments.txt : file describing the channels acquired in
    %   
    %   in each subfolder roi(n)_tile1 or pos(n): 
    %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
    %               n = number of time
    %               p = z focal plane
    %               DM300_327-353 = name of the channel
    %       metadat.txt : files describing all metadat associated with the
    %       acquisition : x, y, z position, camera settings .... etc.
    % 
    % read the <metadata.txt> file in order to find the information of the
    % fields :
    %  in the first summary :
    %       Slices : number of z plane
    %       Interval_ms : time between two frames in time series
    %       z-step-um : interval between two slices
    %       Width and Height of the image in pixel unit
    %       Frames : number of time
    %       PixelSize
    
    %  for each "Framekey"
    %       exposure-ms : time of exposure for each image
    %       Time : get the acquisition time
    %       XpositionUm
    %       YpositionUm
    %       ZpositionUm

    %
    % 

%% use
    % [nTime,timeStep,nSlice,zStep,spixel,width,height,xPos,yPos,zPos,tAcq,Chan,timeTelemos]=readMetadataTelemos(dirTelemos,listChannel)

    % 
    
%% Comments
    % adapted for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % from readMetadataTelemos function 
    % written in project build mosaic images for telemos microscope of the DISCO line At SOLEIL
    % generalisation of the function written for proposal 20130978
 
    
%% Author
    % MF Devaux
    % INRA- BIA-PVPP

    
%% date
    %  4 decembre 2014
    % 15 d�cembre 2017 : ROI
    % 3 septembre 2018 : comments and check
    % 14 ma 2019 : filenames in the order of acquisition
%     4 juin 2019 : replace exist by isfolder or isfile

 
%% context variables 

orig=pwd;           % returns the current directory

%% start

if nargin ~=2
    error('[nTime,timeStep,nSlice,zStep,spixel,width,height,xPos,yPos,zPos,tAcq,Chan,timeTelemos,listim]=readMetadataTelemos(dirTelemos,listChannel)');
end

dirTelemos=deblank(dirTelemos);

if ~isfolder(dirTelemos)
    error('folder %s doesnot exist in folder %s',dirTelemos,orig)
end
cd(dirTelemos)
if ~isfile('metadata.txt')
    error('file <metadata.txt> does not exist in folder %s',dirTelemos)
end
        
warn={''};

%% treatment
% check that channel images exist
listim=dir('*.tif');
nbim=length(listim);
nbch=size(listChannel,1);

for i=1:nbim
    ok=0;
    j=1;
    while ~ok && j<=nbch
        if contains(listim(i).name,deblank(listChannel(j,:)))
            ok=1;
        end
        j=j+1;
    end
    if ok==0
        error('image %s doesnot correspond to any expected channel',listim(i).name);
    end
end

%timeTelemos=[listim.datenum];
%listim=cellstr(char(listim.name));

%%   open <metadata.txt> file
fid=fopen('metadata.txt','r');
if fid==0
    error('could not open <metadata.txt> file')
end

% read "first summary information"
% number of slice
ok=0;
nbrline=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;
    
    if contains(rline,'"Slices":')
        rline=strrep(rline,'"Slices": ','');
        rline=strrep(rline,',','');
        nSlice=str2double(rline);
        ok=1;
    end
end

% interval between times
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;

    if contains(rline,'"Interval_ms": ')
        rline=strrep(rline,'"Interval_ms": ','');
        rline=strrep(rline,',','');
        timeStep=str2double(rline);
        timeStep=timeStep/1000;
        ok=1;
    end
end

% Z step
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;
    
    if contains(rline,'"z-step_um": ')
        rline=strrep(rline,'"z-step_um": ','');
        rline=strrep(rline,',','');
        zStep=str2double(rline);
        ok=1;
    end
end

% width
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;
    
    if contains(rline,'"Width":')
        rline=strrep(rline,'"Width": ','');
        rline=strrep(rline,',','');
        width=str2double(rline);
        ok=1;
    end
end

% ROI
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;
    if contains(rline,'"ROI":')
        rline=fgetl(fid);
        nbrline=nbrline+1;
        rline=strrep(rline,',','');
        ROIPos(:,1)=str2double(rline);
         rline=fgetl(fid);
        nbrline=nbrline+1;
        rline=strrep(rline,',','');
        ROIPos(:,2)=str2double(rline);
        ok=1;
    end
end

% height
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;
    
    if contains(rline,'"Height":')
        rline=strrep(rline,'"Height": ','');
        rline=strrep(rline,',','');
        height=str2double(rline);
        ok=1;
    end
end

% pixel Size
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;
    
    if contains(rline,'"PixelSize_um": ')
        rline=strrep(rline,'"PixelSize_um": ','');
        rline=strrep(rline,',','');
        spixel=str2double(rline);
        ok=1;
    end
end

% number of time = frames
ok=0;
while ~feof(fid) && ~ok
    rline=fgetl(fid);
    nbrline=nbrline+1;
    
    if contains(rline,'"Frames": ')
        rline=strrep(rline,'"Frames": ','');
        rline=strrep(rline,',','');
        nTime=str2double(rline);
        ok=1;
    end
end

% initialisation

xPos=zeros(nbim,1);     % xposition
yPos=zeros(nbim,1);     % y position
zPos=zeros(nbim,1);     % z position
tAcq=zeros(nbim,1);     % time of acquisition
Chan=zeros(nbim,1);     % number of channel
filename=cell(nbim,1);  % name of fgiles in the order of acquisition

cpt=0;                  % count...

% go to the first Frame
while ~feof(fid) && cpt <=nbim
    rline=fgetl(fid);
    nbrline=nbrline+1;
   
    
    if contains(rline,'"FrameKey')
        cpt=cpt+1;
        nokTime=1;
        if cpt>1
            
            if ~okXpos
                error('Xposition not found in metadata.txt file folder %s, Framekey  %s',dirTelemos,rline);
            end
            if ~okYpos
                error('Yposition not found in metadata.txt file folder %s, Framekey  %s',dirTelemos,rline);
            end
            
            if ~okZpos
                error('Zposition not found in metadata.txt file folder %s, Framekey  %s',dirTelemos,rline);
            end
            
            if ~okTacq
                error('Exposure time not found in metadata.txt file folder %s, FrameKey  %s',dirTelemos,rline);
            end
            if ~okChan
                error('Channel not found in metadata.txt file folder %s, FrameKey  %s',dirTelemos,rline);
            end
            if ~okTime
                error('Time not found in metadata.txt file folder %s, FrameKey  %s',dirTelemos,rline);
            end
            if ~okFilename
                error('Filename not found in metadata.txt file folder %s, FrameKey  %s',dirTelemos,rline);
            end
        end
        okTacq=0;
        okXpos=0;
        okYpos=0;
        okZpos=0;
        okChan=0;
        okTime=0;
        okFilename=0;
    end
    
    if contains(rline,'"Exposure-ms": ')
        rline=strrep(rline,'"Exposure-ms": ','');
        rline=strrep(rline,',','');
        tAcq(cpt)=str2double(rline);
        okTacq=1;
    end
    
    
    if contains(rline,'"XPositionUm":')
        rline=strrep(rline,'"XPositionUm": ','');
        rline=strrep(rline,',','');
        xPos(cpt)=str2double(rline);
        okXpos=1;
    end
    
    if contains(rline,'"YPositionUm":')
        rline=strrep(rline,'"YPositionUm": ','');
        rline=strrep(rline,',','');
        yPos(cpt)=str2double(rline);
        okYpos=1;
    end
    
    
    
    if contains(rline,'"ZPositionUm":')
        rline=strrep(rline,'"ZPositionUm": ','');
        rline=strrep(rline,',','');
        zPos(cpt)=str2double(rline);
        okZpos=1;
    end
    
    if contains(rline,'"ChannelIndex":')
        rline=strrep(rline,'"ChannelIndex": ','');
        rline=strrep(rline,',','');
        Chan(cpt)=str2double(rline);
        okChan=1;
    end
    
     if contains(rline,'"Time":')
          if nokTime==1
              nokTime=2;
          else
              rline=strrep(rline,'"Time": ','');
              % rline=strrep(rline,'+0100"','');
              rline=strrep(rline,'"','');
              rline=strrep(rline,',','');
              rline=rline(1:(end-5));
              if cpt==1
                  timeTelemos=rline;
              else
                  timeTelemos=char(timeTelemos,rline);
              end
              okTime=1;
          end
     end
     
     if contains(rline,'"FileName": ')
         rline=strrep(rline,'"FileName": ','');
         rline=strrep(rline,'"','');
         rline=strrep(rline,',','');
         rline=strrep(rline,' ','');
         filename{cpt}=rline;
         okFilename=1;
     end
     
end



fclose(fid);


%% post treatment
listim=filename;
% check for the consistency of the data
% some metadata are recorded while acquisition is stopped by the user
% number of image for a given time
nbimindiv=nSlice*nbch;
nbtime=nbim/nbimindiv;
w=0;

if nbtime~=nTime
    w=w+1;
    warn{w}='number of time acquired differ from a priori setting';
    warning(warn{w});
end

if nbtime~=round(nbtime)
    w=w+1;
    warn{w}='acquisition interrrupted during acquisition: number of channel/Zstep not completed for all time points';
    warning(warn{w});
end

% number of series completed
nbtime=floor(nbtime);
nTime=nbtime;

%% end

cd (orig)
    