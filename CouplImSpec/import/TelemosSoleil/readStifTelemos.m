function readStifTelemos
%% description
    % import files acquired with telemos microscope and micromanager 
    % and save in  dso matlab format
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    % this function is specific of telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ


%% input
    % nothing
     
    
%% output
    % nothing 
    
%% principe
    % micro manager save  images in a structured file folder
    % architecture :
    % root folder : name given by the user
    %   subfolders roi(n)_tile1 : n folders for each selected roi
    %   display_and_comments.txt : file describing the channels acquired in
    %   
    %   in each subfolder roi(n)_tile1: 
    %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
    %               n = number of time
    %               p = z focal plane
    %               DM300_327-353 = name of the channel
    %       metadat.txt : files describing all metadat associated with the
    %       acquisition : x, y, z position, camera settings .... etc.
    %
    % images are imported as tif series
    % use the readtifGen couplImSpec function to import data as dso files

%% use
    % readStifTelemos
    
    
%% Comments
    % written for synchrotron project: 
    % "In situ tracking of enzymatic breakdown of cell wall materials by Synchrotron UV Fluorescence Microscopy."
    % Proposal N�: 20130978
    % resp: Estelle bonnin

    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    %  19 aout 2014
    % 12 octobre 2018

%% context variables 
orig=pwd;           % returns the current directory
    
method='TELEMOS3D';
% flag for registration with a brighfield image = 0 here
% required for dso format
regbf=0;
codebf='';              % no brighfield image is associated with the images

%% input

% parameters of telemos images
% rootfolder and rootfolderName: read folder
% dirTelemos : list of subfolder in the root folder
% listChannel : name of the channel
% nTime : number of Time point
% timeStep : step between two times
% nSlice : number of z
% Zstep : step between 2 slices
% spixel : pixel size
% width, height : number of pixel of the image
% x  y z coordinates in pixel unit
 % tAcq : exposure time
 % chan : channel of each image
 % timeTelemos : char array of the time at which individual images are
 % acquired
 % listim : file names found in the subfolder dirTelemos
 % warn : warning of post control of metadata

 
 %[rootFolder, rootFolderName, dirTelemos,listChannel]=getInfoTelemos;
 [rootFolder, rootFolderName, dirTelemos,listChannel]=metadataTelemos;
nbChannel=size(listChannel,1);
genNameSFolder=lower(dirTelemos(1,1:3));

% TODO g�rer les pos ou roi : dirTelemos(1,1:3)
cd(rootFolderName)
pAcqu=readDIV(strcat(rootFolder,'.',genNameSFolder,'.metadata.txt'));

nTime=pAcqu.d(:,1);
if length(unique(nTime))~=1
    error('number of time in %s subfolders differ',genNameSFolder);
end
nTime=unique(nTime);
width=pAcqu.d(1,7);
height=pAcqu.d(1,6);
nSlice=pAcqu.d(1,3);
spixel=pAcqu.d(1,5);

 
 % sfolder
 sfolder=pwd;
 [sfolder]=uigetdir(sfolder,'imported dso files saved in folder : ');
 

%% treatment

% number of roi in the root directory
nb=size(dirTelemos,1);

% for each subfolder
for i=1:nb
    % go to the folder of the current roi
    cd(rootFolderName)
    rfolder=dirTelemos(i,:);
    cd(rfolder)
    display(rfolder)
    
    % fulll folder name
    rfoldert=pwd;
    
    
             
    % for each time
    for t=1:nTime
        if t<=9
             fnamegen='img_00000000';
        else if t<=99
                 fnamegen='img_0000000';
            else
                 fnamegen='img_000000';
            end
        end
        codeg=strcat(fnamegen,num2str(t-1),'_');
        for c=1:nbChannel
           code=strcat(codeg,listChannel(c,:));
                      
           listFile=dir(strcat(code,'*.tif'));
           nbf=length(listFile);
           
           for f=1:nbf
               im=imread(listFile(f).name);
               imz(:,:,f)=im;
           end;
           
           ims(:,:,:,c)=imz;
                      
           
           if c==1
               % z label
               if nbf<=9
                   codeZ=num2str((1:nbf)');
               else if nbf<=99
                       codeZ=strcat('0',num2str((1:9)'));
                       codeZ=char(codeZ,strcat(num2str((10:nbf)')));
                   else
                       codeZ=strcat('0',num2str((1:9)'));
                       codeZ=char(codeZ,strcat(num2str((10:99)')));
                       codeZ=char(codeZ,strcat(num2str((100:nbf)')));
                   end;
               end;
           end;
        
       
        
        % convert to dso
        data=reshape(ims,width*height,nSlice,nbChannel);
        
       %% dso: data set object
        dso=dataset;
        if t<10
            sname=strcat(rootFolder,'.',dirTelemos(i,:),'.0',num2str(t));
        else
            sname=strcat(rootFolder,'.',dirTelemos(i,:),'.',num2str(t));
        end;            
        dso.name=sname;
        dso.author=method;
        dso.data=data;
        dso.axisscale{1}=1:width*height;
        dso.title{1}='pixel';
        dso.axisscale{2}=1:nSlice;
        dso.title{2}='zpixel';
        dso.label{2}=codeZ;
        if nbChannel>1
            dso.axisscale{3}=1:nbChannel;
            dso.title{3}='image';
            dso.label{3}=listChannel;
        end;
        
        dso.type='image';
        dso.imagemode=1;
        dso.imagesize=[height width];
        
        

        % dso.userdata
        % acquisition field
        dso.userdata.acquisition.unit='fluorescence intensity';               % unit of wavenumber
        dso.userdata.acquisition.method=method;         %  image acquisition method
        pixSize.value=spixel;
        pixSize.unit='microns';
        dso.userdata.acquisition.pixSize=pixSize;       % pixel size
        % no coordstage data available a priori....

         % save
        disp('sauvegarde....')
        cd(sfolder);
        % name = generic name + number for the roi + . + number for the
        % time
 
        savedso(dso);
        savedsotrack(dso,fnamegen,rfoldert,sname,sfolder,codeZ,pixSize,method);
    end
end;

%% end

cd (orig)

end

% subfunctions
function savedsotrack(dso,fnamegen,rfoldert,sname,sfolder,code,pixSize,method)

%% matlab function tracking  

fid=fopen(strcat(dso.name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,'Build a dso structure of multivariate image from a set of tif files \r\n');
fprintf(fid,'     Acquired with the TELEMOS microscope of synchrotron SOLEIL     \r\n');
fprintf(fid,'____________________________________________________________________\r\n');

fprintf(fid,'data folder: %s\r\n',rfoldert);
fprintf(fid,'generic name of imported images: %s*.tif\r\n\r\n',fnamegen);

fprintf(fid,'\r\nsaved name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

fprintf(fid,'\r\nfiles included in the multivariate image\r\n');
cd(rfoldert);
liste=dir(strcat(code,'*.tif'));
for i=1:length(liste)
    fprintf(fid,'\t- %s\r\n',liste(i).name);
end;
fprintf(fid,'\r\n');

fprintf(fid,'\r\nname of Z images in the multivariate image\r\n');
vs=dso.label{2};
for i=1:size(vs,1)
    fprintf(fid,'\t- %s\r\n',vs(i,:));
end;
fprintf(fid,'\r\n');

if length(size(dso))==3
    fprintf(fid,'\r\nname of chanel images in the multivariate image\r\n');
    cs=dso.label{3};
    for i=1:size(cs,1)
        fprintf(fid,'\t- %s\r\n',cs(i,:));
    end;
    fprintf(fid,'\r\n');
end;

if pixSize.value~=0
    fprintf(fid,'\r\nPixel size : %6.2f %s\r\n',pixSize.value, pixSize.unit);
else
    fprintf(fid,'\r\nPixel size : unknown\r\n');
end;
fprintf(fid,'\r\n');


fprintf(fid,'\r\nmethode d''acquisition des images : %s\r\n',method);
fprintf(fid,'\r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);



