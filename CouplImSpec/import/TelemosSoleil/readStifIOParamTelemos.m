  function [rootFolder,rootFolderName,dirTelemos,code,codebf,regbf,pixSize,ngendep,sfolder]=readStifIOParamTelemos
  %% description
  
    % input parameters for readStifTelemos function for the specific case of the
    % TELEMOS image acquisition at the synchrotron SOLEIL
    %
    % this function is specific of telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    
    
%% input

    %  nothing
    
%% output
    
    % rootfolder : root folder name
    % rootFolderName: absolute root folder name
    % dirTelemos : list of folders in the root folder
    % code : series of extension to be added to generic name to read the file
    % pixSize : pixel size as a structure with 2 fields : value and unit
    % ngendep : generic name for saving the resulting dso files
    % sfolder : folder where to save imported data


%% principe

    % intercative input of parameters to import TELEMOS images in dso format
    % use getinfoTelemos and readMetadataTelemos function
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm

%% use

    % [rootFolder, rootFolderName,
    % dirTelemos,listChannel,code,pixSize,sfolder]=readStifIOParamTelemos;
    
%% Comments

    % adapted for synchrotron project: 
    % "In situ tracking of enzymatic breakdown of cell wall materials by Synchrotron UV Fluorescence Microscopy."
    % Proposal N�: 20130978
    % resp: Estelle bonnin
    % from readStifIOParamGEn.m function 
    % written in couplImSpec AIC project
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 19 aout 2014

%% context variables 
fnamegen='img_000000000_';      % base name of image fropm micro manager
ext='tif';                                              % image type

orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: [rfolder,fnamegen,code,codebf,pixSize]=readStifIOParamGen;');
end


%% input

% get info from Telemos acquisition
[rootFolder, rootFolderName, dirTelemos,listChannel]=getInfoTelemos;
 [nTime,timeStep,nSlice,zStep,spixel,width,height,xPos,yPos,zPos,tAcq,Chan,timeTelemos,listim,warn]=readMetadataTelemos(dirTelemos,listChannel)

% % find the name of one of the images
% cd(rootFolderName)
% rfolder=dirTelemos(1,:);
% cd(rfolder);
% 
% list=dir('*.tif');
% nom=list(1).name;
% fnamegen=strrep(nom,'_000.tif','');
% fnamegen=strrep(fnamegen,listChannel(1,:),'');
% fnamegen=fnamegen(1:(end-1));
% ext='tif';
% 
% % saving folder
% cd(rootFolderName)
% cd ..                   % foldertype of sample folder
% cd ..                   % date folder
% cd ..                   % 'acquisition' folder
% cd ..                   % 'traiter' folder
% cd('analyse\dso');
% % define a generic names for saving files : the first 3 character of the
% % root folder are selected
% % find generic name...
% ngendep=rootFolder(1:3);
% rep = inputdlg('Generic name for saving files','Generic name',1,{ngendep});
% ngendep=rep{1};
% 
% % create a separate saving folder for each sample
% if ~exist(ngendep,'dir')
%     mkdir(ngendep);
% end
% cd(ngendep);
% 
 cd(rootFolderName)
 if ~exist('dso','dir')
     mkdir('dso')
 end
 cd('dso')
sfolder=pwd;


%% selection of code 
cd(rootFolderName)
cd(rfolder);
liste=dir(strcat(fnamegen,'*.',ext));
for i=1:length(liste)
    codei=strrep(liste(i).name,fnamegen,'');
    codei=strrep(codei,strcat('.',ext),'');
    code{i}=codei; %#ok<*AGROW>
end;


%% read some MetadataTelemos and save
cd(sfolder)
fid=fopen(strcat(rootFolder,'.readStifTelemos.txt'),'w');

if fid==0
    error('enable to open %s file',strcat(rootFolder,'.readStifTelemos.txt'));
end;

fprintf(fid,'\r\nMetadata imported from TELEMOS image acquisition at synchrotron SOLEIL\r\n\r\n');
fprintf(fid,'root folder: %s\r\n\r\n',rootFolder);

for i=1:size(dirTelemos,1)
    cd(rootFolderName)
    [nTime,timeStep,nSlice,zStep,spixel,width,height,xPos,yPos,zPos,tAcq,timeTelemos]=readMetadataTelemos(dirTelemos(i,:),listChannel);
    cd(dirTelemos(i,:))
    listim=dir('*.tif');
    
    % save in a track file rootFolder.readStifTelemos.txt
    fprintf(fid,'________________________________________________________________________________________________________\r\n');
    fprintf(fid,'Subfolder: %s\r\n\r\n',dirTelemos(i,:));
    fprintf(fid,'\t- Number of Times: %d\t\t\t- Time step %6.2f s\r\n',nTime,timeStep);
    fprintf(fid,'\t- Number of z images: %d\t\t- Z step %8.4f microns\r\n',nSlice,zStep);
    fprintf(fid,'\t- Number of channels: 1\t\t\t- Channel: %s\r\n',listChannel);
    
    fprintf(fid,'\t- Pixel size: %8.4f microns\r\n',spixel);
    fprintf(fid,'\t- Image size = width x height: %d * %d pixels = %6.2f * %6.2f microns\r\n',width,height,width*spixel,height*spixel);
    fprintf(fid,'\t- Exposure time of each image: %6.2f ms\r\n',unique(tAcq));
    
    fprintf(fid,'\r\n\r\nIndividual image parameter:\r\n\r\n');
    fprintf(fid,'Name of image \t\t\t\txPositionUm\tyPositionUm\tzPositionUm\tTime\r\n');
    
    for j=1:length(xPos)
        fprintf(fid,'%s: \t%6.2f\t%6.2f\t\t%6.2f\t\t%s\r\n',listim(j).name,xPos(j),yPos(j),zPos(j),timeTelemos(j,:));
    end;
    fprintf(fid,'\r\n\r\n');
end
    
% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
fclose(fid);

%% pixel size

pixSize.value=spixel;

unitList={'microns','mm','cm','unknown'};
pixSize.unit=char(unitList(1));



%% matlab function tracking  
% no tracking for this function

%% end

cd (orig)
    