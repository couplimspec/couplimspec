  function [dso]=readspc(fname,sfolder)

 %% description
    % Read spc file (fluorescence) : DEDICATED to POLYPHEME acquisition at
    % synchrotron SOLEIL
    
%% input

    % fname: 'filename.spc' 
    % sfolder : save folder for dso.mat file

    
%% output
    
    % dso: datasetobject of map 

%% principe
    % Adapted from spcreadr from PLS Toolbox
    % provided by F Jamme Synchrotron SOLEIL

%% use
    % [dso]= readspc('filename.spc'); return dataset "dso" of spc map.
    % dso= readomnic;
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % reviewed for CouplImSPec
    % initially writen for F.Allouche Phd Thesis
    
    
    
%% Author
    % F.Jamme INRA/SOLEIL 
    % modified par F. Allouche, MF Devaux   dso.userdata.visim
    % MF Devaux dso.userdata.visim
    
%% date
    % 19/09/2014 : couplimspecdso
    % 03/03/2011  
    % 28/04/2011 ( change load visible image)
    % 21/11/2019 : modified to new life !
    % 20/01/2020 : take into account two deadpixels inices 75 and 76 in
    % POLYPHEME files

%% context variables 
orig=pwd;           % returns the current directory
% dead pixels : indexes for correct values surrounding the two dead pixels 75 and 76 
deadPixels=[75 76];
surroundingDeadPixelStart=74;
surroundingDeadPixelEnd=77;


%% start

if nargin >2 
    error('use: [dso_read]= readspc(filename,sfolder)');
end

if nargin >0
    if ~exist(fname,'file')
        error('no file %s in folder %s',fname,orig)
    end
    [~,name,ext]=fileparts(fname);
    pathname=pwd;
    sname=strcat(name,'.dso.mat');
end

if nargin ==1
    % file.dso.mat and track file will be saved in the same folder than the
    % original spc file
    sfolder=pathname;
end

if nargin ==0
    % open file from folder 
    [fname,pathname]=uigetfile({'*.spc'},'select *.spc file');
    [~, name, ext] = fileparts(fname);
    cd(pathname);
    % name and folder to save results
    [sname,sfolder] = uiputfile({'*.mat'},'result file name',strcat(name,'.dso.mat'));
end

%% load visible 

visimname=strcat(name,'.tif');
if exist(visimname,'file') 
    visim = imread(visimname);
else
    error('visible image %s does not exist in folder %s',visimname,pathname);
end


%% load file to workspace
% use spcreadr function from the PLS toolbox that return a dataset object
[dsor]=spcreadr(fname);
dso=dataset; %#ok<DTSET>
dso.name=strrep(fname,'.spc','');

% the D.axisscale{1} correspond to the coordinates in �m in the horizontal
% direction (column numbers). The following orders aim at finding the number of lines in the
% image
DD=zeros(size(dsor.axisscale{1}));
for i=1:length(dsor.axisscale{1})
    DD(i)=dsor.axisscale{1}(1)-dsor.axisscale{1}(i);
end
   
nr=length(find(not(DD))); % number of raw
nc=length(dsor.axisscale{1})/nr; % number of columns


% check every attribute of dataset obejct
dso.author='POLYPHEME';
% spectra matrix : dso.data from labsepc fluorescence spectra are in reverse order of
% wavelength: we put the data from the lower to the highest wavelength
dso.data=fliplr(dsor.data);
dso.axisscale{2}=fliplr(dsor.axisscale{2});

% assess the 3D array of the spectral image
nblo=size(dso.data,2);                      % number of wavelength
tmp=reshape(dso.data,nc,nr,nblo);
% permute raw and columns to respect the image aspect in Labspec
% and the order of spectral acquisition
tmp=permute(tmp,[2 1 3]);
% reconstitute the 2D array of spectral data 
dso.data=reshape(tmp,nr*nc,nblo);

% interpolate dead pixels values
[a,b]=lineEq([surroundingDeadPixelStart surroundingDeadPixelEnd],dso.data(:,[surroundingDeadPixelStart surroundingDeadPixelEnd]));
dso.data(:,deadPixels)=deadPixels.*a+b;


% identifyer
dso.title{1}='pixel';
dso.label{2}=num2str(dso.axisscale{2}');
dso.title{2}='Wavelength';

% convert to image type
dso.type='image';
dso.imagemode=1;
dso.imagesize=[nr nc];              

% change the userdata to put values in description
dso.description=dsor.userdata{1};


% userdata
% acquisition field
dso.userdata.acquisition.unit='nm';               % unit of wavenumber
dso.userdata.acquisition.method='FLUORESCENCE-POLYPHEME-DISCO';         %  image acquisition method
pixSize=dsor.axisscale{1}(2)-dsor.axisscale{1}(1);
dso.userdata.acquisition.pixSize.value=pixSize;       % pixel size
dso.userdata.acquisition.pixSize.unit='micron';       % pixel size

DX=dsor.axisscale{1}(1:nc);
DY=(1:nr)*pixSize;
dso.userdata.acquisition.coordStage.col=DX;  % position en �m en X as recovered from data
dso.userdata.acquisition.coordStage.row=DY;                 % position en �m en Y  from 1 to number of step

if exist('visim','var')
    dso.userdata.visim.name='visimname';           % image visible
    dso.userdata.visim.pathname=pathname;
end


%% plot imagedata sum 

h=figure(27);
set(h,'windowstyle','docked');
area=sum(dso.imagedata,3);

% works with %the PLS - toolbox
%imagesc(dso.imageaxisscale{2,1},dso.imageaxisscale{1,1},area); 

imagesc(DX,DY,area);
colormap(jet)
title(name,'Fontsize',12);
set(gca,'dataAspectRatio',[1 1 1])


%% save dso
cd(sfolder);
save(sname,'dso');

%% save sum of wavenumbers as a grey level image

% adjust values to 0-255 grey levels
mini=min(area(:));
maxi=max(area(:));

a=255/(maxi-mini);
b=-a*mini;

imaff=uint8(area*a+b);

imwrite(imaff,strcat(name,'.sum.tif'),'tif','compression','none');

%% matlab function tracking  

fid=fopen(strcat(name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Import data set object from LAbspec *.spc map file \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput file name: %s%s\r\n',name,ext);
fprintf(fid,'data folder: %s\r\n',pathname);

fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);


%% end

cd (orig)
    