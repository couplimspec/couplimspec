  function [method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar,sfolder]=readStifIOParam
  %% description
  
    % entr�e des parametres de la fonction stifRead
    
%% input

    %  nothing
    
%% output
    
    % method : method of image acquisition 
    % rfolder : read folder for tiff images
    % fnamegen : generic name of images 
    % code : series of extension to be added to generic name to read the file
    % codebf : code of visible image, if there is none : codebf must be ''
    % regbf : flag indicating if the spectral image is registrated or not
    %           to the visible image
    % pixSize : pixel size as a structure with 2 fields : value and unit
    % codevar : code of spectral variables to be included in dso file if exists
    % sfolder : save folder for dso.mat file


%% principe

    % entree interactive des param�tres pour les fonctions readStif et
    % processCol (TODO)


%% use

    % [method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar,sfolder]=readStifIOParam
    % dso= readStif(fnamegen,rfolder,sfolder,code,codebf,method,codevar);
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments

%   review for CouplimsPEC tOOLBOX
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 25 Aout 2014 : bug for  brightfiled image registration
    % 26 mars 2014: start with method to separate fucntions according to
    % method
    % 7 mars 2014 : review for couplImSpec toolbox and Mathias Corcel PhD
    
    % 28 f�vrier 2013
    % 20 aout 2019 : point virgules and check


%% context variables 

orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: [method,rfolder,fnamegen,code,codebf,regbf,pixSize,codevar,sfolder]=readStifIOParam;');
end


%% input
%nom de la m�thode d'acquisition
listemethod={'CONFOCAL-Nantes','MACROFLUO-Nantes','other'};
[selection ]=listdlg('liststring',listemethod,'selectionmode','single','initialvalue',1,'name','image acquisition technique','promptstring','image acquisition technique');
method=char(listemethod(selection));

if strcmp(method,'other')
    method=inputdlg('name of the acquisition method');
    method=char(method);
end

% D�finition des variables spectrales 
switch method
    case {'CONFOCAL-Nantes'}
        [rfolder,fnamegen,code,codebf,pixSize,codevar]=readStifIOParamConfocalNantes;
        if ~isempty(codebf)
            regbf=1;        % brigtfield image is supposed to be registrated at acquisition
       else
            regbf=0;
        end
       

    case {'MACROFLUO-Nantes';'MACROFLUO-Montpellier'}
        [rfolder,fnamegen,code,codebf,pixSize,codevar]=readStifIOParamMacrofluoNantes;
        if ~isempty(codebf)
            regbf=yesno('is the spectral image registrated to the visible image?');
        else
            regbf=0;
        end

    otherwise
        [rfolder,fnamegen,code,codebf,pixSize]=readStifIOParamGen;
        codevar='';
        if ~isempty(codebf)
            regbf=yesno('is the spectral image registrated to the visible image?');
        else
            regbf=0;
        end
end



% name and folder to save results
sname=strcat(fnamegen,'.dso.mat');
[~,sfolder] = uiputfile({'*.mat'},'result file name',sname);



%% matlab function tracking  
% no tracking for this function

%% end

cd (orig)
    