function calibrCreatImSMIS
%% etalonnage d'une image visible pour laquelle on a les coordonnees
% image et donnees SMIS
%
%% param�tres d'entr�e
%
%   pas de parametres d'entree
%
%
%% param�tres de sortie
%   dl : droite calibration en y ( ligne des images)
%   dc : droite calibration en x (colonne des images)
%
%% principe
%  

%% Usage : 
%   [dc,dl]=calibrImSMIS

%% auteurs : 
%   MF Devaux 
%   BIA PVPP
%   17 mars 2015

%% contexte
% projet SOLEIL degradation enzymatqiue

%% input

[fname,rfolder]=uigetfile({'*.tif'},'name of visible image with plate coordinate','*.tif');

cd(rfolder)
im=imread(fname);

%% calibration
figure
imshow(im)

 yesno('X axis. ready for calibration? So place line on xaxis !');
 
  
h = imdistline(gca);
api = iptgetapi(h);
api.setLabelVisible(false);
api.setPosition([75 size(im,1)-75; size(im,2)-75 size(im,1)-75 ])
zoom(2)
pan on

 % pause -- you can move the edges of the segment and then press  a key to continue
 pause();
 zoom out
 
 % get the distance
 dist = round(api.getDistance());

 
 n1=str2double(inputdlg('distance in �m'));

 
 tpix=n1/dist;
 
 yesno('Y axis. ready for calibration? So place line on yaxis !');
 
  
h = imdistline(gca);
 api = iptgetapi(h);
 api.setLabelVisible(false);
api.setPosition([75 size(im,1)-75; 75 75 ])
zoom(2)
pan on

 % pause -- you can move the edges of the segment and then press  a key to continue
 pause();
 
 zoom out

 % get the distance
 dist = round(api.getDistance());

 
 n1=str2double(inputdlg('distance in �m'));

 
 tpiy=n1/dist;
 
 
% 
% 
% yesno('clic for FIRST point on x axis. ready for calibration?');
% n1=str2double(inputdlg('point x coordinate in �m'));
% 
% [x1,y1,~]=impixel(im);
% x1=x1(1);
% y1=y1(1);
% 
% 
% 
% yesno('clic for LAST point. on x axis ready for calibration?');
% n2=str2double(inputdlg('point x coordinate in �m'));
% 
% [x2,y2,~]=impixel(im);
% x2=x2(1);
% y2=y2(1); 
% 
% if abs(y1-y2)>2
%     error('line is not horizontal')
% end
% 
% tpix=(abs(n2-n1))/abs(x2-x1);
% display(tpix)
% 
% 
% yesno('clic for FIRST point on y axis. ready for calibration?');
% n1=str2double(inputdlg('point y coordinate in �m'));
% 
% [x1,y1,~]=impixel(im);
% x1=x1(1);
% y1=y1(1);
% 
% 
% 
% yesno('clic for LAST point. on y axis ready for calibration?');
% n2=str2double(inputdlg('point y coordinate in �m'));
% 
% [x2,y2,~]=impixel(im);
% x2=x2(1);
% y2=y2(1); 
% 
% if abs(x1-x2)>2
%     error('line is not vertical')
% end
% 
% tpiy=(abs(n2-n1))/abs(x2-x1);
% 
% display(tpiy)
% 
if (abs(tpix-tpiy)>0.01)
    error('check calibration')
end

tpix=(tpiy+tpix)/2;

fliph=yesno('flip image horizontally ?');

% find subimage
mask=imerode(rgb2gray(im)<255,strel('square',7));

res=regionprops(mask,{'boundingbox'});
res=round(res.BoundingBox);

ims=im((res(2):(res(2)+res(4)-1)),res(1):(res(1)+res(3)-1),:);

fim=imopen(max(ims,[],3),strel('square',7));

if fliph
    fim=fliplr(fim);
end

if fliph
    sname=strrep(lower(fname),'.tif','.ext.fh.tif');
else
    sname=strrep(lower(fname),'.tif','.ext.tif');
end;

imwrite(fim,sname,'tif','compression','none');

%% save
 variable=char('nblig','nbcol','tpix','tlig','tcol');
 d.i=strrep(sname,'.tif','');
 d.v=variable;
d.d=[size(im,1) size(im,2) tpix tpix*size(im,1) tpix*size(im,2)];

 writeDIV(d,strcat(sname,'.size.txt'));
 
end   