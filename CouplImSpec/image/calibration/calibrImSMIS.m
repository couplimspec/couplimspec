function calibrImSMIS
%% etalonnage d'une image visible pour laquelle on a les coordonnees
% image et donnees SMIS
%
%% param�tres d'entr�e
%
%   pas de parametres d'entree
%
%
%% param�tres de sortie
%   dl : droite calibration en y ( ligne des images)
%   dc : droite calibration en x (colonne des images)
%
%% principe
%  

%% Usage : 
%   [dc,dl]=calibrImSMIS

%% auteurs : 
%   MF Devaux 
%   BIA PVPP
%   17 mars 2015

%% contexte
% projet SOLEIL degradation enzymatqiue

%% input

[fname,rfolder]=uigetfile({'*.tif'},'name of visible image with plate coordinate','*.tif');

cd(rfolder)
im=imread(fname);

%% calibration
figure
imshow(im)

 yesno('X axis. ready for calibration? So place line on xaxis !');
 
  
h = imdistline(gca);
api = iptgetapi(h);
api.setLabelVisible(false);
api.setPosition([75 size(im,1)-75; size(im,2)-75 size(im,1)-75 ])

 % pause -- you can move the edges of the segment and then press  a key to continue
 pause();

 % get the distance
 dist = round(api.getDistance());

 
 n1=str2double(inputdlg('distance in �m'));

 
 tpix=n1/dist;
 
 yesno('Y axis. ready for calibration? So place line on yaxis !');
 
  
h = imdistline(gca);
 api = iptgetapi(h);
 api.setLabelVisible(false);
api.setPosition([75 size(im,1)-75; 75 75 ])

 % pause -- you can move the edges of the segment and then press  a key to continue
 pause();

 % get the distance
 dist = round(api.getDistance());

 
 n1=str2double(inputdlg('distance in �m'));

 
 tpiy=n1/dist;
 
 
% 
% 
% yesno('clic for FIRST point on x axis. ready for calibration?');
% n1=str2double(inputdlg('point x coordinate in �m'));
% 
% [x1,y1,~]=impixel(im);
% x1=x1(1);
% y1=y1(1);
% 
% 
% 
% yesno('clic for LAST point. on x axis ready for calibration?');
% n2=str2double(inputdlg('point x coordinate in �m'));
% 
% [x2,y2,~]=impixel(im);
% x2=x2(1);
% y2=y2(1); 
% 
% if abs(y1-y2)>2
%     error('line is not horizontal')
% end
% 
% tpix=(abs(n2-n1))/abs(x2-x1);
% display(tpix)
% 
% 
% yesno('clic for FIRST point on y axis. ready for calibration?');
% n1=str2double(inputdlg('point y coordinate in �m'));
% 
% [x1,y1,~]=impixel(im);
% x1=x1(1);
% y1=y1(1);
% 
% 
% 
% yesno('clic for LAST point. on y axis ready for calibration?');
% n2=str2double(inputdlg('point y coordinate in �m'));
% 
% [x2,y2,~]=impixel(im);
% x2=x2(1);
% y2=y2(1); 
% 
% if abs(x1-x2)>2
%     error('line is not vertical')
% end
% 
% tpiy=(abs(n2-n1))/abs(x2-x1);
% 
% display(tpiy)
% 
if (abs(tpix-tpiy)>0.01)
    error('check calibration')
end

tpix=(tpiy+tpix)/2;


%% save
 variable=char('nblig','nbcol','tpix','tlig','tcol');
 d.i=strrep(fname,'.tif','');
 d.v=variable;
d.d=[size(im,1) size(im,2) tpix tpix*size(im,1) tpix*size(im,2)];

 writeDIV(d,strcat((fname),'.size.txt'));
 
end   