function [ima,a,b]=im2uint8(im,in,out)
%% description: convert linearly images intensities in uint8 

%% input
    %  im : image
    % optional 
        % in : vector of min and max grey level values of the input image for
        % conversion 
        % out : vector of min and max values for uint8 result
    
%% output
    %  ima : uint8 resulting image
    % a and b : cossficient of the linear conversion y=ax+b

%% principle
    % if no input grey levels have been given, in =min and max of the input image
    % if no output grey leve are given they are equal to 0 and 255
    % if grey level under min or over max input grey levels are givent, they are set to 
    % min and max output values
    
%% use
    % [ima,a,b]=im2uint8(im,in,out);
        
%% Comments
    % adapted for couplimSpec 
    
%% Authors
    % MF Devaux
    % INRA - BIA-PVPP

%% date
    % 7 novembre 2014

%% start 
if (nargin==0)&&(nargin>3)
   error('USE : [ima,a,b]=im2uint8(im,<in>,<out>)');
end;

%% input
if nargin==1
    in=[min(im(:)) max(im(:))];
    out=[0 255];
end;
    
if nargin>=2
    if size(in)~=2
        error('vector of [min max] input grey level values expected');
    end;
    if in(1)>=in(2)
        error('[min max] input grey level values [%f6.2 %f6.2] does not seem correct',in(1), in(2));
    end
end;

if nargin==2
    out=[0 255];
end

if nargin==3
    if size(out)~=2
        error('vector of [min max] output grey level values expected');
    end;
    
    if out(1)>=out(2)
        error('[min max] output grey level values [%f6.2 %f6.2] does not seem correct',out(1), out(2));
    end
    
    if out(2)>255 
        error('uint8 value expected');
    end;
end;

%% treatment
mini=double(in(1));
maxi=double(in(2));
mino=double(out(1));
maxo=double(out(2));

% conversion coefficient
a=(maxo-mino)/(maxi-mini);
b=mino-a*mini;

% convert image
ima=double(im)*a+b;
ima=uint8(ima);

