function [ima,a,b]=imAdjust(im,mini,maxi)

if (nargin~=1)&&(nargin~=3)
   error('USAGE : ima=ajusterimage(im,<min,max> optionnels)');
end;

if nargin==1
    mini=min(im(:));
    maxi=max(im(:));
end;
    
mini=double(mini);
maxi=double(maxi);

a=255/(maxi-mini);
b=-a*mini;

ima=double(im)*a+b;
ima=uint8(ima);

