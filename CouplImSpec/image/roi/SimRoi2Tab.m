function [tab]=SimRoi2Tab(im,mask)

 %% description
    % reshape a spectral image to form a usual data table
    % use a mask to select only required pixels
    
%% input

    %  im : spectral image as a 3 way data table : x X y X spectral  dimension
    %  mask : logical image of the same size than im x X y : show pixels
    %  that will be selected
    %
    
%% output
    
    %   tab : table of selected pixels as a 2 way data table : n x spectral  dimension
    %   	n = number of selected pixels = sum(mask);

%% principle
    % unfold the spectral image
    %  apply the mask to select pixels

%% use
    % [tab]=SimRoi2Tab(im,mask)
    
%% Comments
    % written  for large image clustering
    % following M corcel PhD
    
    
%% Author
    % MF Devaux
    % URBIA
    % PVPP
    
%% date
    % 29 juin 2017



%% start

if nargin ~=2
    error('use: [tab]=SimRoi2Tab(im,mask)');
end



%% treatement

nl=size(im,1);  % number of lines
nc=size(im,2);  % number of columns
ns=size(im,3);  % number of spectral variables

% unfold the image to form a 2-way data table of pixels x spectral
% variables
tab=reshape(im,nl*nc,ns);

% select pixels within the mask
tab=tab(reshape(mask,nl*nc,1),:);



%% matlab function tracking  

% no tracking for theis fucntion
% to be callled within other fucntions

%% end


    


