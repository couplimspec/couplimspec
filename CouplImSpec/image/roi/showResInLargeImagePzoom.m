function [vLarge,setOfZoom,hf]=showResInLargeImagePzoom(res,mask,setOfMask,titre,map,varargin)



% if nargin>=5
%     map=varargin{1};
% else
%     map=grey(256);
%     
% end;

if nargin>=6
    flagview=varargin{1};
else
    flagview=[1 1 1];
end

if nargin ==7
    newfig=varargin{2};
else
    newfig=1;
end;

tmp=uint8(zeros(size(mask)));
tmp(mask)=uint8(res);


if flagview(1)
    if newfig
        figure
        hf(1)=imshow(tmp,[]);
    else
        hf(1)=subimage(tmp,[]);
        axis off
    end
    
    
    vLarge.image=tmp;
    
    if exist('titre','var')
        title(titre)
    end
%     if exist('map','var')
%         colormap(map);
%         vLarge.map=map;
%     end
else
    vLarge=[];
    
    
end;
%imwrite(vim,strcat(dso.name,num2str(i),'.tif'),'tif','compression','none');
% ma=max(tmp(:));
% mi=min(tmp(:));
%vLarge=convIm2uint16(tmp,[mi ma]);
% if exist('map','var')
%     vLarge=ind2rgb(tmp,map);
% else
%end

if sum(flagview(2:end)>=1)
    setOfZoom=setOfMask;
    for i=1:length(setOfMask)
        
        setOfZoom{i}=uint8(setOfZoom{i});
    end
    
    
    
    for i=1:length(setOfMask)
        %ism1=find(sm1(mask));
        sm1=setOfMask{i};
        [ii,jj]=find(sm1);
        s1=sm1(min(ii):max(ii),min(jj):max(jj));
        m1=sm1(mask);
        
        if flagview(i+1)
            
            
            tmp=uint8(zeros(size(s1)));
            tmp(s1)=uint8(res(m1));
            
            if newfig
                figure
                hf(i+1)=imshow(tmp,[]);
            else
                hf(i+1)=subimage(tmp,[]);
                axis off

            end
            vim=tmp;
            % end
            setOfZoom{i}=vim;
            
            if exist('titre','var')
                title(titre)
            end
%             if exist('map','var')
%                 %             %colormap(map(unique(tmp)+1,:));
%                 colormap(map((min(tmp(:))+1):max(tmp(:))+1,:));
%                 setOfZoom{i}.map=map((min(tmp(:))+1):max(tmp(:))+1,:);
%             end;
            %
            %         %vim=convIm2uint16(tmp,[mi ma]);
            %             vim=ind2rgb(tmp,map(1:max(tmp(:))+1,:));
            %         else
            
        end
        %imwrite(vim,strcat(dso.name,num2str(i),'e1.tif'),'tif','compression','none');
    end
else
    setOfZoom=[];
end

% %ism2=find(sm2(mask));
% [ii,jj]=find(sm2);
% s2=sm2(min(ii):max(ii),min(jj):max(jj));
% m2=sm2(mask);
%
% figure
% tmp=zeros(size(s2));
% tmp(s2)=dso.data(m2,i);
% imshow(tmp,[])
% vim=convIm2uint16(tmp,[mi ma]);
% imwrite(vim,strcat(dso.name,num2str(i),'e2.tif'),'tif','compression','none');
