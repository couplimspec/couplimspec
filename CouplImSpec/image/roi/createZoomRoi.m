  function createZoomRoi

 %% description
    % propose region of interest from exiting roi
    
%% input

    %  nothing: intercative function applied to series of images
    
%% output
    
    %   nothing: zoom roi are saved automatically

%% principe
    % from logical mask : 
    %       - select a region in the centre of the initial mask 
    %       - select a region of the rgight of the initial mask
    % images of the same size than the mask are saved as zoom rois


%% use
    % createZoomRoi
    
%% Comments
    % written to show results or protions of large images
    % following M corcel pHd 2017
    
    
%% Author
    % MF Devaux
    % INRA - Nantes
    % BIA PVPP
    
%% date
    % 5 juillet 2017
    % 8 aout 2019 : % consider any syntax name for rois 

%% context variables 
orig=pwd;           % returns the current directory
facteurZoom=4;

%% start

if nargin >0
    error('use: createZoomRoi');
end


%% input
[~,rfolder]=uigetfile({'*.tif'},'name of first roi to process','*.tif');
cd (rfolder)
list=dir('*.tif');
nb=length(list);

valider=yesno('associated grey level images ?');
if valider
    cd ..
    listg=dir('*.tif');
    nbg=length(listg);
    if nbg~=nb
        error('number of grey level image files differ from number of rois');
    end
    rfolderg=pwd;
    cd(rfolder)
    if ~exist('voir','dir')
        mkdir('voir')
    end
end


%% treatment

cd(rfolder)

for i=1:nb
    display(list(i).name)
    mask=imread(list(i).name);
    
    nl=size(mask,1);
    nc=size(mask,2);
    
   szl=round(nl/facteurZoom);
   szc=round(nc/facteurZoom);
   
   % zoom near border. find the place with maximum of pixels
   
   idl=0;
   for il=1:szl:nl
       idl=idl+1;
       
       idc=0;
       for ic=1:szc:nc
           idc=idc+1;
           s(idl,idc)=sum(sum(mask(il:min(il+(szl),nl),ic:min(ic+(szc),nc)))); %#ok<AGROW>
       end
   end
   
   bord{1}=s(1,:);
   bord{2}=s(end,:);
   bord{3}=s(:,1);
   bord{4}=s(:,end);
   
   for ii=1:4
       f(ii)=max(bord{ii});
   end
   
   fi=find(f==max(f));
   fi=fi(1);
   
   fr=find(bord{fi}==max(bord{fi}));
   
   switch fi
       case 1
           fil=1;
           fic=fr;
       case 2
           fil=size(s,1);
           fic=fr;
       case 3
           fil=fr;
           fic=1;
       case 4
           fil=fr;
           fic=size(s,2);
   end
   
   
   dl=1:szl:nl;
   dc=1:szc:nc;
   
   zoom= logical(zeros(size(mask)));
   
   zoom(dl(fil):min((dl(fil)+szl-1),size(zoom,1)),dc(fic):min((dc(fic)+szc-1),size(zoom,2)))=1;
   
   zoombord=zoom&mask;
   
    % zoom in the centre of the mask
    pl=round(nl/2);
   pc=round(nc/2);
    
   zoomcentre= false(size(mask));
   zoomcentre(pl-round(szl/2):pl+round(szl/2),pc-round(szc/2):pc+round(szc/2))=mask(pl-round(szl/2):pl+round(szl/2),pc-round(szc/2):pc+round(szc/2));
   
   if valider
       cd(rfolderg)
       display(listg(i).name)
       img=imread(listg(i).name);
       
       cimg=class(img);
       
       if size(img,1)~=nl || size(img,2) ~=nc
           error('size of grey level image differ from that of mask');
       end
       
        [~,setOfZoom]=showResInLargeImagePzoom(img(mask),mask,{zoombord, zoomcentre},'');

       
%        vim=zeros(size(img));
%        
%        
       
      % vim(zoombord)=img(zoombord);
      
      vim=setOfZoom{1};
       
       switch cimg
           case 'uint8'
               vim=uint8(vim);
           case 'uint16'
               vim=uint16(vim);
       end

cd(rfolder)
       
       cd ('voir')
       
       imwrite(vim,strrep(listg(i).name,'.tif','zoomEdge.tif'));
       
       %vim(zoombord)=0;
       
       %
       
       %vim(zoomcentre)=img(zoomcentre);
       vim=setOfZoom{2};
       switch cimg
           case 'uint8'
               vim=uint8(vim);
           case 'uint16'
               vim=uint16(vim);
       end
       imwrite(vim,strrep(listg(i).name,'.tif','zoomCentre.tif'));
       
   end
   
   %% save 
   cd(rfolder)
   imwrite(zoombord,strrep(list(i).name,'.tif','.zoomEdge.tif'));
   imwrite(zoomcentre,strrep(list(i).name,'.tif','.zoomCentre.tif'));

end;



%% matlab function tracking  
name='createZoomRoi';
cd(rfolder)
fid=fopen(strcat(name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Create zoom roi image from rois \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nRoi image folder: %s\r\n',rfolder);


fprintf(fid,'\r\n\r\n');
fprintf(fid,'\r\nRoi zoom image correspond roughly to 1/%d of the initial roi\r\n',4*4);
fprintf(fid,'\r\nTwo rois are created\r\n');
fprintf(fid,'\t- near the edge of the image\r\n');
fprintf(fid,'\t-in the middle of the image\r\n');
fprintf(fid,'\r\n\r\n');

if valider
    fprintf(fid,'Folder oflevel images associated to rois: %s\r\n',rfolderg);
    fprintf(fid,'regions slected by zoom rois are shown in folder %s\voir\r\n',rfolder);
end;

fprintf(fid,'\r\n\r\n');

fprintf(fid,'zoom roi image are saved in the same folder than original roi\r\n');
fprintf(fid,'Edge rois: <roiName>.zoomEdge.tif\r\n');
fprintf(fid,'Middle rois: <roiName>.zoomCentre.tif\r\n');

fprintf(fid,'\r\n\r\n');

fprintf(fid,'\r\nProcessed images: \r\n');
for i=1:nb
    fprintf(fid,'\t%s\r\n',list(i).name);
end

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    