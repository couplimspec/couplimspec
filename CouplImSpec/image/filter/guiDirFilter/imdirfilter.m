function res = imdirfilter(img, varargin)
%IMDIRFILTER apply several directional filters, and compute max of them
%
%   Apply a directional filter, with linear structural element, and 
%   compute min or max of results. Result is the same type as input
%   image 'img'.
%
%   Classical uses of such filters is max of opening, which performs
%   good enhancement of lines.
%
%   For the use of median or mean filters, we also provides immedian and
%   immean functions, which have syntax similar to imopen or imclose.
%
%
%   examples :
%
%   RES = imdirfilter(SRC, 'IMMEAN', 'MAX', 20, 8);
%   compute the mean in each 8 directions, with linear tructuring element
%   of length 2*20, and keep the max over all directions.
%   
%   RES = imdirfilter(SRC, 'IMMEAN', 20, 8); also work, 'max' is
%   computed as default for second operator.
%
%   RES = imdirfilter(SRC, 20, 8); also work, the
%   default operator for first transform is 'imopen'.
%
%   RES = imdirfilter(SRC, 'IMMEAN', 'MAX', 20, 8, 3);
%   also specifies width of the line (obtained by dilatation).
%
%
%   ---------
%
%   author : David Legland 
%   INRA - TPV URPOI - BIA IMASTE
%   created the 16/02/2004.
%

%   HISTORY
%   17/02/2004: debug, added some doc, manage different types of images
%   26/02/2007: cleanup code


%% default values

% opertors
op1 = 'imopen';
op2 = 'max';

% parameters for structuring element
Nd = 32;
N = 65;
w = 1;


%% Process input parameters

if length(varargin)>0
    op1 = varargin{1};
end
if length(varargin)>1
    var = varargin{2};
    
    if ischar(var)
        % third argument is name of second operator
        op2 = var;
        
        % fourth argument is length of the line
        if length(varargin)>2
            N = varargin{3};
        end
        % fifth argument is number of directions
        if length(varargin)>3
            Nd = varargin{4};
        end
        % sixth arguement is width of the line
        if length(varargin)>4
            w = varargin{5};
        end
    else
        % third argument is length of the line
        N = varargin{1};
        
        % fourth argument is number of directions
        if length(varargin)>3
            Nd = varargin{4};
        end
        % fifth arguement is width of the line
        if length(varargin)>4
            w = varargin{5};
        end
    end
end


%% Initialisations

% memory allocation, creating result the same type as input
if strcmp(op2, 'max')
    res = eval(sprintf('%s(zeros(size(img)))', class(img)));
elseif strcmp(op2, 'min')
    res = eval(sprintf('%s(ones(size(img)))', class(img)));
else
    error('don''t know how to manage "%s" operator', op2);
end


%% Iteration on directions

% iterate on each directions
for d=1:Nd
    % compute structuring element base is a  line, eventually dilated by a
    % ball
    filt = getnhood(strel('line', N,  (d-1)*180/Nd));
    if w>1
        filt = imdilate(filt, ones(3*ones(1, length(size(img)))), 'full');
    end
    
    % keep max or min along all directions
    res = feval(op2, res, feval(op1, img, filt));
end

