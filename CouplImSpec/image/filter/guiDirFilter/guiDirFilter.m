function varargout = guiDirFilter(varargin)
% GUIDIRFILTER Interactive directional filtering for single image or image series
%
%   Usage: 
%   guiDirFilter;
%   Opens a dialog with many widgets.
%   - First panel allows to select an image, and perform a directional
%       filtering on this image.
%   - Second panel can select a series of images in a given directory, and
%       perform the directional filtering on each image. Result is saved in
%       a file depending on a 'pattern', the name of the original image,
%       and eventually the index of the image in the list (See below).
%   - Third panel can select options for directional filtering:
%       o type of element (can be only 'line')
%       o length of the line segment
%       o dilation of the line segment
%       o number of discrete orientations (2: only orthogonal directions,
%           4: orthogonal and diagonals...)
%       o operation applied to each direction
%       o operation applied to the image stack, resulting in a planar image
%
%   Output pattern transforms the name of the input file into another name.
%   This is not necessarily useful if output directory is different.
%	'$N' will be replaced by the name of input file, without extension
%	'$E' will be replaced by the extension of the input file
%	'##' will be replaced by the number of the file in the list of inputs
%
%	Examples:
%	If image names are 'img00.tif', 'img01.tif'...
%	pattern '$Nfilt.$E' will save into 'img00filt.tif', 'img01filt.tif'...
%   
%	If image names are 'img1.tif', 'image.png', 'imTmp.tif'...
%	pattern 'filt##.$E' will save into 'filt00.tif', 'filt01.png'...
%


% Last Modified by GUIDE v2.5 04-May-2007 16:51:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @guiDirFilter_OpeningFcn, ...
                   'gui_OutputFcn',  @guiDirFilter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% 
%% Creation and initialisation functions


% --- Executes just before guiDirFilter is made visible.
function guiDirFilter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to guiDirFilter (see VARARGIN)


% Initialize internal data for constants
data.op1Funs    = {'imopen', 'imclose', 'imdilate', 'imerode', 'immean', 'immedian'}; 
data.OP1OPEN    = 1;
data.OP1CLOSE   = 2;
data.OP1DILATE  = 3;
data.OP1ERODE   = 4;
data.OP1MEAN    = 5;
data.OP1MEDIAN  = 6;
data.op2Funs    = {'max', 'min'}; 
data.OP2MAX     = 1;
data.OP2MIN     = 2;

% Initialize internal data for single image
data.fileName   = 'none';
data.pathName   = '.';
data.hImage     = -1;
data.hFilter    = -1;
data.image      = [];
data.filtered   = [];

% data to load images
data.imageFormats   = {...
        '*.gif;*.jpg;*.jpeg;*.tif;*.tiff;*.bmp;*.png', ...
            'All Image Files (*.bmp, *.jpg, *.tif, *.png)'; ...
        '*.tif;*.tiff', 'Tagged Image File Format (*.tif, *.tiff)'; ...
        '*.png',        'Portable Network Graphics (*.png)'; ...
        '*.bmp',        'Windows Bitmap (*.bmp)'; ...
        '*.*',          'All Files (*.*)'};
    
% Initialize internal data for image collection
data.elemNames  = {'line'};
data.elemType   = 1;
data.elemLength = 30;
data.elemWidth  = 1;
data.nDir       = 16;
data.op1        = data.OP1MEDIAN;
data.op2        = data.OP2MAX;

% Initialize internal data for filter
data.inputDir       = '.';
data.inputPattern   = '*.tif';
data.outputDir      = '.';
data.outputPattern  = '$Nf.$E';

% Initialize internal data for internal behaviour
data.verbose        = true;
data.pwd            = pwd;

% setup gui elements to correspond to init values

set(handles.inputImageEdit, 'String', data.fileName);

set(handles.inputSeriesEdit, 'String', data.inputDir);
set(handles.inputPatternEdit, 'String', data.inputPattern);
set(handles.outputSeriesEdit, 'String', data.outputDir);
set(handles.outputPatternEdit, 'String', data.outputPattern);

set(handles.size1Edit, 'String', data.elemLength);
set(handles.size2Edit, 'String', data.elemWidth);
set(handles.nDirEdit, 'String', data.nDir);
set(handles.filter1Popup, 'Value', data.op1);
set(handles.filter2Popup, 'Value', data.op2);



handles.data    = data;

% check params
%displayParams(hObject, eventdata, handles);

% Choose default command line output for guiDirFilter
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes guiDirFilter wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%% 
%% Functions to modify internal state of GUI

function sayHello(hObject, eventdata, handles, varargin)
% a small test function
disp('Hello !');


function displayParams(hObject, eventdata, handles, varargin)
% a small test function

data = handles.data;
disp(sprintf('Element type:  %4s', data.elemNames{data.elemType}));
disp(sprintf('Element Length: %3d', data.elemLength));
disp(sprintf('Element Width:  %3d', data.elemWidth));
disp(sprintf('Directions:     %3d', data.nDir));
strings = get(handles.filter1Popup, 'string');
disp(sprintf('Filter 1:     %3s', strings{data.op1}));
strings = get(handles.filter2Popup, 'string');
disp(sprintf('Filter 2:     %3s', strings{data.op2}));
disp(' ');
disp(sprintf('input dir:        %-10s', data.inputDir));
disp(sprintf('input pattern:    %-10s', data.inputPattern));
disp(sprintf('output dir:       %-10s', data.outputDir));
disp(sprintf('output pattern:   %-10s', data.outputPattern));



%% 
%% Functions to respond to GUI Events

% --- Outputs from this function are returned to the command line.
function varargout = guiDirFilter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% 
%% Functions to respond to GUI concerning single image

% --- Executes on button press in inputImageChangeButton.
function inputImageChangeButton_Callback(hObject, eventdata, handles)
% hObject    handle to inputImageChangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


data = handles.data;

% Choose a new name for image
cd(data.pathName);
[data.fileName, data.pathName] = uigetfile(data.imageFormats, ...
    'Choose an image to filter:');
cd(data.pwd);

% if no valid name is specified, returns
if ~exist(fullfile(data.pathName, data.fileName), 'file')
    return;
end

% update GUI items
fullName = fullfile(data.pathName, data.fileName);
set(handles.inputImageEdit, 'String', fullName);

% load the image
data.image = imread(fullName);


% update GUI Data
handles.data    = data;

% Choose default command line output for guiDirFilter
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



function inputImageEdit_Callback(hObject, eventdata, handles)
% hObject    handle to inputImageEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputImageEdit as text
%        str2double(get(hObject,'String')) returns contents of inputImageEdit as a double

data = handles.data;

fullName = set(handles.inputImageEdit, 'String');
[data.pathName, data.fileName, ext, versn] = fileparts(fullName);

% ensure we have a valid name for image
if ~exist(fullName, 'file')
    disp('Not a valid file name');
    return;
end

% load the image
data.image = imread(fullName);


% update GUI Data
handles.data    = data;
handles.output  = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function inputImageEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputImageEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in imageShowButton.
function imageShowButton_Callback(hObject, eventdata, handles)
% hObject    handle to imageShowButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = handles.data;

% ensure we have a valid name for image
if ~exist(fullfile(data.pathName, data.fileName), 'file')
    [data.fileName, data.pathName] = uigetfile(data.imageFormats, ...
        'Choose an image to filter:');
end

% update GUI items
fullName = fullfile(data.pathName, data.fileName);
set(handles.inputImageEdit, 'String', fullName);


if ~exist(fullfile(data.pathName, data.fileName), 'file')
    handles.data    = data;
    handles.output = hObject;
    guidata(hObject, handles);
    return;
end

% load the image
data.image = imread(fullName);

% if image figure is not created, create it
if data.hImage==-1
    data.hImage = figure;
else
    figure(data.hImage);
end

% show the image
imshow(data.image);
set(data.hImage, 'name', 'Original');



% update GUI Data
handles.data    = data;
handles.output  = hObject;
guidata(hObject, handles);




% --- Executes on button press in imageFilterButton.
function imageFilterButton_Callback(hObject, eventdata, handles)
% hObject    handle to imageFilterButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp('filter an image')

displayParams(hObject, eventdata, handles);

data = handles.data;

if isempty(data.image)
    disp('Need to load an image first');
    return;
end

% extract paramaters of filter
length  = data.elemLength;
width   = data.elemWidth;
nDir    = data.nDir;
op1     = data.op1Funs{data.op1};
op2     = data.op2Funs{data.op2};

% filter the image
data.filtered = imdirfilter(data.image, op1, op2, length, nDir, width);


% if image figure is not created, create it
if data.hFilter==-1
    data.hFilter = figure;
else
    figure(data.hFilter);
end

% show the image
imshow(data.filtered);
set(data.hFilter, 'name', 'Filtered');

% update GUI Data
handles.data    = data;
handles.output  = hObject;
guidata(hObject, handles);


% --- Executes on button press in saveFilterButton.
function saveFilterButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveFilterButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp('save filtered image');

data = handles.data;
if isempty(data.filtered)
    return;
end

% select file to save in
[filterFile, filterPath] = uiputfile(...
    data.imageFormats, 'Save filtered image', 'filtered.tif');
if filterFile==0
    return;
end

% save filtered image
imwrite(data.filtered, fullfile(filterPath, filterFile));



%% 
%% Functions to respond to GUI Events concerning series

% --- Executes on button press in inputSeriesChangeButton.
function inputSeriesChangeButton_Callback(hObject, eventdata, handles)
% hObject    handle to inputSeriesChangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%disp('change input series directory');


data = handles.data;

% Choose a new name for image
cd(data.inputDir);
[inputFile inputDir] = uigetfile(data.imageFormats, ...
    'Choose directory for input collection:');
cd(data.pwd);

% if no valid name is specified, returns
if inputDir==0
    return;
end

data.inputDir = inputDir;

% update GUI items
set(handles.inputSeriesEdit, 'String', data.inputDir);


% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



function inputSeriesEdit_Callback(hObject, eventdata, handles)
% hObject    handle to inputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputSeriesEdit as text
%        str2double(get(hObject,'String')) returns contents of inputSeriesEdit as a double


% --- Executes during object creation, after setting all properties.
function inputSeriesEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function outputSeriesEdit_Callback(hObject, eventdata, handles)
% hObject    handle to outputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outputSeriesEdit as text
%        str2double(get(hObject,'String')) returns contents of outputSeriesEdit as a double


% --- Executes during object creation, after setting all properties.
function outputSeriesEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in outputSeriesChangeButton.
function outputSeriesChangeButton_Callback(hObject, eventdata, handles)
% hObject    handle to outputSeriesChangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



data = handles.data;

% Choose a new name for image
cd(data.outputDir);
outputDir = uigetdir(data.outputDir, ...
    'Choose directory for input collection:');
cd(data.pwd);

% if no valid name is specified, returns
if outputDir==0
    return;
end

data.outputDir = outputDir;

% update GUI items
set(handles.outputSeriesEdit, 'String', data.outputDir);


% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



function inputPatternEdit_Callback(hObject, eventdata, handles)
% hObject    handle to inputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputPatternEdit as text
%        str2double(get(hObject,'String')) returns contents of inputPatternEdit as a double


data = handles.data;

data.outputPattern = get(handles.outputPatternEdit, 'String');


% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function inputPatternEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function outputPatternEdit_Callback(hObject, eventdata, handles)
% hObject    handle to outputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outputPatternEdit as text
%        str2double(get(hObject,'String')) returns contents of outputPatternEdit as a double

data = handles.data;

data.inputPattern = get(handles.inputPatternEdit, 'String');


% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function outputPatternEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in filterSeriesButton.
function filterSeriesButton_Callback(hObject, eventdata, handles)
% hObject    handle to filterSeriesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp('filter a series of images')

data = handles.data;
data.inputPattern = get(handles.inputPatternEdit, 'String');
data.outputPattern = get(handles.outputPatternEdit, 'String');

displayParams(hObject, eventdata, handles);

fileList = dir(fullfile(data.inputDir, data.inputPattern));
if isempty(fileList)
    return;
end

% get parameters of filter
length  = data.elemLength;
width   = data.elemWidth;
nDir    = data.nDir;
op1     = data.op1Funs{data.op1};
op2     = data.op2Funs{data.op2};


for i=1:max(size(fileList))
    % build name of input image
    inputName = fullfile(data.inputDir, fileList(i).name);
    
    % build name of output image
    outputName = fullfile(data.outputDir, ...
        createName(data.outputPattern, inputName, i-1));

    % display what the software is doing
    disp(sprintf('Converting: [%s] to: [%s]', inputName, outputName));
    
    % read and filter image
    img = imread(inputName);
    res = imdirfilter(img, op1, op2, length, nDir, width);
    
    % save result
    imwrite(res, outputName);
end


function pattern = createName(pattern, name, i)
% create a file name from a pattern, a filename, and a number

% separates base name and extension
[path base ext] = fileparts(name);

% removes the dot
ext = ext(2:end);

% replace string with parts of file name
pattern = strrep(pattern, '$F', name);
pattern = strrep(pattern, '$N', base);
pattern = strrep(pattern, '$E', ext);

% replace strings with numbers
pattern = strrep(pattern, '###', sprintf('%03d', i));
pattern = strrep(pattern, '##', sprintf('%02d', i));
pattern = strrep(pattern, '#', sprintf('%01d', i));





%% 
%% Function to respond to GUI Events concerning filter options

% --- Executes on selection change in filter1Popup.
function filter1Popup_Callback(hObject, eventdata, handles)
% hObject    handle to filter1Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns filter1Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filter1Popup

data = handles.data;

data.op1 = get(handles.filter1Popup, 'Value');

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function filter1Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filter1Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in filter2Popup.
function filter2Popup_Callback(hObject, eventdata, handles)
% hObject    handle to filter2Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns filter2Popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filter2Popup

data = handles.data;

data.op2 = get(handles.filter2Popup, 'Value');

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function filter2Popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filter2Popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nDirEdit_Callback(hObject, eventdata, handles)
% hObject    handle to nDirEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nDirEdit as text
%        str2double(get(hObject,'String')) returns contents of nDirEdit as a double

data = handles.data;

value = abs(round(str2double(get(handles.nDirEdit, 'String'))));
if value<1 
    return; 
end

set(handles.nDirEdit, 'String', num2str(value));

data.nDir = value;

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function nDirEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nDirEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function size1Edit_Callback(hObject, eventdata, handles)
% hObject    handle to size1Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of size1Edit as text
%        str2double(get(hObject,'String')) returns contents of size1Edit as a double

data = handles.data;

value = abs(round(str2double(get(handles.size1Edit, 'String'))));
if value<1 
    return; 
end

set(handles.size1Edit, 'String', num2str(value));

data.elemLength = value;

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function size1Edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to size1Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function size2Edit_Callback(hObject, eventdata, handles)
% hObject    handle to size2Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of size2Edit as text
%        str2double(get(hObject,'String')) returns contents of size2Edit as a double

data = handles.data;

value = abs(round(str2double(get(handles.size2Edit, 'String'))));
if value<1 
    return; 
end

set(handles.size2Edit, 'String', num2str(value));

data.elemWidth = value;

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function size2Edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to size2Edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in elementTypePopup.
function elementTypePopup_Callback(hObject, eventdata, handles)
% hObject    handle to elementTypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns elementTypePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from elementTypePopup


data = handles.data;

data.elemType = get(handles.elementTypePopup, 'Value');

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function elementTypePopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elementTypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


