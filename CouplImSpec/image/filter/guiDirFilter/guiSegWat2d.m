function varargout = guiSegWat2d(varargin)
%GUISEGWAT2D M-file for guiSegWat2d.fig
%      GUISEGWAT2D, by itself, creates a new GUISEGWAT2D or raises the existing
%      singleton*.
%
%      H = GUISEGWAT2D returns the handle to a new GUISEGWAT2D or the handle to
%      the existing singleton*.
%
%      GUISEGWAT2D('Property','Value',...) creates a new GUISEGWAT2D using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to guiSegWat2d_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      GUISEGWAT2D('CALLBACK') and GUISEGWAT2D('CALLBACK',hObject,...) call the
%      local function named CALLBACK in GUISEGWAT2D.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help guiSegWat2d

% Last Modified by GUIDE v2.5 19-Jun-2007 14:35:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @guiSegWat2d_OpeningFcn, ...
                   'gui_OutputFcn',  @guiSegWat2d_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% Initialisation

% --- Executes just before guiSegWat2d is made visible.
function guiSegWat2d_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)


% data to load images
data.imageFormats   = {...
        '*.gif;*.jpg;*.jpeg;*.tif;*.tiff;*.bmp;*.png', ...
            'All Image Files (*.bmp, *.jpg, *.tif, *.png)'; ...
        '*.tif;*.tiff', 'Tagged Image File Format (*.tif, *.tiff)'; ...
        '*.png',        'Portable Network Graphics (*.png)'; ...
        '*.bmp',        'Windows Bitmap (*.bmp)'; ...
        '*.*',          'All Files (*.*)'};
  
% Initialize internal data for single image
data.fileName   = 'none';
data.pathName   = '.';
data.hImage     = -1;
data.hSegment   = -1;
data.image      = [];
data.segmented  = [];


% Initialize internal data for filter
data.filterNames    = {'none', 'flat', 'gaussian'};
data.filterType     = 1;
data.filterSize     = 3;
data.filterWidth    = 1;

data.connectivity   = 4;

data.minimaType     = 1;
data.minimaValue    = 20;


% Initialize internal data for image collection
data.inputDir       = '.';
data.inputPattern   = '*.tif';
data.outputDir      = '.';
data.outputPattern  = '$Nf.$E';

% Initialize internal data for internal behaviour
data.verbose        = true;
data.pwd            = pwd;

handles.data = data;

% Choose default command line output for guiSegWat2d
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes guiSegWat2d wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = guiSegWat2d_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%% Process single image

% --- Executes on button press in inputImageChangeButton.
function inputImageChangeButton_Callback(hObject, eventdata, handles)
% hObject    handle to inputImageChangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


data = handles.data;

% Choose a new name for image
cd(data.pathName);
[data.fileName, data.pathName] = uigetfile(data.imageFormats, ...
    'Choose an image to filter:');
cd(data.pwd);

% if no valid name is specified, returns
if ~exist(fullfile(data.pathName, data.fileName), 'file')
    return;
end

% update GUI items
fullName = fullfile(data.pathName, data.fileName);
set(handles.inputImageEdit, 'String', fullName);

% load the image
data.image = imread(fullName);


% update GUI Data
handles.data    = data;

% Choose default command line output for guiDirFilter
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


function inputImageEdit_Callback(hObject, eventdata, handles)
% hObject    handle to inputImageEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputImageEdit as text
%        str2double(get(hObject,'String')) returns contents of inputImageEdit as a double


data = handles.data;

fullName = set(handles.inputImageEdit, 'String');
[data.pathName, data.fileName, ext, versn] = fileparts(fullName);

% ensure we have a valid name for image
if ~exist(fullName, 'file')
    disp('Not a valid file name');
    return;
end

% load the image
data.image = imread(fullName);


% update GUI Data
handles.data    = data;
handles.output  = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function inputImageEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputImageEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in imageShowButton.
function imageShowButton_Callback(hObject, eventdata, handles)
% hObject    handle to imageShowButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


data = handles.data;

% ensure we have a valid name for image
if ~exist(fullfile(data.pathName, data.fileName), 'file')
    [data.fileName, data.pathName] = uigetfile(data.imageFormats, ...
        'Choose an image to filter:');
end

% update GUI items
fullName = fullfile(data.pathName, data.fileName);
set(handles.inputImageEdit, 'String', fullName);


if ~exist(fullfile(data.pathName, data.fileName), 'file')
    handles.data    = data;
    handles.output = hObject;
    guidata(hObject, handles);
    return;
end

% load the image
data.image = imread(fullName);

% if image figure is not created, create it
if data.hImage==-1
    data.hImage = figure;
else
    figure(data.hImage);
end

% show the image
imshow(data.image);
set(data.hImage, 'name', 'Original');



% update GUI Data
handles.data    = data;
handles.output  = hObject;
guidata(hObject, handles);



% --- Executes on button press in imageSegmentButton.
function imageSegmentButton_Callback(hObject, eventdata, handles)
% hObject    handle to imageSegmentButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp('segment image   -----------------------------');
displayParams(hObject, eventdata, handles);

data = handles.data;

% first filter image
switch data.filterType
    case 1, filtered = data.image;
    case 2, 
        s   = data.filterSize;
        h   = ones(s, s)/s^2;
        filtered    = imfilter(data.image, h);
    case 3,
        s   = data.filterSize;
        w   = data.filterWidth;
        h   = fspecial('gaussian', s, w);
        filtered    = imfilter(data.image, h);
    otherwise,
        return;
end

% extract connectivity
conn = data.connectivity;

% then detect minima
switch data.minimaType
    case 1,
        value   = data.minimaValue;
        emin    = filtered<value;
    case 2,
        value   = data.minimaValue;
        emin    = imextendedmin(filtered, value, conn);
    otherwise,
        return;
end

imp = imimposemin(filtered, emin, conn);
wat = watershed(imp, conn);

data.segmented = wat>0;


% if image figure is not created, create it
if data.hSegment==-1
    data.hSegment = figure;
else
    figure(data.hSegment);
end

% show the image
imshow(data.segmented);
set(data.hSegment, 'name', 'Segmented');


% update GUI Data
handles.data    = data;
handles.output  = hObject;
guidata(hObject, handles);


% --- Executes on button press in saveSegmentedButton.
function saveSegmentedButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveSegmentedButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


disp('save segmented image');

data = handles.data;
if isempty(data.segmented)
    return;
end

% select file to save in
[filterFile, filterPath] = uiputfile(...
    data.imageFormats, 'Save segmented image', 'segmented.tif');
if filterFile==0
    return;
end

% save filtered image
imwrite(data.segmented, fullfile(filterPath, filterFile));



% --- Called by other functions 
function displayParams(hObject, eventdata, handles)

data = handles.data;

disp(sprintf('filter type: %s', data.filterNames{data.filterType}));
disp(sprintf('filter size: %d', data.filterSize));
disp(sprintf('filter width: %d', data.filterWidth));

disp(sprintf('connectivity: %d', data.connectivity));

disp(sprintf('minima type: %d', data.minimaType));
disp(sprintf('minima value: %d', data.minimaValue));




%% Process images series

function outputSeriesEdit_Callback(hObject, eventdata, handles)
% hObject    handle to outputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outputSeriesEdit as text
%        str2double(get(hObject,'String')) returns contents of outputSeriesEdit as a double


% --- Executes during object creation, after setting all properties.
function outputSeriesEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in outputSeriesChangeButton.
function outputSeriesChangeButton_Callback(hObject, eventdata, handles)
% hObject    handle to outputSeriesChangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


data = handles.data;

% Choose a new name for image
cd(data.outputDir);
outputDir = uigetdir(data.outputDir, ...
    'Choose directory for input collection:');
cd(data.pwd);

% if no valid name is specified, returns
if outputDir==0
    return;
end

data.outputDir = outputDir;

% update GUI items
set(handles.outputSeriesEdit, 'String', data.outputDir);


% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



function outputPatternEdit_Callback(hObject, eventdata, handles)
% hObject    handle to outputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outputPatternEdit as text
%        str2double(get(hObject,'String')) returns contents of outputPatternEdit as a double

data = handles.data;

data.outputPattern = get(handles.outputPatternEdit, 'String');

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function outputPatternEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in inputSeriesChangeButton.
function inputSeriesChangeButton_Callback(hObject, eventdata, handles)
% hObject    handle to inputSeriesChangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


data = handles.data;

% Choose a new name for image
cd(data.inputDir);
[inputFile inputDir] = uigetfile(data.imageFormats, ...
    'Choose directory for input collection:');
cd(data.pwd);

% if no valid name is specified, returns
if inputDir==0
    return;
end

data.inputDir = inputDir;

% update GUI items
set(handles.inputSeriesEdit, 'String', data.inputDir);


% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



function inputSeriesEdit_Callback(hObject, eventdata, handles)
% hObject    handle to inputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputSeriesEdit as text
%        str2double(get(hObject,'String')) returns contents of inputSeriesEdit as a double


% --- Executes during object creation, after setting all properties.
function inputSeriesEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputSeriesEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function inputPatternEdit_Callback(hObject, eventdata, handles)
% hObject    handle to inputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputPatternEdit as text
%        str2double(get(hObject,'String')) returns contents of inputPatternEdit as a double



data = handles.data;

data.outputPattern = get(handles.outputPatternEdit, 'String');


% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function inputPatternEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in segmentSeriesButton.
function segmentSeriesButton_Callback(hObject, eventdata, handles)
% hObject    handle to segmentSeriesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


disp('filter a series of images')
displayParams(hObject, eventdata, handles);

data = handles.data;
data.inputPattern = get(handles.inputPatternEdit, 'String');
data.outputPattern = get(handles.outputPatternEdit, 'String');

displayParams(hObject, eventdata, handles);

fileList = dir(fullfile(data.inputDir, data.inputPattern));
if isempty(fileList)
    return;
end

% get parameters of filter
s       = data.filterSize;
w       = data.filterWidth;
conn    = data.connectivity;
value   = data.minimaValue;



for i=1:max(size(fileList))
    % build name of input image
    inputName = fullfile(data.inputDir, fileList(i).name);
    
    % build name of output image
    outputName = fullfile(data.outputDir, ...
        createName(data.outputPattern, inputName, i-1));

    % display what the software is doing
    disp(sprintf('Segment: [%s] to: [%s]', inputName, outputName));
    
    % read and filter image
    img = imread(inputName);
    
    % first filter image
    switch data.filterType
        case 1, filtered = img;
        case 2, filtered = imfilter(img, ones(s, s)/s^2);
        case 3, filtered = imfilter(img, fspecial('gaussian', s, w));
        otherwise,  return;
    end

    % then detect minima
    switch data.minimaType
        case 1,	emin    = filtered<value;
        case 2, emin    = imextendedmin(filtered, value, conn);
        otherwise, return;
    end

    % impose minima, and segment
    imp = imimposemin(filtered, emin, conn);
    wat = watershed(imp, conn);
    res = wat>0;    
    
    % save result
    imwrite(res, outputName);
end




function pattern = createName(pattern, name, i)
% create a file name from a pattern, a filename, and a number

% separates base name and extension
[path base ext] = fileparts(name);

% removes the dot
ext = ext(2:end);

% replace string with parts of file name
pattern = strrep(pattern, '$F', name);
pattern = strrep(pattern, '$N', base);
pattern = strrep(pattern, '$E', ext);

% replace strings with numbers
pattern = strrep(pattern, '###', sprintf('%03d', i));
pattern = strrep(pattern, '##', sprintf('%02d', i));
pattern = strrep(pattern, '#', sprintf('%01d', i));




%% manage fitler options

% --- Executes on selection change in filterTypePopup.
function filterTypePopup_Callback(hObject, eventdata, handles)
% hObject    handle to filterTypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns filterTypePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filterTypePopup


data = handles.data;

data.filterType = get(handles.filterTypePopup, 'Value');

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function filterTypePopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filterTypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function filterSizeEdit_Callback(hObject, eventdata, handles)
% hObject    handle to filterSizeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filterSizeEdit as text
%        str2double(get(hObject,'String')) returns contents of filterSizeEdit as a double

data = handles.data;

value = abs(round(str2double(get(handles.filterSizeEdit, 'String'))));
if value<1 
    return; 
end

set(handles.filterSizeEdit, 'String', num2str(value));

data.filterSize = value;

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function filterSizeEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filterSizeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function filterWidthEdit_Callback(hObject, eventdata, handles)
% hObject    handle to filterWidthEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filterWidthEdit as text
%        str2double(get(hObject,'String')) returns contents of filterWidthEdit as a double


data = handles.data;

value = abs(round(str2double(get(handles.filterWidthEdit, 'String'))));
if value<1 
    return; 
end

set(handles.filterWidthEdit, 'String', num2str(value));

data.filterWidth = value;

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function filterWidthEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filterWidthEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%% Connexity


% --- Executes on selection change in connectivityPopup.
function connectivityPopup_Callback(hObject, eventdata, handles)
% hObject    handle to connectivityPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns connectivityPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from connectivityPopup


data = handles.data;

choice = get(handles.connectivityPopup, 'Value');
if choice==1
    data.connectivity = 4;
elseif choice==2
    data.connectivity = 8;
else
    return;
end

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function connectivityPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to connectivityPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%% Minima detection management




% --- Executes on selection change in minimaTypePopup.
function minimaTypePopup_Callback(hObject, eventdata, handles)
% hObject    handle to minimaTypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns minimaTypePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from minimaTypePopup


data = handles.data;

data.minimaType = get(handles.minimaTypePopup, 'Value');

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function minimaTypePopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minimaTypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function minimaValueEdit_Callback(hObject, eventdata, handles)
% hObject    handle to minimaValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minimaValueEdit as text
%        str2double(get(hObject,'String')) returns contents of minimaValueEdit as a double


data = handles.data;

value = abs(round(str2double(get(handles.minimaValueEdit, 'String'))));
if value<1 
    return; 
end

set(handles.minimaValueEdit, 'String', num2str(value));

data.minimaValue = value;

% update GUI Data
handles.data    = data;
handles.output = hObject;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function minimaValueEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minimaValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
