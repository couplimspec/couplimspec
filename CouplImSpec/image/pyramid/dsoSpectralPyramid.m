function dsoSpectralPyramid
%% description
    % compute a serie of pyramid images from a dso spectral image
    
%% input
    %  nothing interactive function
    
%% output
    
    %   im : image monochrome de meem type que l'image d'entree

%% principe
    % les images sont converties en monochrome selon 3 possibilites
        % choix d'un des trois canaux RGB
        % utilisation de la fonction rgb2gray de matlab : rgb2gray converts RGB values to grayscale values by forming a weighted sum of the R, G, and B components:

                    % 0.2989 * R + 0.5870 * G + 0.1140 * B 

        % calcul de l'image en niveaux de gris a partir de la formule
                    % 0.3333 * R + 0.3333 * G + 0.3333 * B 


%% use
    % [dso]= readspc('filename.spc'); return dataset "dso" of spc map.
    % dso= readomnic;
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % 
    
    
%% Author
    % 
    
%% date
    % 

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: [dso_read]= readspc(filename)');
end


%% input

fprintf('read dso....')
[dso,rfolder]=loaddso;
cd(rfolder);
[~,rep]=fileparts(fileparts(rfolder));
fprintf('read mask....\r\n')

%cd (strcat(rfolder,rep,'.roi'))
cd ('dso.roi')
%cd(rfolder)         % to remove
%cd('roi')               % to remove

mask=imread(strcat(dso.name,'.roi.tif'));
listzoom=dir(strcat(dso.name,'.roi.zoom*.tif'));
zoom={mask};

for i=1:length(listzoom)
    zoom{i}=imread(listzoom(i).name);
end;

dso.data(~reshape(mask,size(mask,1)*size(mask,2),1),:)=0;
cd(rfolder);

f=fspecial('average',4);
sim=dso.imagedata;

fprintf('pyramid level: 1...');

simp{1,1}=sim;
simp{1,2}=mask;
simp{1,3}=zoom;

 

 
for j=1:8
    fprintf('%d...', j+1);

    pyr=zeros(ceil(size(simp{j,1},1)/2),ceil(size(simp{j,1},2)/2),size(dso,2))    ;
    for i=1:size(dso,2)
        tmp=imfilter(sim(:,:,i),f,'symmetric');
        pyr(:,:,i)=imresize(tmp,1/2,'nearest');
    end
    
    clear tmp
    
    tmpz=imfilter(mask,f,'symmetric');
    tmpz=imresize(tmpz,1/2,'nearest');
    mask=tmpz;
        
    for i=1:length(zoom)
         [ii,jj]=find(zoom{i});
         tmpz=logical(zeros(size(tmpz)));
         tmpz(round(min(ii)/2):round(max(ii)/2),round(min(jj)/2):round(max(jj)/2))=1;
         tmpz(tmpz)=mask(tmpz);
         zoom{i}=tmpz;
    end
    
    clear tmpz
    
    simp{j+1,1}=pyr;
    simp{j+1,2}=mask;
    simp{j+1,3}=zoom;
    
    sim=pyr;
    
    
    tmp.pyr=pyr;
    tmp.mask=mask;
    tmp.zoom=zoom;
    
    clear pyr
    
    save(strcat(dso.name,'.',num2str(j+1),'.mat'), 'tmp')
    clear tmp;
end;

fprintf('\r\n');
