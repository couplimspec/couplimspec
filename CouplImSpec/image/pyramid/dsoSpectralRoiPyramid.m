function dsoSpectralRoiPyramid(dsoFilename,sfolder)
%% description
% compute a serie of pyramid images and their roi from a dso spectral image

%% input
%  dsoFilename : name of dso spectral image
%  sfolder : folder to save pyramid structures
% nothing = interactive function

%% output

%   save on disk a serie of matlab files that contains the pyramid
%   image and the roi with low resolution
%

%% principe
%
% compute the serie of pyramids and save them on disk as matlab
% structure
% take into account rois and related zoom roi
%
%
% Parameters:
%    Average pyramid
%    the number of levels depends on the initial image size
%    the final size of image pyramid should not ne lower than 50 x50

% resulting pyramid mat files are matlab structure\r\n');
%   Pyr.data contains the 3-way spectral image:   xspatial-yspatial-spectral
% if rois exist :
%       Pyr.mask contains the Region of Interest
%   if zoom rois exist
%       Pyr.zoom contains the cell list of zoom to dispaly results

%% use
% dsoSpectralRoiPyramid

%% Comments
% written  for large image clustering
% following M corcel PhD


%% Author
% MF Devaux
% URBIA
% PVPP

%% date
% 29 juin 2017


%% context variables
orig=pwd;           % returns the current directory

% filter to assess low resolution pyramid
filtPyr=fspecial('average',4);


%% start

if nargin >3
    error('use: dsoSpectralRoiPyramid(dsofilename (optional), sfolder (optional)');
end


%% input
% load initial file
fprintf('read dso....')

if nargin==0
    [dso,rfolder]=loaddso;
    cd(rfolder);
    if ~exist('pyramid','dir')
        mkdir('pyramid');
        cd ('pyramid')
        sfolder=pwd;
    end;
else
    rfolder=orig;
    dso=loaddso(dsoFilename);
end


% read mask and zoom
if exist('dso.roi','dir')
    withroi=1;
    fprintf('read mask....\r\n')
    
    cd ('dso.roi')
    % read mask
    mask=imread(strcat(dso.name,'.roi.tif'));
    
    % check for zoom masks
    listzoom=dir(strcat(dso.name,'.roi.zoom*.tif'));
    
    if ~isempty(listzoom)
        zoom={mask};
        
        for i=1:length(listzoom)
            zoom{i}=imread(listzoom(i).name);
        end;
    end;
else
    ok=yesno('No folder dso.roi. do you agree ?');
    
    if ~ok
        error('Check for roi folder');
    else
        withroi=0;
    end
end

%% Pretreatment
% pre-process
if withroi
    % put extra values to 0 to assess large pixel values
    dso.data(~reshape(mask,size(mask,1)*size(mask,2),1),:)=0;
end;

% get spectral image as 3-way data table = x-y-spectral = refold image
sim=dso.imagedata;
sname=dso.name;

clear dso;


% pyramid structure
%% Treatment initial level
fprintf('pyramid level: 1...');

Pyr.data=sim;

% take into account rois if exist : full image + zoom
if withroi
    Pyr.mask=mask;
    if ~isempty(listzoom)
        Pyr.zoom=zoom;
    end;
end

%% save initial level with same logic as following levels
cd(sfolder);

save(strcat(sname,'.Pyr01.mat'), 'Pyr')


%% Treatment loop for all levels

% number of level of pyramid
nl=size(sim,1);
nc=size(sim,2);
ns=size(sim,3);

nblevel=round(min(log2(nl),log2(nc)))-4;

for j=1:nblevel
    fprintf('%d...', j+1);
    
    pyr=zeros(ceil(size(sim,1)/2),ceil(size(sim,2)/2),ns);
    
    for i=1:ns
        % filtering
        tmp=imfilter(sim(:,:,i),filtPyr,'symmetric');
        % resizing
        pyr(:,:,i)=imresize(tmp,1/2,'nearest');
    end
    
    clear tmp
    
    Pyr.data=pyr;
    
    
    if withroi
        % resize rois
        tmpz=imfilter(mask,filtPyr,'symmetric');    % filter
        tmpz=imresize(tmpz,1/2,'nearest');          % resize
        % new mask
        mask=tmpz>0;
        Pyr.mask=mask;
        
        % zooms
        if ~isempty(listzoom)
            for i=1:length(zoom)
                % init
                tmpz(tmpz)=0;
                % select pixels in zoom images
                [ii,jj]=find(zoom{i});
                % rough resize
                tmpz(round(min(ii)/2):round(max(ii)/2),round(min(jj)/2):round(max(jj)/2))=1;
                % take mask as reference to get final zoom roi
                tmpz(tmpz)=mask(tmpz);
                
                zoom{i}=tmpz;
            end
            Pyr.zoom=zoom;
        end
        clear tmpz
        
        
    end;
    
    sim=pyr;
    
    
    
    %% save pyramid levels
    if (j+1)<=9
        save(strcat(sname,'.Pyr0',num2str(j+1),'.mat'), 'Pyr');
    else
        save(strcat(sname,'.Pyr',num2str(j+1),'.mat'), 'Pyr');
    end
    
    clear Pyr;
end;

fprintf('\r\n');

%% matlab function tracking
fid=fopen(strcat(sname,'.pyramid.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Compute spectral pyramid of images taking into account roi if exist \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput dso file name: %s\r\n',sname);
fprintf(fid,'data folder: %s\r\n',rfolder);

if withroi
    fprintf(fid,'\r\nRois : \r\n');
    fprintf(fid,'roi folder: %s\\dso.roi\r\n',rfolder);
    fprintf(fid,'\t-main Roi: %s.roi.tif\r\n',dsoFilename);
    if ~isempty(listzoom)
        for i=1:length(zoom)
            fprintf(fid,'t-Zoom Roi: %s.roi.zoom%d.tif\r\n',dsoFilename,i);
        end
    end
end

fprintf(fid,'\r\n\r\n');
fprintf(fid,'Parameters: \r\n');
fprintf(fid,'\t- Average pyramid\r\n');
fprintf(fid,'\t- Number of levels: %d\r\n',nblevel);

fprintf(fid,'\r\n\r\n');

fprintf(fid,'save data folder: %s\r\n',sfolder);
fprintf(fid,'\r\nsaved file names : %s.Pyr<num>.mat \r\n',sname);
fprintf(fid,'\Pyramid mat files are matlab structure\r\n');
fprintf(fid,'\t- Pyr.data contains the 3-way spectral image: xspatial-yspatial-spectral\r\n');
if withroi
    fprintf(fid,'\t- Pyr.mask contains the Region of Interest \r\n');
    if ~isempty(listzoom)
        fprintf(fid,'\t- Pyr.zoom contains the cell list of zoom to dispaly results\r\n');
    end
end


% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end
cd(orig)
