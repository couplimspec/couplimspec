function simp=spectralPyramide

fprintf('read dso....')
[dso,rfolder]=loaddso;
cd(rfolder);
[~,rep]=fileparts(fileparts(rfolder));
fprintf('read mask....\r\n')

%cd (strcat(rfolder,rep,'.roi'))
cd ('dso.roi')
%cd(rfolder)         % to remove
%cd('roi')               % to remove

mask=imread(strcat(dso.name,'.roi.tif'));
listzoom=dir(strcat(dso.name,'.roi.zoom*.tif'));
zoom={mask};

for i=1:length(listzoom)
    zoom{i}=imread(listzoom(i).name);
end;

dso.data(~reshape(mask,size(mask,1)*size(mask,2),1),:)=0;
cd(rfolder);

f=fspecial('average',4);
sim=dso.imagedata;

fprintf('pyramid level: 1...');

simp{1,1}=sim;
simp{1,2}=mask;
simp{1,3}=zoom;

 

 
for j=1:8
    fprintf('%d...', j+1);

    pyr=zeros(ceil(size(simp{j,1},1)/2),ceil(size(simp{j,1},2)/2),size(dso,2))    ;
    for i=1:size(dso,2)
        tmp=imfilter(sim(:,:,i),f,'symmetric');
        pyr(:,:,i)=imresize(tmp,1/2,'nearest');
    end
    
    clear tmp
    
    tmpz=imfilter(mask,f,'symmetric');
    tmpz=imresize(tmpz,1/2,'nearest');
    mask=tmpz;
        
    for i=1:length(zoom)
         [ii,jj]=find(zoom{i});
         tmpz=logical(zeros(size(tmpz)));
         tmpz(round(min(ii)/2):round(max(ii)/2),round(min(jj)/2):round(max(jj)/2))=1;
         tmpz(tmpz)=mask(tmpz);
         zoom{i}=tmpz;
    end
    
    clear tmpz
    
    simp{j+1,1}=pyr;
    simp{j+1,2}=mask;
    simp{j+1,3}=zoom;
    
    sim=pyr;
    
    
    tmp.pyr=pyr;
    tmp.mask=mask;
    tmp.zoom=zoom;
    
    clear pyr
    
    save(strcat(dso.name,'.',num2str(j+1),'.mat'), 'tmp')
    clear tmp;
end;

fprintf('\r\n');
