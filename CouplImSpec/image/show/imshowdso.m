function gim=imshowdso(varargin)

 %% description
    % compute a grey level image from a datasetobject (dso) spectral image for a given wavelength, 
    % a band between two wavelength or as the sum or norm of the whole spectra. 
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input parameters can be
    % - no parameter
    % or
    % 1 - spectral image in dso format
    % 2 - string to indicate that the folllowing parameter is the method to
    %   be employed for computing the grey level image: expected values are:
    %           - 'WAVELENGTH' or 'wavelength' for a given wavelength
    %           - 'BAND' or 'band' for the area under a band
    %           - 'SUM' or 'sum' for the sum of all intensities of the
    %           spectra
    %           - 'NORM' or 'norm' for the norm of intensities for the
    %           whole spectra 
    %     default='SUM'
    % 3 - in case of method 'LO': vector of one value indicating the wavelength or wavenumber
    %   - in case of method 'BAND': vector of two values indicating the
    %                               wavelengths or wavenumbers that delimitate the band
    %     must be = 0 in case of 'SUM' and 'NORM' method
    % 4 - save folder 
    %     default=pwd
    % 
    
%% output
    
    % - nothing 
    % or
    % - the grey level image as a 2D uint8 array

%% principle
    %
    % - case 'WAVELENGTH' : the intensisty at a given wavelength is represented as a
    %               grey level image
    % 
    % - case 'BAND' : the sum of intensity between two bands is calculated
    %                   after a linear baseline correction under the band
    %
    % - case 'SUM' :  the sum of intensities of the whole spectra is
    %                   computed
    %
    % - case 'NORM' : the sum of squared intensities is assessed
    %
    %
    % in all cases : grey level values are adjusted to fit the 0-255 grey
    %                   level range and converted to unsigned int 8
    %
    % if no output argument is provided : the grey level image is saved in
    % a sub-folder 'showim' using the name found in the dso format
    % 
    % if the function is launched without any parameter : 
    %   a new figure if open and the image is shown
    %   the resulting image is saved in the current folder
    %
    % if the function is launched with parameters : 
    %   the resulting image is saved in a folder named <imshowdso>
    %   with the extension automatically defined according to the method
    %   chosen

%% use
    % imshowdso;
    
    % ims=readomnic;
    % gim=imshowdso(ims,'BAND',[1200 800])
    
%% Comments, context
    % review for Couplimpec toolbox
    %
    % from showspectrimage.m function
    % written for the PhD thesis of Fatma Allouche
    
    
%% Author
    % MF Devaux et F Allouche
    % BIA - PVPP INRA Nantes
    
%% date
    % 1 avril 2014
    %
    % 2 mars 2011
    % 10 mars 2011 : bug in data input nargin ==1

%% context variables 
orig=pwd; % returns the current directory



%% input data
sfolder='imshowdso';

if nargin == 0
    % input image
    [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

    cd(repl)
    ims=loaddso(nom);
    
    [method,lo]=imshowdsoIOParam(ims);
    
    sfolder=pwd;

end;

if nargin >= 1
    ims=varargin{1};
    ims=testIms(ims);
end;

if nargin==1    
    method='SUM';
    exts='.sum.tif';
    sfolder=pwd;
end;

if nargin >= 2
    method=upper(varargin{2});
end;

if nargin == 2
    if ~strcmp(method,'SUM') && ~strcmp(method,'NORM')
        disp('Wrong method or wrong number of parameters');
        error('use: gim=imshowdso(ims,''WAVELENGTH'' or ''BAND'', wavelength or wavevenumber)');
    end;
end;

if nargin ==3      
   sfolder=varargin{3};
end

if nargin ==4        
    if ~strcmp(method,'WAVELENGTH') && ~strcmp(method,'BAND')
        disp('Wrong method or wrong number of parameter');
        error('Usage: gim=imshowdso(ims,''SUM'' or ''NORM''');
    end;

    if strcmp(method,'WAVELENGTH')
        lo=varargin{3}; 
    end;

    if strcmp(method,'BAND')
        lo=varargin{3}; 
        if length(lo)~=2
            error('two values wavelengths or wavenumber values expected ');
        end
    end;

    sfolder=varargin{4};
end;

if nargin >4
    disp('Wrong number of parameter')
    error('Usage: gim=imshowdso(ims,''METHOD'', <wavelength or wavevenumber>,sfolder');
end;

% test des output parameters
if  nargout >1
    error('wrong number of output parameter')
end;


%% Treatment: 

% Compute grey level image

switch method
    case 'SUM'
        gim=sum(double(ims.imagedata),3);
        
    case 'NORM'
        gim=sum(double(ims.imagedata).*double(ims.imagedata),3);

    case 'WAVELENGTH'
        lot=ims.axisscale{2};           % liste of wavelengths or wavenumbers of dso
        % find indice
        silo=lot-repmat(lo,1,length(lot));
        indicelo=(abs(silo)==min(abs(silo)));

        % image
        gim=ims.imagedata(:,:,indicelo);
        
    case 'BAND'
        lot=ims.axisscale{2};           % liste of wavelengths or wavenumbers of dso
        % find indice
        silo=lot-repmat(lo(1),1,length(lot));
        indicelo(1)=find(abs(silo)==min(abs(silo)));
        silo=lot-repmat(lo(2),1,length(lot));
        indicelo(2)=find(abs(silo)==min(abs(silo)));

        % correction de ligne de base des spectres pour la bande considérée
        if indicelo(1)<indicelo(2)      % case fluorescence (set longueur d'onde)
            i1=indicelo(1);
            i2=indicelo(2);
        else                            % case IR ou Ramand with inverted spectral variables
                                        % in graphs and normal order in
                                        % data table
            i1=indicelo(2);
            i2=indicelo(1);
        end;
            
        sp=ims.data(:,i1:i2);           % select band
        nl=size(sp,2);
        
        % baseline
        a=(sp(:,nl)-sp(:,1))/(nl-1);
        b=sp(:,1)-a;
        
        % remove baseline
        spc=sp-repmat(a,1,nl).*repmat(1:nl,size(sp,1),1)-repmat(b,1,nl);
        
        % sum of intensity of the band
        gim=sum(spc,2);
        gim=reshape(gim,ims.imagesize(1),ims.imagesize(2));
        
end;


% adjust values to 0-255 grey levels
mini=min(gim(:));
maxi=max(gim(:));

a=255/(maxi-mini);
b=-a*mini;

gim=uint8(gim*a+b);

% show grey level image
if nargin<1
    figure;
    imshow(gim,[]);
    colormap(jet)
    title(ims.name,'Fontsize',12);
else
    h=figure(27);
    set(h,'windowstyle','docked');
    imshow(gim,[]);
    colormap(jet)
    title(ims.name,'Fontsize',12);
end

%% save 
% name of the file
if ~exist('exts','var')
    switch method
        case 'WAVELENGTH'
            exts=strcat('.w',num2str(round(lo)),'.tif');
        case 'BAND'
            exts=strcat('.w',num2str(round(lo(1))),'-',num2str(round(lo(2))),'.tif');
        case 'SUM'
            exts='.sum.tif';
        case 'NORM'
            exts='.norm.tif';
    end;  
end;
sname=strcat(ims.name,exts);

% save folder
if exist('sfolder','var')
    if ~exist(sfolder,'dir')
        mkdir(sfolder)
    end;
end;

% save
cd(sfolder)
imwrite(gim,sname,'tif','compression','none');



%% no matlab function tracking  


%% end

cd (orig)

function ims=testIms(ims)
% test wether the input parameter ims is a datasetobject or a filename

    if ischar(ims)      % name of file
        nom=ims;
         % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        ims=loaddso(nom);
    else if ~strcmp(class(ims),'dataset')
        error('give a data set as first input parameter');
        end;
    end;
    