function [RGB,Z,RGBImax]=selecChannelsTelemos(listChannels,folder,nslice)

 %% description
    % select the channels to put in a RGB image
    
%% input

    %  listChannels   : list of channels as a char vector
    % folder : where images are found
    % nslice : number of slice in the 3D images
    
%% output
    
    %   RBG : 3 values vector that contain the number corresponding to the
    %   channel
    %   Z : number of the slice selected 
    %   prctile : percentage of saturated pixels in individual channel
    %   images

%% principle
    % interactive selection by the user
    
%% use
    % [RGB,Z,prctile]=selecChannelsTelemos(listChannels,folder,nslice)
    
%% Comments
        %  written for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % making films of the degradation

    
%% Author
    % % MF Devaux
    % INRA- BIA-PVPP

    
%% date
    % 13 mars 2015
    %  7 mai 2019 : inversion arguments selZandChannelRGB

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~= 3
    error('use: [RGB,Z,RGBImax]=selecChannelsTelemos(listChannels,folder,nslice)');
end


%% input
% TODO: check if list channels is a vecchar
nbChannel=size(listChannels,1);

%% treatement
cd(folder)
listFileCh=cell(nbChannel,nslice);
for i=1:nbChannel
    list=dir(['*',listChannels(i,:),'*.tif']);
    for zpos=1:nslice
        listFileCh{i,zpos}=list(zpos).name;
        if i==1 && zpos==1
            im=imread(listFileCh{i,zpos});
            typIm=str2func(class(im));
            tim=typIm(zeros(size(im,1),size(im,2),nslice));
            tim(:,:,zpos)=im;
            clear im
        else
            tim(:,:,zpos)=imread(listFileCh{i,zpos});
        end
    end
    if i==1
        sim=typIm(zeros(size(tim,1),size(tim,2),nslice,nbChannel));
    end
    sim(:,:,:,i)=tim;
    clear tim;
end

[RGB,Z,RGBImax]=selZandChannelRGB(sim,listChannels);


%% matlab function tracking 
% no fucntion tracking

%% end

cd (orig)
    
