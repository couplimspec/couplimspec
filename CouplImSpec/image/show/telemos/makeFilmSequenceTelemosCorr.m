function makeFilmSequenceTelemosCorr

%% description
    % function to create a film sequance from a tiem serie of images acquired on the TELEMOS microscope 
    % 
    % this function is specific of telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    
    
%% input
    %  nothing 
    % interactive function
    
%% output
    % nothing the film is created on the disk
    
%% principe
    % information concerning the acquisition must have been extractedd using metadataTelemos function
    % the user must choose a z slice
    %           and the images to put in the RBG channels
    
    
%% use
    % makeFilmSequence
    % 
    
%% Comments
    %  written for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % making films of the degradation
    
    
%% Author
    % MF Devaux
    % INRA- BIA-PVPP

    
%% date
    %  26 f�vrier 2015

 
%% context variables 
porig=pwd;

%% start

if nargin ~=0
    error('Use: makeFilmSequence;');
end

%% input
% find the root folder and the subfolders
[nameFolder,rootFolder,listDir,listChannel]=metadataTelemos;
cd(rootFolder);

listfic=dir(strcat(nameFolder,'.pos.metadata.txt'));
if isempty(listfic)
    listfic=dir(strcat(nameFolder,'.roi.metadata.txt'));
end

% read extracted metadata
posdata=readDIV(listfic.name);

% number of roi or position acquired
nbpos=size(posdata.d,1);

%number of Z image
nSlice=posdata.d(1,3);

% select the z images
%zpos=inputNumber(['choose Z slice to make film: 0=all z images, max=',num2str(nSlice)],'vmin',0,'vmax',nSlice,'def',1);


% select channels
[RGB,zpos,RGBImax]=selecChannelsTelemos(listChannel,listDir(1,:),nSlice);
    
% retain only selected channels
nChannels=unique(RGB);
nbChannels=length(nChannels);
cim=cell(nbChannels,1);

% saving data to
cd(rootFolder)
if ~exist('films','dir')
    mkdir('films');
end;
cd('films')
sfolder=pwd;
cd ..
if ~exist('showimages','dir')
    mkdir('showimages');
end;
cd('showimages')
sfolderIm=pwd;

%% treatment

% manage zpos
if zpos==0
    dz=1;
    fz=nSlice;
else
    dz=zpos;
    fz=zpos;
end;
gzpos=zpos;



ic=0;   % count the number of intensity adjustement

% for each sub folder
for sfi=1:nbpos
        
    % for each zpos
    for zpos=dz:fz
          % root folder
          cd(rootFolder)
        % go to the sub folder
        cd(listDir(sfi,:))
    
        % for each selected channel
        for sci=1:nbChannels;
            ic=ic+1;
            list=dir(['*',listChannel(sci,:),'*',num2str(zpos-1),'.tif']);
            nb=length(list);
            % read each file
            tim=imread(list(1).name);
            im=zeros(size(tim,1),size(tim,2),nb);
            for i=1:nb
                im(:,:,i)=imread(list(i).name);
            end
                        
            % convert to uint8 by eliminating outliers values
%             pHigh=100-prctile;
%             pLow=prctile;
%             %[im,Imin,Imax,flMaxPlus]=imTelemos2PrctileUint8(im,pHigh,pLow);
%             tim=im(:,:,1);
%             tim=medfilt2(tim,'symmetric');
%             [~,Imin,Imax,flMaxPlus]=imTelemos2PrctileUint8(tim,pHigh,pLow);
            for i=1:3
                if RGB(i)==sci;
                    Imax=RGBImax(i);
                end;
            end;
            Imin=0;
            im=convIm2uint8(im,[Imin, Imax],[0 225]);
%             if flMaxPlus
%                 im=im+500;
%             end;
            
            %im=im(358:778,:,:);
            
            % save selected image
            cim{sci}=im;
            
            % save Imin and IMax values
            intMinMax.d(ic,1)=Imin;
            
            intMinMax.d(ic,2)=Imax;
           % intMinMax.d(ic,3)=0; %flMaxPlus;
%             if flMaxPlus
%                 intMinMax.d(ic,2)=Imax-500;
%             else
%                 intMinMax.d(ic,2)=Imax;
%             end;

            nameInd=[listDir(sfi,:),'-',listChannel(sci,:),'-',num2str(zpos-1)];
            
            if ic==1
                intMinMax.i=nameInd;
            else
                intMinMax.i=char(intMinMax.i,nameInd);
            end;
            

            
        end;
        
        % calculate time between acquisition
        stime=[list.datenum];       % serial time number from Matlab
        % time between two acquisitions converted in h, min, sec
        tAcq=datevec(cumsum(diff(stime)));
        tAcq=[0 0 0 0 0 0 ;tAcq];
        
        % round second to have result in minutes
        time=minute(tAcq(:,4:6));
        
        
        
        % create color image
        vim=uint8(zeros(size(im,1),size(im,2),3,nb));
        for i=1:3
            numsel=RGB(i);
            im=cim{numsel};
            vim(:,:,i,:)=im;
        end;
        
        
        
        cd(sfolder)
        sname=[nameFolder '.' listDir(sfi,:) '.' num2str(zpos) '.avi'];
        makeFilm(vim,time,sname,sfolder);
        
    end;
    
    zpos=gzpos;
    
end

% recover depfc and finfc
cd(rootFolder)
for sfi=1:nbpos
    list=dir(['*' listDir(sfi,:)]);
    for i=1:length(list)
        if ~strcmp(list(i).name,listDir(sfi,:))
            cd(list(i).name)
            if exist('Pos0','dir')
                cd('Pos0');
                if exist('img_000000000_DM300_Nofilter_000.tif','file')
                    im=imread('img_000000000_DM300_Nofilter_000.tif');
                    im=imTelemos2PrctileUint8(im,99.999,0.001);
                    cd(sfolderIm)
                    imwrite(im,strcat('c',list(i).name,'.tif'),'tif','compression','none');
                    cd(rootFolder);
                end
            end
        end
    end
end

%% save

% save min max intenisties values
%intMinMax.v=char('Imin','Imax','flagMax+500');
intMinMax.v=char('Imin','Imax');
cd(sfolder)
writeDIV(intMinMax,strcat(nameFolder,'.',listDir(1,1:3),'.',num2str(zpos),'.intMinMax.txt'));

%% matlab function tracking  
fid=fopen(strcat(nameFolder,'.',listDir(1,1:3),'.',num2str(zpos),'.avi.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t\r\n\r\n',datestr(now,0));
fprintf(fid,'Make film sequence from time lapse Telemos Images \r\n');
fprintf(fid,'_________________________________________________ \r\n');

fprintf(fid,'\r\nexperiment: %s\r\n',nameFolder);
fprintf(fid,'data folder: %s\r\n',rootFolder);

fprintf(fid,'\r\n');
fprintf(fid,'\r\nChannels of colour image\r\n');
fprintf(fid,'\t - Red:   %s\r\n',listChannel(RGB(1),:));
fprintf(fid,'\t - Green: %s\r\n',listChannel(RGB(2),:));
fprintf(fid,'\t - Blue:  %s\r\n',listChannel(RGB(3),:));

fprintf(fid,'\r\n');
if zpos~=0
    fprintf(fid,'\r\nZ focal plane of films : %d \r\n',zpos);
else
    fprintf(fid,'\r\nFilms for each focal plane \r\n');
end

fprintf(fid,'\r\nContrast adjustment performed for each channel :\r\n');

% fprintf(fid,'\t - 0 (black) correspond to percentile %7.4f %% of the grey level histogram\r\n',pLow);
% fprintf(fid,'\t - 255(white) correspond to percentile %7.4f %% of the grey level histogram\r\n',pHigh);
% fprintf(fid,'\t - 500 is added when max percentile is lower than 700 in function <imTelemos2PrctileUint8.m>\r\n');

fprintf(fid,'\r\n');
fprintf(fid,'\r\nfilms :\r\n');
for i=1:size(listDir,1)
    if zpos==0
        fprintf(fid,'\t - %s.%s.*.avi\r\n',nameFolder,listDir(i,:));
    else
        fprintf(fid,'\t - %s.%s.%d.avi\r\n',nameFolder,listDir(i,:),zpos);
    end;
end;
fprintf(fid,'\r\n');

fprintf(fid,'\r\nIndividual images of film and starting en final brightfield images saved in folder: %s \r\n',sfolderIm);
fprintf(fid,'\r\n');

fprintf(fid,'\r\nMin and max fluorescence intensities saved in file : %s \r\n',strcat(nameFolder,'.intMinMax.txt'));
fprintf(fid,'\r\n');

fprintf(fid,'Files saved in folder : %s \r\n',sfolder);
fprintf(fid,'\r\n');
% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'Function name: \r\n\t- %s.m ',mfilename);
res=dir(info);
fprintf(fid,'\t- %s \r\n',res.date);
fprintf(fid,'saved in function folder: %s \r\n\r\n',repprog);

info=which ('selecChannelsTelemos.m');
fprintf(fid,'=> Call function: \r\n\t- %s ','selecChannelsTelemos.m');
res=dir(info);
fprintf(fid,'\t- %s \r\n',res.date);
fprintf(fid,'saved in function folder: %s \r\n\r\n',repprog);

info=which ('selZandChannelRGB.m');
fprintf(fid,'=> Call function: \r\n\t\t- %s ','selZandChannelRGB.m');
res=dir(info);
fprintf(fid,'\t- %s \r\n',res.date);
fprintf(fid,'saved in function folder: %s \r\n\r\n',repprog);

info=which ('imTelemos2PrctileUint8.m');
fprintf(fid,'=> Call function: \r\n\t- %s ','imTelemos2PrctileUint8.m');
res=dir(info);
fprintf(fid,'\t- %s \r\n',res.date);
fprintf(fid,'saved in function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);


%% end
cd(porig)

end % main function


function makeFilm(vim,time,sname,sfolder)

cd(sfolder)

% make film
writerObj = VideoWriter(sname);
writerObj.FrameRate=2;

open(writerObj);

if ~exist('..\showimages','dir')
    cd ..
    mkdir('showimages')
    cd(sfolder)
end
cd('..\showimages')

%Create a set of frames and write each frame to the file. repeat film 10
%times. save each individual images once
repFilm=10;
for j=1:repFilm
    for k = 1:size(vim,4)
        imshow(vim(:,:,:,k),[]);
        hold on
        text(20,30,[num2str(time(k)) ' min'],'fontsize',24,'color','w','fontweight','bold')
        hold off
        frame = getframe;
        writeVideo(writerObj,frame);
        im=frame2im(frame);
        if j==1
            if k<10
                imwrite(im,strcat(strrep(sname,'.avi',''),'.im0',num2str(k),'.png'),'png');
            else
                imwrite(im,strcat(strrep(sname,'.avi',''),'.im',num2str(k),'.png'),'png');
            end;
        end
    end
end

close(writerObj);

end








