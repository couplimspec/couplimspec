  function [im,Imin,Imax,fMaxPlus]=imTelemos2PrctileUint8(im,pHigh,pLow)

 %% description
    % convert Telemos image uint16 in uint8 by considering percentile
    % intensity values
    % 
    % TELEMOS: multispectral fluorescence microscope at synchrotron SOLEIL
    % DISCO beamline
    
%% input

    %  im : Telemos image : can be 2D or 3 or 4D. 
    %  pHigh and pLow : value between 0 and 1 of the prcentile to assess
    %  the min and max intensity values that will correspond to 0 and 255.
    
%% output
    
    %   im : Telemos image converted in Uint8 image
    % Imin : original inteniity corresponding to 0
    % Imax : original intensity corresponding to 255

%% principle
    % conversion  in uint8
    
    % the min ans max intensity values put to 0 and 255 correspond to the
    % percentile of he histogram of the image
    % all the values are considered even if the image if 3 or 4D


%% use
    % [im,Imin,Imax]=imTelemos2PrctileUint8(im,pHigh,pLow)
    
%% Comments
    %  written for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % making films of the degradation
    
    
%% Author
    % MF Devaux
    % INRA- BIA-PVPP

    
%% date
    %  12 mars 2015
    

%% start
if nargin ~=3
    error('use: [im,Imin,Imax]=imTelemos2PrctileUint8(im,pHigh,pLow)');
end

%% treatement
% convert to double
im=double(im);

% and convert to uint8 by eliminating outliers values
Imax=round(prctile(im(:),pHigh));
Imin=round(prctile(im(:),pLow));

% if the signal is too low, then the image is set to black
if Imax < 700
    Imax=Imax+500;
    fMaxPlus=1;
else
    fMaxPlus=0;
end;

im=uint8((double(im)-Imin)/(Imax-Imin)*255);

%% end

    