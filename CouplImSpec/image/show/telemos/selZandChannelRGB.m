function varargout = selZandChannelRGB(varargin)
% SELZANDCHANNELRGB MATLAB code for selZandChannelRGB.fig
%      SELZANDCHANNELRGB, by itself, creates a new SELZANDCHANNELRGB or raises the existing
%      singleton*.
%
%      H = SELZANDCHANNELRGB returns the handle to a new SELZANDCHANNELRGB or the handle to
%      the existing singleton*.
%
%      SELZANDCHANNELRGB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SELZANDCHANNELRGB.M with the given input arguments.
%
%      SELZANDCHANNELRGB('Property','Value',...) creates a new SELZANDCHANNELRGB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before selZandChannelRGB_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to selZandChannelRGB_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help selZandChannelRGB

% Last Modified by GUIDE v2.5 13-Mar-2015 15:47:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @selZandChannelRGB_OpeningFcn, ...
                   'gui_OutputFcn',  @selZandChannelRGB_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before selZandChannelRGB is made visible.
function selZandChannelRGB_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to selZandChannelRGB (see VARARGIN)

%set(hObject,'units','normalised','position',[0.05 0.5 0.5 0.5 ]);
if length(varargin)~=2
    error('Invalid number of arguments. USE : [RGB,Z]=selChannelRGB(listChannels,im);');
end

% first argument: image as a 4dimension image : nlig x ncol x 1 x dim
%(dim=number of channels)
if length(size(varargin{1}))~=4 && ~isnumeric(varargin{1})
    error('invalid image: not numeric or wrong number of dimension');
end

% second arguments: char array of channel name
if ~ischar(varargin{2})
    error('list of channels expected as first argument');
end
handles.listChannels=varargin{2};

if size(varargin{1},4)~=size(handles.listChannels,1)
    error(' number of image does not correspond to number of channels');
end

handles.im=varargin{1};
handles.oim=handles.im;            % image with original contrast


% create the resulting color image
handles.vim=uint8(zeros(size(handles.im,1),size(handles.im,2),3));
handles.RGB=zeros(1,3);

% put the name of channels in the list box
set(findobj('Tag','selChannelSpectral'),'string',handles.listChannels);

% create the figure to show the color image
handles.fig=findobj('tag','showIMageselZandRGB');
%set(handles.fig,'position',[0.05 0.05 0.5 0.5 ],'units','normalised');
imshow(handles.vim);

% get the number of Z slice
handles.nslice=size(handles.im,3);
% initial Z selected
handles.Z=1;

% set min max values for slider
set(findobj('tag','selZ'),'min',1);
set(findobj('tag','selZ'),'max',handles.nslice);
set(findobj('tag','selZ'),'value',1);
sStep=1/(handles.nslice-1);
set(findobj('tag','selZ'),'sliderstep',[sStep,0.5]);

% Choose default command line output for selZandChannelRGB
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes selZandChannelRGB wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = selZandChannelRGB_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% retrun RGB vector as result
varargout{1} = handles.RGB;

% return Z pos 
varargout{2}=round(handles.Z);

% return percentage of saturated pixels
%varargout{3}=handles.prctile;
varargout{3}=handles.RGBImax;

% close gui
close(handles.figure1);


% --- Executes on button press in selChannelRed.
function selChannelRed_Callback(hObject, eventdata, handles)
% hObject    handle to selChannelRed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of selChannelRed

% selection of channels to put in the red channel of the color image
[selRed,ok] = listdlg('PromptString','Select a channel:',...
                'SelectionMode','single',...
                'ListString',char('none',handles.listChannels));    

if ok
    if selRed==1    % none selected
        handles.vim(:,:,1)=0;
        set(findobj('tag','selChannelsRedOK'),'string','none');  
        % save number of spectral channel
        handles.RGB(1)=0;    
    else
        selRed=selRed-1;
        
        
        switch selRed
            case handles.RGB(2)
                selim=handles.vim(:,:,2);
                handles.RGBImax(1)=handles.RGBImax(2);
            case handles.RGB(3)
                selim=handles.vim(:,:,3);
                handles.RGBImax(1)=handles.RGBImax(3);
            otherwise
                % select the image from multichannel im image
                selim=handles.im(:,:,handles.Z,selRed);
                
                % select max for visualisation
                selim=medfilt2(selim,'symmetric');
                Imax=round(prctile(double(selim(:)),99.99));
                Imax=inputNumber('max value for Red channel','def',Imax);
                selim=convIm2uint8(selim,[0,Imax],[0,225]);
                handles.RGBImax(1)=Imax;
        end
        
        
        % save uint8 image in color image
        handles.vim(:,:,1)=selim;
        
        % update name
        set(findobj('tag','selChannelsRedOK'),'string',handles.listChannels(selRed,:));
        
        % save number of spectral channel
        handles.RGB(1)=selRed;
    end
end

% show result
set(handles.figure1,'currentaxes',handles.fig);
imshow(handles.vim);

% Update handles structure
guidata(hObject, handles);
            
            


% --- Executes on button press in selChannelGreen.
function selChannelGreen_Callback(hObject, eventdata, handles)
% hObject    handle to selChannelGreen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of selChannelGreen

% selection of channels to put in the green channel of the color image
[selGreen,ok] = listdlg('PromptString','Select a channel:',...
                'SelectionMode','single',...
                'ListString',char('none',handles.listChannels));
if ok
    if selGreen==1    % none selected
        handles.vim(:,:,2)=0;
        set(findobj('tag','selChannelsGreenOK'),'string','none');  
        % save number of spectral channel
        handles.RGB(2)=0;
    else
        selGreen=selGreen-1;
        
        switch selGreen
            case handles.RGB(1)
                selim=handles.vim(:,:,1);
                handles.RGBImax(2)=handles.RGBImax(1);
            case handles.RGB(3)
                selim=handles.vim(:,:,3);
                handles.RGBImax(2)=handles.RGBImax(3);
            otherwise
                % select the image from multichannel im image
                selim=handles.im(:,:,handles.Z,selGreen);
                
                % select max for visualisation
                selim=medfilt2(selim,'symmetric');
                Imax=round(prctile(double(selim(:)),99.99));
                Imax=inputNumber('max value for Green channel','def',Imax);
                selim=convIm2uint8(selim,[0,Imax],[0,225]);
                handles.RGBImax(2)=Imax;
        end
        
        % save uint8 image in color image
        handles.vim(:,:,2)=selim;
        
        % update name
        set(findobj('tag','selChannelsGreenOK'),'string',handles.listChannels(selGreen,:));
        
        % save number of spectral channel
        handles.RGB(2)=selGreen;
    end
end
 
% show result
set(handles.figure1,'currentaxes',handles.fig);
imshow(handles.vim);

% Update handles structure
guidata(hObject, handles);
            


% --- Executes on button press in selChannelBlue.
function selChannelBlue_Callback(hObject, eventdata, handles)
% hObject    handle to selChannelBlue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of selChannelBlue

% selection of channels to put in the blue channel of the color image
[selBlue,ok] = listdlg('PromptString','Select a channel:',...
                'SelectionMode','single',...
                'ListString',char('none',handles.listChannels));
            
            
if ok
    if selBlue==1    % none selected
        handles.vim(:,:,3)=0;
        set(findobj('tag','selChannelsBlueOK'),'string','none');  
        % save number of spectral channel
        handles.RGB(3)=0;
    else    
        selBlue=selBlue-1;
        
        switch selBlue
            case handles.RGB(2)
                selim=handles.vim(:,:,2);
                handles.RGBImax(3)=handles.RGBImax(2);
            case handles.RGB(1)
                selim=handles.vim(:,:,1);
                handles.RGBImax(3)=handles.RGBImax(1);
            otherwise
                % select the image from multichannel im image
                selim=handles.im(:,:,handles.Z,selBlue);
                
                % select max for visualisation
                selim=medfilt2(selim,'symmetric');
                Imax=round(prctile(double(selim(:)),99.99));
                Imax=inputNumber('max value for Blue channel','def',Imax);
                selim=convIm2uint8(selim,[0,Imax],[0,225]);
                handles.RGBImax(3)=Imax;
        end
        
        % save uint8 image in color image
        handles.vim(:,:,3)=selim;
        
        % update name
        set(findobj('tag','selChannelsBlueOK'),'string',handles.listChannels(selBlue,:));
        
        % save number of spectral channel
        handles.RGB(3)=selBlue;
    end
end

% show result
set(handles.figure1,'currentaxes',handles.fig);
imshow(handles.vim);

% Update handles structure
guidata(hObject, handles);

% --- Executes on selection change in selChannelSpectral.
function selChannelSpectral_Callback(hObject, eventdata, handles)
% hObject    handle to selChannelSpectral (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selChannelSpectral contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selChannelSpectral


% --- Executes during object creation, after setting all properties.
function selChannelSpectral_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selChannelSpectral (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selChannelsOK.
function selChannelsOK_Callback(hObject, eventdata, handles)
% hObject    handle to selChannelsOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% test
if sum(handles.RGB)==0
    warning('no channels selected');
end

% end and continue
uiresume(gcbf);


% --- Executes on slider movement.
function selZ_Callback(hObject, eventdata, handles)
% hObject    handle to selZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% get final z pos value
set(findobj('tag','selallZandRGBcb'),'value',0);
handles.Z=get(findobj('tag','selZ'),'value');
handles.Z=round(handles.Z);

% RGB channels
for i=1:3
    if handles.RGB(i)~=0
        % select the image from multichannel im image
        selim=handles.im(:,:,handles.Z,handles.RGB(i));
        
        % select max for visualisation
        selim=medfilt2(selim,'symmetric');
        selim=convIm2uint8(selim,[0,handles.RGBImax(i)],[0,225]);
                

         % save uint8 image in color image    
        handles.vim(:,:,i)=selim;
    end
end

% show result
set(handles.figure1,'currentaxes',handles.fig);
imshow(handles.vim);    
    
% update name
set(findobj('tag','selZTitle'),'string',['Z slice ', num2str(handles.Z)]);

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function selZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in selallZandRGBcb.
function selallZandRGBcb_Callback(hObject, eventdata, handles)
% hObject    handle to selallZandRGBcb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of selallZandRGBcb
% get final z pos value
sAll=get(hObject,'Value');

if sAll
    handles.Z=0;
end

% Update handles structure
guidata(hObject, handles);



 
