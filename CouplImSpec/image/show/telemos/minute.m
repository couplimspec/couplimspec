  function Tminut=minute(time)

 %% description
    % calculate the time in minute from a table hour minute second
    
%% input

    %  time : table of three columns 
    %           hour minute second
    
%% output
    
    %   Tminut : time in number of minutes

%% principle
    % conversion  in minutes
    
    % round(60 * hour + minute + second/60)


%% use
    % Tminut=minute(time)
    
%% Comments
    %  written for synchrotron project: 
    % "Following enzyme localization and cell wall modification during biomass 
    %   hydrolysis by autofluorescence and infrared imaging"
    % Proposal N�: 20140308
    % resp: Marie-francoise Devaux
    % making films of the degradation
    
    
%% Author
    % MF Devaux
    % INRA- BIA-PVPP

    
%% date
    %  26 f�vrier 2015
    

%% start

if nargin ~=1
    error('use: Tminut=minute(time)');
end

if size(time,2)~=3
    error('three-columns table expected:  hour minute second');
end

%% treatement
Tminut=round(60*time(:,1)+time(:,2)+(time(:,3)/60));

%% end

    