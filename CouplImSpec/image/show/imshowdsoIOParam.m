function [method,lo]=imshowdsoIOParam(ims)
%% description
    % interactive choice of input parameter for imshowdso
    
    % imshowdso compute a grey level image from a datasetobject (dso) spectral image 
    % for a given wavelength, a band between two wavelength or as the sum 
    % or norm of the whole spectra. 
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input parameters can be
    % ims : spectral image as datasetobject ims
    
%% output
    
    % method : 'WAVELENGTH','BAND', 'SUM', or 'NORM'
    % lo : wavelength or vector of two wavelength for 'WAVELENGTH','BAND'
        % option, 0 otherwise

%% principle
    % use the ims example image to select the method chosen to assess a grey
    % level representation of the spectral image stored in the
    % datasetobject 
    % 
    % - case 'WAVELENGTH' : the intensisty at a given wavelength is represented as a
    %               grey level image
    %
    % - case 'BAND' : the sum of intensity between two bands is calculated
    %                   after a linear baseline correction under the band
    %
    % - case 'SUM' :  the sum of intensities of the whole spectra is
    %                   computed
    %
    % - case 'NORM' : the sum of squared intensities is assessed
    %
    %
    % in all cases : grey level values are adjusted to fit the 0-255 grey
    %                   level range and converted to unsigned int 8
    %
    % if no output argument is provided : the grey level image is saved in
    % a sub-folder 'showim' using the name found in the dso format
    %
    % the function is called by imshowdso and processCol

%% use
    % [method,lo]=imshowdsoIOParam(ims)
    
    
%% Comments, context
    % review for Couplimpec toolbox
    %
    % from showspectrimage.m function
    % written for the PhD thesis of Fatma Allouche
    
    
%% Author
    % MF Devaux 
    % BIA - PVPP INRA Nantes
    
%% date
    % 1 avril 2014

    
%% context variable
%% start

if nargin ~= 1
    error('Use: [method,lo]=imshowdsoIOParam(ims)');
end;

%% input/treatment...
        
% choice of the method
choix={'sum of intensities', 'norm of intensities','one wavelength','sum between two wavelengths'};
codemethod={'SUM','NORM','WAVELENGTH','BAND'};
[nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
    'name','method to compute grey level image','listsize',[300 160]);
if ok 
    method=codemethod{nummethod};
else if isempty(nummethod)  % cancel of no method chosen
        disp('method choice cancelled imshowdso');
        return;
    end;
end;
        
% according to the method wavelengths are required :
switch method
    %% 'WAVELENGTH'
  case 'WAVELENGTH'
    lot=ims.axisscale{2};           % wavelengths or wavenumbers
    figure(28)                       % draw spectra
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        imst=ims(1:st:nbs,:);
        plotdso(imst,0,0);
        x=ginput(1);
        x=x(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x)) ' ' ims.userdata.acquisition.unit])
        ok=yesno(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.acquisition.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength or wavenumber chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=yesno([num2str(x) ': out of bound wavelength or wavenumber, continue ?']);
                if ~ok
                    error('imshowdso cancelled');
                else
                    ok=0;
                end;
            end;
        end;

        hold off
    end;
    lo=x;
    clear x

 
    %% 'BAND'

  case 'BAND'
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(28)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        imst=ims(1:st:nbs,:);
        plotdso(imst,0,0);
        x=ginput(1);
        x=x(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=yesno(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.acquisition.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength or wavenumber chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=yesno([num2str(x) ': out of bound wavelength or wavenumber, continue ?']);
                if ~ok
                    error('imshowdso cancelled');
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(1)=x;
    clear x
    ok=0;
    while ~ok
        plotdso(imst,0,0);
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x]=ginput(1);
        x=x(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=yesno(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.acquisition.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=yesno([num2str(x) ': out of bound wavelength or wavenumber, continue ?']);
                if ~ok
                    error('imshowdso cancelled');
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(2)=x;
    clear x
            

    %% 'SUM' and 'NORM'
    otherwise
        lo=0;
end;
       

%% no function tracking    


end  
