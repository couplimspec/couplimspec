function varargout = imTreatment(varargin)
% IMTREATMENT MATLAB code for imTreatment.fig
%      IMTREATMENT, by itself, creates a new IMTREATMENT or raises the existing
%      singleton*.
%
%      H = IMTREATMENT returns the handle to a new IMTREATMENT or the handle to
%      the existing singleton*.
%
%      IMTREATMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMTREATMENT.M with the given input arguments.
%
%      IMTREATMENT('Property','Value',...) creates a new IMTREATMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before imTreatment_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to imTreatment_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help imTreatment

% Last Modified by GUIDE v2.5 20-Jun-2014 14:48:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @imTreatment_OpeningFcn, ...
                   'gui_OutputFcn',  @imTreatment_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before imTreatment is made visible.
function imTreatment_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to imTreatment (see VARARGIN)

% Choose default command line output for imTreatment
handles.output = hObject;

% logo
axLogoIT(handles)


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes imTreatment wait for user response (see UIRESUME)
% uiwait(handles.imtreatment);


% --- Outputs from this function are returned to the command line.
function varargout = imTreatment_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pLoadImageIT.
function pLoadImageIT_Callback(hObject, eventdata, handles)
% hObject    handle to pLoadImageIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% clear appdata
clearAppDataIT(hObject, handles);

% input image name and  folder
[imName,rfolder]=uigetfile({'*.tif'},'image name');

% read image
cd(rfolder)
im=imread(imName);

% display in the axes of the guide
hax=handles.axImshowIT;
axes(hax);
imshow(im,[])

% display image name
set(handles.tImNameIT,'string',imName);

% update workflow
done=sprintf('- Read image:%s \r\n\tFolder:%s\r\n',imName, rfolder);

% save workflow and image data
setappdata(handles.imtreatment,'done',done)
setappdata(handles.imtreatment,'im',im);
setappdata(handles.imtreatment,'rfolder',rfolder);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pInvertIT.
function pInvertIT_Callback(hObject, eventdata, handles)
% hObject    handle to pInvertIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recover workflow
done=getappdata(handles.imtreatment,'done');

% recover image 
im=getappdata(handles.imtreatment,'im');

% invert
im=imcomplement(im);

% display in the axes of the guide
hax=handles.axImshowIT;
axes(hax);
imshow(im,[])

% update workflow
done=sprintf('%s- Invert grey levels\r\n',done);

% display image name
set(handles.tSaveImNameIT,'string','Not Saved','foregroundcolor','r');

% save workflow and image data
setappdata(handles.imtreatment,'done',done)
setappdata(handles.imtreatment,'im',im);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pTopHatIT.
function pTopHatIT_Callback(hObject, eventdata, handles)
% hObject    handle to pTopHatIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% recover image 
im=getappdata(handles.imtreatment,'im');

% get top hat size 
sTopHat=inputNumber('enter structuring element size (odd number)','vmin',3,'title','Top Hat squared structuring element size (odd number)');

% top hat
thim=imtophat(im,strel('square',sTopHat));

% figure
h27=figure(27);
set(h27,'units','normalized','outerposition',[0.1 0.1 0.85 0.85]);
subplot(1,2,1)
imshow(im,[])
title('start','fontsize',18)
subplot(1,2,2)
imshow(thim,[])
title('TOP HAT','fontsize',18)

% ok ?
ok=yesno('accept image ?');

if ok
    im=thim;
    
    % display in the axes of the guide
    hax=handles.axImshowIT;
    axes(hax);
    imshow(im,[])
    
    % display top hat size
    set(handles.tTopHatIT,'string',['square of size: ',num2str(sTopHat)]);
    
    % recover and update workflow
    done=getappdata(handles.imtreatment,'done');
    done=sprintf('%s- Top Hat with a squared structuring element of size %d \r\n',done,sTopHat);
    % display image name
    set(handles.tSaveImNameIT,'string','Not Saved','foregroundcolor','r');
    
    % save workflow and image data
    setappdata(handles.imtreatment,'done',done)
    setappdata(handles.imtreatment,'im',im);
    
    % Update handles structure
    guidata(hObject, handles);

end

% --- Executes on button press in pImResizeIT.
function pImResizeIT_Callback(hObject, eventdata, handles)
% hObject    handle to pImResizeIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% recover image 
im=getappdata(handles.imtreatment,'im');

% get top hat size 
scale=inputNumber('enter scale factor to resize image (any positive float)','vmin',0,'title','Image scale factor');

% resize image
rim=imresize(im,scale,'bilinear');

% figure
h27=figure(27);
set(h27,'units','normalized','outerposition',[0.1 0.1 0.85 0.85]);
subplot(1,2,1)
imshow(im,[])
title('start','fontsize',18)
subplot(1,2,2)
imshow(rim,[])
title('IMAGE RESIZED','fontsize',18)


% ok ?
ok=yesno('accept image ?');

if ok
    im=rim;
    
    % display in the axes of the guide
    hax=handles.axImshowIT;
    axes(hax);
    imshow(im,[])
    
    % display top hat size
    set(handles.tImResizeIT,'string',['scale factor: ',num2str(scale)]);
    
    % recover and update workflow
    done=getappdata(handles.imtreatment,'done');
    done=sprintf('%s- image scaled by a factor of %6.2f (bilinear) \r\n',done,scale);
    % display image name
    set(handles.tSaveImNameIT,'string','Not Saved','foregroundcolor','r');
    
    % save workflow and image data
    setappdata(handles.imtreatment,'done',done)
    setappdata(handles.imtreatment,'im',im);
    
    % Update handles structure
    guidata(hObject, handles);

end

% --- Executes on button press in pCropIT.
function pCropIT_Callback(hObject, eventdata, handles)
% hObject    handle to pCropIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recover image 
im=getappdata(handles.imtreatment,'im');

hax=handles.axImshowIT;

% crop
hr=imrect(hax);

% recover rectangle position after double click as integer
pos=round(wait(hr));

dl=pos(2);      % starting line
sl=pos(4);      % number of lines
dc=pos(1);      % starting column
sc=pos(3);      % number of columns

% croped image
cim=im(dl:(dl+sl),dc:(dc+sc));

% figure
h27=figure(27);
set(h27,'units','normalized','outerposition',[0.1 0.1 0.85 0.85]);
subplot(1,2,1)
imshow(im,[])
title('start','fontsize',18);
subplot(1,2,2)
imshow(cim,[])
title('croped image','fontsize',18);

ok=yesno('continue ?');

if ok
    im=cim;
    
    % update workflow
    % recover workflow
    done=getappdata(handles.imtreatment,'done');
    done=sprintf('%s- image croped \r\n',done);
    % display image name
    set(handles.tSaveImNameIT,'string','Not Saved','foregroundcolor','r');
    
    % display in the axes of the guide
    hax=handles.axImshowIT;
    axes(hax);
    imshow(im,[])
     
    
    % save workflow and image data
    setappdata(handles.imtreatment,'done',done)
    setappdata(handles.imtreatment,'im',im);
    setappdata(handles.imtreatment,'cropPos',[dl dc sl sc]);
    
    % Update handles structure
    guidata(hObject, handles);

end;

% --- Executes on button press in pDirFiltIT.
function pDirFiltIT_Callback(hObject, eventdata, handles)
% hObject    handle to pDirFiltIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recover image 
im=getappdata(handles.imtreatment,'im');

% get filter line size 
lineSize=inputNumber('enter length of lines for directional filtering (>3)','vmin',3,'title','Line Length');

% choose filter  
ok=0;
while ~ok
    listop={'imopen','imclose','immedian','immean',};
    
    [numop,ok]=listdlg('liststring',listop,'selectionmode','single','name','choose filter');   
end

op=listop{numop};


% directional filtering : combing opertor is set to MAX and number of
% direction to 16
fim=imdirfilter(im,op,'max',lineSize,16);
 
% figure
h27=figure(27);
set(h27,'units','normalized','outerposition',[0.1 0.1 0.85 0.85]);
subplot(1,2,1)
imshow(im,[])
title('start','fontsize',18);
subplot(1,2,2)
imshow(fim,[])
title('FILTERED','fontsize',18);

ok=yesno('continue ?');

if ok
    im=fim;
    
    % update workflow
    % recover workflow
    done=getappdata(handles.imtreatment,'done');
    done=sprintf('%s- directional filtering with <%s> filter: lines of length %d, 16 directions combined using <MAX>  operator\r\n',done,op,lineSize);
    
    % display image name
    set(handles.tSaveImNameIT,'string','Not Saved','foregroundcolor','r');
    
    % display in the axes of the guide
    hax=handles.axImshowIT;
    axes(hax);
    imshow(im,[])
     
    % display line length
    set(handles.tDirFiltSizeIT,'string',['line length: ',num2str(lineSize)]);
    % display filter Name
    set(handles.tDirFiltIT,'string',['Filter: ',op]);
    
    % save workflow and image data
    setappdata(handles.imtreatment,'done',done)
    setappdata(handles.imtreatment,'im',im);
    setappdata(handles.imtreatment,'dirFilt',{op;lineSize;16;'MAX'});
    
    % Update handles structure
    guidata(hObject, handles);

end;


% --- Executes on button press in pSaveImageIT.
function pSaveImageIT_Callback(hObject, eventdata, handles)
% hObject    handle to pSaveImageIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recover workflow
done=getappdata(handles.imtreatment,'done');

% recover image 
im=getappdata(handles.imtreatment,'im');

% input image name and  folder
[simName,sfolder]=uiputfile({'*.tif'},'result image name');

% save
cd(sfolder)
imwrite(im,simName,'tiff','compression','none');

% display image name
set(handles.tSaveImNameIT,'string',simName);

done=sprintf('%s- Save image:%s \r\n\tFolder:%s\r\n',done,simName, sfolder);

setappdata(handles.imtreatment,'done',done)
 
    
    
% save track file
saveTrack(simName,handles);

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function axLogoIT(handles)
% hObject    handle to axLogoIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axLogoIT

logoMF=imread('couplimspec.tif');
hax=handles.axLogoIT;
axes(hax)
imshow(logoMF,[])

% clear appdata
function clearAppDataIT(hObject, handles)
% clear all user appdata when a new image is loaded

% test the different fields
% crop
if isappdata(handles.imtreatment,'crop position')
    rmappdata(handles.imtreatment,'crop position');
end;
if isappdata(handles.imtreatment,'dirFilt')
    rmapppdata(handles.imtreatment,'dirFilt');
end
   

% name of saved image
% display image name
set(handles.tSaveImNameIT,'string','Not Saved','foregroundcolor','r');
% top hat
set(handles.tTopHatIT,'string','');
% im resize
set(handles.tImResizeIT,'string','');
% directional filtering : line length
set(handles.tDirFiltSizeIT,'string','');
% directional filtering : filter Name
set(handles.tDirFiltIT,'string','');

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function axLogoIT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axLogoIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axLogoIT

% save track
function saveTrack(simName,handles)
% function that save the track file of the image

% recover workflow
if isappdata(handles.imtreatment,'done')
    done=getappdata(handles.imtreatment,'done');
else
    error('no processing have been done');
end

% track file
fname=strrep(simName,'.tif','.track.txt');
fid=fopen(fname,'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,'Process grey level images for registration \r\n');
fprintf(fid,'___________________________________________\r\n');

% name of starting image
fprintf(fid,'%s\r\n',done);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);
