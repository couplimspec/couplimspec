function meanIntensity

 %% description
    % Compuite average grey level of each channel of a serie of images in a
    % single folder
   
   
    
%% input

    %  nothing
    % interactive input of grey level to compute average values....
    
%% output
    
    %  nothing average intensity values are saved on disk on  asingle file
    % if tif files contain one image, one single value is recorded,
    % otherwise, intenisty profiles are saved

%% principe
    % all the image of the folder are analysed
    % 
    % the average intensity of each channel is computed 
    %
    % NB : only uint8 and uint16 values are considered
    
%% use
    % IntensitesMoyennes
    
%% Comments
    %writen first for Joel Passicousset PhD InetnsitesMoyennes
  
    
%% Author
    % MF Devaux 
    % INRA Nantes BIA-PVPP
    
%% date
    % 21 juin 2016

%% context variables 
orig=pwd;           % returns the current directory


%% start

if nargin >1
    error('use: meanIntensity');
end


%% input
% image folder
[~,rfolder]=uigetfile({'*.tif'},'first image to be analysed','*.tif');
cd(rfolder)
listim=dir('*.tif');
nbim=length(listim);


seuil=inputNumber('grey level threshold (average values of pixels with grey level over threshold will be taken into acccount)','def',0,'vmin',0);




%% treatement
% for each image of the folder
for i=1:nbim
     cd(rfolder)
     nom=listim(i).name;        % image name
     disp(nom);
     info=imfinfo(nom);
     nim=length(info);
    
    % read the tiff image
    for j=1:nim
        fprintf('%d ',j);
        tmp=imread(nom,j);
        if j==1
            im=tmp;
       else
            im(:,:,j)=tmp;
        end;
    end
    fprintf('\r\n');
    
    % initialisation
    if i==1
        % intensity data
        switch class(im)
            case 'uint16'
                IntMoy.d=uint16(zeros(nbim,nim));
            case 'uint8'
                IntMoy.d=uint8(zeros(nbim,nim));
            otherwise
                error('class %s not tacken into account',class(im));
        end;
        if nim>1
            [fnameVar,rfnameVarFolder]=uigetfile({'*.txt'},sprintf('Name of variable for the %d chanlles of the multi-tif image',nim),'*.txt');
            cd(rfnameVarFolder);
            nameVar=readStringVec(fnameVar);
        else
            nameVar='GLintensity';
        end;
    end;
    
    % maximum intensities of each channel
    mesInt=zeros(1,nim);
    for j=1:nim
        tmp=im(:,:,j);
        mesInt(j)=mean(tmp(im(:,:,j)>seuil));
    end;
         
    IntMoy.d(i,:)=round(mesInt);
    
end;
        
    
% save intenisty data
if ~exist('meanIntensity','dir')
    mkdir('meanIntensity')
    
end;
cd('meanIntensity')
sfolder=pwd;

IntMoy.i=char(listim.name);
IntMoy.v=nameVar;

sname=strcat('MeanIntensity',num2str(seuil));
writeDIV(IntMoy,sname);

                           

%% fucntion tracking
% remplissage d'un fichier trace pour indiquer les t�ches effectu�es
% actualisation du fichier trace.txt
fic=fopen(strcat(sname,'.track.txt'),'w');
if fic==0
    errordlg('probl�me d''�criture du fichier <track.txt>');
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'-\t Grey level avergae intensity of images\r\n');
fprintf(fic,'__________________________________________________________________________\r\n');

fprintf(fic,'\r\n\r\n');


fprintf(fic,'image folder : %s\r\n',rfolder);

fprintf('grey level threshold  (average values of pixels with grey level over threshold will be taken into acccount): %d\r\n\r\n',seuil);
fprintf(fic,'average intensities saved in folder : %s\r\n',sfolder);
fprintf(fic,'average intensities  saved in file : meanIntensity.txt\r\n');

fprintf(fic,'\r\n\r\n');

if exist('fnameVar','var')
    fprintf(fic,'name of channel file : %s\r\n',fnameVar);
    fprintf(fic,'read in folder : %s\r\n',rfnameVarFolder);
end

% save of function used
fprintf(fic,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fic,'function name: %s ',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);
%fprintf(fic,'__________________________________________________________________________\r\n');

fclose(fic);

%% end

cd (orig)