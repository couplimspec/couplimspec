function GLhisto(seuil)
%
%   programme de calcul d'histogramme des niveaux de gris � partir
%   d'un seuil donn� par l'utilisateur
%
%   programme �crit dans le cadre du 
%   Projet Tomate Texture du P�ricarpe
%
%   calcul des histogrammes de niveau de gris des images du 
%   r�pertoire courant
%
%   si les images sont des images couleurs, l'mage est convertie en niveaux
%   de gris et l'histogramme de l'image de niveau de gris est calcul�e
%   les histogrammes individuels des trois canaux sont �galement calcul�s
%   
%   param�tres d'entr�e
%       valeur de seuil � partir de laquelle les niveaux de gris sont consid�r�s
%       dans l'image en niveaux de gris
%   
%   param�tre de sortie
%       cr�ation d'un fichier histo_peri.seuil.txt dans le r�pertoire courant
%       contenant l'ensemble des histogrammes de niveaux de gris des images
%       pour les images couleur cr�ation �galement des fichiers
%       histo_peri.R(V ou B).seuil.txt
%   
% Principe 
%   pour toutes les images du r�pertoire courant, lire les images, 
%   les convertir en noir et blanc, 
%   %       
% usage : 
%       histo_seuil(20);
%       hp=lire('histo_20');
%       figure
%       plot(str2num(hp.v),hp.d');
%
% Auteur MF Devaux 
%        Version du 5 mars 2007
%           modification pour les noms de sauvegarde des fichiers
%           BIA-PV
%
%	     Version 1 du 5/2/2002
%                   + L. Ait-Ameur Programme AQS Tomate Texture
%        URPOI MicMac
%        Version 2 du 31 mars 2006
%                   pour la prise en compte de formats de fichier
%                   quelconque
%           BIA-PV

% variable de d�part
porig=pwd;


% test param�tres d'entr�e
if (nargin~=0)&&(nargin~=1)
	error('Usage : histo_seuil ou histo_seuil(seuil)')
end;


% variables g�n�rales : 
[nom,nomrep]=uigetfile({'*.tif';'*.bmp';'*.jpg';},'s�lection des images');
cd(nomrep)
k=findstr(nom,'.');
k=k(length(k));
extensionim=nom((k+1):length(nom));

% lecture de la s�quence d'images
listim=dir(sprintf('*.%s', extensionim));
nbim=size(listim,1);

% d�termination du type des images
%lecture de la premi�re image
imc=imread(listim(1).name);
typim=class(imc);
if ~(strcmp(typim,'uint8')||strcmp(typim,'uint16'))
    error('type d''image non g�r� actuellement')
end;
% NB : le type uint16 est pr�vu pour les images LEICA qui sont cod�es entre
% 0 et 4096 niveaux de gris

if nargin==0                % entr�e manuelle du seuil
    mini=0;
    if strcmp(typim,'uint8')
        maxi=255;
    else
        maxi=256*256-1;
    end;
    rep=0;
    while ~rep
        rep=inputdlg('seuil de niveau de gris : ','seuil de niveau de gris',1,{'0'});
        seuil=str2num(char(rep));
        if seuil<mini || seuil>maxi
            rep=0;
        else
            rep=1;
        end;
    end;
end;


seuil_gris=seuil;      % seuil de niveaux de gris � partir duquel 
                       % les pixels sont condid�r�s pour le calcul des histogrammes de niveaux de gris 
                                
% sauvegarde des r�sultats
[noms,reps]=uiputfile('*.txt','sauvegarde des r�sultats',strcat('histo.',num2str(seuil),'.txt'));
k=findstr(noms,'.');
p=k(length(k)-1);
nomgens=noms(1:(p-1));
                       

% boucle sur toutes les images
for i=1:nbim
    listim(i).name
    imc=imread(listim(i).name);
    if (class(imc)~=typim)
        error('toutes les images ne sont pas du meme type');
    end;
        
    if size(size(imc),2)==3
        %im=noir_et_blanc(imc);
        im=rgb2gray(imc);
    else
        im=imc;
    end;
    
    % d�termination de la zone d'int�ret du p�ricarpe
    
    zone_peri=im>=seuil_gris;
         
    % s�lection des valeurs de zone peri
    %val_gris=im(zone_peri);
    
    if strcmp(typim,'uint8')
        histo_peri.d(i,:)=imhist(im(zone_peri));
    else if strcmp(typim,'uint16')
            histo=imhist(im(zone_peri),65536);
            histo_peri.d(i,:)=histo;
        end;
    end;
    histo_peri.d(i,:)=histo_peri.d(i,:)/sum(histo_peri.d(i,:));
    
    if size(size(imc),2)==3
        im=imc(:,:,1);
        histo_perir.d(i,:)=imhist(im(zone_peri));
        im=imc(:,:,2);
        histo_periv.d(i,:)=imhist(im(zone_peri));
        im=imc(:,:,3);
        histo_perib.d(i,:)=imhist(im(zone_peri));
        histo_perir.d(i,:)=histo_perir.d(i,:)/sum(histo_perir.d(i,:));
        histo_periv.d(i,:)=histo_periv.d(i,:)/sum(histo_periv.d(i,:));
        histo_perib.d(i,:)=histo_perib.d(i,:)/sum(histo_perib.d(i,:));
    end;
end;


% sauvegarde des r�sultats
cd(reps)

histo_peri.i=char(listim.name);

if strcmp(typim,'uint8')
    histo_peri.v=num2str((0:255)');
else if strcmp(typim,'uint16')
        histo_peri.v=num2str((0:4095)');
    end;
end;

writeDIV(histo_peri,strcat(nomgens,'.',num2str(seuil)));

if exist('histo_perir','var');
    histo_perir.i=char(listim.name);
    histo_periv.i=char(listim.name);
    histo_perib.i=char(listim.name);
    histo_perir.v=histo_peri.v;
    histo_periv.v=histo_peri.v;
    histo_perib.v=histo_peri.v;
    writeDIV(histo_perir,strcat(nomgens,'.r.',num2str(seuil)));
    writeDIV(histo_periv,strcat(nomgens,'.v.',num2str(seuil)));
    writeDIV(histo_perib,strcat(nomgens,'.b.',num2str(seuil)));
end;
    
    
% fin
cd(porig)