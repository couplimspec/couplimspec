function createImageSizeFile(imFilename,tpix)

%% create the size.txt file associated to an ipmage for image registration
%
%% parametres d'entree
%
%   no parameter or
%
%   tpix=pixel size as a number
%   imFilename : name of image recorded on the disk as tif file
%
%
%% parametres de sortie
%   create fichier imFilename.size.txt  in the place of imFilename
%
%% principe
%  nombre de ligne nombre de colonne taille du pixel et taille du champ de
%  vue en ligne et colonne
% fichier texte enregistré sur le disuqe et dont le nom suit le nom de
% l'image

%% Usage :
%  createImageSizeFile

%% auteurs :
%   MF Devaux
%   BIA PVPP
%   17 octobre 2017
%   26 mars 2020 for proposal 20190336
%   9 avril 2020 : to be called in a fucntion : include an input parameter
%   = pixelsize

%% contexte
% projet SOLEIL degradation enzymatqiue
% proposal 20150929

%% start
orig=pwd;

if nargin>2
    error('use createImageSizeFile or createImageSizeFile(imFilename,taillePixel)');
end

%% input

if nargin==0
    [imFilename,rfolder]=uigetfile({'*.tif'},'select image','*.tif');
    tpix=inputNumber('pixel size');
else
    if ~ischar(imFilename)
        error('image filename expected as parameter 1');
    else
        [~,~,ext] = fileparts(imFilename);
        if ~strcmp(ext,'.tif')
            error('tif image expected');
        end
    end
    if ~isnumeric(tpix)
        error('number expected as pixel size')
    end
    rfolder=pwd;
end


%% read image information

cd(rfolder)
metadata=imfinfo(imFilename);
nblig=metadata.Height;
nbcol=metadata.Width;



%% treatment
variable=char('nblig','nbcol','tpix','tlig','tcol');
d.i=strrep(imFilename,'.tif','');
d.v=variable;
d.d=[nblig nbcol tpix tpix*nblig tpix*nbcol];



%% save

writeDIV(d,strrep(imFilename,'.tif','.size.txt'));


%% matlab function tracking

% no fucntion tracking

%% end

cd (orig)