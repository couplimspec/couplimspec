function sizIm

%% etalonnage d'une image visible issue du macrofluo
%
%% param�tres d'entr�e
%
%   pas de parametres d'entree
%
%
%% param�tres de sortie
%   fichier size.txt :
%
%% principe
%  nombre de ligne nombre de colonne taille du pixel et atille du champ de
%  vue en ligne et colonne
% fichier texte enregistr� sur le disuqe et dont le nom suit le nom de
% l'image

%% Usage :
%  sizeIm

%% auteurs :
%   MF Devaux
%   BIA PVPP
%   17 octobre 2017

%% contexte
% projet SOLEIL degradation enzymatqiue
% proposal 20150929

%% start
orig=pwd;

%% input

[fname,rfolder]=uigetfile({'*.tif'},'name of visible image from macroscope','*.tif');


[sname,sfolder]=uiputfile({'*.tif'},'save resulting image as',fname);

%% read

cd(rfolder)
im=imread(fname);
im=rgb2gray(im);


metadata=imfinfo(fname);

tpix=metadata.UnknownTags(2).Value;



%% treatment
figure
imshow(im,[])


subim=yesno('select sub image ?');

if subim
    % find subimage
    h=imrect;
    
    pos=round(h.getPosition);
    pause()
    
    if pos(1)<1
        pos(1)=1;
    end
    
    if pos(2)<1
        pos(2)=1;
    end
    
    if (pos(1)+pos(3)-1)>size(im,2)
        pos(3)=size(im,2)-pos(1)+1;
    end
    
    if (pos(2)+pos(4)-1)>size(im,1)
        pos(4)=size(im,1)-pos(2)+1;
    end;
    
    ims=im((pos(2):(pos(2)+pos(4)-1)),pos(1):(pos(1)+pos(3)-1),:);
    
    im=ims;
end;




%% save

cd(sfolder)
imwrite(im2uint8(im),sname,'tif','compression','none');

variable=char('nblig','nbcol','tpix','tlig','tcol');
d.i=strrep(sname,'.tif','');
d.v=variable;
d.d=[size(im,1) size(im,2) tpix tpix*size(im,1) tpix*size(im,2)];

writeDIV(d,strcat(sname,'.size.txt'));

%% matlab function tracking

fid=fopen(strrep(sname,'.tif','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Macrofluoreference Image \r\n');
fprintf(fid,'______________________________________________________________________________________\r\n');

fprintf(fid,'\r\nStart with reference image of bloc:%s\r\n',fname);
fprintf(fid,'Folder : %s\r\n',rfolder);

if subim
    fprintf(fid,'\r\n \r\n');
    fprintf(fid,'Sub image selected \r\n');
end


fprintf(fid,'\r\n \r\n');
fprintf(fid,'Image save as:%s\r\n', sname);
fprintf(fid,'\ in folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)