  function hplot=plotdsoGen(hfig,xvar,data,xlab,ylab,titre,varargin)

 %% description
    % plot spectra in a view figure using the plot function and writting
    % xlabel and ylabel
    
%% input
    % hfig: handle of the figure
    % xvar: variable for x axis
    % data: data table of spectra
    % xlab, ylab: x and ylabel
    % titre : title of figure
    % coul : map of color
    % lw : line width     
    
%% output
    
    % 

%% principe
    % spectra are drawn using the plot function
    % x and y labels are written
    % 

%% use
    % hfig=figure
    % plotdsoGen(hfig,xvar,data,xlab,ylab,titre)
    
    
%% Comments
    % review for couplImSpec toolbox
    % written to generalise plotdso
    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 14 f�vrier 2014
    % 19 aout 2013
    % 6 f�vrier 2014    couplImSpec
    % 10 f�vrier 2017 : zero line for loadings
    % 22 mai 2017 suite...
    % 8 octobre 2019 : xlimite...

%% test param
fcoul=0;
flw=0;
flz=0;
if length(varargin)==1
    varargin=varargin{1};
end
nargplus=length(varargin);

i=1;
while i<=nargplus
    switch (varargin{i})
        case 'linezero'
            flz=1;
            i=i+1;
        case 'color'
            fcoul=1;
            coul=varargin{i+1};
            i=i+2;
        case 'linewidth'
            flw=1;
            lw=varargin{i+1};
            i=i+2;
        otherwise
            error('unexpected argument %s',char(varargin{i}));
    end
end
  


%% treatement
figure(hfig);
if fcoul 
    if flw
        hplot=plot(xvar,data','color',coul,'linewidth',lw);
    else
        hplot=plot(xvar,data','color',coul);
    end
else
    if flw
        hplot=plot(xvar,data','linewidth',lw);
    else
        hplot=plot(xvar,data');
    end
end

if flz
    hold on
    xlimite=get(gca,'xlim');
    plot(xlimite,[0 0 ],':k','linewidth',1);
    hold off
end

xlabel(xlab,'fontsize',16);
ylabel(ylab,'fontsize',16);
title(titre,'fontsize',16);
set(gca,'fontsize',14)


%% matlab function tracking  

% no function tracking

%% end


    