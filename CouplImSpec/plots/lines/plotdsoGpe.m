  function plotdsoGpe

 %% description
    % plot of spectra from a data set object according to groups
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input
    % nothing
    
    
%% output
    
    % nothing

%% principe
    % spectra are drawn using the plot function
    % axis are reversed depending on the method
    % x, y labels, title are extrated from dso  data
    % all the spexctra are drawn in black
    % the spectra of each group are added as red and thick curves
    % one figure is generated per group
    % a last figure of the average spectra is also drawn
    % all figures are saved

%% use
    % plotdsoGpe
    
    
%% Comments
    % review for couplImSpec toolbox
    % from plotdso and courbegpe
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 31 aout 2020 : nouvelles couleurs
    % 4 avril 2014
    % 1 avril 2014
    % 3 mars 2011
    % 14 mars 2011 : flag for a new figure
    % 11 mai 2012 : no label for other images
    % 8 novembre 2013 : pour les groupes

%% context variables 
orig=pwd;           % returns the current directory
% color
% coul=[0 0 0 ;...
%     1 0 0;...
%     0 1 0;...
%     0 0 1;...
%     0.75 0.75 0;...
%     0 1 1;...
%     0.5 0.5 0.5;...
%     0.75 0.25 0;...
%     0.25 0.75 0;...
%     0 0.25 0.75; ...
%     0.5 0 0 ; ...
%     0 0.5 0;...
%     0 0 0.5];
% coul=[coul;coul;coul;coul];
% coul=[coul;coul;coul;coul];
coul=[0 0 1;1 0 0;0 1 0;1 1.034483e-01 7.241379e-01;1 8.275862e-01 0;5.172414e-01 5.172414e-01 1;0 0 1.724138e-01;...
    6.206897e-01 3.103448e-01 2.758621e-01;0 1 7.586207e-01;0 3.448276e-01 0;0 5.172414e-01 5.862069e-01;...
    5.862069e-01 8.275862e-01 3.103448e-01;9.655172e-01 6.206897e-01 8.620690e-01;9.655172e-01 5.172414e-01 3.448276e-02];
coul=[coul;coul;coul;coul];


%% input

if nargin ==0
     % input image
     [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');
    
     cd(repl)
     ims=loaddso(nom);
    
     sauve=1;
     sfolder=pwd;
     %newfig=1;
     
     label=ims.label{1};
     [nomcode, nom_repcode] = uigetfile({'*.txt'},'name of code file'); 
     cd(nom_repcode)
     code=readStringVec(nomcode);    % code d'identificaton des groupes
     dep=inputdlg({['starting position of codes in label : ex : ' label(1,:)]},'position of code in label',1,{'1'},'on');
     dep=str2num(char(dep)); %#ok<*ST2NM>
    
    
end
     
if nargin >0
     error('use: plotdsoGpe');
end

%% treatement

% find groups
gpe=findGroups(label,code,dep);

ngpe=size(code,1);

% initialisation of moy
moy=zeros(ngpe,size(ims.data,2));

% find unit dso version 1 and 2
unit='';
if isfield(ims.userdata,'acquisition') 
    if isfield(ims.userdata.acquisition,'unit')
        unit=ims.userdata.acquisition.unit;
    end
end

xlab=[ ims.title{2} ' (' unit ')'];

titre=ims.name;

% find method dso version 1 and 2
if isfield(ims.userdata,'method') 
    method=ims.userdata.method;
else
    if isfield(ims.userdata.acquisition,'method')
        method=ims.userdata.acquisition.method;
    end
end


switch method
    case 'CONFOCAL-Nantes'
        ylab='confocal fluorescence intensity';
        [exc,em]=decodeDsoSpectralConfocalVar(ims.label{2});
        xlab='emission wavelengths (nm)';
        
        for i=1:ngpe
            if sum(gpe==i)>0
                h=figure;
                plotdsoConfocal(h,exc,em,ims.data,xlab,ylab,strcat(titre,'-',code(i,:)),0,'color','k');
                hold on
                plotdsoConfocal(h,exc,em,ims.data(gpe==i,:),xlab,ylab,'',0,'color','r',2);
                hold off
                if sauve
                    cd(sfolder)
                    sname=strcat(ims.name,'.',num2str(dep),'.',code(i,:),'.sp.png');
                    saveas(gcf,sname,'png');
                end
                % mean of group i
                moy(i,:)=mean(ims.data(gpe==i,:),1);
            end
        end
        h=figure;
        [i,~]=find(moy==max(moy(:)));
        plotdsoConfocal(h,exc,em,moy(i,:),xlab,ylab,strcat(titre,'-mean'),0,'color','w');
        hold on
        ia=0;
        iaff= false(1,max(gpe));
        for i=1:ngpe
            if sum(gpe==i)>0
                iaff(i)=1;
                ia=ia+1;
                hm(ia)=plotdsoConfocal(h,exc,em,moy(i,:),xlab,ylab,strcat(titre,'-mean'),0,'color',coul(i,:),2);
                hold on
            end
        end
        if exist('hm','var')
            legend(hm,code(iaff,:));
        else
            error('no codes find in labels. Wrong position or wrong code file?');
        end
        if sauve
           cd(sfolder)
           sname=strcat(ims.name,'.',num2str(dep),'.',code(i,:),'.moy.png');
           saveas(gcf,sname,'png');
        end
           
     case 'MACROFLUO-Nantes'
        ylab='Macrofluo fluorescence intensity';
        xlab='Macrofluo images';
        
        for i=1:ngpe
            if sum(gpe==i)>0
                h=figure;
                plotdsoMacrofluo(h,ims.label{2},ims.data,xlab,ylab,'','color','k');
                hold on
                plotdsoMacrofluo(h,ims.label{2},ims.data(gpe==i,:),xlab,ylab,'','color','r','linewidth',2);
                hold off
                title(strcat(titre,'-',code(i,:)),'fontsize',16);
                if sauve
                    cd(sfolder)
                    sname=strcat(ims.name,'.',num2str(dep),'.',code(i,:),'.sp.png');
                    saveas(gcf,sname,'png');
                end
                % mean of group i
                moy(i,:)=mean(ims.data(gpe==i,:),1);
            end
        end
        h=figure;
        [i,~]=find(moy==max(moy(:)));
        plotdsoMacrofluo(h,ims.label{2},moy(i,:),xlab,ylab,strcat(titre,'-mean'),'color','w');
        hold on
        ia=0;
        iaff= false(1,max(gpe));
        for i=1:ngpe
            if sum(gpe==i)>0
                iaff(i)=1;
                ia=ia+1;
                hm(ia)=plotdsoMacrofluo(h,ims.label{2},moy(i,:),xlab,ylab,strcat(titre,'-mean'),'color',coul(i,:),'linewidth',2);
                hold on
            end
        end
        if exist('hm','var')
            legend(hm,code(iaff,:));
        else
            error('no codes find in labels. Wrong position or wrong code file?');
        end        
        if sauve
           cd(sfolder)
           sname=strcat(ims.name,'.',num2str(dep),'.',code(i,:),'.moy.png');
           saveas(gcf,sname,'png');
        end
           
     

    otherwise
        switch method
            case 'INFRARED'
                ylab='infrared absorbance';
                
            case 'RAMAN'
                ylab='Raman intensity';
            
            case 'FLUORESCENCE'
                ylab='fluorescence intensity';
            
            otherwise
                ylab=sprintf('%s intensity',method);
        end
               
        for i=1:ngpe
            if sum(gpe==i)>0
                h=figure;
                plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,'','color','k');
                hold on
                plotdsoGen(h,ims.axisscale{2},ims.data(gpe==i,:),xlab,ylab,'','color','r','linewidth',2);
                hold off
                title(strcat(titre,'-',code(i,:)),'fontsize',16);
                if strcmp(method,'INFRARED') || strcmp(method,'RAMAN')
                    set(gca,'xdir','reverse')
                end
                if sauve
                    cd(sfolder)
                    sname=strcat(ims.name,'.',num2str(dep),'.',code(i,:),'.sp.png');
                    saveas(gcf,sname,'png');
                end
                % mean of group i
                moy(i,:)=mean(ims.data(gpe==i,:),1);
            end
        end
        h=figure;
        [i,~]=find(moy==max(moy(:)));
        plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,'','color','w');
        hold on
        ia=0;
        iaff=false(1,max(gpe));
        for i=1:ngpe
            if sum(gpe==i)>0
                iaff(i)=1;
                ia=ia+1;
                hm(ia)=plotdsoGen(h,ims.axisscale{2},moy(i,:),xlab,ylab,strcat(titre,'-mean'),'color',coul(i,:),'linewidth',2);
                hold on
            end
        end
        if exist('hm','var')
            legend(hm,code(iaff,:));
        else
            error('no codes find in labels. Wrong position or wrong code file?');
        end        
        if strcmp(method,'INFRARED') || strcmp(method,'RAMAN')
            set(gca,'xdir','reverse')
        end
        if sauve
           cd(sfolder)
           sname=strcat(ims.name,'.',num2str(dep),'.',code(i,:),'.moy.png');
           saveas(gcf,sname,'png');
        end
           
end



%% matlab function tracking  

% no function tracking

%% end

cd (orig)
    