  function h=plotdso(ims,newfig,sauve,sfolder,varargin)

 %% description
    % plot of spectra from a data set object 
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input
    % nothing
    %or
    %  dso : data set object or file name of dataset object
    %  newfig : flag to draw in a new figure (optional) default = 1
    %  sauve : flag for saving the figure (optional) default = 1
    %  sfolder : save folder (optional)
    %  varargin : arguments like linewidth...
    % specific arguments : 
    %       'linezero' : if present : line of 0 will be drawn on the plot :
    %       for loadings
    
    
%% output
    
    % h : handle of the figure

%% principe
    % spectra are drawn using the plot function
    % axis are reversed depending on the method
    % x, y labels, title are extrated from dso  data
    % 

%% use
    % sauve=1
    % plotdso(dso,0,sauve); 
    % plotdso
    
    
%% Comments
    % review for couplImSpec toolbox
    % initially written for F Allouche PhD thesis
    
    
%% Author
    % initial F  Allouche et MF Devaux
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 1 avril 2014 to manage newfig and save options
    % 13 f�vrier 2014
    % 3 mars 2011
    % 14 mars 2011 : flag for a new figure
    % 11 mai 2012 : no label for other images
    % 6 mars 2014 : couplImSpec
    % 10 f�vrier 2017 : add zeros lines for loadings
    % 11 mai 2017 : add zeros lines for loadings
    % 16 octobre 2020 for TELEMOS

%% context variables 
orig=pwd;           % returns the current directory

flz=0;                  % flag for 0 line in the ploat (loadings)

%% input

if nargin ==0
     % input image
     [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');
    
     cd(repl)
     ims=loaddso(nom);
          
     sauve=1;
     sfolder=pwd;
     newfig=1;
end
     
if nargin >=1 
    if ischar(ims)
        nom=ims;
         % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end
        ims=loaddso(nom);
 
    else
        if ~strcmp(class(ims),'dataset') %#ok<STISA>
            error('give a data set as first input parameter');
        end
    end
end
if nargin<=2
     sauve=1;
     sfolder=pwd;
end
if nargin ==1
    newfig=1;
end

if nargin ==3
     sfolder=pwd;
end

% if nargin>=5
%     autreargs=varargin;
% end

%% treatement
% in a separate figure or not
if newfig
    h=figure;
else
    h=gcf;
    set(h,'windowstyle','docked');
end

% find unit dso version 1 and 2
unit='';
if isfield(ims.userdata,'unit') 
    unit=ims.userdata.unit;
else
    if isfield(ims.userdata,'acquisition') 
        if isfield(ims.userdata.acquisition,'unit')
            unit=ims.userdata.acquisition.unit;
        end
    end
end
xlab=[ ims.title{2} ' (' unit ')'];

titre=ims.name;
% find method dso version 1 and 2
method='';
if isfield(ims.userdata,'method') 
    method=ims.userdata.method;
else
    if isfield(ims.userdata,'acquisition') 
        if isfield(ims.userdata.acquisition,'method')
            method=ims.userdata.acquisition.method;
        end
    end
end

switch method
    case 'INFRARED'
        ylab='infrared absorbance';
        plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,titre,varargin);
        set(gca,'xdir','reverse')

    case 'RAMAN'
        ylab='Raman intensity';
        plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,titre,varargin);
        set(gca,'xdir','reverse')

    case 'FLUORESCENCE'
        ylab='fluorescence intensity';
        plotdsoGen(h,ims.axisscale{2},ims.data,xlab,ylab,titre,varargin);

    case 'CONFOCAL-Nantes'
        ylab='confocal fluorescence intensity';
        [exc,em]=decodeDsoSpectralConfocalVar(ims.label{2});
        xlab='emission wavelengths (nm)';
        plotdsoConfocal(h,exc,em,ims.data,xlab,ylab,titre,varargin);
        
    case 'MACROFLUO-Nantes'
        ylab='Macrofluo fluorescence intensity';
        xlab='Macrofluo images';
        plotdsoMacrofluo(h,ims.label{2},ims.data,xlab,ylab,titre,varargin);
    case 'TELEMOS'
        ylab='TELEMOS fluorescence intensity';
        xlab='TELEMOS images';
        plotdsoTELEMOS(h,ims.label{2},ims.data,xlab,ylab,titre,varargin);
        
    otherwise
        ylab=sprintf('%s intensity',ims.userdata.acquisition.method'');
        plotdsoGen(h,ims.axisscale{2},ims.data',xlab,ylab,titre,varargin);
        %set(gca,'xticklabel',ims.label{2});
end



%% save 
if sauve
    cd(sfolder)
    sname=strcat(ims.name,'.sp.png');
    saveas(gcf,sname,'png');
end


%% matlab function tracking  

% no function tracking

%% end

cd (orig)
    