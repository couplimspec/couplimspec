  function plotdsoConfocal(hfig,exc,em,data,xlab,ylab,titre,varargin)

 %% description
    % plot spectra in a view figure using the plot function and writting
    % xlabel and ylabel
    
%% input
    % hfig: handle of the figure
    % exc : excitation wavelengths
    % em : emission wavelengths the size of exc and em are the same
    % data: data table of spectra
    % xlab, ylab: x and ylabel
    % titre : title of figure
    % lz : draw zero line (for loadings)
    % coul : map of color
    % lw : line width
     
    
%% output
    
    % 

%% principe
    % spectra are drawn using the plot function
    % x and y labels are written
    % 

%% use
    % hfig=figure
    % plotdsoConfocal(hfig,exc,em,data,xlab,ylab,titre,coul)
    
    
%% Comments
    % review for couplImSpec toolbox
    % written to generalise plotdso
    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 14 f�vrier 2014
    % 19 aout 2013
    % 8 novembre 2013
    % 22 mai 2017 : zero line for loadings
   

%% test param

fcoul=0;
flw=0;
flz=0;
if length(varargin)==1
    varargin=varargin{1};
end;
nargplus=length(varargin);

i=1;
while i<=nargplus
    switch (varargin{i})
        case 'linezero'
            flz=1;
            i=i+1;
        case 'color'
            fcoul=1;
            coul=varargin{i+1};
            i=i+2;
        case 'linewidth'
            flw=1;
            lw=varargin{i+1};
            i=i+2;
        otherwise
            error('unexpected argument %s',char(varargin{i}));
    end
end
  


%% treatement
figure(hfig);

debaxt=0.125;
debayt=0.1;
taillaxt=0.8;
taillayt=0.8;
haxt=axes('position',[debaxt-0.055 debayt-0.03 taillaxt+0.03 taillayt+0.035],'visible','off');
set(haxt,'ytick',[],'xtick',[])
hxl=xlabel(xlab,'fontsize',16);%,'fontweight','bold');
hyl=ylabel(ylab,'fontsize',16);%,'fontweight','bold');
ht=title(titre,'fontsize',14,'fontweight','bold');
set(hxl,'visible','on')
set(hyl,'visible','on')
set(ht,'visible','on')

% nombre de longueur d'onde d'excitation
excu=unique(exc);
nbexc=length(excu);

% nb total de variables
nbvar=length(em);
tgraphlo=taillaxt/nbvar;       % taille r�serv�e pour chaque variable

ylimite=[min(data(:)) max(data(:))];

debax=debaxt;
debay=debayt;
taillay=taillayt;


% boucle sur les longueurs d'onde d'excitation
for i=1:nbexc
    % nombre de longueur d'onde d'�mission
    nbem=sum(exc==excu(i));
    % taille du graphique associ�
    taillax=nbem*tgraphlo;
    
    % d�finition des axes
    hax=axes('position',[debax debay taillax taillay]);
    if fcoul
        if flw
            plot(hax,em(exc==excu(i)),data(:,exc==excu(i)),'color',coul,'linewidth',lw);
        else
            plot(hax,em(exc==excu(i)),data(:,exc==excu(i)),'color',coul);
        end;
    else
        if flw
            plot(hax,em(exc==excu(i)),data(:,exc==excu(i)),'linewidth',lw);
        else
            plot(hax,em(exc==excu(i)),data(:,exc==excu(i)));
        end;
    end;
    
    xlimite=[min(em(exc==excu(i))),max(em(exc==excu(i)))];
    xlim(xlimite);
    ylim(ylimite);
    
    if flw
        hold on
        plot(xlimite,[0 0 ],':k','linewidth',1);
        hold off
    end;
    
    if i>1
        set(hax,'ytick',[])
    end;
    set(gca,'box','off')
    hold on
    plot(xlimite,[ylimite(2) ylimite(2)],'k','linewidth',0.5)
    if i==nbexc
         plot([xlimite(2) xlimite(2)],ylimite,'k','linewidth',0.5)
    end

    hold off
    
    
    postx=(max(em(exc==excu(i)))+ min(em(exc==excu(i))))/2;
    posty=double(ylimite(2)*0.95);
    text(postx,posty,[num2str(excu(i)) ' nm'],'fontsize',14,'fontweight','bold','horizontalAlignment','center');
    set(gca,'fontsize',12)%,'fontweight','bold')
    
    debax=debax+taillax;
end;


%% matlab function tracking  

% no function tracking

%% end


    