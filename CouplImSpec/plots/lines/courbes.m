function d=courbes(t)
% dessine un ensemble de courbes contenues dans un tableau
%   ou dans un fichier
%
%   param�tres d'entr�e : le tableau t (t.d, t.i, t.v et t.n) (optionnel)
%       si aucun param�tre n'est donn�, le fichier � tracer est lu depuis
%       le disque
%
%   param�tre de sortie : tableau trac�
%           si t est pass� en param�tre, d=t
%           sinon d est le fichier lu sur le disque
%
% une figure est ajout�e dans laquelle sont trac�es les courbes des �chantillons
%   si les variables ne sont pas des nombres, les variables sont trac�es de
%   1 � nvar (nombre de variable) dans l'ordre du fichier
%
% Usages : courbe
%           
%          d=courbe;

%          d=lire(nom)
%          courbe(d) 
%
% Auteur : Marie-Fran�oise Devaux
%           URPOI_Parois
%           14 mai 2003
%           4 avril 2013 : on affihce les l�g�ndes que si pas trop de
%           points (<20)
%
%

if (nargin ~=0 )&&(nargin ~=1)
    error('USAGE d=courbe(<tableau (optionnel)>);')
end;

% r�pertoire de d�part
rorig=pwd;

if nargin==0
    % lecture du fichier � tracer depuis le disque
    [nomfic,repertoire]=uigetfile({'*.txt'},'entrer le nom de fichier � tracer');

    pos=findstr('.',nomfic);
    pos=pos(length(pos));
    extension=nomfic((pos+1):length(nomfic));
    nom=nomfic(1:(pos-1));

    cd(repertoire)

    d=readDIV(nom);
else
    % donn�es pass�es en param�tre
    d=t;
    nom=t.n;
end;

% trac� des courbes
figure

if size(str2num(d.v(1,:)))
    % cas de variables num�riques
    plot(str2num(d.v),d.d')
else
    % cas de variables quelconques
    plot(d.d')
end;

if size(d.i,1)<=20
    legend(d.i)
end
title(nom)

saveas(gcf,strcat(nom,'.png'),'png')
cd(rorig)


