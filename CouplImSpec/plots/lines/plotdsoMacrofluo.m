  function hplot=plotdsoMacrofluo(hfig,labvar,data,xlab,ylab,titre,varargin)

 %% description
    % plot spectra in a view figure using the plot function and writting
    % xlabel and ylabel
    
%% input
    % hfig: handle of the figure
    % labvar: variable for x axis under the form of a vector of char array
    % data: data table of spectra
    % xlab, ylab: x and ylabel
    % titre : title of figure
    % flz : flag to draw zero line for loadings
    %varargin : 
        % coul : map of color
        % lw : line width
     
    
%% output
    
    % 

%% principe
    % spectra are drawn using the plot function
    % x and y labels are written
    % 

%% use
    % hfig=figure
    % plotdsoMacrofluo(hfig,labvar,data,xlab,ylab,titre,flz,varargin)
    
    
%% Comments
    % review for couplImSpec toolbox
    % written to generalise plotdso
    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 14 f�vrier 2014
    % 20 aout 2013
    % 10 fevrier 2017 : zeros line for loadings
    % 11 mai 2017 : continued...

%% test varargin 
fcoul=0;
flw=0;
flz=0;
if length(varargin)==1
    varargin=varargin{1};
end;
nargplus=length(varargin);

i=1;
while i<=nargplus
    switch (varargin{i})
        case 'linezero'
            flz=1;
            i=i+1;
        case 'color'
            fcoul=1;
            coul=varargin{i+1};
            i=i+2;
        case 'linewidth'
            flw=1;
            lw=varargin{i+1};
            i=i+2;
        otherwise
            error('unexpected argument %s',char(varargin{i}));
    end
end
  


%% treatement
figure(hfig);
if fcoul 
    if flw
        hplot=plot(data','color',coul,'linewidth',lw);
    else
        hplot=plot(data','color',coul);
    end
else
if flw
        hplot=plot(data','linewidth',lw);
    else
        hplot=plot(data');
    end
end

if flz
    hold on
    plot(get(gca,'xlim'),[0 0 ],':k','linewidth',1);
    hold off
end;

xlabel(xlab,'fontsize',16);
ylabel(ylab,'fontsize',16);
title(titre,'fontsize',16);
set(gca,'fontsize',12);
nbvar=size(labvar,1);
set(gca,'xtick',1:nbvar);
labvar=reshape(strrep(reshape(labvar,1,nbvar*4),'-',''),nbvar,3);
labvar(:,3)=lower(labvar(:,3));
set(gca,'xticklabel',labvar);
xlim([0 nbvar+1]);


%% matlab function tracking  

% no function tracking

%% end


    