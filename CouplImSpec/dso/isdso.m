  function [test,message]=isdso(dso)

 %% description
    % test if the input is a dso 
    %       % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm

    
    
%% input

    %  dso : datasetobject
    
%% output
    
    % test =1 if dso is identifyed as a dataset
    % test= 0 otherwise
    % message : error message if test=0;

%% principe
    % test if dso is a dataset
    % attention : dataset is also a structure from matlab statistical
    % toolbox
    
%% use
    % test=isdso(don);
    
%% Comments
    % 
    
    
%% Author
    % MF devaux
    % BIA-PVPP
    % INRA Nantes
    
%% date
    % 7 mars 2014   review for couplImSpec
    % 21 octobre 2011


%% start

if nargin >1 || nargin ==0
    error('use: test = isdso(dso)');
end


%% treatement

message = '';
if ~strcmp(class(dso),'dataset')
    message=sprintf('structure is not a ''dataset''');
    test=0;
else 
    test=1;
end;


    

%% end

    