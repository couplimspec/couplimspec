  function catdso(rfolder,sname,sfolder)

 %% description
    % concatenate all the dso files of a folder into a single dso 
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input
    % nothing
    %or
    %  rfolder : inout folder
    %  sname : name of resultlting dso file
    % sfolder : save folder
    
    
%% output
    
    % 

%% principe
    % all the dso files are concatenated using the cat dataset command.
    % it is checked that variables are the sames
    % 

%% use
    % catdso
    % or
    % catdso(rfolder,sname,sfolder);
    
    
%% Comments
    % from catdso function
    % written for F Allouche PhD thesis and macrofluo images
    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 13 f�vrier 2014 for couplImSpectToolbox
    % 9 aout 20111
    % 8 octobre 2013 : cumul eds donnees userdata de type coordonnees
    % 7 d�cembre 2018 : add a class to keep the origin of the individual
    % data

%% context variables 
orig=pwd;           % returns the current directory

%% input
if nargin >3
    error('use: catdso(rfolder,sname,sfolder)');
end

if nargin >0
    if ~exist(rfolder,'dir')
        error('no directory found of name %s',rfolder)
    end    
end

if nargin ==0
    % input image
    [~,rfolder]=uigetfile({'*.dso.mat'},'name of the first spectral image','*.dso.mat');
end

if nargin <=1
    % file.dso.mat and track file will be saved in the same folder than the
    % original  file
    cd(rfolder)
    if ~exist('catdso','dir')
        mkdir('catdso')
    end
    cd('catdso');
    sfolder=pwd;
    indice=strfind(rfolder,'\');
    sname=rfolder((indice(length(indice)-1)+1):(indice(length(indice))-1));
    sname=strcat(sname,'.dso.mat');
end

if nargin ==2
    sfolder=rfolder;
end

if nargin ==3
    if ~exist(sfolder,'dir')
        error('no directory found of name %s',sfolder)
    end 
end
    

%% treatement
% liste of dso file into the folder
cd(rfolder)
liste=dir('*.dso.mat');

if isempty(liste)
    error('pas de fichiers dso.mat dans le dossier %s',rfolder);
end

% liste des fichiers
for i=1:length(liste)
    fprintf('%d: %s\n\r',i,liste(i).name)
    ims=loaddso(liste(i).name);
    
    % change dataset type to data even if it was an image type
    % after cat fundtion : data will not be managed as pure images
    ims.type='data';
  
    if i==1
        tot=ims;
        tot.class{1,1}=ones(size(tot,1),1);
        refx=ims.axisscale{2};
        userdata=ims.userdata;
        % CouplImSpec       % first dso version
        if isfield(userdata,'dX') || isfield(userdata,'dY')
            error('first version of dso. Please convert in second version using updateDso1to2Col function');
        end
        if isfield(userdata,'coord')
            error('first version of dso. Please convert in second version using updateDso1to2Col function');
        end
        
        % CouplImSpec CoordimOrig Field
        if isfield(userdata,'coordImOrig')
            flcoord=1;
        else
            flcoord=0;
        end

    else

        % test variables
        x=ims.axisscale{2};
        if sum(x-refx)~=0 
            if sum(x-refx)/sum(x)>10^-6 
                error('variables in file %s differ from variables in in file %s',liste(i).name,liste(1).name);
            else
                warning('variables  in file %s differ from variables in in file %s : sum(difference)/sum(varFichier1)=%e',liste(i).name,liste(1).name,sum(x-refx)/sum(x));
            end
        end
        ims.class{1,1}=ones(size(ims,1),1)*i;
        tot=[tot;ims]; %#ok<AGROW>
        tot.name='';
        
                
        if flcoord && isfield(ims.userdata,'coordImOrig') 
            userdata.coordimOrig=catCoordImOrig(userdata.coordimOrig,ims.userdata.coordImOrig);
        else
            flcoord=0;
            warning('field coordImOrig could not be maintained: no information in file %s',liste(i).name);
        end
             
    end
end
if ~flcoord && isfield(userdata,'coordImOrig')
    % no gestion of coordinate will be possible
    userdata=rmfield(userdata,'coordImOrig');
end

tot.name=strrep(sname,'.dso.mat','');
tot.title{1}='';
tot.userdata=userdata;

       


%% save 
cd(sfolder)
savedso(tot);
nomListFile=[tot.name '.listFile.txt'];
writeStringVec(char(liste.name),nomListFile);

%% matlab function tracking  

cd(sfolder)
tname=strcat(strrep(sname,'.dso.mat','.track.txt'));
fid=fopen(tname,'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,' Concatenate dso.mat files : individual size  \r\n');
fprintf(fid,'______________________________________________\r\n\r\n');

fprintf(fid,'\r\n');
% processed files
fprintf(fid,'Input files folder: %s\r\n',rfolder);
fprintf(fid,'\r\n');

fprintf(fid,'Contatenated files: \r\n');

for i=1:length(liste)
    fprintf(fid,'\t- %s\r\n',liste(i).name);
end

fprintf(fid,'\r\n');
fprintf(fid,'\r\n');

fprintf(fid,'Userdata field of resulting file: \r\n');

names=fieldnames(tot.userdata);
for i=1:length(names)
    fprintf(fid,'\t- %s\r\n',names{i});
end
fprintf(fid,'\r\n');

% save file
fprintf(fid,'saved file : %s\r\n',sname);
fprintf(fid,'saved list of file : %s\r\n',nomListFile);
% ouput folder
fprintf(fid,'Output folder: %s\r\n',sfolder);

fprintf(fid,'\r\n');
% save function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);


%% end

cd (orig)
    