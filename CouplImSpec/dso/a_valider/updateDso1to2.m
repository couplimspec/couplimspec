function ims=updateDso1to2(varargin)

 %% description
    % uptdate userdata fields between first dso version (F Allouche PhD) to second (couplImSpec) 
    % in the dso.mat file
    
%% input parameters can be
    % - no parameter
    % or
    % 1 - spectral data in dso format
    % 2 - save folder 
    
%% output
    
    % - nothing 
    % or
    % - the average spectrum vector as a dataset object

%% principle
    % update dso userdata fields
    % data is organised to build the dso structure as described in file
    % dso.docx (folder help of couplImSpec toolbox)
    % dso.userdata
            % image: [1x1 struct]						donn�es d�identification des pixels du fichier
            % acquisition: [1x1 struct]					donn�es issues de l�acquisition 
            % visim: [1x1 struct]						donn�es d�identification d�une image visible 
                                            %				associ�e
    % if files are read from disk, they are saved under the same name in
    % the same folder except when a different output folder is explicitely
    % given
    
   %%
    % <https://www.dropbox.com/s/8r9upsgx5j12c9m/DSO.docx>
    

%% use
    % dsoUpdate1to2;
    
    % ims=readomnic;
    % gim=dsoUpdate1to2(ims)
    
%% Comments, context
    % written for couplImSpec Toolbox
    
    
%% Author
    % MF Devaux 
    % BIA - PVPP INRA Nantes
    
%% date
    % 13 f�vrier 2014 for CouplImSpecToolbox
 

%% context variables 
orig=pwd; % returns the current directory


%% input data
if nargin == 0
    % input image
    [fname,repl]=uigetfile({'*.dso.mat'},'name of dataset file','*.dso.mat');

    cd(repl)
    dso=load(fname);
    ims=getfield(dso,char(fieldnames(dso)));
    clear dso;
   
    sfolder=repl;
end;

if nargin >= 1
    % test si l'image en entr�e est un dataset or a file
    ims=varargin{1};
    if ischar(ims)
        fname=ims;
         % test du type de fichier
        point=strfind(fname,'.');
        point=point(length(point)-1);
        ext=fname((point+1):(length(fname)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        dso=load(fname);
        ims=getfield(dso,char(fieldnames(dso)));
        clear dso;
    else if ~strcmp(class(ims),'dataset')
        error('give a data set as first input parameter');
        end;
    end;
    if ~exist('fname','var')
        fname=strcat(ims.name,'.dso.mat');
    end;
    repl=pwd;                               % reading folder
end;

if nargin == 1
    sfolder=repl;
end;

if nargin == 2
    sfolder=varargin{2};
end;

if nargin >2
    disp('Wrong number of parameter')
    error('Usage: mspim=dsoUpdate1to2(ims,sfolder');
end;

% test des param�tres de sortie
if  nargout >1
    error('wrong number of output parameter')
end;


%% uptdate userdata field
userdata=ims.userdata;

names=fieldnames(userdata);

nm=0;
for i=1:length(names)
    switch names{i}
        % already dso v2
        case {'image','acquisition'}
            % do nothing
        % visible image
        case 'visim'
            if ~isfield(userdata.visim,'name')
                % first dso version
                % visible image : not compatible with second version : new import will be
                % suggested
                userdata=rmfield(userdata,'visim');
                nm=nm+1;
                mess{nm}='Non compatibility: visible image suppressed, re-import to get the information';
                warning(mess{nm});
            else
                % do nothing
            end;
        % acquisition
        case 'dX'
            userdata.acquisition.coordStage.row=userdata.dX;
            userdata=rmfield(userdata,'dX');
        case 'dY'
            userdata.acquisition.coordStage.row=userdata.dY;
            userdata=rmfield(userdata,'dY');
        case 'method'
            userdata.acquisition.method=userdata.method;
            userdata=rmfield(userdata,'method');
        case 'unit'
            userdata.acquisition.unit=userdata.unit;
            userdata=rmfield(userdata,'unit');
        % image
        case 'coord'
            userdata.image.coord.row=userdata.coord(:,1);
            userdata.image.coord.col=userdata.coord(:,2);
            userdata=rmfield(userdata,'coord');
        % other
        case 'liste'
            userdata=rmfield(userdata,'liste');
            nm=nm+1;
            mess{nm}='Non compatibility: liste suppressed, see track file to recover the information';
            warning(mess{nm});
        case 'path'
            userdata=rmfield(userdata,'path');
            nm=nm+1;
            mess{nm}='Non compatibility: path suppressed, see track file to recover the information';
            warning(mess{nm});
            
        
        otherwise
            nm=nm+1;
            mess{nm}=sprintf('unknown field %s, just copied',names{i});
            warning(mess{nm});
    end
end;

% orig field
% if ~isfield(userdata,'orig')
%     userdata.orig.name=fname;
%     userdata.orig.pathname=repl;
%     userdata.orig.num=repmat(size(ims.data,1),1,1);
% end;
    
% replace old userdata field
ims.userdata=userdata;

%% save 
sname=ims.name;
cd(sfolder)
savedso(ims,sname)



%% matlab function tracking  

sname=strrep(sname,'.dso.mat','');
% 
fid=fopen(strcat(sname,'.track.txt'),'a');

if fid==0
     errordlg('enable to open track file');
 end;

fprintf(fid,'\r\n%s\t\r\n',datestr(now,0));
fprintf(fid,'__________________________________________________\r\n');
fprintf(fid,'update userdata field from dso 1st to 2nd version \r\n');
fprintf(fid,'__________________________________________________\r\n');

fprintf(fid,'\r\nInput dso filename: %s\r\n',fname);
fprintf(fid,'data folder: %s\r\n\r\n',repl);

for i=1:nm
    fprintf(fid,'\r\n%s',mess{i});
end;

if nm
    fprintf(fid,'\r\n\r\n');
end;
    
fprintf(fid,'\r\nsaved file name : %s.dso.mat \r\n',sname);
fprintf(fid,'save folder : %s \r\n',sfolder);


% save of function used
fprintf(fid,'\r\n__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n\r\n',repprog);


fclose(fid);


%% end

cd (orig)
    