function updateDso1to2Col

 %% description
    % uptdate userdata fields between first dso version (F Allouche PhD) to second (couplImSpec) 
    % in the dso.mat file
    % applied to all the files ina folder
    
%% input parameters can be
    % - no parameter
    % 
    
%% output
    
    % - nothing 
  

%% principle
    % update dso userdata fields
    % data is organised to build the dso structure as described in file
    % dso.docx (folder help of couplImSpec toolbox)
    % dso.userdata
            % image: [1x1 struct]						donn�es d�identification des pixels du fichier
            % acquisition: [1x1 struct]					donn�es issues de l�acquisition 
            % visim: [1x1 struct]						donn�es d�identification d�une image visible 
                                            %				associ�e
    % if files are read from disk, they are saved under the same name in
    % the same folder except when a different output folder is explicitely
    % given
    % all files of the folder are updated
    
   %%
    % <https://www.dropbox.com/s/8r9upsgx5j12c9m/DSO.docx>
    

%% use
    % dsoUpdate1to2Col;
    
    % ims=readomnic;
    % gim=dsoUpdate1to2(ims)
    
%% Comments, context
    % written for couplImSpec Toolbox
    
    
%% Author
    % MF Devaux 
    % BIA - PVPP INRA Nantes
    
%% date
    % 13 f�vrier 2014 for CouplImSpecToolbox
 

%% context variables 
orig=pwd; % returns the current directory


%% input data
if nargin == 0
    % input image
    [fname,repl]=uigetfile({'*.dso.mat'},'name of first dataset file','*.dso.mat');

    cd(repl)
    liste=dir('*.dso.mat');
    
    [~,sfolder]=uiputfile({'*.dso.mat'},'save file',fname);
end;

if nargin >0
    disp('Wrong number of parameter')
    error('Usage: updateDso1to2Col');
end;


%% loop
nbim=length(liste);
for i=1:nbim
    fname=liste(i).name;
    disp(fname)
    dso=load(fname);
    ims=getfield(dso,char(fieldnames(dso)));
    clear dso;

    updateDso1to2(ims,sfolder);
end

%% save 


%% matlab function tracking  

sname='updateDso1to2Col';
% 
fid=fopen(strcat(sname,'.track.txt'),'w');

if fid==0
     errordlg('enable to open track file');
 end;

fprintf(fid,'\r\n%s\t\r\n',datestr(now,0));
fprintf(fid,'__________________________________________________\r\n');
fprintf(fid,'update userdata field from dso 1st to 2nd version \r\n');
fprintf(fid,'__________________________________________________\r\n');

fprintf(fid,'input data folder: %s\r\n\r\n',repl);

fprintf(fid,'all files in input data folder have been updated\r\n\r\n');

fprintf(fid,'save folder : %s \r\n',sfolder);


% save of function used
fprintf(fid,'\r\n__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n\r\n',repprog);


fclose(fid);


%% end

cd (orig)
    