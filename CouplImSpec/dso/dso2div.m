  function [div]=dso2div(dso)

 %% description
    % convert dso structure into a div structure with input and output on the disk 
    
%% input

    %  nothing 
    % or
    % dso = dso structure or name of dso file
    
%% output
    
    % div structure

%% principe
    % r�cup�re les donn�es n�cessaires � la constitution de la structure
    % div et sauve au format div si option sauve
    % 

%% use
    % div=dso2div(dso);
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % review for couplImSpec
    % written to be able to use all matalb files developed for div files
    
    
%% Author
    % MF Devaux
    % INRA -BIA - PVPP
    
%% date
    % 27 mars 2014
    % 14 f�vrier 2014
    % 11 aout 2011
    % 11 mai 2012 : gestion des r�pertoires de lecture et sauvegarde

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: [div]=dso2div(dso)');
end

%% input

if nargin == 0
    % input image
    [fname,rfolder]=uigetfile({'*.dso.mat'},'name of dso file','*.dso.mat');
    sname=strrep(fname,'.dso.mat','.txt');
    
    cd(rfolder)
    dso=loaddso(fname);
     
    % ouput file
    [sname,sfolder]=uiputfile({'*.txt'},'name of the resulting file',sname);   
    sauve=1;
end;


if nargin == 1
    rfolder=pwd;
    if ischar(class(dso))
        if ~exist(dso,'file')
            error('pas de fichier de nom %s dans le dossier %s',dso,rfolder);
        end;
        if isempty(strfind(dso,'.dso.mat'))
            error('le fichier %s n''est probablement pas un fichier dso');
        end;
        sauve=1;
        sname=strrep(dso,'.dso.mat','');
        sfolder=rfolder;
        fname=dso;
        
        dso=loaddso(dso);
        
    else
        if ~strcmp(class(dso),'dataset')
            error('dso structure required as input data ')
        end
        sauve=0;
    end;
end;
        





%% treatement

% donnees
div.d=dso.data;
if ~isempty(dso.label{1})
    div.i=dso.label{1};
else if ~isempty(dso.axisscale{1})
    div.i=dso.axisscale{1};
    else
        div.i=num2str((1:size(div.d,1))');
    end
end;
if ~isempty(dso.label{2})
    div.v=dso.label{2};
else if ~isempty(dso.axisscale{2})
    div.v=dso.axisscale{2};
    else
        div.n=num2str((1:size(div.d,2))');
    end
end;

%% save 

if sauve
    cd(sfolder)
    writeDIV(div,sname);


    %% matlab function tracking  

    fid=fopen(strcat(strrep(sname,'.txt',''),'.track.txt'),'a');

    if fid==0
        errordlg('enable to open track file');
    end;

    fprintf(fid,'\r\n%s\r\n',datestr(now,0));
    fprintf(fid,'Convert dso file into div file \r\n');
    fprintf(fid,'_______________________________\r\n');

    fprintf(fid,'\r\n');
    fprintf(fid,'\r\ninput file name: %s\r\n',fname);
    fprintf(fid,'data folder: %s\r\n',rfolder);
    fprintf(fid,'\r\n');

    fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
    fprintf(fid,'data folder: %s\r\n',sfolder);
    fprintf(fid,'\r\n');

    % save of function used
    fprintf(fid,'__________________________________________________________________________\r\n');
    info=which (mfilename);
    os=computer;        % return the type of computer used : windows, mac...
    switch os(1)
        case 'P'                        % for windows
            ind=strfind(info,'\');                          
        case 'M'                        % for Mac
            ind=strfind(info,'/');
        otherwise
            ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
    end;

    repprog=info(1:(ind(length(ind))-1));
    fprintf(fid,'function name: %s ',mfilename);
    res=dir(info);
    fprintf(fid,'on %s \r\n',res.date);
    fprintf(fid,'function folder: %s \r\n',repprog);
    %fprintf(fid,'__________________________________________________________________________\r\n');

    fclose(fid);

end;

%% end

cd (orig)
    