  function [dso]=div2dso(div,method,sfolder)

 %% description
    % convert div structure into a dso structure with input and output on the disk 
    
%% input

    %  nothing 
    % or
    % div = div structure or name of div file
    % method = method of acquisition
    
%% output
    
    % dso structure

%% principe
    % 
    % 

%% use
    % dso=div2dso(div,method);
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % review for CouplImSpec
    % written to be able to use all matalb files developed for div files
    
    
%% Author
    % MF Devaux
    % INRA -BIA - PVPP
    
%% date
    % 16 mars 2015 : add method for automatic conversion
    % 27 mars 2014
    % 17 f�vrier 2014
    % 28 octobre 2011
    % 15 mai 2012 : creation de 0 de la structure dso
    % 24 novembre 2015 : bug nargin
    % 16 janvier 2018 : bug metadat associated line 134
    

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=3 && nargin ~= 2 && nargin~= 0
    error('use: [dso]=div2dso(div,method,sfolder) or div2dso');
end

%% input

if nargin == 0
    % input image
    [fname,rfolder]=uigetfile({'*.txt'},'select file '); 
    cd(rfolder);
    div=readDIV(fname);
    sname=strrep(fname,'.txt','');
    
    % input userdata
    rep=yesno('associated dso file for metadata ?');
    if rep
        [funame,rufolder]=uigetfile({'*.dso.mat'},'name of associated dso file for metadata','*.dso.mat');
    else
            %nom de la m�thode d'acquisition
        method=inputdlg('name of acquisition method');
        method=char(method);
    end;
    
       
    % ouput file
    [sname,sfolder]=uiputfile({'*.dso.mat'},'name of the resulting file',strcat(sname,'.dso.mat'));   
    sauve=1;
end;


if nargin >=2
    rfolder=pwd;
    if ischar(class(div))
        if ~exist(div,'file')&&~exist(strcat(div,'.txt'),'file')
            error('file %s not found in folder %s',div,rfolder);
        end;
        
        sauve=1;
        sname=strrep(div,'.txt','.dso.mat');
        sfolder=rfolder;
        fname=div;
        
        div=readDIV(fname);

    else
        if ~isdiv(div)
            error('div structure required as input data ')
        end
        sauve=0;
        sname='importe';
    end;
    
    if ~ischar(method)
        error('name of method expected, use: [dso]=div2dso(div,method)');
    end
    rep=0;
end;

if nargin==3
    
    sauve=1;
    
    if ~exist(sfolder,'dir')
        error('third argument is not a folder');
    end;
end;
        

%% treatement
% dso: data set object

if exist('funame','var')
    cd(rufolder)
    dso=loaddso(funame);
    dso=dso(1:size(div.d,1),1:size(div.d,2));
    cd(sfolder)
   
else
    dso=dataset;
    description='imported from a DIV file';
    dso.description=description;
    dso.userdata.acquisition.unit='';           % unit of wavenumber
    dso.userdata.acquisition.method=method;         %  image acquisition method
end;

dso.name=strrep(sname,'.dso.mat','');

dso.data=div.d;
if isempty(str2num(div.i))
    dso.axisscale{1}=1:size(div.i,1);
else
    dso.axisscale{1}=str2num(div.i);
end;
if isempty(str2num(div.v))
    dso.axisscale{2}=1:size(div.v,1);
else
    dso.axisscale{2}=str2num(div.v);
end;

dso.label{1}=div.i;
dso.label{2}=div.v;



%% save dso
if sauve
    cd(sfolder)
    savedso(dso);


    %% matlab function tracking  

    fid=fopen(strrep(sname,'.dso.mat','.dso.track.txt'),'w');

    if fid==0
        errordlg('enable to open track file');
    end;

    fprintf(fid,'\r\n%s\r\n',datestr(now,0));
    fprintf(fid,'Convert div file into dso file \r\n');
    fprintf(fid,'_______________________________\r\n');

    fprintf(fid,'\r\n');
    fprintf(fid,'\r\ninput file name: %s\r\n',fname);
    fprintf(fid,'data folder: %s\r\n',rfolder);
    if rep
        fprintf(fid,'\r\ninput associated dso file name: %s\r\n',funame);
        fprintf(fid,'associated dso data folder: %s\r\n',rufolder);
    else
        fprintf(fid, 'no associated dso file\r\n');
    end;
    
    fprintf(fid,'\r\n');


    fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
    fprintf(fid,'data folder: %s\r\n',sfolder);
    fprintf(fid,'\r\n');

    % save of function used
    fprintf(fid,'__________________________________________________________________________\r\n');
    info=which (mfilename);
    os=computer;        % return the type of computer used : windows, mac...
    switch os(1)
        case 'P'                        % for windows
            ind=strfind(info,'\');                          
        case 'M'                        % for Mac
            ind=strfind(info,'/');
        otherwise
            ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
    end;

    repprog=info(1:(ind(length(ind))-1));
    fprintf(fid,'function name: %s ',mfilename);
    res=dir(info);
    fprintf(fid,'on %s \r\n',res.date);
    fprintf(fid,'function folder: %s \r\n',repprog);
    %fprintf(fid,'__________________________________________________________________________\r\n');

    fclose(fid);

end;

%% end

cd (orig)
    