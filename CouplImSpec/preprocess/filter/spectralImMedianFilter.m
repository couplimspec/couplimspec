function [ims] =spectralImMedianFilter(varargin)

%% description

    % median filtering of each channel of a spectral image
    
%% input 

    % soit pas de parametres
    % soit nom du fichier
    % soit dataset object en memoire matlab
    
    % windowsize : taille du filtre median (filtre carre)
    % sfolder : repertoire de sauvegarde
    
%% output

    % ims : corrected dataset object

%% principe

   % la fonction est medfilt2.m de matlab
   % tous les canaux de l'image spectrale sont traités indépendemment les
   % uns des autres
   % le dataset object est sauve en format .dso.mat, le nom par defaut
   % est celui du fichier de depart auquel on ajoute l'extension.med<size>. 

%% use
 
    %   ims = spectralImMedianFilter
    %
    %      size=3
    %       ims = corriger_spike(nomfichier,size,nomsauve)

%% comments

    % fonction ecrite pour les spectres acquis en fluorescence sur
    % au macrofluorimètre 
    % manip MAXXIS - thèse Mathias Corcel
    
%% Author
    % MArie-Francoise Devaux, 
    % INRA - BIA - PVPP

%% Date 
    % 29 octobre 2015
    

%% context variables

porig=pwd;          % returns the current directory

%% default values

sizeMed = 3;           % filter size

%% start

if nargin >3
    error('incorrect number of argument : spectralImMedianFilter or spc = spectralImMedianFilter(nomfic,size,sfolder)');
end;

%% input
if nargin >=1
    if ischar(varargin{1})
        nom=varargin{1};
        % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        repl=pwd;
    else if strcmp(class(varargin{1}),'dataset')
            ims=varragin;
            nom=ims.name;
        else
            error('incorrect arguments : spectralImMedianFilter or spc = spectralImMedianFilter(nomfic,size,sfolder)');
        end;
    end;
end;

if nargin >=2 
    sizeMed=varargin{2};
    if sizeMed <0
        error('invalid value for median filter size')
    end;
    reps=pwd;
end;

if nargin ==3
    reps=varargin{3};
end;

if nargin==0
    [nom,repl]=uigetfile({'*.mat'},'name of spectral dso file to filter : ','*.dso.mat');
    cd(repl)
    point=strfind(nom,'.');
    point=point(length(point)-1);
    ext=nom((point+1):(length(nom)));
    
    sizeMed=inputNumber('size of median filtering (square shape)','def',3,'vmin',0);    
end;

% mode interactif pour le choix des parametres
if nargin == 0
    verbose =1;
else
    verbose =0;
end;
   
if verbose
    % nom de sauvegarde du fichier resultat
    if exist('nom','var')
        noms=strrep(nom,ext,strcat('.med',num2str(sizeMed),'.dso.mat'));
    else
        noms=strcat('*.med',num2str(sizeMed),'.dso.mat');
    end
    [noms,reps]=uiputfile({'*.mat'},'save dso file name',noms);

end;

%% treatment

% load dso file
disp('read');
ims=loaddso(nom);
if ~verbose
        noms=strcat(ims.name,'.med',num2str(sizeMed));
end


sim=ims.imagedata;

nbl=size(sim,1);
nbc=size(sim,2);
nbs=size(sim,3);

for i=1:nbs
    sim(:,:,i)=medfilt2(sim(:,:,i),[sizeMed sizeMed],'symmetric');
    disp(i)
end

ims.data=reshape(sim,nbl*nbc,nbs);
ims.name=noms;

    
%% save
disp('save');

cd(reps)
savedso(ims,noms,reps)
 
    

%% matlab function tracking

fic=fopen(strcat(strrep(noms,'.dso.mat',''),'.track.txt'),'w');
if fic==0
    errordlg('probleme d''ecriture du fichier %s',strcat(strrep(noms,'.dso.mat',''),'.track.txt'));
end;

fprintf(fic,'%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'\t\t Spectral image median filtering\r\n\r\n');

% parametres de correction 
fprintf(fic,'Parameters :\r\n');
fprintf(fic,'\t- Matlab filtering function: medfilt2\r\n\');
fprintf(fic,'\t- Size of filter (Square shape): %d\r\n',sizeMed);
fprintf(fic,'\r\n');

fprintf(fic,'Input folder :\r\n\t- %s\r\n',repl);
fprintf(fic,'\t- Filename : %s\r\n',nom);
fprintf(fic,'\r\n');
fprintf(fic,'Save file in : \r\n\t- %s\r\n',reps);
fprintf(fic,'\t- Filename : %s \r\n',noms);

% sauvegarde du programme utilis?
fprintf(fic,'__________________________________________________________________________\r\n');
info=which(mfilename);
repprog=fileparts(info);
fprintf(fic,'Software : %s ',mfilename);
res=dir(info);
fprintf(fic,'last save: %s\r\n',res.date);
fprintf(fic,'\r\nsoftware folder : %s \r\n',repprog);

fclose(fic);


%% end
cd(porig)

    
            
            
            
                
        