function [windowSize,sfolder]=dsoSmoothSpectra(varargin)
%
%% description
% smooth all spectra of a data set object using moving average filtering
%
%% input
%           soit pas de param�tres
%           soit
%               1 - nom du fichier
%                   ou dataset object en m�moire matlab
%               2 - windowsize : taille du lissage en nombre de point
%               3 - sfolder : r�pertoire de sauvegarde
%
%% output
%           nothing : smoothed data set daved on disk
%
%% principe
%
%: tous les spectres du fichier de d�part sont trait�s
%            la m�thode utilis�e est un lissage par moyenne mouvante
%               la taille de la fen�tre est donn�es par l'utilisateur
%
%
%% Use :
%       dsoSmoothSpectra(<dsoNameFile>,<readFolder>,<windowSize>,<dsoSaveNameFile>,<saveFolder>)
%
%
%% Auteur :
%           MArie-Fran�oise Devaux et Sylvie Durand
%           INRA - BIA - PVPP
%
%% Date : 12 mars 2010
%        8 fevrier 2018
%        21 novembre 2019
%
%

%% context variables
porig=pwd;

%% input

if nargin~=0 && nargin~=5
    error('use: dsoSmoothSpectra(<dsoNameFile>,<readFolder>,<windowSize>,<dsoSaveNameFile>,<saveFolder>)');
end

if nargin == 5
    if ischar(varargin{1})
        nom=varargin{1};
    else
        error('use: dsoSmoothSpectra(<dsoNameFile>,<readFolder>,<windowSize>,<dsoSaveNameFile>,<saveFolder>)');
    end
    if ischar(varargin{2})
        rfolder=varargin{2};
        if ~exist(rfolder,'dir')
            error('%s folder does not exist',rfolder);
        end
    else
        error('use: dsoSmoothSpectra(<dsoNameFile>,<readFolder>,<windowSize>,<dsoSaveNameFile>,<saveFolder>)');
    end
    
    cd(rfolder)
    
    % test du type de fichier
    point=strfind(nom,'.');
    point=point(length(point)-1);
    ext=nom((point+1):(length(nom)));
    if ~strcmp(ext,'dso.mat')
        error('format .dso.mat expected')
    end
    ims=loaddso(nom);
    
%     if isinteger(varargin{3})
         windowSize=varargin{3};
%     else
%         error('interger expected as window size');
%     end
%     
%     
    if ischar(varargin{4})
        snom=varargin{4};
    else
        error('use: dsoSmoothSpectra(<dsoNameFile>,<readFolder>,<windowSize>,<dsoSaveNameFile>,<saveFolder>)');
    end
    if ischar(varargin{5})
        sfolder=varargin{5};
        if ~exist(sfolder,'dir')
            mkdir(sfolder);
        end
    else
        error('use: dsoSmoothSpectra(<dsoNameFile>,<readFolder>,<windowSize>,<dsoSaveNameFile>,<saveFolder>)');
    end
    
    verbose=0;
    
end


if nargin==0
    [nom,rfolder]=uigetfile({'*.mat'},'nom du fichier des spectres a traiter : ','*.dso.mat');
    cd(rfolder)
     
    ims=loaddso(nom);
    
    % window size
    ok=0;
    while ~ok
        windowSize=inputNumber('size of smoothing window (odd number please)');
        if abs(round(windowSize/2)*2-windowSize)==1
            ok=1;
        end
    end
    
    snom=strcat(ims.name,'.sm.dso.mat');
    
    % sfolder=strcat(rfolder,'.sm');
    cd ..
    
    [snom,sfolder]=uiputfile({'*.mat'},'save resulting file as dso: ',snom);
    
    verbose=1;
end


%%traetment
if verbose
    close all
    % affichage des spectres de d�part
    if size(ims,1)<1000
        plotdso(ims,1,0)
    else
        plotdso(ims(1:10:end,:),1,0)
    end   
end

% matrix of spectra
sp=ims.data;

% moving average use : movmean matlab function
sp=sp'; % transpose


% resulting spectr
spltf=movmean(sp,windowSize);
spltf=spltf';   % transpose

% remove forst and last point of the moving average
spltf=spltf(:,(floor(windowSize/2)+1):(end-floor(windowSize/2)));

imsc=ims(:,(floor(windowSize/2)+1):(end-floor(windowSize/2)));
imsc.data=spltf;
imsc.name=strrep(snom,'.dso.mat','');

if verbose
    if size(ims,1)<1000
        plotdso(imsc,1,0)
    else
        plotdso(imsc(1:10:end,:),1,0)
    end
    title('spectres liss�s');
end


if ~exist(strcat(sfolder,'plotdso'),'dir')
    mkdir(strcat(sfolder,'plotdso'))
end

cd(sfolder)
cd('plotdso')

if size(ims,1)<1000
    plotdso(imsc,0,1)
else
    plotdso(imsc(1:10:end,:),0,1)
end


%% sauvegarde des r�sultats
cd(sfolder)
savedso(imsc);

%% trace de l'ex�cution du programme

fic=fopen(strcat(strrep(snom,'.dso.mat',''),'.track.txt'),'w');
if fic==0
    errordlg('probl�me d''�criture du fichier %s',strcat(strrep(noms,'.dso.mat',''),'.track.txt'));
end

fprintf(fic,'%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'\t\t lissage des spectres par moyenne mouvante\r\n\r\n');
fprintf(fic,'R�pertoire de lecture du fichier des spectres :\r\n\t- %s\r\n',rfolder);
fprintf(fic,'\r\nFichier : %s\r\n',nom);
fprintf(fic,'\r\n\r\n');

% param�tres de correction
fprintf(fic,'Param�tres de lissage : \r\n\r\n');
fprintf(fic,'\tfonctions matlab: movmean.m\r\n');
fprintf(fic,'\tlargeur de la fen�tre : %d\r\n',windowSize);
fprintf(fic,'\tElimination des %d points au d�but et � la fin du spectre''\r\n',floor(windowSize/2));

fprintf(fic,'R�pertoire de sauvegarde : \r\n\t- %s\r\n',rfolder);
fprintf(fic,'\r\nFichier : %s \r\n',snom);

% sauvegarde du programme utilis�
fprintf(fic,'__________________________________________________________________________\r\n');
info=which(mfilename);
repprog=fileparts(info);
fprintf(fic,'Programme : %s ',mfilename);
res=dir(info);
fprintf(fic,'du %s\r\n',res.date);
fprintf(fic,'\r\nR�pertoire du programme : %s \r\n',repprog);

fclose(fic);


%% fin
cd(porig)






