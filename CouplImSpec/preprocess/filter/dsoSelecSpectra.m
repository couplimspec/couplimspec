function [threshold,flag,sfolder]=dsoSelecSpectra(varargin)
%
%% description
% select spectra on the basis of min or max intensities
%
%% input
%           soit pas de parametres
%           soit
%               1 - nom du fichier
%                   ou dataset object en memoire matlab
%               2 - threshold
%               3 - 'LOWER' or 'HIGHER'
%               3 - sfolder : repertoire de sauvegarde
%
%% output
%           nothing : selected data set saved on disk
%
%% principe
%
%   the threshold define the min or max intensities to select spectra
%   the 'LOWER' or 'HIGHER' flag determine wither spectra with 
%   intensities LOWER' or 'HIGHER then the threshold are selected
%   If the dso is an image : the mask is saved as a region of interest and
%   removed spectra are put to 0
%   If the dso is a data set, spectra are removed from the dso before
%   saving and no region of interest are saved
%
%
%% Use :'
%       [threshold,flag,sfolder]=dsoSelecSpectra(<dso file or datatset>, < threshold>, <'LOWER' or 'HIGHER' flag>, < sfolder>) or [threshold,flag,sfolder]=dsoSelecSpectra )
%
%
%% Auteur :
%           MArie-Fran�oise Devaux 
%           INRA - BIA - PVPP
%
%% Date : 12 janvier 2021 2010
%
%

%% context variables
porig=pwd;

%% input

if nargin~=0 && nargin~=4
    error('use: [threshold,flag,sfolder]=dsoSelecSpectra(<dso file or datatset>, < threshold>, <''LOWER'' or ''HIGHER'' flag>, < sfolder>) or [threshold,flag,sfolder]=dsoSelecSpectra )');
end

if nargin == 4
    if ischar(varargin{1})
        nom=varargin{1};
        % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
            error('format .dso.mat expected')
        end
    else
        error('use: [threshold,flag,sfolder]=dsoSelecSpectra(<dso file or datatset>, < threshold>, <''LOWER'' or ''HIGHER'' flag>, < sfolder>) or [threshold,flag,sfolder]=dsoSelecSpectra )');
    end
    if isnumeric(varargin{2})
         threshold=varargin{2};
    else
        error('number expected as threshold');
    end
    
    if ischar(varargin{3})
        flag=varargin{3};
        
        if ~strcmp(flag,'MIN') || strcmp(flag,'MAX')
            error('''MIN'' or ''MAX'' expected as 3rd input');
        end
    end
        
    if ischar(varargin{4})
        sfolder=varargin{4};
        if ~exist(sfolder,'dir')
            mkdir(sfolder);
        end
    else
        error('use: [threshold,flag,sfolder]=dsoSelecSpectra(<dso file or datatset>, < threshold>, <''LOWER'' or ''HIGHER'' flag>,  < sfolder>) or [threshold,flag,sfolder]=dsoSelecSpectra )');
    end
    
    verbose=0;
    
end


if nargin==0
    [nom,rfolder]=uigetfile({'*.mat'},'nom du fichier des spectres a traiter : ','*.dso.mat');
    
     
    
    % threshold
    threshold=inputNumber('intensity threshold');
    
    % <''LOWER'' or ''HIGHER'' flag>, 
    MinMax={'LOWER', 'HIGHER'};
    [selection ]=listdlg('liststring',MinMax,'selectionmode','single','initialvalue',1,'name','select Intensity','promptstring','Select <''LOWER'' or ''HIGHER'' flag>');
    flag=char(MinMax(selection));

    % save folder
    cd(rfolder)
    snom=strcat(strrep(nom,'.dso.mat',''),'.selec.dso.mat');

    cd ..
    
    [~,sfolder]=uiputfile({'*.mat'},'save resulting file as dso: ',snom);
    cd(sfolder)
    if ~isfolder('plotRemovedSpectra')
        mkdir('plotRemovedSpectra')
    end
    
    verbose=1;
end


%% treatment
cd(rfolder)
dso=loaddso(nom);

if verbose
    close all
    % affichage des spectres de depart
    if size(dso,1)<1000
        plotdso(dso,1,0)
    else
        facteur=round(size(dso,1)/1000);
        plotdso(dso(1:facteur:end,:),1,0)
    end   
end

% max of intensity for each spectrum
if dso.imagemode
    maxi=max(dso.imagedata,[],3);
else
    maxi=max(dso.data,[],2);
end

% thresholding
switch  flag
    case 'LOWER'
        mask=maxi<threshold;
    case 'HIGHER'
        mask=maxi>threshold;
end

% save plot of selected spectra
if sum(mask(:))<1000
    facteur=1;
else
    facteur=round(sum(mask(:))/1000);
end
indice=find(mask(:));
cd(sfolder)
plotdso(dso(indice(1:facteur:end),:),1,1);


% save plot of removed spectra
if sum(~mask(:))<1000
    facteur=1;
else
    facteur=round(sum(~mask(:))/1000);
end
indice=find(~mask(:));
    
plotdso(dso(indice(1:facteur:end),:),1,0);
cd(sfolder)
cd('plotRemovedSpectra')
saveas(gcf,strcat(dso.name,'.',flag,'.',num2str(threshold),'.png'))


% select spectra
if dso.imagemode
    dso.data(~mask(:),:)=0;
else
    dso=dso(mask(:),:);
end


%% sauvegarde des resultats
dso.name=strcat(dso.name,'.selec');
cd(sfolder)
savedso(dso);

if dso.imagemode
    if ~isfolder('ROIselec')
        mkdir('ROIselec')
    end
    cd('ROIselec')
    imwrite(mask,strcat(dso.name,'.tif'));
end

%% trace de l'execution du programme
cd(sfolder)
fic=fopen(strcat(dso.name,'.track.txt'),'w');
if fic==0
    errordlg('probleme d''ecriture du fichier %s',strcat(dso.name,'.track.txt'));
end

fprintf(fic,'%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'\t\t selction de spectres\r\n\r\n');
fprintf(fic,'Repertoire de lecture du fichier des spectres :\r\n\t- %s\r\n',rfolder);
fprintf(fic,'\r\nFichier : %s\r\n',nom);
fprintf(fic,'\r\n\r\n');

if dso.imagemode
    fprintf(fic,'TYPE de données : image dso'\r\n');
end

fprintf(fic,'\r\n\r\n');

% parametres 
fprintf(fic,'seuillage d''intensité %6.2f: \r\n\r\n',threshold);
fprintf(fic,'\tselection de spectres dont l''intensite est %s au seuil\r\n',flag);

fprintf(fic,'\r\n\r\n');

fprintf(fic,'Repertoire de sauvegarde : \r\n\t- %s\r\n',sfolder);
fprintf(fic,'\r\nFichier : %s \r\n',strcat(dso.name,'.dso.mat'));
if dso.imagemode
    fprintf(fic,'\tdso image : region d''interet sauvee dans le dossier ''ROIselec''\r\n');
end

if facteur>1
    fprintf(fic,'ATTENTION : plot de 1 spectres sur %d dans les figures enregistrees \r\n',facteur);
end

% sauvegarde du programme utilise
fprintf(fic,'__________________________________________________________________________\r\n');
info=which(mfilename);
repprog=fileparts(info);
fprintf(fic,'Programme : %s ',mfilename);
res=dir(info);
fprintf(fic,'du %s\r\n',res.date);
fprintf(fic,'\r\nRepertoire du programme : %s \r\n',repprog);

fclose(fic);


%% fin
cd(porig)






