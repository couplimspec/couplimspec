function [imft,faisceau] =fftFilterTelemos(varargin)
% correction du bruit dans des images de fluorescence
%
% fonction ecrite pour les images acquis en fluorescence sue
% la ligne DISCO du synchrotron soleil sur le microscope TELEMOS
%
% l'intensit� de fluorescence d�pend de la "forme du faisceau"
% les f�rquences sont des fr�quences tr�s basses : un filtre gaussien de
% petite taille est utilis�
% il est propos� de corriger le bruit contenu dans les hautes fr�quences
% par un filtre gaussien de taille � fixer
%
%% param�tres d'entr�e :
%           soit pas de param�tres
%           soit
%               1 - nom du fichier image � traiter
%
%               2 - windowsize : taille du lissage haute fr�quence de la transform�e de fourier
%               3 - sfolder : r�pertoire de sauvegarde
%
%% param�tres de sortie : imft : corrected image
%
%%  principe :
%   l'image est corrig�e
%            la m�thode utilis�e est un filtrage des fr�quenceq de la
%            transform�e de Fourier selon la m�thode d�crite pour les
%            images dans Gonzales et al., 2009 : Digital Image Processing
%            using Matlab
%
%           1.          la transform�e de Fourier des spectres est calcul�e
%            2.         shift de la transform�e pour se retrouver avec les valeurs de basse fr�quence au centre du spectre
%            3. constitution d'un filtre gaussien pour �liminer les basses fr�quences
%           4. constirtution du filtre gaussien pour filtrer les hautes fr�quences
%                     le filtre gaussien garde les plus basse fr�quence et
%                       d�croit progressivement les plus hautes fr�quences. la
%                       largeur du gaussien d�finit un "seuil" de coupure des
%                       fr�quence
%
%           5. multiplication des filtres entre eux
%           6. appication des filtres � la transform�e de fourrier de
%                       l'image = multiplication du filtre et de la transform�e de
%                       Fourier
%            7. shift du r�sultat pour remettre les fr�quences comme apr�s une transform�e
%             8. transform�e de Fourier inverse
%            9. r�cup�ration de la partie r�elle comme image liss�
%
%
%
%% Usage :
%       [imsc] =fftFilterTelemos;
% ou
%       [imsc] =fftFilterTelemos(nomfic, windowsizeHF, sfolder))
% ou
%
%% Auteur : MArie-Fran�oise Devaux
%           INRA - BIA - PVPP
%
%% Date :
%      8 d�cembre 2015
%               a partir de corrigerbruit_fft
%
%% comment
%

%% variables utiles / param�tres par d�faut
porig=pwd;

% window size for low frequency
%WsizeLF=0.75;
%WsizeLF=2;
%WsizeLF=1;

WsizeLF=inputNumber('taille du filtrage basse fr�quence : ');

sauve=1;

%% entr�e des donne�s
if nargin ==3
    if ischar(varargin{1})
        nom=varargin{1};
        % test du type de fichier
        point=strfind(nom,'.');
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'tif')
            error('tif image attendu')
        end;
        im=imread(nom);
        rfolder=pwd;
    else if isnumeric(varargin{1})
            im=varargin{1};
            nom='';
            sauve=0;
        else
            error('image argument incorrect : usage  [imsc] =fftFilterTelemos(nomfic, windowsizeHF, sfolder))');
        end;
    end;
    if isnumeric(varargin{2})
        WsizeHF=varargin{2};
        if WsizeHF==0
            rNoise=0;
        else
            rNoise=1;
        end;
    else
        error('windowsize argument incorrect : usage  [imsc] =fftFilterTelemos(nomfic, windowsizeHF, sfolder))');
    end;
    if ~exist(varargin{3},'dir')
        mkdir(varargin{3})
    end
    cd (varargin{3})
    sfolder=pwd;
    
    verbose =0;

end;

if nargin ~=3 && nargin ~=0
    error('argument incorrect : usage fftFilterTelemos ou  [imsc] =fftFilterTelemos(nomfic, windowsizeHF, sfolder)');
end;

if nargin==0
    [nom,rfolder]=uigetfile({'*.tif'},'nom du fichier image � traiter : ','*.tif');
    [~,f]=fileparts(fileparts(rfolder));
    cd(rfolder)
    im=imread(nom);
    
    % save file
    cd ..
    if ~exist(strcat(f,'.fftTELEMOS'),'dir') 
        mkdir(strcat(f,'.fftTELEMOS'))
    end;  
    cd( strcat(f,'.fftTELEMOS'))
     sfolder=pwd;
    
     % interactive choice of noise correcfion parameter
    rNoise=yesno('remove high frequencey noise ?');
    
    if rNoise
        
        % defaulf values
        WsizeHF=150;
        
        prompt = {'taille de la fen�tre (>0) :'};
        dlg_title = 'param�tres du lissage du bruit des image TELEMOS par transform�e de Fourier';
        num_lines = 1;
        def = {num2str(WsizeHF)};
        options.Resize='on';
        suite=0;
        while ~suite
            reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
            ll=str2double(reponse{1});
            if ll<0
                errordlg('largeur incorrecte : donner un nombre  positif')
            else
                WsizeHF=ll;
                suite=1;
            end;
        end;
    else
        WsizeHF=0;
    end
    verbose =1;
end

if sauve
    
    noms=strcat(strrep(nom,'.tif','.fftTelemos.tif'));
end;


%% correction des spectres

% remove first and last lines and columns
im=im(6:(end-5),6:(end-5));

nl=size(im,1);
nc=size(im,2);
    
if verbose
   close all
    % affichage image de d�part
    figure(1)
    imshow(im,[])
    hold on
    
    
    plot([1 nl],[1 nc],'r')
    plot([nl 1],[1 nc],'g')
    [x,y,~]=improfile(im,[1 nl],[1 nc],round(sqrt(nl^2+nc^2)));
    c=improfile(im,x,y);
    figure(2)
    plot(c)
    hold on
    [x2,y2,~]=improfile(im,[nl 1],[1 nc],round(sqrt(nl^2+nc^2)));
    c2=improfile(im,x2,y2);
    figure(3)
    plot(c2);
    hold on
end;

% transformee de fourier de im
fim=fft2(im);

% shift de la transformee de fourrier
sfim=fftshift(fim);


% filtre du faisceau
f=fspecial('gaussian',[nl nc],WsizeLF);
f=f/max(f(:));                                                      % keep value in the original range
f1=f;
f=1-f;                                                                   % keep only high frequencies...

if rNoise
    % filtrer le bruit haute fr�quence
    fl=fspecial('gaussian',[nl nc],WsizeHF);
    fl=fl/max(fl(:));

    % filtre total 
    ff=fl.*f;
else
    ff=f;
end;


if verbose
    % image du faisceau, de la d�formation
    sfimf=sfim.*f1;
    
    % shift inverse
    isfimf=ifftshift(sfimf);

    % transformee de fourier inverse
    imft=ifft2(isfimf);
    % r�cupere la partie reelle
    faisceau=real(imft);
    
    figure(13)
    imshow(faisceau,[])
    
     cft=improfile(faisceau,x,y);
    cft2=improfile(faisceau,x2,y2);
    
    figure(2)
    plot(cft,'r','linewidth',1.5)
    figure(3)
    plot(cft2,'g','linewidth',1.5)
    
    figure(4)
    imshow(imft,[])
   
end;
    

% filtrer la transformee de Fourier
sfimf=sfim.*ff;
% shift inverse
isfimf=ifftshift(sfimf);

% transformee de fourier inverse
imft=ifft2(isfimf);
% r�cupere la partie reelle
imft=real(imft);

% recover avergae intenisty of original image
imft=imft+mean(im(:));

% put the image in the original class
imft=uint16(imft);

if verbose
    cft=improfile(imft,x,y);
    cft2=improfile(imft,x2,y2);

    figure(2)
    plot(cft,'r','linewidth',1.5)
    figure(3)
    plot(cft2,'g','linewidth',1.5)
    
    figure(4)
    imshow(imft,[])
end;


    

    
   
%% sauvegarde des r�sultats
if sauve
    
    
    cd(sfolder)
    imwrite(imft,noms);
    
end

%% trace de l'ex�cution du programme

fic=fopen(strcat(strrep(noms,'.tif',''),'.track.txt'),'w');
if fic==0
    errordlg('probl�me d''�criture du fichier %s',strcat(strrep(noms,'.tif',''),'.track.txt'));
end;

fprintf(fic,'%s\r\n',datestr(now,0));
fprintf(fic,'_______________________________________________________________________________________________________\r\n');
fprintf(fic,'\t\t correction des images TELEMOS par filtre gaussien des tr�s basses et des hautes fr�quences de la transform�e de Fourier\r\n\r\n');
fprintf(fic,'R�pertoire de lecture de l''image:\r\n\t- %s\r\n',rfolder);
fprintf(fic,'\r\nFichier : %s\r\n',nom);
fprintf(fic,'\r\n\r\n');

% param�tres de correction 
fprintf(fic,'Param�tres de lissage par filtre gaussien de la transform�e de Fourier : \r\n\r\n');
fprintf(fic,'\tfonctions matlab pour calculer la transform�e de Fourier : fft et ifft\r\n\');
fprintf(fic,'\tfonctions matlab pour shifter la transform�e de Fourier : shiftfft et ishiftfft\r\n\');
fprintf(fic,'\tfonctions matlab pour calculer le filtre gaussien : fspecial  + option ''gaussian''\r\n\');
fprintf(fic,'\tlargeur du filtre gaussien des basses fr�quences : %d\r\n',WsizeLF);
if rNoise
    fprintf(fic,'\tlargeur du filtre gaussien des hautes fr�quences: %d\r\n',WsizeHF);
end
fprintf(fic,'\tr�cup�ration de la partie r�elle du spectre apr�s lissage''\r\n\');

fprintf(fic,'\tr�stitution de l''intensit� moyenne de l''image par addition de la moyenne apr�s fitrage des fr�quences''\r\n\');

if sauve
    
    fprintf(fic,'R�pertoire de sauvegarde : \r\n\t- %s\r\n',sfolder);
    fprintf(fic,'\r\nFichier : %s \r\n',noms);
end

% sauvegarde du programme utilis�
fprintf(fic,'__________________________________________________________________________\r\n');
info=which(mfilename);
repprog=fileparts(info);
fprintf(fic,'Programme : %s ',mfilename);
res=dir(info);
fprintf(fic,'du %s\r\n',res.date);
fprintf(fic,'\r\nR�pertoire du programme : %s \r\n',repprog);

fclose(fic);


%% fin
cd(porig)

    
            
            
            
                
        