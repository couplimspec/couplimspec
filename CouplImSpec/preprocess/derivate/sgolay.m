
function [dso_deriv1]= sgolay(dso,width,order,deriv)

%% description
    %First or second derivative by sgolay with display of raw spectra and derivative 
    
%% input

    %d:dataset spectrum or set of spectra
    %width:width number of points
    %order:order of polynome
    %deriv:order of derivative 
    
%% output
    
    %dso_deriv1:dataset spectrum of sgolay derivative

%% principe
    % Adapted for plotting both raw spectra and derivative(plotyy) 
    % Multiplication by (-1) for spectroscopic view
    % function used is the PLS toolbox function savgol
    % default variables: width=9,order=3,deriv=1

%% use
    % [dso_deriv1]= sgolay(d,9,3,2) return second derivative of dataset "d" with polynome
    % order 3 and taking 9 points 
    
%% Comments
    % writen for F.Allouche PhD Thesis
    
%% Author
    % F.Jamme INRA/SOLEIL 
    
%% Date
    % 18/01/2011

%% context variables 

orig=pwd;

%% start

if nargin ~=0 && nargin ~= 1 && nargin ~=4
   
        error('use: [dso_deriv1]= sgolay(d,width,order,deriv');
end

if nargin <=1
        
        deriv=1;
        order=3;
        width=9;
end

if nargin ==0
        
        % open file from folder 
        [fname,pathname]=uigetfile({'*.dso.mat'},'select dataset file');
        [path, name, ext] = fileparts(fname);
        cd(pathname);
        load(fname);% load dso to workspace
        % name and folder to save results
        [sname,sfolder] = uiputfile({'*.dso.mat'},'result file name',strcat(name,'.d',num2str(deriv),'.mat'));
end

    
deriv=2;
        order=3;
        width=15;

%% Sgolay deriv from PLS toolbox
    %[z] = savgol(y,width,order,deriv)

[dso_deriv] = savgol(dso,width,order,deriv);

%multiply by (-1)

%dso_deriv1=times(dso_deriv,-1);
dso_deriv1=dso_deriv;
dso_deriv1.data=dso_deriv1.data*-1;
dso_deriv1.axisscale{1}=dso.axisscale{1};
dso_deriv1.axisscale{2}=dso.axisscale{2};
dso_deriv1.label{1}=dso.label{1};
dso_deriv1.label{2}=dso.label{2};

%plot the result

figure;
set(gcf,'Color',[1 1 1]); % select the background color
axis tight;
[AX,H1,H2]=plotyy(dso.axisscale{2},dso.data,dso.axisscale{2},dso_deriv1.data,'plot');
xlabel('Wavenumber [cm^-^1]'); % xlabel
set(get(AX(1),'Ylabel'),'String','Absorbance') 
set(get(AX(2),'Ylabel'),'String','A.u') 
set(H1,'Color','b')
set(H2,'Color','r')
title(['width ',num2str(width), ' ','order ',num2str(order),' ','deriv ',num2str(deriv)]); % title
set(AX,'XDir','reverse'); % this is for a chemist view

%% save the result

cd(sfolder);
save(sname,'dso_deriv1');


%% matlab function tracking  

i=strfind(name,'.');
gname=name(1:i(1)-1);

fid=fopen(strcat(gname,'.track.txt'),'a');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'%s\r',datestr(now,0));
%fprintf(fid,'__________________________________________________________________________\r\n');
fprintf(fid,'Stavisky golay derivative of data set object \r\n\r\n');
fprintf(fid,'input data folder :\r\n %s\r\n',pathname);
fprintf(fid,'file : %s%s\r\n',name,ext);
%fprintf(fid,'\r\n\r\n');

% parameter used
fprintf(fid,'list of parametres used : \r\n');
fprintf(fid,'\t function PLS toolbox used  : savgol\r\n\');
fprintf(fid,'\t Matlab function used : plotyy\r\n\');
fprintf(fid,'\t number of point used  : %d\r\n',width);
fprintf(fid,'\t degree of polynome used  : %d\r\n',order);
fprintf(fid,'\t derivative  : %d\r\n',deriv);

fprintf(fid,'folder : \r\n %s',sfolder);
fprintf(fid,'\r\n saved file name : %s \r\n',sname);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
ind=strfind(info,'\');
repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function folder : %s \r\n',repprog);
fprintf(fid,'function : %s ',mfilename);
res=dir(info);
fprintf(fid,'from %s \r\n',res.date);
fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
