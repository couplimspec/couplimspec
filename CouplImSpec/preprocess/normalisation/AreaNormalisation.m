function sim=AreaNormalisation(varargin)

 %% description
    % Normalisation of spectra using area
    
%% input parameters can be
    % - no parameter
    % or
    % ims = spectral image in dso format
    % fPos : flag to take into account only positive values
    % sfolder - save folder 
    
%% output
    
    % - nothing 
    % or
    % sim - data set object with a reduce number of spectra

%% principle
    %
    % all sepctral intensities will be divided by the raw sam of
    % intensities
    % the raw sim of intensities can take into accourn all intensities or
    % only positive values

%% use
    % AreaNormalisation;
    
   
    
%% Comments, context
    % written for the proposal SOLEIL 20150929
    
    
%% Author
    % MF Devaux 
    % BIA - PVPP INRA Nantes
    
%% date
    % 16 october 2018
    % 30 juin 2020: remove display image
    
%% context variables 
orig=pwd; % returns the current directory



%% input data
if nargin == 0
    % input image
    [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

    cd(repl)
    ims=loaddso(nom);
  
    % choose option all values or only positive values
    listMethod={'Sum of intensities';'Sum of positive intensities';'Unit vector = Sum of squared intensities'};

    codeMethod={'SUM';'SUMPOS';'UNITVECTOR'};
    [numMethod,ok] = listdlg('ListString',listMethod, 'SelectionMode','single','PromptString','choose method',...
    'name','Noramisation of spectra','listsize',[300 160]);
    
    if ok
        code=codeMethod{numMethod};
    else
        if isempty(numMethod)  % cancel of no method chosen
            disp('Normalisation method choice cancelled');
            return;
        end
    end

    
    % save
    [sname,sfolder]=uiputfile({'*.mat'},'save file as',strrep(nom,'.dso.mat','.norm.dso.mat'));
    sname=strrep(sname,'.dso.mat','');
     
end

if nargin >= 1
    % test si l'image en entr�e est un dataset or a file
    ims=varargin{1};
    if ischar(ims)
        nom=ims;
         % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end
        ims=loaddso(nom);
        
    else
        if ~strcmp(class(ims),'dataset') %#ok<STISA>
            error('give a data set as first input parameter');
        end
    end

    repl=pwd;                               % reading folder
end

if nargin == 1
   %choose threshold for max value
   % choose option all values or only positive values
    listMethod={'Sum of intensities';'sum of positive intensities';'unit vector'};

    codeMethod={'SUM';'SUMPOS';'UNITVECTOR'};
    [numMethod,ok] = listdlg('ListString',listMethod, 'SelectionMode','single','PromptString','choose method',...
    'name','Noramisation of spectra','listsize',[300 160]);
    
    if ok
        code=codeMethod{numMethod};
    else
        if isempty(numMethod)  % cancel of no method chosen
            disp('Normalisation method choice cancelled');
            return;
        end
    end
    
    sfolder=repl;
    sname=strcat(ims.name,'.norm');
end

if nargin >= 2
    code=varargin{2}; 
    if ~ischar(code)
        error('Wrong code method (''SUM'',''SUMPOS'' or ''UNITVECTOR'' expected)');
    end
    if ~strcmp(code,'SUM') || ~strcmp(code,'SUMPOS') || ~strcmp(code,'UNITVECTOR')
        error('Wrong code method (''SUM'',''SUMPOS'' or ''UNITVECTOR'' expected)');
    end
    
end

       
if nargin == 2
    sfolder=repl;
    sname=strcat(ims.name,'.norm');
end

if nargin == 3
    sfolder=varargin{3};
    if ~exist(sfolder,'dir')
        error('%s directory not found',sfolder);
    end
    sname=strcat(ims.name,'.norm');
end

if nargin >3
    disp('Wrong number of parameter')
    error('Usage: sim=AreaNormalisation(ims, METHOD,sfolder');
end

% test des param�tres de sortie
if  nargout >1
    error('wrong number of output parameter')
end
% 

%% treatment
% factor of normalisation
switch code
    case 'SUM'
        s=sum(ims.data,2);
    case 'SUMPOS'
        tmp=ims.data;
        tmp(tmp<0)=0;
        s=sum(tmp,2);
        clear tmp;
    case  'UNITVERCTOR'
        sp=ims.data;
        s=sum((sp.*sp),2);
        clear sp;
end


% normalisation
sim=ims;                        % initialisation of the resulting dataset object
sim.data=sim.data./repmat(s,1,size(ims,2));

if ~exist('sname','var')
    sim.name=strcat(sim.name,'.norm');
else
    sim.name=sname;
end

%% plot imagedata sum 

if sim.imagemode
    h=figure(27);
    set(h,'windowstyle','docked');
    area=sum(sim.imagedata,3);
    if isfield(sim.userdata.acquisition,'coordStage')
        imagesc(sim.userdata.acquisition.coordStage.col,sim.userdata.acquisition.coordStage.row,area);
        set(gca,'dataAspectRatio',[1 1 1])
    else
        imshow(area,[])
    end
    colormap(jet)
    title(sim.name,'Fontsize',12);
end


%% save 
% dso
cd(sfolder);
savedso(sim,sname,sfolder);

%% matlab function tracking  

fid=fopen(strcat(sim.name,'.track.txt'),'w');

if fid==0
     errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Individual spectral normalisation \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nInput spectral image name: %s.mat\r\n',ims.name);
fprintf(fid,'data folder: %s\r\n\r\n',repl);

fprintf(fid,'Method: ');
switch code
    case 'SUM'
        fprintf(fid,'SUM of INTENSITIES\r\n');
    case 'SUMPOS'
        fprintf(fid,'SUM of POSITIVE INTENSITIES. Negative values are ignored\r\n');
    case  'UNITVERCTOR'
        fprintf(fid,'SUM of SQUARED INTENSITIES\r\n');
end

fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'\r\nsave folder : %s \r\n',sfolder);


% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);


fclose(fid);

%% end
cd (orig)

end


    