function [nim]=spespa_norm(varargin)
%% description
    % spatial and spectral normalisation of intensities in spectral images
    % applied to dataset objects
    
    
%% input

    % nothing
        
    % or 
    
    % ims = dataset object or filename
    % method - string to indicate that the folllowing parameter is the method to
    %   be employed for computing the grey level image: expected values are:
    %           - 'SUM' or 'sum' for the sum of all intensities of the
    %           spectra
    %           - 'NORM' or 'norm' for the norm of intensities for the
    %           whole spectra 
    %           - 'BAND' or 'band' for the area under a band
    %           - 'WAVELENGTH' or 'wavelength' for a given wavelength

    % lo - in case of method 'WAVELENGTH': vector of one value indicating the wavelength or wavenumber
    %    - in case of method 'BAND': vector of two values indicating the
    %                               wavelengths or wavenumbers that delimitate the band
    % taille - size of smoothing for the moving average
    % sfolder - save folder 
    
%% output
    
    % normalised data set object

%% principle
    % normalisation following Icl = (Il-min)/(Fn)
    % where Il is the initial intensity value of each pixel at a given wavelength l
    %       min is the minimum value of the spectra    
    %       Fn is the normalisation factor 
    %       Icl is the corrected intensity at wavelength l
    %
    % Fn is assessed by 
    %       considering 
    %               - the sum of intensity of the spectra
    %               - the norm of the spectra
    %               - the intensity at a given wavelength
    %               - the sum of intensity between two wavelengths
    %       modifying the criteria by taking into account neighbour pixels:
    %           holes are filled
    %           result is smoothed by a moving average operation
    
    
%% use
    % spespa_norm;
    
    % ims=readomnic;
    % sfolder =pwd;
    % taille=10
    % fMin=25;
    % gim=spespa_norm(ims,'SUM',taille,sfolder)
    
   
    
%% Comments
    % writen for F.Allouche PhD Thesis
    
    
%% Author
    % MF Devaux
    % BIA - PVPP

    
%% Date
    % 15 mars 2011
    % 8 juillet 2011 : do not subtract min in the case of norm of
    % intensities + sqrt(sum of square)
    % 3 novembre 2011 : gestion des noms...
    % 13 septembre 2012 : normalisation selon la somme positive + pas de
    % soustrcation de la mvaleur minimum (cas Raman...)
    % 21 novembre 2019 : polypheme
    
%% TODO
    % param�trer la soustraction de la valeur minimale

%% context variables 
orig=pwd;

%factMin=20; % pourcentage d'intensit� des minimas.

%% input data
if nargin == 0
    % input image
    [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

    cd(repl)
    ims=loaddso(nom);
    
   
    
    [method,lo,indicelo]=choix_param_methodlo(ims);

    [taille]=choix_param_taille;
    
    [factMin]=choix_param_factMin;
    
    [sname,sfolder]=uiputfile({'*.mat'},'nom de sauvegarde du fichier r�sultat',strrep(nom,'.dso.mat','.norm.dso.mat'));
    sname=strrep(sname,'.dso.mat','');
    
    affichage=yesno('show and save intermediate images ?');
else
    affichage=0;
end

if nargin >= 1
    % test si l'image en entr�e est un dataset or a file
    ims=varargin{1};
    if ischar(ims)
        nom=ims;
         % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end
        ims=loaddso(nom);
        
    else
        if ~strcmp(class(ims),'dataset')
        error('give a data set as first input parameter');
        end
    end

    repl=pwd;                               % reading folder
end

if nargin == 1
    [method,lo,indicelo]=choix_param_methodlo(ims);
    [taille]=choix_param_taille;
    [factMin]=choix_param_factMin;

    sfolder=repl;  
end

if nargin == 2 || nargin==3
    disp('invalid number of parameter for function spespa_norm');
    error('usage: nim=spespa_norm; or nim=spespa_norm(ims,method, lo(option),taille,fMin,sfolder)');

end

if nargin>=4
    method=upper(varargin{2});
    if ~strcmp(method,'WAVELENGTH') && ~strcmp(method,'BAND') &&~strcmp(method,'SUM') &&~strcmp(method,'SUMPOS') && ~strcmp(method,'NORM')
        disp('Wrong method or wrong number of parameter');
        error('usage: nim=spespa_norm; or nim=spespa_norm(ims,method, lo(option),taille,sfolder)');
    end
end

if nargin ==4
    if ~strcmp(method,'SUM') && ~strcmp(method,'SUMPOS') && ~strcmp(method,'NORM')
        disp('Wrong method or wrong number of parameter');
        error('usage: nim=spespa_norm; or nim=spespa_norm(ims,method, lo(option),taille,fMin,sfolder)');
    end
    taille=varargin{3};
    factMin=varargin{4};
    sfolder=repl;
   % sname=strrep(ims.name,'.dso.mat','.norm.dso.mat');
end
        
if nargin >=5  
    switch method
        case 'WAVELENGTH'
            lo=varargin{3}; 
            if ~strcmp(class(lo),'double')
                error('Wrong wavelength or wavenumber (real value expected)');
            end
            if length(lo)>1
                error('Wrong number of wavelength or wavenumber (1 expected)');
            end
            lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
            if lo<min(lot)|| lo>max(lot)
                error('Wrong wavelength value: must be between %s and %s',min(lot),max(lot))
            end
            silo=lot-repmat(lo,1,length(lot));
            indicelo=find(abs(silo)==min(abs(silo)));
            lo=lot(indicelo);
            
            taille=varargin{4};
            factMin=varargin{5};
           
        case 'BAND'
            lo=varargin{3}; 
            if ~strcmp(class(lo),'double')
                error('Wrong wavelength or wavenumber (real value expected)');
            end
            if length(lo)~=2
                error('Wrong number of wavelength or wavenumber (2 expected)');
            end
            lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
            if lo(1)<min(lot)|| lo(1)>max(lot)
                error('Wrong wavelength value 1: must be between %s and %s',min(lot),max(lot))
            end
            if lo(2)<min(lot)|| lo(2)>max(lot)
                error('Wrong wavelength value 2: must be between %s and %s',min(lot),max(lot))
            end
            silo=lot-repmat(lo(1),1,length(lot));
            indicelo(1)=find(abs(silo)==min(abs(silo)));
            lo(1)=lot(indicelo(1));
            silo=lot-repmat(lo(2),1,length(lot));
            indicelo(2)=find(abs(silo)==min(abs(silo)));
            lo(2)=lot(indicelo(2));
            
            taille=varargin{4};
            factMin=varargin{5};
        
        case {'SUM','SUMPOS','NORM'}
            taille=varargin{3};
            factMin=varargin{4};
            sfolder=varargin{5};
    end
   % sname=strrep(ims.name,'.dso.mat','.norm.dso.mat');
end
    
if nargin == 5
    if strcmp(method,'WAVELENGTH') || strcmp(method,'BAND') 
        sfolder=repl;
    end
end

if nargin == 6
    sfolder=varargin{6};
end

if nargin >6
    disp('Wrong number of parameter')
    error('usage: nim=spespa_norm; or nim=spespa_norm(ims,method, lo(option),fMin,taille,sfolder)');
end

% test des param�tres de sortie
if  nargout >1
    error('wrong number of output parameter')
end
% 


%% subtracting minima of spectra
% to remove constant baseline
nblig=ims.imagesize(1);  % number of lines
nbcol=ims.imagesize(2);  %number of columns
nblo=length(ims.axisscale{2}); % number of wavelength

im1=reshape(ims.data,nblig,nbcol,nblo);
if ~strcmp(method,'NORM') && ~strcmp(method,'SUMPOS')
    im1=im1-repmat(min(im1,[],3),[1,1,nblo]);
end

%% normalisation factor
switch method
    case 'SUM'
        imax=sum(im1,3);                % image of the sum of intensities
    case 'SUMPOS'                       % image of the sum of positive intensities
        tmp=zeros(size(im1));
        tmp(im1>0)=im1(im1>0);
        imax=sum(tmp,3);
        clear tmp;
    case 'NORM'
        imax=sqrt(sum(im1.*im1,3));                % image of the norm of intensities

    case 'BAND'
        
        % correction de ligne de base des spectres pour la bande consid�r�e
        if indicelo(1)<indicelo(2)      % cas fluorescence (set longueur d'onde)
            i1=indicelo(1);
            i2=indicelo(2);
        else                            % cas IR ou Ramand avec nombre d'onde invers�s dans les graphiques
                                        % mais dans le sens croissant dans la matrice des donn�es
            i1=indicelo(2);
            i2=indicelo(1);
        end
            
        sp=ims.data(:,i1:i2);           % s�lection de la bande d'int�r�t
        nl=size(sp,2);
        
        % calcul de la droite � soustraire
        a=(sp(:,nl)-sp(:,1))/(nl-1);
        b=sp(:,1)-a;
        
        % soustraction de la droite pour la bande consid�r�e
        spc=sp-repmat(a,1,nl).*repmat(1:nl,size(sp,1),1)-repmat(b,1,nl);
        
        % calcul de l'aire sous la bande
        imax=sum(spc,2);
        imax=reshape(imax,nblig,nbcol);
        
    case 'WAVELENGTH'
      
        % image
        imax=im1(:,:,indicelo);
end
        
        
if affichage
    close all
    h=figure(1);
    set(h,'windowstyle','docked','name','raw norm');
    imshow(imax,[]);
end

% mosaique image consituted with duplication of image flipped left, right,
% up and down. the objective is to define borders to fill holes connected
% to the inital border of the  image
% tempeim=[fliplr(flipud(imax)) flipud(imax) fliplr(flipud(imax));...
%     fliplr(imax) imax fliplr(imax);...
%     fliplr(flipud(imax)) flipud(imax) fliplr(flipud(imax))];

% if affichage
%     h=figure(2);
%     set(h,'windowstyle','docked','name','miror...');
%     imshow(tempeim,[])
% end;

% mini and maxi are values to force displaying all the images with the same
% contrast of grey levels
mini=double(min(min(imax)));
maxi=double(max(max(imax)));

% save intermediate images
% if affichage
%     cd(sfolder)
%     imwrite(uint8(ajusterimage((imresize(tempeim,2,'nearest')),mini,maxi)),strrep(nom,'.dso.mat','.miror.tif'),'tif','compression','none')
% end

% fill holes
%ii=imfill(tempeim,'holes');
%tempeim=imfill(imax,'holes');
tempeim=imax;

% profondeur de minima prise en compte
ii=imfill(tempeim,'holes');
pmin=round((maxi-mini)/factMin);
ii=imhmin(ii,pmin);


% recover the center of image
%ii=ii((nblig+1):2*nblig,(nbcol+1):2*nbcol);

if affichage
    h=figure(3);
    set(h,'windowstyle','docked','name','hole fill');
    imshow(ii,[mini maxi]);
end

% save intermediate images
if affichage
    cd(sfolder)
    imwrite(uint8(ajusterimage(imresize(ii,2,'nearest'),mini,maxi)),strrep(nom,'.dso.mat','.imfill.tif'),'tif','compression','none')
end

% smoothing of normalisation factor 
f=fspecial('average',taille);           % average filter with a squared mask of size 'taille'
iim=imfilter(ii,f,'replicate');         % apply filter

if affichage
    h=figure(4);
    set(h,'windowstyle','docked','name','smoothed');
    imshow(iim,[mini maxi]);
end

% save intermediate images
if affichage
    cd(sfolder)
    imwrite(uint8(ajusterimage(imresize(iim,2,'nearest'),mini,maxi)),strrep(nom,'.dso.mat',strcat('.smooth',num2str(taille),'.tif')),'tif','compression','none')
end


%% normalisation
im1n=im1./repmat(iim,[1,1,nblo]);



%% dso

nim=ims;
nim.data=reshape(im1n,nblig*nbcol,nblo);
nim.name=strcat(ims.name,'.norm');

if affichage
    if size(ims,1)<1000
        plotdso(ims,1)
    else
        plotdso(ims(1:10:end,:),1)
    end
    set(gcf,'windowstyle','docked','name','spec raw');
    if size(ims,1)<1000
        plotdso(nim,1)
    else
        plotdso(nim(1:10:end,:),1)
    end
    set(gcf,'windowstyle','docked','name','spec norm');
    % display surface represnetation fo images
    h=figure;
    set(h,'windowstyle','docked','name','surf raw');
    surf(1:nblig,1:nbcol,imax')
    set(gca,'dataaspectratio',[1 1 max(imax(:))/mean(nblig,nbcol)])
    title('original image')

    h=figure;
    set(h,'windowstyle','docked','name','surf def estim');

    surf(1:nblig,1:nbcol,iim')
    set(gca,'dataaspectratio',[1 1 max(iim(:))/mean(nblig,nbcol)])
    title('estimation of intensity variations')
    
    h=figure;
    set(h,'windowstyle','docked','name','surf normalised');


    surf(1:nblig,1:nbcol,sum(im1n,3)');
    set(gca,'dataaspectratio',[1 1 max(max(sum(im1n,3)))/mean(nblig,nbcol)])
    title('corrected image')
end

% show normalised image
h=figure(37);
set(h,'windowstyle','docked');
subplot(1,2,1)
imagesc(ims.userdata.acquisition.coordStage.col,ims.userdata.acquisition.coordStage.row,sum(im1,3));
colormap(jet)
title(ims.name,'Fontsize',12);
set(gca,'dataAspectRatio',[1 1 1])
subplot(1,2,2)
imagesc(ims.userdata.acquisition.coordStage.col,ims.userdata.acquisition.coordStage.row,sum(im1n,3));
colormap(jet)
title(nim.name,'Fontsize',12);
set(gca,'dataAspectRatio',[1 1 1])



%% save
cd(sfolder)
if ~exist('sname','var')
    sname=nim.name;
end
savedso(nim,sname,sfolder)



%% matlab function tracking  
%gname=nim.name;
gname=sname;

fid=fopen(strcat(gname,'.track.txt'),'a');

if fid==0
     errordlg('enable to open track file');
end
 
fprintf(fid,'%s\r\n',datestr(now,0));
fprintf(fid,'__________________________________________________________________________\r\n');
fprintf(fid,'\t\t test normalisation of spectral image \r\n\r\n');
fprintf(fid,'input data folder :\r\n\t- %s\r\n',repl);
fprintf(fid,'\r\n file : %s\r\n',nom);
fprintf(fid,'\r\n\r\n');

% parameter used
fprintf(fid,'Normalise each pixel spectrum S as : Sc(l) = (S(l)-min(S))/Norm(S,v)\r\n\r\n');
fprintf(fid,'where : \r\n');
fprintf(fid,'\t\t S(l) : intensity of spectrum S at wavelength l \r\n');
fprintf(fid,'\t\t min(S) : minimum value of intensity of spectrum S \r\n');
fprintf(fid,'\t\t Norm(S,v) : normalisation factor of spectrum S calculated by: \r\n');
switch method
    case 'SUM'
        typim='imsum';
        fprintf(fid,'\t\t\t\t - assessing sum of intensity of spectrum S: sum(S) for each spectrum\r\n');
        fprintf(fid,'\t\t\t\t - considering the image formed by the sum of intensities <imsum>\r\n');
    case 'SUMPOS'
        typim='imsumpos';
        fprintf(fid,'\t\t\t\t - assessing sum of postivie intensities of spectrum S: sum(S>0) for each spectrum\r\n');
        fprintf(fid,'\t\t\t\t - considering the image formed by the sum of intensities <imsumpos>\r\n');
    case 'NORM'
        typim='imnorm';
        fprintf(fid,'\t\t\t\t - assessing norm of spectrum intensity S: sum(S.S) for each spectrum\r\n');
        fprintf(fid,'\t\t\t\t - considering the image formed by the sum of intensities <imnorm>\r\n');
    case 'BAND'
        typim='imband';
        fprintf(fid,'\t\t\t\t - assessing sum of intensity of spectrum S between %s : %d and %d %s for each spectrum\r\n',lower(ims.title{2}),round(lo(1)),round(lo(2)),ims.userdata.unit);
        fprintf(fid,'\t\t\t\t - considering the image formed by the sum of intensities of the band <imband>\r\n');
    case 'WAVELENGTH'
        typim='imwave';
        fprintf(fid,'\t\t\t\t - picking intensity of spectrum S at %d %s for each spectrum\r\n',round(lo),ims.userdata.unit);
        fprintf(fid,'\t\t\t\t - considering the image formed by the sum of intensities of the band <imband>\r\n');
end
      
%fprintf(fid,'\t\t\t\t - filling holes of %s: <imsumholesfilled>\r\n',typim);
fprintf(fid,'\t\t\t\t - suppressing minima whith relative intensity lower than %d in image <%s> to obtain <imsumholesfilled>image \r\n',pmin, typim);
fprintf(fid,'\t\t\t\t - facteur to assess the minima intensity intMin f=%d and pmin=(Imaxi-Imini)/f\r\n\r\n',factMin);

fprintf(fid,'\t\t\t\t - smoothing <imsumholesfilled> using an average filter of size v=%d \r\n',taille);
if ~strcmp(method,'NORM') && ~strcmp(method,'SUMPOS')
    fprintf(fid,'\t\t\t\t - subtract minimum value of <imsumholesfilledsmoothe> \r\n');
end
fprintf(fid,'\r\n\r\n');

fprintf(fid,'R�pertoire de sauvegarde : \r\n\t- %s\r\n',sfolder);
fprintf(fid,'\r\nfichier : %s \r\n',sname);

if affichage
    fprintf(fid,'\r\nintermediate images : \r\n\r\n');
    fprintf(fid,'\t miror image: %s \r\n',strcat(gname,'.miror.tif'));
    fprintf(fid,'\t after imfill(''holes''): %s \r\n',strcat(gname,'.imfill.tif'));
    fprintf(fid,'\t after smoothing: %s \r\n',strcat(gname,'.smooth.tif'));
end


% sauvegarde du programme utilise
fprintf(fid,'__________________________________________________________________________\r\n');
info=which(mfilename);
repprog=fileparts(info);
fprintf(fid,'Programme : %s ',mfilename);
res=dir(info);
fprintf(fid,'du %s\r\n',res.date);
fprintf(fid,'\r\nR�pertoire du programme : %s \r\n',repprog);

fclose(fid);

%% end

cd (orig)

end


function [method,lo,indicelo]=choix_param_methodlo(ims)
% interactive choice of parameter for showspectrimage
        
        
% choice of the method
choix={'sum of intensities', 'norm of intensities','one wavelength','sum between two wavelengths'};
codemethod={'SUM','NORM','BAND','WAVELENGTH'};
[nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
    'name','normalisation image','listsize',[300 160]);
if ok 
    method=codemethod{nummethod};
else
    if isempty(nummethod)  % cancel of no method chosen
        disp('method choice cancelled showspectrimage');
        return;
    end
end
        
% according to the method wavelengths are required :
switch method
    case {'SUM','NORM'}
        lo=0;
        indicelo=0;
    case 'WAVELENGTH'
        lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
        figure(28)                       % graphes de la s�rie des spectres
        ok=0;
        while ~ok
            nbs=size(ims.data,1);
            st=round(nbs/100);
            plot(lot,ims.data(1:st:nbs,:)); 
            if strcmp(ims.title{2},'Wavenumber')
                set(gca,'Xdir','reverse');
            end
            xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
            ylabel('intensity', 'fontsize',16)
            [x,~]=ginput(1);
            hold on
            plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
            title(['selected ' ims.title{2} ': ' num2str(round(x)) ' ' ims.userdata.unit])
            ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
            if ~ok
                los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                if isempty(los)
                    disp('no wavelength chosen');
                    return;
                end
                x=str2double(los);

                if x>min(lot) && x<max(lot)
                    ok=1;
                else
                    ok=ouinon('valeur erron�e, voulez vous continuer ?');
                    if ~ok
                        error('choice of %s cancelled in showspectrimage',ims.title{2});
                    else
                        ok=0;
                    end
                end
            end

            hold off
        end
        lo=x;
        clear x
        clear y

        silo=lot-repmat(lo,1,length(lot));
        indicelo= abs(silo)==min(abs(silo));
        lo=lot(indicelo);



  case 'BAND'
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(1)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,~]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end
            end
        end
        hold off
    end
    lo(1)=x;
    clear x
    clear y
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x,~]=ginput(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end
            end
        end
        hold off
    end
    lo(2)=x;
    clear x
    clear y                
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));

    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));

    clear silo
                
            
end
    
end  

%% sub function
function [taille]=choix_param_taille

% defaulf values
taille=11;

prompt = {'taille du filtre moyen (nombre impair >0 et <200) :'};
dlg_title = 'lissage de l''image de normalisation';
num_lines = 1;
def = {num2str(taille)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if ll >0 && (ll-1)/2==round((ll-1)/2)
        taille=ll;
        suite=1;
    end
end

end


%% sub function
function [factMin]=choix_param_factMin

% defaulf values
factMin=20;
vMin=1;
vMax=99;

prompt = {'taille du facteur f � appliquer pour estimer l''intensit� des minimax relatifs (entre 1 et 99 %) :(Imax-Imin)/f'};
dlg_title = 'suppresion des minimas';
num_lines = 1;
def = {num2str(factMin)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if ll >=vMin && ll<=vMax
        factMin=ll;
        suite=1;
    end
end

end




