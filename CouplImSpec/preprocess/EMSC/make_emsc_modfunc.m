function [EMSC]=make_emsc_modfunc(Zsaisir,option);
%make_emsc_modfunc   [EMSC]=make_emsc_modfunc(Zsaisir,option)
%
%                    'establishes the EMSC model'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                      %
%  Achim Kohler                                                                        %
%  Center for Biospectroscopy and Data Modelling                                       %
%  Matforsk                                                                            %
%  Norwegian Food Research Institute                                                   %
%  Osloveien 1                                                                         %
%  1430 ?s                                                                             %
%  Norway                                                                              %
%                                                                                      %
%  12.03.05                                                                            %
%                                                                                      %
%                                                                                      %
%--------------------------------------------------------------------------------------%
%  function [EMSC]=make_emsc_modfunc(Zsaisir,option);                                  %
%                                                                                      %
%  If option is not defined: default is EMSC with linear and quadratic effect          %
%  option=2: MSC plus linear                                                           %
%  option=3: MSC                                                                       %
%  option=4: EMSC + cubic                                                              % 
%  option=5: EMSC + cubic + fourth order                                               % 
%                                                                                      %
%                                                                                      %
%  Creates the basic emsc modell fucntions:                                            %
%  baseline,linear,quadratic,reference (in this oder)                                  %
%                                                                                      %
%                                                                                      %
%  Input:   Zsaisir                                                                    %
%                                                                                      %
%  Output:  EMSC structure with modell functions (see def. at end of file)             % 
%                                                                                      %
%                                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% check the input
if (nargin==1)
    %default: all model function
    Option=1;
elseif (nargin==2)
    Option=option;
end


%% Calculate the basic model functions
[Nx Ny]=size(Zsaisir.d);
WaveNum=str2num(Zsaisir.v);
Start=WaveNum(1);
End=WaveNum(Ny);

C=0.5*(Start+End);
M=4.0/((Start-End)*(Start-End));

for i=1:Ny
   Baseline(i)=1.0;
   Linear(i)=2.0*(Start-WaveNum(i))/(Start-End)-1.0;
   Quadratic(i)=M*(WaveNum(i)-C)*(WaveNum(i)-C);
   Cubic(i)=M*(1/(Start-End))*(WaveNum(i)-C)*(WaveNum(i)-C)*(WaveNum(i)-C);
   FourthOrder(i)=M*M*(WaveNum(i)-C)*(WaveNum(i)-C)*(WaveNum(i)-C)*(WaveNum(i)-C);
end
Mean=mean(Zsaisir.d,1);


%% collect the basic model functions
if (Option==1)
    MModel=[Baseline;Linear;Quadratic;Mean];
    ModelSpecNames=['Baseline        ',
                    'Linear          ',
                    'Quadratic       ',
                    'Mean            '];
elseif (Option==2)
    MModel=[Baseline;Linear;Mean];
    ModelSpecNames=['Baseline        ',
                    'Linear          ',
                    'Mean            '];
elseif (Option==3)
    MModel=[Baseline;Mean]; % MSC
    ModelSpecNames=['Baseline        ',
                    'Mean            '];
%% test
elseif (Option==4)
    MModel=[Baseline;Linear;Quadratic;Cubic;Mean];
    ModelSpecNames=['Baseline        ',
                    'Linear          ',
                    'Quadratic       ',
                    'Cubic           ',
                    'Mean            '];
elseif (Option==5)
    MModel=[Baseline;Linear;Quadratic;Cubic;FourthOrder;Mean];
    ModelSpecNames=['Baseline        ',
                    'Linear          ',
                    'Quadratic       ',
                    'Cubic           ',
                    'fourth order    ',
                    'Mean            '];

%% end test
                
                
end
[L,O]=size(MModel);


EMSC.Model=MModel';
EMSC.ModelSpecNames=ModelSpecNames;
EMSC.NumBasicModelFunc=L;  % this defines the order of basic mod func
EMSC.NgoodSpec=0;          % Number of good spectra  
EMSC.NbadSpec=0;           % Number of bad spectra in Modell function
EMSC.NModelFunc=L;% total number of model functions
EMSC.ModelVariables=Zsaisir.v;
