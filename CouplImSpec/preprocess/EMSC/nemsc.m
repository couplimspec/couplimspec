% EMSC correction from A.Kohler 
%
%modified by FJ


clear all;
close all;
 
%%load the data
load('dataemsc');

% Plot raw spectra
figure; 
set(gcf,'Color',[1 1 1]); % select the background color
plot(d.axisscale{2},d.data);
axis tight;
set(gca,'XDir','reverse'); % this is for a chemist view
xlabel('Wavenumber [cm^-^1]'); % xlabel
ylabel('Absorbance'); % ylabel
title('Raw specta'); % title



% convert to i,d,v format 

ZRaw.i=d.label{1};
ZRaw.d=d.data;
ZRaw.v=d.label{2};

% Reduce the spectral region  

% [y,i2]=min(abs(str2num(ZRaw.v)-850)); % select the wavelenght lower limit
% [y,i1]=min(abs(str2num(ZRaw.v)-1800)); % select the higher limit 
% ZRaw=selectcol(ZRaw,[i2:i1]); % select the interesting col of your Raw data

% EMSC correction

[EMSCModel]=make_emsc_modfunc(ZRaw);

% EMSC
[Z_corr,ZResiduals,ZParameters]=run_emsc(ZRaw,EMSCModel);

% MSC
%[Z_corr]=msc(ZRaw);

dc=dataset;
dc.data=Z_corr.d;
dc.name=([d.name,' _ corrected']);
dc.author=d.author;
dc.axisscale{2}=str2num(Z_corr.v);
dc.label{1}=Z_corr.i;
dc.label{2}=Z_corr.v;

% Plot corrected spectra
figure; 
set(gcf,'Color',[1 1 1]); % select the background color
plot(dc.axisscale{2},dc.data);
axis tight;
set(gca,'XDir','reverse'); % this is for a chemist view
xlabel('Wavenumber [cm^-^1]','FontSize',12); % label
ylabel('Absorption','FontSize',12); % label
title(texlabel('EMSC corrected spectra'),'FontSize',12); % title

