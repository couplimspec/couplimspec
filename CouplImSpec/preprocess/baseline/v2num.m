function v = v2num(valeurs)

%convertit des valeurs contenu dans le champ v d'une structure DIV en
%numerique, peut s'appliquer � n'importe quelle structure contenant des
%cellule de numerique ou de caract�res convertissable en num�rique.
%
%Param�tres d'entr�e : 
%   une liste de char, un liste de cellule ou autre
%param�tre de sortie : 
%   la m�me liste mais en num�rique
%Exemple d'utilisation : 
%   num = v2num (don.v)
%
% Auteur Cyrille BOITEL
%        BIA PVPP
%			date 15/04/08
%


v = str2num(char(valeurs));
