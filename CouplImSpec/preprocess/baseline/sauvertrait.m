function [nom,nomtrace] = sauvertrait(nomgen, don, nomtrace)
%Permet de sauver des donn�es dans le module traitement
%
%param�tres d'entr�e
%   Le nom generique, la structure div des spectres et le nom du fichier
%   trace
%param�tre de sortie
%   le nom de sauvegarde et le om du fichier trace   
%
% usage : 
%   [nom,nomtrace] = sauvertrait(nomgen, don, nomtrace)
% Auteur Cyrille BOITEL
%        BIA PV
%			date 13/05/08
%


%on propose un nom par defaut : nomgen.pretrait1.spp.txt, si ce nom existe
%deja, i = i+1 et on propose nomgen.pretrait2.spp.txt...
i =1;
while exist ([nomgen,'.pretrait',num2str(i),'.spp.txt'], 'file')
    i = i+1;
end
nomselec = [nomgen,'.pretrait',num2str(i),'.spp.txt'];
[nom, newpath] = uiputfile(nomselec,'Choisissez le r�pertoire d''enregistrement des donn�es : ');

%% Fichier traces
porig = pwd;

cd('Traces');

% remplissage d'un fichier trace pour indiquer les t�ches effectu�es
% actualisation du fichier trace.txt
fic=fopen(nomtrace,'a+');
if fic==0
    errordlg('probl�me d''�criture du fichier trace')
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'----------------------------------------------------------------------------\r\n');
fprintf(fic,'\t SAUVEGARDE \r\n');
fprintf(fic,'----------------------------------------------------------------------------\r\n');

% a modifier en fonction de ce que fait le programme
fprintf(fic,'\n-Origine des donn�es : module traitements\r\n');
fprintf(fic,'\n-Sauvegarde de la structure sous le nom : %s \r\n', nom);

fprintf(fic,'\n- programme adapt� pour des spectres IR \r\n');


% sauvegarde du programme utilis�
prog=which('sauvertrait.m');
prop=dir(prog);
dateprog=prop.date(1:11);

fprintf(fic,'\r\n\r\nprogramme utilis� : %s du %s\r\n',prog,dateprog);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n\r\n\r\n\r\n');

fclose all;

% fin


cd (porig)

%%
%creation du dossier et du fichier trace
nomtrace = createtrace(nom, newpath);


%on enregistre les donn�es sous la forme d'un fichier txt du type classique
%tableWrite (don,nom);
writeDIV (don,nom);

