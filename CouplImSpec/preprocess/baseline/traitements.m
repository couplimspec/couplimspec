
function varargout = traitements(varargin)

%Interface principale du module traitement
%TRAITEMENTS M-file for traitements.fig
%      TRAITEMENTS, by itself, creates a new TRAITEMENTS or raises the existing
%      singleton*.
%
%      H = TRAITEMENTS returns the handle to a new TRAITEMENTS or the handle to
%      the existing singleton*.
%
%      TRAITEMENTS('Property','Value',...) creates a new TRAITEMENTS using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to traitements_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      TRAITEMENTS('CALLBACK') and TRAITEMENTS('CALLBACK',hObject,...) call the
%      local function named CALLBACK in TRAITEMENTS.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help traitements

% Last Modified by GUIDE v2.5 23-May-2008 15:35:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @traitements_OpeningFcn, ...
                   'gui_OutputFcn',  @traitements_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before traitements is made visible.
function traitements_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for traitements
handles.output = hObject;

handles.data.i = 1;
handles.data.don = 0;
handles.data.temp = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes traitements wait for user response (see UIRESUME)
%uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = traitements_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%  delete(handles.figure1);


%% --------------------------------------------------------------------
function fichier_Callback(hObject, eventdata, handles)
% hObject    handle to fichier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % --------------------------------------------------------------------
    function chargerspe_Callback(hObject, eventdata, handles)
    % hObject    handle to chargerspe (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)


    %Choisir le fichier .spe � importer, renvoie le nom et l'adresse
    [handles.data.nom, newpath]= uigetfile ({'spe.txt'},'Selectionner le fichier des spectres');


    handles.data.nomgen = createnomgen(handles.data.nom);

    cd (newpath);

    
    %creation du dossier et du fichier trace
    handles.data.nomtrace = createtrace(handles.data.nom, newpath);

    %chargerspe les donn�es en m�moire
    handles.data.don = readDIV (handles.data.nom);

    %affichage des spectres
    spectre(handles.data.don);

    % Update handles structure
    guidata(hObject, handles);
    
    
    % --------------------------------------------------------------------
    function chargerspp_Callback(hObject, eventdata, handles)
    % hObject    handle to chargerspp (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)



    %Choisir le fichier .spe � importer, renvoie le nom et l'adresse
    [handles.data.nom, newpath]= uigetfile ({'spp.txt'},'Selectionner le fichier des spectres');


    handles.data.nomgen = createnomgen(handles.data.nom);

    cd (newpath);
    
    %creation du dossier et du fichier trace
    handles.data.nomtrace = createtrace(handles.data.nom, newpath);


    %charger les donn�es en m�moire
%    handles.data.don = tableRead (handles.data.nom);
    handles.data.don = lire (handles.data.nom);
    
    %affichage des spectres
    spectre(handles.data.don);


    % Update handles structure
    guidata(hObject, handles);

    % --------------------------------------------------------------------
    function sauver_Callback(hObject, eventdata, handles)
    % hObject    handle to sauver (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    
    handles.data.nomgen = createnomgen(handles.data.nom);
    [handles.data.nomgen, handles.data.nomtrace] = sauvertrait(handles.data.nomgen, handles.data.don,handles.data.nomtrace);
    
    % Update handles structure
    guidata(hObject, handles);    
    
%% --------------------------------------------------------------------
function edit_Callback(hObject, eventdata, handles)
% hObject    handle to edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


    % --------------------------------------------------------------------
    function annuler_Callback(hObject, eventdata, handles)
    % hObject    handle to annuler (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    %restaurer
    handles.data.don = restaurer(handles.data.tmp, handles.data.nomtrace);
    
    %affichage des spectres
    spectre(handles.data.don);


    % Update handles structure
    guidata(hObject, handles);

% --------------------------------------------------------------------
function gestion_Callback(hObject, eventdata, handles)
% hObject    handle to gestion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % --------------------------------------------------------------------
    function selectionner_Callback(hObject, eventdata, handles)
    % hObject    handle to selectionner (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    %sauvegarde des donn�es 
    handles.data.tmp = handles.data.don;
    
    handles.data.don = selec(handles.data.don, handles.data.nomtrace);

    %affichage des spectres
    spectre(handles.data.don);

    % Update handles structure
    guidata(hObject, handles)
    
 
    



%% --------------------------------------------------------------------
function correction_Callback(hObject, eventdata, handles)
% hObject    handle to correction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

   % --------------------------------------------------------------------
    function corrh2o_Callback(hObject, eventdata, handles)
    % hObject    handle to corrh2o (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    %sauvegarde des donn�es 
    handles.data.tmp = handles.data.don;
    
    %correction de la vapeur d'eau
    handles.data.don = corh2o(handles.data.don, handles.data.nomtrace);
    spectre (handles.data.don);
    
    % Update handles structure
    guidata(hObject, handles)    
    
    % --------------------------------------------------------------------
    function affine_Callback(hObject, eventdata, handles)
    % hObject    handle to affine (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    
    %sauvegarde des donn�es 
    handles.data.tmp = handles.data.don;
    
    handles.data.don = affine(handles.data.don, handles.data.nomtrace);
    
    
    %affichage des spectres
    spectre(handles.data.don);

    % Update handles structure
    guidata(hObject, handles)    

    % --------------------------------------------------------------------
    function mie_Callback(hObject, eventdata, handles)
    % hObject    handle to mie (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

        % --------------------------------------------------------------------
        function acpmie_Callback(hObject, eventdata, handles)
        % hObject    handle to acpmie (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    structure with handles and user data (see GUIDATA)
        [handles.data.mie.vp,handles.data.mie.moy, handles.data.mie.nbval] = acpmienc(handles.data.don.v);
        % Update handles structure
        guidata(hObject, handles) 
        % --------------------------------------------------------------------
        function corrparmie_Callback(hObject, eventdata, handles)
        % hObject    handle to corrparmie (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    structure with handles and user data (see GUIDATA)
        
        %sauvegarde des donn�es 
        handles.data.tmp = handles.data.don;
        
        
        %si le champ mie existe
        if isfield (handles.data, 'mie')
   
            %correction par Mie
            handles.data.don = mie(handles.data.don, handles.data.nomtrace, handles.data.mie.vp, handles.data.mie.nbval);   
        else
            
            [handles.data.mie.vp,handles.data.mie.moy, handles.data.mie.nbval] = acpmienc(handles.data.don.v);
            %correction par Mie
            handles.data.don = mie(handles.data.don, handles.data.nomtrace, handles.data.mie.vp, handles.data.mie.nbval);   
        end

        %affichage des spectres
        spectre(handles.data.don);

        % Update handles structure
        guidata(hObject, handles)    


%% --------------------------------------------------------------------
function titrecentrernormer_Callback(hObject, eventdata, handles)
% hObject    handle to titrecentrernormer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    

    % --------------------------------------------------------------------
    function centrernormer_Callback(hObject, eventdata, handles)
    % hObject    handle to centrernormer (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
 
    %sauvegarde des donn�es 
    handles.data.tmp = handles.data.don;
    
    %Centrernormer
    handles.data.don = centrernormer(handles.data.don, handles.data.nomtrace);
    
    %affichage des spectres
    spectre(handles.data.don);

    
    % Update handles structure
    guidata(hObject, handles)    



%% --------------------------------------------------------------------
function aide_Callback(hObject, eventdata, handles)
% hObject    handle to aide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


    % --------------------------------------------------------------------
    function help_Callback(hObject, eventdata, handles)
    % hObject    handle to help (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)


%% --- Executes on button press in terminer.
function terminer_Callback(hObject, eventdata, handles)
% hObject    handle to terminer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%reprise de l'execution du programme
close 



