function spectre (don)


% Dessine les spectres pass�s en parametres.
%
%
% param�tres d'entr�e :
%  une structure DIV
% param�tre de sortie :
%   rien
%
% Principe :
%   On affiche les spectres de la structure DIV avec le nombre d'onde en
%   abscisse et l'intensit� en ordonn�e.
%
% Utilisation :
%   spectre (don)
%
% Auteur Cyrille BOITEL
%        BIA PV
%			date 09/04/08
%



onde =v2num(don.v);

%affichage des spectres
plot (onde,don.d)

% for i = 1 : size (don.i,1)
%     leg{i} = don.i{i,:};
% end
leg=don.i;

%renverser l'axe des abscisses
set (gca, 'xdir', 'reverse');

%xlabel
xlabel('nombre d''onde (cm^{-1})')

%mise en place de la l�gende
legend(leg,'location','eastoutside')