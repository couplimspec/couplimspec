function don = corrspectre(spec, corr,k)

%Permet de soustraire une ligne de base a un ou une serie de spectres.
%
%parametres d'entr�e :
%   spec est une structure div contenant les spectres
%   corr est une structure div contenant la ligne de base
%
%parametre de sortie :
%   les spectres corrig�s
%
%exemple d'utilisation : 
%   spectrecorr = corrspectre (spectre, ligne_de_base)
%
% Auteur Cyrille BOITEL
%        BIA PVPP
%			date 28/04/08
%


%soustraction de la ligne de base
don = spec.d(k,:)- corr.d;