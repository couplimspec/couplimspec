function nomgen = createnomgen (nom)

%recupere un nom generique a partir du nom d'enregistrement
%
%param�tres d'entr�e
%   le nom d'enregistrement
%param�tre de sortie
%    le nom generique
%
%usage : 
%   nomgen = createnomgen (nom)
%
% Auteur Cyrille BOITEL
%        BIA PV
%			date 13/05/08
%


if ~isempty (findstr('.spp',nom))
    %On retire .spp.txt du nom d'enregistrement
    nomgen = strrep(nom,'.spp.txt','');
elseif ~isempty (findstr('.spe',nom))
    %On retire .spe.txt du nom d'enregistrement
    nomgen = strrep(nom,'.spe.txt','');
else
    nomgen=nom;
end
