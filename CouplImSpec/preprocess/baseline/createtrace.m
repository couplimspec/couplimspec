function nomtrace = createtrace (nom, newpath)

%Cree un fichier trace et retourne son nom
%
%param�tres d'entr�e
%   le nom d'enregistrement et son chemin d'acc�s
%param�tre de sortie
%    le nom du fichier trace
%
%usage : 
%   nomtrace = createtrace (nom, newpath)
%
% Auteur Cyrille BOITEL
%        BIA PV
%			date 14/05/08
%

%on enleve l'extension .txt du nom du fichier
nom = strrep(nom,'.txt','');

%si le dossier Traces n'existe pas
if ~exist ('Traces','dir')
    %On le cr�e
   mkdir('Traces');
end

%Recuperation de l'adresse courante
porig = pwd;

%On se place dans le dossier Traces
cd('Traces');

% remplissage d'un fichier trace pour indiquer les t�ches effectu�es
% actualisation du fichier trace.txt
nomtrace=strcat(nom,'.trace.txt')      ;      % � d�finir
fic=fopen(nomtrace,'a+');
if fic==0
    errordlg('probl�me d''�criture du fichier trace')
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'----------------------------------------------------------------------------\r\n');
fprintf(fic,'\t OUVERTURE DANS LE MODULE TRAITEMENT \r\n');
fprintf(fic,'----------------------------------------------------------------------------\r\n');

% a modifier en fonction de ce que fait le programme
fprintf(fic,'\n-Origine des donn�es : module importation\r\n');
fprintf(fic,'\n- R�pertoire du fichier de d�part : %s\r\n',newpath);

fprintf(fic,'\n- programme adapt� pour des spectres IR \r\n');


% sauvegarde du programme utilis�
prog=which('traitements.m');
prop=dir(prog);
dateprog=prop.date(1:11);

fprintf(fic,'\r\n\r\nprogramme utilis� : %s du %s\r\n',prog,dateprog);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n\r\n\r\n\r\n');

fclose all;

% fin

%On revient dans le repertoire de debut
cd (porig)