function varargout = affine(varargin)

%Interface de soustraction d'une ligne de base affine par morceaux
%affine M-file for affine.fig
%      affine, by itself, creates a new affine or raises the existing
%      singleton*.
%
%      H = affine returns the handle to a new affine or the handle to
%      the existing singleton*.
%
%      affine('Property','Value',...) creates a new affine using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to affine_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      affine('CALLBACK') and affine('CALLBACK',hObject,...) call the
%      local function named CALLBACK in affine.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help affine

% Last Modified by GUIDE v2.5 28-Apr-2008 15:00:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @affine_OpeningFcn, ...
                   'gui_OutputFcn',  @affine_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before affine is made visible.
    function affine_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for affine
handles.output = hObject;

handles.data.don = varargin{1};
handles.data.nomtrace = varargin{2};

%calcul du spectre moyen
handles.data.moy = spectremoyen (handles.data.don);

%Affichage du spectre moyen
spectre (handles.data.moy);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes affine wait for user response (see UIRESUME)
 uiwait(handles.figure1);

 
% --- Outputs from this function are returned to the command line.
function varargout = affine_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%suppression de la figure
delete (handles.figure1)


% --------------------------------------------------------------------
function correction_Callback(hObject, eventdata, handles)
% hObject    handle to correction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



    % --------------------------------------------------------------------
    function corrmoy_Callback(hObject, eventdata, handles)
    % hObject    handle to corrmoy (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    handles.data.moy = corrspectremoyen(handles.data.moy,handles.data.aff);
    
    %on met a jour les champs hObject et handles
    guidata(hObject,handles);
    
    spectre( handles.data.moy);    
    
    % --------------------------------------------------------------------
    function corrspec_Callback(hObject, eventdata, handles)
    % hObject    handle to corrspec (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    handles.data.don = calculeraffine(handles.data.don, handles.data.i);
    
    spectre( handles.data.don);
    
    %on met a jour les champs hObject et handles
    guidata(hObject,handles);


% --- Executes on button press in demarrer.
function demarrer_Callback(hObject, eventdata, handles)
% hObject    handle to demarrer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%recherche des minima
[handles.data.i,handles.data.aff] = recherchemin(handles.data.moy);

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in terminer.
function terminer_Callback(hObject, eventdata, handles)
% hObject    handle to terminer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


handles.output = handles.data.don;

%on met a jour les champs hObject et handles
guidata(hObject,handles);
%%

porig = pwd;
nomtrace = handles.data.nomtrace;
cd('Traces');

% remplissage d'un fichier trace pour indiquer les t�ches effectu�es
% actualisation du fichier trace.txt
fic=fopen(nomtrace,'a+');
if fic==0
    errordlg('probl�me d''�criture du fichier trace')
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'----------------------------------------------------------------------------\r\n');
fprintf(fic,'\t CORRECTION DE LA LIGNE DE BASE PAR FONCTION affine PAR MORCEAUX \r\n');
fprintf(fic,'----------------------------------------------------------------------------\r\n');

% a modifier en fonction de ce que fait le programme
fprintf(fic,'\n-Origine des donn�es : module traitements\r\n');
fprintf(fic,'\n-Trac� d''une ligne droite par morceaux, passant par des minima locaux. \r\n');

fprintf(fic,'\n- programme adapt� pour des spectres IR \r\n');


% sauvegarde du programme utilis�
prog=which('affine.m');
prop=dir(prog);
dateprog=prop.date(1:11);

fprintf(fic,'\r\n\r\nprogramme utilis� : %s du %s\r\n',prog,dateprog);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n\r\n\r\n\r\n');

fclose all;

% fin
cd (porig);
%%
uiresume(handles.figure1);
