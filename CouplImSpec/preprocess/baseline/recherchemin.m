function [i,res] = recherchemin (don)


%Permet de trouver des minima locaux et de tracer la fonction affine par
%morceaux qui passe par ces minima
%
%parametres d'entr�e :
%   don est une structure div contenant les spectres
%  
%parametre de sortie :
%  res est une structure DIV contenant la fonction affine par morceaux
%  passant par les minima
%
%exemple d'utilisation : 
%   aff = recherchemin(spectre)
%
% Auteur Cyrille BOITEL
%        BIA PVPP
%			date 23/04/08
%

ok = 0;
sel =0;

%tant qu ok est egal � 0
while ok == 0
    
    if ~sel
        h = inputdlg('Entrez un pas h d''extended minima','Entrez h');
   

        %Recheche des extended minima du spectre de pas h
        i = imextendedmin(don.d, v2num(h));
        
       
    end;
    
    
    %affichage des spectres
    spectre (don);
    hold on;
    
    %affichage des extended minima en rouge
    % plot(v2num(don.v), i*max(don.d),'r');
    plot(v2num(don.v), i.*don.d,'r');

    %attribution d'une valeur differente pour chaque zone de "uns"
    lbl = bwlabel(i);
    
    %recuperation des bornes de don, en valeur et en indices 
%     mini(1) = don.d ( find(v2num(don.v) == max (v2num (don.v))));
%     k(1) =( find(v2num(don.v) == max (v2num (don.v))));
%     
%     mini(max(lbl)+2) = don.d ( find(v2num(don.v) == min (v2num (don.v))));
%     k(max(lbl)+2) = find(v2num(don.v) == min (v2num (don.v)));

    %recuperation des bornes de don, en valeur et en indices 
    mini(1) = don.d (:,1);
    indice (1) = 1;
    
    %mini(max(lbl)+2) = don.d (:,size(don.v, 1));
    %indice (max (lbl)+2) = size(don.v,2);
    %indice (max (lbl)+2) = size(don.v,1);

    
    %recuperation de l'intensite et de l'indice des minima dans les zones
    %des extended minima
    nmin=1;
    for j = 1: max (lbl)
        
        mini (j) = unique(min(don.d (lbl==j)));
        indice(j) = find (don.d==mini(j)&lbl==j);
        nmin=nmin+1;
    end    
    
    if indice(end)==size(don.d,2)
        nmin=nmin-1;
    else
        mini(nmin) = don.d (:,size(don.v, 1));
        %indice (max (lbl)+2) = size(don.v,2);
        indice (nmin) = size(don.v,1);
    end;
    
    [indice,ii]=unique(indice);
    mini=mini(ii);
    
    if max(lbl)~=nmin
        valeur=lbl(indice(2:(length(indice)-1)));
        if valeur(end)==lbl(end)
            mini=mini([1:(length(indice)-2) length(indice)]);
            indice=indice([1:(length(indice)-2) length(indice)]);
            nmin=nmin-1;
        end;
        if valeur(1)==lbl(1)
            mini=mini([1 3:(length(indice))]);
            indice=indice([1 3:(length(indice))]);
            nmin=nmin-1;
        end;
    end;
            
            
     
    %affichage des minima locaux en diamants noirs
    plot (v2num(don.v(indice,:)), mini, 'kd');
    
    
    %pour chaque zones entre deux minima
    %for j = 1 : max(lbl)+1
    for j = 1 : (length(indice)-1)
        
        %calcul des coefficient de la droite d'equation y = ax+b
        a = (mini(j+1)-mini(j))/(indice(j)-indice(j+1));
        b = mini(j) + a* indice(j);
        
        %indices de la zone
%         if v2num(don.v(1,:))>v2num(don.v(2,:))
%             l = find(v2num(don.v)<=v2num(don.v(indice(j)))&v2num(don.v)>=v2num(don.v(indice(j+1))));
%         else
%             l = find(v2num(don.v)>=v2num(don.v(indice(j),:))&v2num(don.v)<=v2num(don.v(indice(j+1),:)));
%         end
       % vv=v2num(don.v);
       if j==1
           l=1:indice(j+1);
       else
           l=(indice(j)):indice(j+1);
       end;

        %attribution des champs � la structure de retour
        res.i = don.i;
        res.v(l,:) = don.v(l,:);
        res.d(l) =(-l)*a+b; 
        
        %affichage de la doite sur la zone
        plot(v2num(res.v(l,:)),res.d(l),'g');
    end

    hold off;
    
    if ~sel
        %verification de la convenance du pas
        ok = yesno('Ce pas vous convient-il ?');
    else
        ok=1;
    end;
    
    if ok & ~sel
        %s�lection des minimum
        numorig=don.v(indice,:);        
        %numorig=flipud(don.v(indice)');
        [selection]=listdlg('liststring',numorig,'selectionmode','multiple','name','selection des minima','promptstring','choisir ou �liminer les minima');
        sel=selection;
%        sel=selection(2:(length(selection)-1));
         %inew=lbl==(sel(1)-1);
         inew=lbl==sel(1);
         for t=2:length(sel)
             inew=inew|(lbl==sel(t));
         end
         i=inew;
        %i=false(size(i));
        %i(indice(selection))=1;
        clear mini
        clear indice
        ok=0;
        sel=1;
        nmin=length(sel);
    end;

end

 spectre (don);
    hold on;

  if indice(1)~=1
        indice=[1 indice];
        mini=[don.d(:,1)  mini];
    end
    
    if indice(end)~=size(don.d,2)
        indice=[indice size(don.d,2)];
        mini=[mini don.d(:,end)];
    end;
    
     for j = 1 : (length(indice)-1)
        
        %calcul des coefficient de la droite d'equation y = ax+b
        a = (mini(j+1)-mini(j))/(indice(j)-indice(j+1));
        b = mini(j) + a* indice(j);
        

           l=(indice(j)):indice(j+1);

        %attribution des champs � la structure de retour
        res.i = don.i;
        res.v(l,:) = don.v(l,:);
        res.d(l) =(-l)*a+b; 
        
        %affichage de la doite sur la zone
        plot(v2num(res.v(l,:)),res.d(l),'g');
     end

    hold off

clear mini
clear indice;

% lbl=bwlabel(i);
% for j=2:max(lbl-1)
%     mini{j}=(imdilate(lbl==j,strel('line',11,0)));
% end;
% mini{1}=lbl==1;
% mini{max(lbl)}=lbl==max(lbl);
% i=mini;

% spectre (don);
% hold on;
    
    %affichage des extended minima en rouge
    % plot(v2num(don.v), i*max(don.d),'r');
%plot(v2num(don.v), i.*don.d,'r');
