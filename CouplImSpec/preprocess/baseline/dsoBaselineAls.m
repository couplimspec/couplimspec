function imsc=dsoBaselineAls(varargin)
%
%% description
% remove baseline from a set of curves in dso
%
%% input
%           soit pas de param�tres
%           soit
%               1 - nom du fichier
%                   ou dataset object en m�moire matlab
%               2 - lambda : the importance of weight for smooth baseline
%               3 - p wieght for  positive difference between signal and
%               baseline
%
%% output
%           imsc : smoothed data set saved on disk
%
%% principe
%
% penalized least sqaure method proposed by eilers and boelens (2005)
% Baseline Correction with Asymmetric Least Squares Smoothing
% P. H. C. Eilers and H. F. M. Boelens, http://www.science.uva.nl/%7Ehboelens/publications/draftpub/Eilers_2005.pdf, 2005.
%
% basleine correcvtion is written as :
%           S = sum( wi(yi ? zi)^2 + Lambda*sum(diff2(zi)^2).
%
%   where  yi is the signal,
%               zi the estimated baseline,
%               diff2(zi)=(zi?z(i?1))?(z(i?1)?z(i?2))
%               lambda is the regularisation parameter to give a wieght to
%               the fact that baseline should be smooth (recommended values
%               are between 10^2 and 10^9
%               wi is a weigth that penalised positive versus negative
%               difference between signal and basleine
%               w = p * (y > z) + (1 - p) * (y < z);
%               recommended values for b are between  0.001  and  0.1 for a signal with positive peaks

%               first test on infrared spectra between 1600 and 900 cm-1
%               are lambda =10^4.5 and p=0.0001.
%
%   following matlab code is directly taken from the publication
%
%% Use :
%       dsob=dsoBaselineAls(<dsoNameFile>,<lambda>,<p>)
%
%
%% Auteur :
%           MArie-Fran�oise Devaux et Sylvie Durand
%           INRA - BIA - PVPP
%
%% Date : 12 mars 2010
%       13 f�vrier 2018
%
%

%% context variables
porig=pwd;

%% input

if nargin~=0 && nargin~=3
    error('use: dsob=dsoBaselineAls(<dsoNameFile>,<lambda>,<p>)');
end

if nargin == 3
    if ischar(varargin{1})
        nom=varargin{1};
        % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
            error('format .dso.mat expected')
        end
        ims=loaddso(nom);
        rfolder=pwd;
        
    else
        if strcmp(class(varargin{1}),'dataset') %#ok<STISA>
            ims=varragin;
            nom=ims.name;
        else
            error('use: dsob=dsoBaselineAls(<dsoNameFile>,<lambda>,<p>)');
        end
    end
    
    snom=strcat(ims.name,'.base.dso.mat');
    
    
    if isnum(varargin{2})
        lambda=varargin{2};
    else
        error('numeric value expected as lambda parameter');
    end
    
    if isnum(varargin{3})
        p=varargin{3};
    else
        error('numeric value expected as p parameter');
    end
    
    
    verbose=0;
    
end


if nargin==0
    [nom,rfolder]=uigetfile({'*.mat'},'nom du fichier des spectres � traiter : ','*.dso.mat');
    cd(rfolder)
    
    ims=loaddso(nom);
    
    %     % lambda
    %     lambda=inputNumber('value of lambda paremeter : recommended values between 100 and 1 000 000 000 (FTIR 1600-900 cm-1 : 10^4.5 =31623)','def',10^4.5);
    %
    %     % p
    %     p=inputNumber('value of p paremeter : recommended values between 0.001 and 0.1  (FTIR 1600-900 cm-1 : 0.0001)','def',0.0001);
    prompt = {'value of lambda paremeter : recommended values between 100 and 1 000 000 000 (FTIR 1600-900 cm-1 :','value of p paremeter : recommended values between 0.001 and 0.1  (FTIR 1600-900 cm-1 : 0.005):'};
    dlg_title = 'Input';
    num_lines = 1;
    defaultans = {'50000','0.001'};
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    lambda=str2double(answer{1});
    p=str2double(answer{2});
    
    % sfolder=strcat(rfolder,'.sm');
    cd ..
    
    snom=strcat(strrep(ims.name,'.dso.mat',''),'.base.dso.mat');
    
    
    [~,sfolder]=uiputfile({'*.mat'},'save resulting file as dso: ',snom);
    
    verbose=1;
end




%% treatment
if verbose
    close all
    % affichage des spectres de d�part
    plotdso(ims,1,0)
    title('Initial spectra');
    
end

% matrix of spectra
sp=ims.data;


ok=0;
while ~ok
    lb=sp;
    for i=1:size(sp,1)
        lb(i,:)=baselineals(sp(i,:)',lambda,p);
    end
    
    spb=sp-lb;
    
    if verbose
        
        figure
        subplot(1,2,1)
        plot(ims.axisscale{2},lb)
        if strcmp(ims.userdata.acquisition.method,'INFRARED') | strcmp(ims.userdata.acquisition.method,'RAMAN')
            set(gca,'xdir','reverse')
        end
        subplot(1,2,2)
        plot(ims.axisscale{2},spb)
        if strcmp(ims.userdata.acquisition.method,'INFRARED') | strcmp(ims.userdata.acquisition.method,'RAMAN')
            set(gca,'xdir','reverse')
        end
        
        ok=yesno('is it ok ?');
        if ~ok
            %             % lambda
            %             lambda=inputNumber('value of lambda paremeter : recommended values between 100 and 1 000 000 000 (FTIR 1600-900 cm-1 : 10^4.5 =31623)','def',10^4.5);
            %
            %             % p
            %             p=inputNumber('value of p paremeter : recommended values between 0.001 and 0.1  (FTIR 1600-900 cm-1 : 0.0001)','def',0.0001);
            
            defaultans = {num2str(lambda),num2str(p)};
            answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
            lambda=str2double(answer{1});
            p=str2double(answer{2});
            
        end
        
    else
        ok=1;
    end
end




imsc=ims;
imsc.data=spb;
imsc.name=strrep(snom,'.dso.mat','');


if verbose
    plotdso(imsc,1,0)
    title('spectra after baseline correction');
end


% baseline

base=ims;
base.data=lb;
base.name=strrep(nom,'.dso.mat','.baseline');



%% sauvegarde des r�sultats
if verbose
    cd(sfolder)
    savedso(imsc);
    savedso(base);
end

%% trace de l'ex�cution du programme

if verbose
    
    fic=fopen(strcat(strrep(snom,'.dso.mat',''),'.track.txt'),'w');
    if fic==0
        errordlg('probl�me d''�criture du fichier %s',strcat(strrep(noms,'.dso.mat',''),'.track.txt'));
    end
    
    fprintf(fic,'%s\r\n',datestr(now,0));
    fprintf(fic,'__________________________________________________________________________\r\n');
    fprintf(fic,'\t\t Baslien correction with Asymmetric Least Squares Smoothing\r\n\r\n');
    fprintf(fic,'Read folder :\r\n\t- %s\r\n',rfolder);
    fprintf(fic,'\r\nFile : %s\r\n',nom);
    fprintf(fic,'\r\n\r\n');
    
    fprintf(fic,'Computing based on: P. H. C. Eilers and H. F. M. Boelens, http://www.science.uva.nl/%%7Ehboelens/publications/draftpub/Eilers_2005.pdf, 2005.\r\n');
    fprintf(fic,'\r\n\r\n');
    
    % param�tres de correction
    fprintf(fic,'Parameters: \r\n\r\n');
    fprintf(fic,'\t- lambda (regularisation for smoothing): %e\r\n',lambda);
    fprintf(fic,'\t- p (regularisation to avoid negative difference between signal and baseline): %e''\r\n',p);
    
    fprintf(fic,'\r\n\r\n');
    fprintf(fic,'save file :%s \r\n',snom);
    fprintf(fic,'in folder: %s \r\n',rfolder);
    
    % sauvegarde du programme utilis�
    fprintf(fic,'__________________________________________________________________________\r\n');
    info=which(mfilename);
    repprog=fileparts(info);
    fprintf(fic,'Programme : %s ',mfilename);
    res=dir(info);
    fprintf(fic,'du %s\r\n',res.date);
    fprintf(fic,'\r\nR�pertoire du programme : %s \r\n',repprog);
    
    fclose(fic);
    
    
    %% fin
    cd(porig)
end
end

function z = baselineals(y, lambda, p)
% Estimate baseline with asymmetric least squares
m = length(y);
D = diff(speye(m), 2);
w = ones(m, 1);
for it = 1:10
    W = spdiags(w, 0, m, m);
    C = chol(W + lambda * (D' * D));
    z = C \ (C' \ (w .* y));
    w = p * (y > z) + (1 - p) * (y < z);
end

end






