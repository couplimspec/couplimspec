function  resultat = calculeraffine(don, i) 
%Permet de calculer la fonction affine passant par les minima locaux
%definis dans ses zones pass�es en param�tres et la soustraire aux
%spectres.
%
%parametres d'entr�e :
%   don est une structure div contenant les spectres, un masque contenant
%   les zones de minima
%  
%parametre de sortie :
%  resultat est une structure DIV contenant les spectres corrig�s.
%
%exemple d'utilisation : 
%   aff = calculeraffine(spectre)
%
% Auteur Cyrille BOITEL
%        BIA PVPP
%			date 28/04/08
%


%attribution d'une valeur differente pour chaque zone de "uns"
lbl = bwlabel(i);
    
%recuperation des bornes de don en indice
% indice (1) = 1;
% indice (max (lbl)+2) = size(don.d,2);

for k = 1 : size (don.i,1)
    display(k)
    sp=don.d(k,:);
    clear mini;
    clear indice;
    
    %recuperation des bornes de don, en valeur
    mini(1) = don.d (k,1);
    indice (1) = 1;
    %mini(max(lbl)+2) = don.d (k,size(don.d, 2));


    %recuperation de l'intensite et de l'indice des minima dans les zones
    %des extended minima
    nmin=1;
    
    
   % for j = 2: (max (lbl)-1)
    for j = 1: (max (lbl))
      %  nmin=nmin+1;
        mini (j) = unique(min(don.d (k,lbl==j)));
        indice(j) = find (don.d(k,:)==mini(j)&lbl==j,1);
            
    end    
    
    nmin=max(lbl);
    
    %mini(nmin+1) = sp(size(don.v, 1));
    %indice (max (lbl)+2) = size(don.v,2);
   % indice (nmin+1) = size(don.v,1);
    
    [indice,ii]=unique(indice);
    mini=mini(ii);
    nmin=length(mini);
    
%    for j = 1: max (lbl)
%        mini (j+1) = unique(min(don.d (k,(lbl==j))));
%        indice(j+1) = find (don.d(k,:)==mini(j+1)&lbl==j,1);
%    end    


    %pour chaque zones entre deux minima
   % for j = 1 : max(lbl)+1
   
    if indice(1)~=1
        indice=[1 indice];
        mini=[sp(:,1)  mini];
    end
    
    if indice(end)~=size(don.d,2)
        indice=[indice size(don.d,2)];
        mini=[mini sp(:,end)];
    end;
    
    nmin=length(indice);
    
    for j = 1 : nmin-1
        %calcul des coefficient de la droite d'equation y = ax+b
        a = (mini(j+1)-mini(j))/(indice(j)-indice(j+1));
        b = mini(j) + a* indice(j);

        %indices de la zone
      %  l = find(v2num(don.v)>=v2num(don.v(indice(j),:))&v2num(don.v)<=v2num(don.v(indice(j+1),:)));
      
       if j==1
           l=1:indice(j+1);
       else
           l=(indice(j)):indice(j+1);
       end;


        %attribution des champs � la structure de retour
        res.i = don.i;
        res.v(l,:) = don.v(l,:);
        res.d(l) =(-l)*a+b; 
        

    end
    resultat.v = don.v;
    resultat.i = don.i;
    resultat.d(k,:) = corrspectre(don, res,k);
    
%     figure(27)
%     plot(str2num(don.v),sp)
%     set(gca,'xdir','reverse')
%     hold on
%     plot(str2num(don.v),res.d)
%     hold off
%     input('')
end
