function moy = spectremoyen (don)

%la fonction spectremoyen permet de calculer le spectre moyen 
%
%Parametres d'entr�e : 
%   don est une strucure DIV de type cell contenant les spectres
%
%Parametres de sortie : 
%  moy est une structure DIV type Cell contenant le spectre moyen
%
%exemple d'utilisation : 
%   map = createmap(don)
%
% Auteur Cyrille BOITEL
%        BIA PVPP
%			date 20/04/08
%

%Attribution des champs V et I
moy.v = don.v;
moy.i = {'moyenne'};

%Calcul de la moyenne
moy.d = mean (don.d,1);
