function don = corrspectremoyen(spec, corr)

%Permet de soustraire une ligne de base a un ou une serie de spectres.
%
%parametres d'entr�e :
%   spec est une structure div contenant les spectres
%   corr est une structure div contenant la ligne de base
%
%parametre de sortie :
%   une structure div contenant les spectres corrig�s
%
%exemple d'utilisation : 
%   spectrecorr = corrspectre (spectre, ligne_de_base)
%
% Auteur Cyrille BOITEL
%        BIA PVPP
%			date 24/04/08
%

%attribution de tous les champs de spec � don
don = spec;

%soustraction de la ligne de base
don.d = spec.d- repmat(corr.d, size(spec.i));