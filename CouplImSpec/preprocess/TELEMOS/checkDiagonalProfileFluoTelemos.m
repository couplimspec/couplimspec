function checkDiagonalProfileFluoTelemos

 %% description
    % plot diagonal profile of a series of image found in folder
    
%    this function is specific of Telemos images (DISCO line SOLEIL)
% acquired using micromanager software linked to imageJ

%% input

    %  nothing interactive fonction
%% output
    
    %   nothing results are saved on disk

%% principe
% Images found in a folder are considered

%
% the intensities along the diagonal top left to bottom right are ecovered for each image

% profile are drawn on the same graph


%% use
    % checkDiagonalProfileFluoTelemos

    
%% Comments
    %   tool
    
    
%% Author
    % MF Devaux 
    % INRA BIA
    % PVPP
    
%% date
     % 26 avril 2019  

%% context variables 
orig=pwd;           % returns the current directory


%% start

if nargin >0
    error('use: checkDiagonalProfileFluoTelemos');
end


%% input
[~,rfolder]=uigetfile({'*.tif'},'name of first TELEMOS fluorescence image','*fluo*.tif');

cd (rfolder)

list=dir('*fluo*.tif');
if isempty(list)
    list=dir('*.tif');
end
list={list.name}';

nb=length(list);

%% treatment


for i=1:nb
    
    im=imread(list{i});

    %initialisation
    if i==1
        nl=size(im,1);
        nc=size(im,2);
        sim=zeros(nl,nc,nb);
    end
    
    sim(:,:,i)=im;
    
    
    
    
    c=improfile(im,[1 nc],[1 nl]);
    
    p.d(i,:)=c;
end




nom=strrep(list,'.tif','');
nom=strrep(nom,'img_000000000_','');
nom=strrep(nom,'_fluo_000','');
nom=strrep(nom,'_','-');

nom=char(nom);

p.i=nom;
p.v=(1:size(p.d,2))';

%% save
if ~exist('diagonalProfiles','dir')
    mkdir('diagonalProfiles')
end
cd('diagonalProfiles')
sfolder=pwd;

writeDIV(p,'diagonalProfiles');

figure
plot(p.d','linewidth',1.5)
xlabel('pixel','fontsize',18)
title('TELEMOS fluorescence intensity','fontsize',18)

ho=legend(nom);
set(ho,'location','eastoutside','fontsize',12)
title('diagonal profiles','fontsize',18)
saveas(gcf,'diagonalProfiles.png')

figure
imshow(mean(sim,3),[])
hold on
plot([1 nc],[1 nl],'r','linewidth',2)
title('image sum of intensities','fontsize',18);
saveas(gcf,'diagonalProfiles.locate.png')


%% tracking
%% matlab function tracking  
cd(sfolder);

fid=fopen('DiagonalProfiles.track.txt','w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Telemos Fluorescence diagonal intensity profiles of all images found in a single folder \r\n');
fprintf(fid,'____________________________________________________________________________\r\n\r\n');

fprintf(fid,'\r\Image files found in folder: %s\r\n',rfolder);
fprintf(fid,'\r\n ');
fprintf(fid,'Plot of diagonal profiles diagonalProfiles.png\r\nSaved in folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    

