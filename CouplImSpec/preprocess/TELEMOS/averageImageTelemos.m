function averageImageTelemos


%% description
% average  images found in a folder

% this function is specific of telemos images (DISCO line SOLEIL)
% acquired using micromanager software linked to imageJ
%% input

%  nothing interactive fonction
%% output

%   nothing average images are saved on disk

%% principe
%
% root folder : name given by the user
%
%   subfolders roi(n)_tile1 : n folders for each selected roi
%           or pos(n) : n folders for each selected position
%
%   display_and_comments.txt : file describing the channels acquired
%
% in each subfolder roi(n)_tile1 or Pos(n) :
%       images files with name img_00000000(n)_DM300_327-353_00(p).tif
%               n = number of time
%               p = z focal plane
%               DM300_327-353 = name of the channel
%       metadata.txt : files describing all metadata associated with the
%       acquisition : x, y, z position, camera settings .... etc.
%
% (Nb: depending on the date of acquisition, an extension fluo is found or
% not in the name of fluorescence image. this extension is given in
% contrast to the visible image.)
%
% IMAGES: function developped for averaging dark and white images
%
% raw images are search in the PosO folder in the root directory
% all images (filters, t, z)  in the PosO are processed.
%
% nb: the case of rois is not taken into account
%
% WHAT IS DONE: 
% Images found Pos0 folder are averaged and restored in the initial
% image type
%
% SAVED
%
% images are saved on disk on a rootFolder at the same level than the original RootFolder
% the name of the new folder is <rootFolder>_average
%


%% use
% averageImageTelemos

%% Comments
%   tool


%% Author
% MF Devaux
% INRA BIA
% PVPP

%% date
% 26 avril 2019

%% context variables
orig=pwd;           % returns the current directory


%% start

if nargin >0
    error('use: averageImageTelemos');
end


%% input
[rootFolder]=uigetdir('.','Root Folder of images to be averaged');
cd(rootFolder)
if ~exist('Pos0','dir')
    error('Pos0 folder does not exist. TELEMOS folder structure expected')
end
cd('Pos0')
rfolder=pwd;
[~,name,~] = fileparts(rootFolder) ;

cd(rootFolder)
cd ..
if ~exist(strcat(name,'_average'),'dir')
    mkdir(strcat(name,'_average'))
end
cd(strcat(name,'_average'))
sfolder=pwd;

%save
sname = strcat(name,'.average.tif');

%% treatement
% list of images
cd(rfolder)
list=dir('*.tif');
nb=length(list);
if nb==0
    error('no image found in folder %s\Pos0',rfolder);
end

% compute the sum
for i=1:nb
    fprintf('%d: %s\r\n',i,list(i).name)
    im=imread(list(i).name);
    
    imtype=class(im);
    
    % check image type
    if i==1
        typedep=imtype;
    else
        if ~strcmp(typedep,imtype)
            error('image type of images %d differ from the fisrt image',i)
        end
    end
    
    % sum
    imtmp=double(im);
    
    if i==1
        imtot=imtmp;
    else
        imtot=imtot+imtmp;
    end
    
end

% average
imtot=imtot/nb;

% conversion to initial image type
fimtype=str2func(typedep);
imtot=fimtype(imtot);



% save
cd(sfolder);
imwrite(imtot,sname);


%% matlab function tracking
cd(sfolder);

fid=fopen(strcat(sname,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Average all images found in a single folder \r\n');
fprintf(fid,'____________________________________________________________________________\r\n\r\n');

fprintf(fid,'\r\nRaw files found in folder: %s\r\n',rootFolder);

fprintf(fid,'Average images %s\r\nSaved in folder: %s\r\n',sname,sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
