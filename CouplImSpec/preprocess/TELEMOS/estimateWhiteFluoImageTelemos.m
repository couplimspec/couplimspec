function estimateWhiteFluoImageTelemos


%% description
% estimation of the white fluorescence image shape of synchrotron light from
% experimental  images of Telemos microscope

% this function is specific of Telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    

%% input

%  nothing : interactive function

%% output

%  nothing : the final reference fluorescence image is saved on disk

%% principe
%  NB: MICROMANAGER save images in a structured file folder architecture :
%
% root folder : name given by the user
%
%   subfolders roi(n)_tile1 : n folders for each selected roi
%           or pos(n) : n folders for each selected position
%
%   display_and_comments.txt : file describing the channels acquired 
%
% in each subfolder roi(n)_tile1 or Pos(n) :
%       images files with name img_00000000(n)_DM300_327-353_00(p).tif
%               n = number of time
%               p = z focal plane
%               DM300_327-353 = name of the channel
%       metadata.txt : files describing all metadata associated with the
%       acquisition : x, y, z position, camera settings .... etc.
%
% (Nb: depending on the date of acquisition, an extension fluo is found or
% not in the name of fluorescence image. This extension is given in
% contrast to the visible image.)
%
%
%
% IMAGES : fluorescence images are considered
% 
%
% IMPORTANT:
%       it is expected that in the folders at least several images cover the whole field of view 
%       illuminated by the synchrtron light
%       IF IT IS NOT THE CASE, IT WON'T WORK
%
% BASIC: the estimated white image is the MAX of the selected images
%       when interactive mode is selected, only first time and first z are
%       proposed
%       for automatic computing, all images are taken into account
%
% WHAT IS DONE:
%        0 - the user chosse the channels that are considered  in the estimation of the
% white fluorescence image
%       
%       1 - spurious pixels (three lines and columns around the image) are not taken
% in filtering and replaced by line and column number 4 and end-3
%
%       2 - Filtering
% the estimated dark image is filtered :
%        opening to remove  white regions brighter than the low frequency signal that should correspond to  synchrotron light shape (should be small)
%        closing to remove black regions that remains (should be small to avoid synchrotron light shape deformation)
%        average filtering

% the image computed should be used  to
%   - to find the best white z plane from the reference white stack
%   acquired for ex. using the so called "Matthieu lame"%
%   - correct images for intensities

 

%% use
%       estimatedwhiteFluoImageTelemos

%% Comments
%   adapted from proposals 20161050 and 20171187


%% Author
% MF Devaux
% INRA BIA
% PVPP

%% date
% 5 octobre 2017:
% 23 mars 2018
% 3 septembre 2018 : comments and general case
% 16 avril 2019: name of function and default filtering values
% 29 avril 2019 : new comments and spurious pixels
% 27 mai 2019 : offset dark 
%     4 juin 2019 : replace exist by isfolder or isfile
% 14 juin : close figure at the end

%% context variables
orig=pwd;           % returns the current directory


%% start

if nargin ~=0
    error('use: estimatedwhiteFluoImageTelemos');
end


%% input
[nameFolder,rootFolder,listDir,listChannel]=getInfoTelemos('Root folder to process');

cd(rootFolder)

nb=size(listDir,1);

% channels
lc =cellstr(listChannel);
[csel,ok] = listdlg('Name','Channels','promptString','Select channels','SelectionMode','multiple','ListString',lc);
if ok
    lc=lc(csel);
    nc=length(lc);
end

inter=yesno('interactive checking of images included for estimation?                 Choose this option for a small number of image');


darkCorrection=yesno('Subtract background for estimated white image ?                        Choose this option if you have no reference image from the so called "lame Matthieu"');
if darkCorrection
    cd(rootFolder)
    cd ..
    [ndark,rfolderDarkforWhite]=uigetfile('*.tif','Dark image for estimated white image');
end


% for saving fluorescence estimated white image
cd(rootFolder)
if~isfolder('whiteEstimate')
    mkdir('whiteEstimate')
end
cd('whiteEstimate')
sfolder=pwd;

sname=uiputfile({'*.tif'},'save fluorescence estimated white image of sample','whiteEstimate.tif');

%% treatement

h=figure(1);
set(h,'units','normalized');
set(h,'position',[0.1 0 0.8 0.8]);

% initialisation for each subfolder of the micromanager structure
if inter
    sfile=cell(nb);
end

% for each folder
for i=1:nb
    disp(listDir(i,:));
    
    cd(rootFolder)
    cd(listDir(i,:));
    
    % for each channel selected : build the list of file to be considered
    for c=1:nc
        if inter                % interactive selection : only first time and first z considered
            ltmp=dir(['img_000000000_' lc{c} '*000.tif']);    
        else                     % automatic selection : all image = all time and all z considered
            ltmp=dir(['*' lc{c} '*.tif']);
        end
        
        % list of image file to consider
        if c==1
            lfile=char(ltmp.name);
            %dir('*fluo*.tif');
        else
            lfile=char(lfile,ltmp.name);
        end
    end
    nbf=size(lfile,1);
    
    if inter            % interactive mode: initialisation of selection
        sel=zeros(nbf,1);
    end
    
    % for each fluorescence image considered
    for j=1:nbf
        im=imread(lfile(j,:));
        
        oks=1;
    
        if inter                        % ask for each image if itis selected or not
            figure(1)
            subplot(1,2,1)
            imshow(im,[prctile(im(:),0.1) prctile(im(:),99.5)])
            title(lfile(j,:))
            sel(j)=yesno('select current image');
            if sel(j)
                oks=1;
            else
                oks=0;
            end
            
        end
    
        % progressively built the MAX of all selected images
        if oks
            if ~exist('imref','var')
                imref=im;
            else
                imref=max(imref,im);
            end
        end
        
        
        if inter && exist('imref','var')            % interactive mode: show image max at each iteration
            figure(1)
            subplot(1,2,2)
            imshow(imref,[prctile(imref(:),0.1) prctile(imref(:),99.5)])
            title('raw max image')
            sfile{i}=lfile(logical(sel),:);
        end
    end
       
end


% show resulting image MAX at the end iof all iterations
figure(1)
subplot(1,2,1)
imshow(imref,[prctile(imref(:),0.1) prctile(imref(:),99.5)])
title('raw max image ')

% modifiy spurious pixels on the side of the images
imtmp=imref;
imref=imref(4:(end-3),4:(end-3));
imtmp(1:3,4:(end-3))=repmat(imref(1,:),3,1);
imtmp((end-2):end,4:(end-3))=repmat(imref(end,:),3,1);
imtmp(:,1:3)=repmat(imtmp(:,4),1,3);
imtmp(:,(end-2):end)=repmat(imtmp(:,end-3),1,3);
imref=imtmp;
clear imtmp;

% post treatment of image MAX
ok=0;
openSize=75;
closeSize=75;
aveSize=75;


while ~ok
    % opening to remove white particles
    tmp=imopen(imref,strel('square',openSize));
    
    % closing to remove dark particles
    tmp=imclose(tmp,strel('square',closeSize));
    
    % average smoothing
    f=fspecial('average',aveSize);
    tmp=imfilter(tmp,f,'symmetric');
    
    figure(1)
    subplot(1,2,2)
    imshow(tmp,[prctile(tmp(:),0.1) prctile(tmp(:),99.5)])
    title('estimated white image ')

    
    ok=yesno('Ok for this estimated white image?');
    
    if ok
        imref=tmp;
    else
       defaultanswer={num2str(openSize),num2str(closeSize),num2str(aveSize)};
       tmax=floor(min(size(imref))/6)*2+1;
       uiwait(msgbox(['enter odd numbers between 1 and ' num2str(tmax) ' in the following dialog box'],'Filerting parameters','modal'));

       answer=inputdlg({'Remove bright details: enter opening size ',...
           'Remove dark details: enter closing size  ',...
           'Smooth image: enter average filtering size '},...
           'filtering parameters',1,defaultanswer);
        
       openSize=str2num(answer{1}); %#ok<*ST2NM>
       if openSize<=0
           openSize=1;                  % = no opening
       end
       openSize=floor(openSize/2)*2+1;
       closeSize=str2num(answer{2});
       if closeSize<=0
           closeSize=1;                 %  = no closing
       end
       closeSize=floor(closeSize/2)*2+1;
       
       aveSize=str2num(answer{3});
       if aveSize<=0
           aveSize=1;                   % no average
       end
       aveSize=floor(aveSize/2)*2+1;

    end
end
        

if darkCorrection
    cd(rfolderDarkforWhite)
    dark=imread(ndark);
    
    % check dark image size
    nlig=size(imref,1);
    ncol=size(imref,2);
    
    if size(dark,1)~=nlig && size(dark,2)~=ncol
        
        % check for roi position
        cd(rootFolder)
        %read position of roi in white image
        pos=readDIV([nameFolder '.roiAcq.txt']);
        
        yl=pos.d(1,2);
        xc=pos.d(1,1);
        if yl==0
            yl=1;
        end
        if xc==0
            xc=1;
        end
        dark=dark(yl:(yl+nlig-1),xc:(xc+ncol-1));
    end
    
    
    
    % check for offset
    tmp=double(imref)-double(dark);
    % check for more than 1% of pixels with <0 values
    if sum(sum(tmp<0))>nlig*ncol/100
        % then an offset is required so that background is not
        % strictly 0 : avoid saturation in dark
         fdarkOffset=1;
         darkOffset=-round(mean(tmp(tmp<0)))+5;
    else
         fdarkOffset=0;
         darkOffset=0;
    end
    
    imb=double(imref)-double(dark)+double(darkOffset);
    imref=uint16(imb);
end
        
% save
cd(sfolder);
imwrite(imref,sname,'tif','compression','none');

figure(1)
close

%% matlab function tracking

fid=fopen(strrep(sname,'.tif','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS: Estimate white fluorescence image from a given experiment \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nRoot folder : %s\r\n',rootFolder);

fprintf(fid,'\r\nChannels considered in the estimation \r\n');
for i=1:nc
    fprintf(fid,'\r\n\t- %s',lc{i});
end

fprintf(fid,'\r\n');
fprintf(fid,'\r\n The max of each image is computed to estimate the raw white image for each channel \r\n');
fprintf(fid,'\r\n');

fprintf(fid,'Modification of pixels: three lines and columns around the image are replaced by line and column number 4 and end-3 \r\n');

fprintf(fid,'\r\n\r\n');
fprintf(fid,'Size of opening to remove white regions:%d\r\n',openSize);
fprintf(fid,'Size of closing to remove dark regions:%d\r\n',closeSize);
fprintf(fid,'Size of average filtering:%d\r\n',aveSize);

fprintf(fid,'\r\n');
if darkCorrection
    fprintf(fid,'Background camera subtracted from estimated white image:\r\n');
    fprintf(fid,'\t- background camera image %s\r\n\t- found in folder %s\r\n',ndark,rfolderDarkforWhite);
    if fdarkOffset
        fprintf(fid,'White images was offsetted after dark correction to avoid black saturation\r\n');
        fprintf(fid,'\t- offset %d\r\n',darkOffset);
    end
else
     fprintf(fid,'No background camera subtracted from estimated white image:\r\n');
end


fprintf(fid,'\r\n\r\n');
fprintf(fid,'estimated white image: %s\r\nSaved in folder: %s\r\n',sname,sfolder);


fprintf(fid,'\r\n');
if inter
    fprintf(fid,'\r\nImages considered in the estimation \r\n');
    for j=1:nb
        nbf=size(sfile{j},1);
        if nbf>0
            lfile=sfile{j};
            fprintf(fid,'Folder %s:\r\n',listDir(j,:));
            for i=1:nbf
                fprintf(fid,'\t-%s\r\n',lfile(i,:));
            end
        end
    end
else
    fprintf(fid,'\r\nAll images acquired using selected channel(s) considered in the estimation \r\n');
end

fprintf(fid,'\r\n \r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
