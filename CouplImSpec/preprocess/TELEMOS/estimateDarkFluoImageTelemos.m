function estimateDarkFluoImageTelemos


%% description
% estimation of the dark image due to the camera
% connected to the Telemos microscope

% this function is specific of Telemos images (DISCO line SOLEIL)
% acquired using micromanager software linked to imageJ


%% input

%  nothing : interactive function

%% output

%  nothing : the final estimated dark images are saved on disk

%% principe
% NB: MICROMANAGER save images in a structured file folder architecture :
%
% root folder : name given by the user
%
%   subfolders roi(n)_tile1 : n folders for each selected roi
%           or pos(n) : n folders for each selected position
%
%   display_and_comments.txt : file describing the channels acquired 
%
% in each subfolder roi(n)_tile1 or Pos(n) :
%       images files with name img_00000000(n)_DM300_327-353_00(p).tif
%               n = number of time
%               p = z focal plane
%               DM300_327-353 = name of the channel
%       metadata.txt : files describing all metadata associated with the
%       acquisition : x, y, z position, camera settings .... etc.
%
% (Nb: depending on the date of acquisition, an extension fluo is found or
% not in the name of fluorescence image. this extension is given in
% contrast to the visible image.)
%
% IMAGE: fluorescence images are considered
%
% GENERAL CONCEPT:
%       Dark image have been shown to depend on the filter and on the time of
%   acquisition
%
%  as a consequence : one dark image is estimated for each channel
%
% IMPORTANT:
%       it is expected that in the folders? at least several images contains some
%       background signal that can be used to estimate the shape of the dark
%       image of the camera. IF IT IS NOT THE CASE, IT WON'T WORK
%
% BASIC: the dark image is the MIN of the selected images.
%       When interactive mode is selected, only first time and first z are
%       proposed.
%       When automatic computing is performed, all images are taken into account
%
% WHAT IS DONE:
%
%       1 - spurious pixels (three lines and columns around the image) are not taken
% in filtering and replaced by line and column number 4 and end-3
%
%       2 - Filtering: the estimated dark image is filtered :
%        opening to remove  white regions brighter than the low frequency signal that should correspond to synchrotron light shape (should be small)
%        closing to remove black regions that remains (should be small to avoid synchrotron light shape deformation)
%        average filtering

% the image computed should be used  to
%
%   - correct images for intensities




%% use
%       estimateDarkFluoImageTelemos

%% Comments
%   adapted from proposals 20161050 and 20171187


%% Author
% MF Devaux
% INRA BIA
% PVPP

%% date
%
% 25 avril 2019 : adapted from estimateWhiteFluoImage
%     4 juin 2019 : replace exist by isfolder or isfile

%% context variables
orig=pwd;           % returns the current directory


%% start

if nargin ~=0
    error('use: estimateDarkFluoImageTelemos');
end


%% input
[~,rootFolder,listDir,listChannel]=getInfoTelemos('Root folder to process');

cd(rootFolder)

nb=size(listDir,1);

% channels
lc =cellstr(listChannel);
nc=length(lc);

% interactivity?
inter=yesno('interactive checking of images included for estimation?                 Choose this option for a small number of image');

% for saving fluorescence reference image
if ~isfolder('estimatedDark')
    mkdir('estimatedDark')
end
cd('estimatedDark')
sname=uiputfile({'*.*'},'generic name for estimated dark fluorescence image of sample','darkEstimate*');
sname=strrep(sname, '.tif','');
if ~isfolder(sname)
    mkdir(sname)
end
cd(sname)
sfolder=pwd;

%% treatement

h=figure(1);
set(h,'units','normalized');
set(h,'position',[0.1 0 0.8 0.8]);

% initialisation of filename for each subfolder of the micromanager structure
sfile=cell(nc);
if inter
    for i=1:nc
        sfile{i}=cell(nb);
    end
end
% initialisation of fitering parameters
sopenSize=zeros(1,nc);
scloseSize=zeros(1,nc);
saveSize=zeros(1,nc);

% for each channel  :
for c=1:nc
    disp(lc{c});
    % for each subfolder in the rootfolder
    for i=1:nb
        disp(listDir(i,:));
        
        cd(rootFolder)
        cd(listDir(i,:));
        
        % build the list of file to be considered
        
        if inter                % interactive selection : only first time and first z considered
            ltmp=dir(['img_000000000_' lc{c} '*000.tif']);
        else                    % automatic selection : all image = all time and all z considered
            ltmp=dir(['*' lc{c} '*.tif']);
        end
        
        % list of image file to consider
        lfile=char(ltmp.name);
        nbf=size(lfile,1);
        
        if inter            % interactive mode: initialisation of selection
            sel=zeros(nbf,1);
        end
        
        % for each image considered
        for j=1:nbf
            im=imread(lfile(j,:));
            
            oks=1;
            
            if inter                        % interactive mode: ask for each image if it is selected or not
                figure(1)
                subplot(1,2,1)
                imshow(im,[prctile(im(:),0.5) prctile(im(:),99.5)])
                title(lfile(j,:))
                sel(j)=yesno('select current image');
                if sel(j)
                    oks=1;
                else
                    oks=0;
                end
                
            end
            
            % progressively built the MIN of all selected images
            if oks
                if ~exist('imref','var')
                    imref=im;
                else
                    imref=min(imref,im);
                end
            end
            
            
            if inter && exist('imref','var')            % interactive mode: show image min at each iteration
                figure(1)
                subplot(1,2,2)
                imshow(imref,[prctile(imref(:),0.5) prctile(imref(:),99.5)])
                title('raw min image')
                sfile{c}{i}=lfile(logical(sel),:);
            end
        end
    end
    
    
    
    
    % show resulting MIN image at the end of all iterations
    figure(1)
    subplot(1,2,1)
    imshow(imref,[prctile(imref(:),0.5) prctile(imref(:),99.5)])
    title(sprintf('raw min image for channel %s',lc{c}))
    
    
    % modifiy spurious pixels on the side of the images
    imtmp=imref;
    imref=imref(4:(end-3),4:(end-3));
    imtmp(1:3,4:(end-3))=repmat(imref(1,:),3,1);
    imtmp((end-2):end,4:(end-3))=repmat(imref(end,:),3,1);
    imtmp(:,1:3)=repmat(imtmp(:,4),1,3);
    imtmp(:,(end-2):end)=repmat(imtmp(:,end-3),1,3);
    imref=imtmp;
    clear imtmp;
    
    % post treatment of MIN image 
    ok=0;
    openSize=75;
    closeSize=75;
    aveSize=75;
    
    
    while ~ok
        % opening to remove white particles
        tmp=imopen(imref,strel('square',openSize));
        
        % closing to remove dark particles
        tmp=imclose(tmp,strel('square',closeSize));
        
        % average smoothing
        f=fspecial('average',aveSize);
        tmp=imfilter(tmp,f,'symmetric');
        
        figure(1)
        subplot(1,2,2)
        imshow(tmp,[prctile(tmp(:),0.5) prctile(tmp(:),99.5)])
        title('estimated dark image ')
        
        
        ok=yesno('Ok for this estimated dark image?');
        
        if ok
            imref=tmp;
        else
            defaultanswer={num2str(openSize),num2str(closeSize),num2str(aveSize)};
            tmax=floor(min(size(imref))/6)*2+1;
            uiwait(msgbox(['enter odd numbers between 1 and ' num2str(tmax) ' in the following dialog box'],'Filerting parameters','modal'));
            
            answer=inputdlg({'Remove bright details: enter opening size ',...
                'Remove dark details: enter closing size  ',...
                'Smooth image: enter average filtering size '},...
                'filtering parameters',1,defaultanswer);
            
            openSize=str2num(answer{1}); %#ok<*ST2NM>
            if openSize<=0
                openSize=1;                  % = no opening
            end
            openSize=floor(openSize/2)*2+1;
            closeSize=str2num(answer{2});
            if closeSize<=0
                closeSize=1;                 %  = no closing
            end
            closeSize=floor(closeSize/2)*2+1;
            
            aveSize=str2num(answer{3});
            if aveSize<=0
                aveSize=1;                   % no average
            end
            aveSize=floor(aveSize/2)*2+1;
            
        end
    end
    
    
    % save
    cd(sfolder);
    imwrite(imref,[sname,'_',lc{c},'.tif'],'tif','compression','none');
    sopenSize(c)=openSize;
    scloseSize(c)=closeSize;
    saveSize(c)=aveSize;
    
    clear imref
end




%% matlab function tracking

fid=fopen(strcat(sname,'.track.txt'),'w');

if fid<=0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS: Estimate dark fluorescence image from a given experiment \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\n Root folder : %s\r\n',rootFolder);

fprintf(fid,'\r\n Channels are considered indivudally for one dark image estimation per channel:');
for i=1:nc
    fprintf(fid,'\r\n\t- %s',lc{i});
end

fprintf(fid,'\r\n \r\n');
fprintf(fid,'\r\n The min of each image is computed to estimate the raw dark image for each channel \r\n');
fprintf(fid,'\r\n');

fprintf(fid,'\r\n Modification of pixels: three lines and columns around the image are replaced by line and column number 4 and end-3 \r\n');
fprintf(fid,'\r\n \r\n');

fprintf(fid,' Estimated dark images saved in folder: %s\r\n\r\n',sfolder);
for i=1:nc
    fprintf(fid,' - image: %s_%s.tif\r\n\r\n',sname,lc{i});
    
    fprintf(fid,'\t Size of opening to remove white regions:%d\r\n',sopenSize(i));
    fprintf(fid,'\t Size of closing to remove dark regions:%d\r\n',scloseSize(i));
    fprintf(fid,'\t Size of average filtering:%d\r\n\r\n',saveSize(i));
    
end

if inter
    fprintf(fid,'\r\nImages considered in the estimation \r\n');
    for c=1:nc
        for j=1:nb
            nbf=size(sfile{c}{j},1);
            if nbf>0
                lfile=sfile{c}{j};
                fprintf(fid,'Folder %s:\r\n',listDir(j,:));
                for i=1:nbf
                    fprintf(fid,'\t-%s\r\n',lfile(i,:));
                end
            end
        end
    end
else
    fprintf(fid,'\r\nAll images acquired using selected channel(s) considered in the estimation \r\n');
end

fprintf(fid,'\r\n \r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
