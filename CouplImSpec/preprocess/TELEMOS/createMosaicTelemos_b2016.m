function createMosaicTelemos_b2016

%% description
% function to  create the mosaic of channels from tile recorded images
% This function is specific of Telemos images mosaics form slide explorer (ROI -> Pos) (DISCO beamline SOLEIL)
% acquired using micromanager software linked to imageJ
% The structure of roi images acquired is the following :
% a set of subdirectories <roi1_tile>, each one corresponding a tile image and
% its meta data file.
% in each subdirectory, the files images are found with names
% e.g. <img_000000000_DM300_327-353_000>, corresponding to the corresponding  channel 327-353
% and the asociated <metadata.txt> file

%% input
%  no input
% interactive function

%% output
% the result of the mosaic images are saved in the folder name <mosaic> created in
% the images folder name (nameMos).

%% principe
% createMosaicTelemos
%
%  (1) call getInfoTelemos : [nameMos,listDir,listChannel]=getInfoTelemos
%      (2) call computeMosaicTelemos : computeMosaicTelemos(nameMos,listDir,listChannel,numChannel,sfolder);
%          (3) call getGridinfoTelemos: [tgrid,tPos,tsim,spixel,timeTelemos]=getGridinfoTelemos(listDir,listChannel);
%               (4) call readMetadataTelemos: [xPos,yPos,width,height,spix,timeTelemos]=readMetadataTelemos(nameDir,listChannel);
%

%% use
% createMosaicTelemos
%

%% Comments
% project build mosaic images for telemos microscope of the DISCO beamline at SOLEIL

%% Author
% MF Devaux
% INRA- BIA-PVPP
% F Jamme
% SOLEIL synchrotron


%% date
%     17 mai 2011 (BlueBlox)
%     20 juin 2013
%     27 mai 2014 (deblank added)
%     06 juin 2014
%     15 Juillet 2014
%     15 decembre 2015: add options
%     20 septembre 2016: add option CamRot
%     3 octobre 2017: IFPEN
%     31 mai 2017 : take into account xy, z, t, channels

%% context variables
% folder separator depends on os
os=computer; % returns the Operating system
switch os(1)
    
    case'P'
        foldSep='\'; %  for Windows OS
    case'M'
        foldSep='/'; %  for Mac OS
    otherwise
        foldSep='/'; %  UNIX (to be checked)
end


orig=pwd;           % returns the current directory

transposeMirror=0;
camRot=0;          % 1 = acquisitions since sept 2016, 0 = before
%camRot=1;          % 1 = acquisitions since sept 2016, 0 = before

%% start

if nargin >0
    error('use: createMosaicTelemos')
end


%% input

[nameMos,rootFolder,listDir,listChannel]=metadataTelemos('Root folder of image to process');
cd(rootFolder)

%% create mosaic sfolder
if ~isfolder('mosaic')
    mkdir('mosaic');
end
cd (deblank('mosaic'));
sfolder=pwd;
cd ..

%% Select number of channels

nbch=size(listChannel,1);
%nbdir=size(listDir,1);

if nbch>1
    %numChannel=chooseChannelTelemos(listDir(1,:),listChannel); % TODO
    %create list box
    [selection,~] = listdlg('PromptString','Select the Channels',...
        'SelectionMode','multiple',...
        'Name','Telemos Selection',...
        'ListSize',[160 100],...
        'ListString',listChannel);
    listChannel=listChannel(selection,:);
    numChannel=size(listChannel,1);
else
    numChannel=1;
end


%% treatment

computeMosaicTelemos(nameMos,listDir,listChannel,numChannel,sfolder,camRot, transposeMirror);

cd (orig)
end



function computeMosaicTelemos(nameMos,listDir,listChannel,numChannel,sfolder,camRot, transposeMirror)
%
%% description
% function to  compute the mosaic of each channels from tile recorded images


%% input
%  nameMos = tile images folder name
% ListDir = List of tile images (without 'mosaic')
% ListChannel = List of Channels
% numChannel = number of Channel
% sfolder = save mosaic folder
% camRot : rotation of camera
% transposeMirror : trsnposition of the mirror

%% output
% the result of the mosaic images are saved in the folder name <mosaic> created in
% the images folder name (nameMos).

%% principe
% getGridinfTelemos and compute mosaic by adding images
%% use
% computeMosaicTelemos(nameMos,listDir,listChannel,numChannel,sfolder,camRot, transposeMirror)

%% Comments
% project build mosaic images for telemos microscope of the DISCO beamline at SOLEIL

%% Author
% MF Devaux
% INRA- BIA-PVPP
% F Jamme
% SOLEIL synchrotron


%% date
%     17 mai 2011 (BlueBlox)
%     20 juin 2013
%     27 mai 2014 (deblank added)
%     06 juin 2014
%     31 mai 2019 : include all possibilities : xy, z t, channels
%     4 juin 2019 : replace exist by isfolder or isfile

%% context variables

porig=pwd;

%% start
if nargin ~=7
    error('use: computeMosaicTelemos(nameMos,listDir,listChannel,numChannel,sfolder,camRot, transposeMirror)');
end

%% getGridinfoTelemos
[tgrid,tPos]=getGridinfoTelemos(nameMos,listDir,camRot, transposeMirror);
sprintf('taille de la mosaique en nombre d''images : %d lignes et %d colonnes',tgrid(1),tgrid(2));

infogen=readDIV(strcat(nameMos,'.roi.metadata.txt'));
spixel=unique(infogen.d(:,5));
nbcolim=unique(infogen.d(:,7));
nbligim=unique(infogen.d(:,6));

nbZ=unique(infogen.d(:,3));
nbTime=unique(infogen.d(:,1));

nbdir=size(listDir,1);


%%

tpospixel=round(tPos/spixel);
tpospixel(:,1)=tpospixel(:,1)-min(tpospixel(:,1))+1; % start from 1
tpospixel(:,2)=tpospixel(:,2)-min(tpospixel(:,2))+1;
tP=int16(tpospixel);

nblig=max(tP(:,2))+nbligim;
nbcol=max(tP(:,1))+nbcolim;




for j=1:numChannel
    sprintf('Channel %s\r', listChannel(j,:))
    
    cd (deblank(listDir(1,:)));
    listIm=dir(['*' listChannel(j,:) '*.tif']);
    
    imosaic=uint16(zeros(nblig,nbcol,nbTime,nbZ));
    imosaictmp2=uint16(zeros(nblig,nbcol,nbTime,nbZ));
    imosaicmask=uint8(zeros(nblig,nbcol,nbTime,nbZ));
    imosaicmasktmp=uint8(zeros(nblig,nbcol,nbTime,nbZ));
    
    cpt=1;
    for k=1:nbTime
        sprintf('time: %d' ,k)
        cd(porig)
        cd (deblank(listDir(1,:)));
        for l=1:nbZ
            sprintf('slice: %d' ,l)
            
            im=imread(listIm(cpt).name);
            imosaic((tP(1,2)+2):(tP(1,2)+2+nbligim-4-1),(tP(1,1)+2):(tP(1,1)+2+nbcolim-4-1),k,l)=im(3:(end-2),3:(end-2));
            imosaicmask((tP(1,2)+2):(tP(1,2)+2+nbligim-4-1),(tP(1,1)+2):(tP(1,1)+2+nbcolim-4-1),k,l)=1;
            cd ..
            %         h = waitbar(0,'Stitching...','Name','Please wait ...',...
            %             'CreateCancelBtn',...
            %             'setappdata(gcbf,''canceling'',1)');
            %         setappdata(h,'canceling',0)
            
            
            for i=2:nbdir
                sprintf('%s',listDir(i,:))
                %             waitbar(i/nbdir,h);
                %             if getappdata(h,'canceling')
                %                 break
                %             end
                
                cd (deblank(listDir(i,:)));
                %listIm=dir('*.tif'); %TODO check that the file exist
                im=imread(listIm(cpt).name); % channels
                
                cd ..
                imosaictmp2((tP(i,2)+2):(tP(i,2)+2+nbligim-4-1),(tP(i,1)+2):(tP(i,1)+2+nbcolim-4-1),k,l)=im(3:(end-2),3:(end-2));
                imosaicmasktmp((tP(i,2)+2):(tP(i,2)+2+nbligim-4-1),(tP(i,1)+2):(tP(i,1)+2+nbcolim-4-1),k,l)=1;
                overlap=logical(imosaicmask(:,:,k,l)&imosaicmasktmp(:,:,k,l)); %
                
                imosaictmp1=imosaic(:,:,k,l);
                imosaic(:,:,k,l)=max(imosaic(:,:,k,l),imosaictmp2(:,:,k,l));
                
                ttmp= imosaic(:,:,k,l);
                ttmp2= imosaictmp2(:,:,k,l);
                
                ttmp(overlap)=max([imosaictmp1(overlap)';ttmp2(overlap)']);
                imosaic(:,:,k,l)=ttmp;
                
                imshow(imosaic(:,:,k,l),[]);
                
                [~,msgId] = lastwarn;
                warnStruct = warning('off',msgId);
            end
            
            %     delete(h);
            % figure;
            % imshow(imosaic,[])
            % addpath('/Applications/Fiji.app/scripts')
            % Miji
            % MIJ.createImage(imosaic)
            
            cd(sfolder)
            idTime='_';
            if k<10
                idTime='_0';
            end
            idZ='_';
            if l<10
                idZ='_00';
            else
                if l<100
                    idZ='_0';
                end
            end
            
            sname=strcat(nameMos,idTime,num2str(k-1),'_',listChannel(j,:),idZ,num2str(l-1),'.tif');
            
            imwrite(imosaic(:,:,k,l),sname,'tif','compression','none');
            
            cpt=cpt+1;
            cd ..
        end
    end
    % msgbox('Stiching Completed !');
    cd(porig)
    warning(warnStruct);
end
end


function [tGrid,tPos]=getGridinfoTelemos(nameMos,listDir,camRot,transposeMirror)

%% description
% function to get the x and y a priori coordinates of an image from TELEMOS microscope
% + the size of the image in pixel and the pixel size
%
% this function is specific of telemos images (DISCO line SOLEIL)
% acquired using micromanager software linked to imageJ
% the structure of images acquired is the following :
% a set of subdirectories, each one corresponding at one tile image and
% its meta data file.
% in each sub directory, the files on one tile are found with names
% xxxxxxxxxxxxxxxxx
% and the
% <metadata.txt> file


%% input
%  optional parameter :
%  foldim = image folder name

%% output

% x  y coordinates in pixel unit
% width higth : size of the image in pixel
% tpixel : pixel size in um

%% principe
%
% read the <metadata.txt> file in order to find the information of the
% fields :
%       XpositionUm
%       YpositionUm
%       PixelSize
% get numerical values
% compute the position in pixel
%
%

%% use
% foldim=5-Pos_000_000
% [x,y]= coordxy(foldim)
%

%% Comments
% project build mosaic images for telemos microscope of the DISCO line At SOLEIL
% from coordxy written for "collage d'images BlueBox"


%% Author
% MF Devaux
% INRA- BIA-PVPP


%% date
%     17 mai 2011 (BlueBlox)
%     20 juin 2013
%     31 mai 2019 : use txt files created by metadataTelemos


%% context variables

orig=pwd;           % returns the current directory
%transposeMirror=0;
%camRot=0;          % 1 = acquisitions since sept 2016, 0 = before
%camRot=1;          % 1 = acquisitions since sept 2016, 0 = before
%% start

if nargin ~=4
    error('[tgrid,tPos]=getGridinfoTelemos(nameMos,listDir,camRot,transposeMirror)');
end

%% treatment

nbdir=size(listDir,1);
tPos=zeros(nbdir,2);    % x and y position of individual image in the mosaic



for i=1:nbdir
    nameDir=listDir(i,:);
    metadataName=strcat(nameMos,'.',nameDir,'.metadata.txt');
    %[xPos,yPos,width,height,spix,timeTelemos]=readMetadataTelemos(nameDir,listChannel);
    metadata=readDIV(metadataName);
    xPos=unique(metadata.d(:,1));
    yPos=unique(metadata.d(:,2));
    
    if camRot
        tPos(i,1)=yPos; %rotation
        tPos(i,2)=xPos; %rotation
    else
        tPos(i,1)=xPos; % pas rotation cam
        tPos(i,2)=yPos; % pas rotation cam
    end
    
 
end

%% for transposeMirrorOption
if transposeMirror
    
    prompt={'XY TransposeMirrorX: :',...
        'XY TransposeMirrorY:'};
    name='XY Stage';
    numlines=1;
    defaultanswer={'0','0'};
    options.Resize='on';
    options.WindowStyle='normal';
    %options.Interpreter='tex';
    answer=inputdlg(prompt,name,numlines,...
        defaultanswer,options);
    
    xaxis=answer{1};
    yaxis=answer{2};
    
    switch xaxis
        
        case '1'
            tPos(:,1)=tPos(:,1);
            
        case '0'
            tPos(:,1)=-tPos(:,1);% done 21/07
    end
    
    switch yaxis
        
        case '1'
            tPos(:,2)= tPos(:,2);
            
        case '0'
            tPos(:,2)=-tPos(:,2);% done 21/07
    end
end
%%
if camRot
    tPos(:,1)=tPos(:,1);% rotation
else
    tPos(:,1)=-tPos(:,1);% done 14/09/2016
end

tPos(:,2)=-tPos(:,2);% done 11/06/2015




xp=tPos(:,1);

diffxp=abs(diff(xp)); % attention valeur positive necessaire


% calculate difference
dxp=unique(diffxp); % attention fonction Matlab unique range par ordre croissant

temp=find(diffxp>dxp(1)+1);


ltemp=isempty(temp); % case column mosaic

if ltemp==1 % case colu mosaic
    xGrid=1; % case column mosaic
else
    xGrid=temp(1);
end

yGrid=nbdir/xGrid;

tGrid=[xGrid,yGrid];


%% end

cd (orig)
end

