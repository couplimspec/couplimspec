function [zmax]=findWhiteReferenceImageTelemos

%% description
% find the white reference Image among a z stack of reference image
% the most correlated to the white image estimated from a serie of acquisition 
%

% this function is specific of Telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    


%% input
%  nothing : interactive function

%% output
%    zmax : focal plane
%   

%% principe
% images were acquired for a given roi which position is found in
% metadata.txt file
% white and dark  images were recorded for the full field of view of the DISCO
% TELEMOS camera
%
% correlation between a estimated  white fluorescence image estalished from
% actual acquisitions of a given sample and all z plane acquired for the reference TELEMOS white
% image (Matthieu slide or any fluorescent homogeneous image) : 

% The  estimated  white fluorescence image  is generallly obtained bt using function whiteFluoImageTelemosestimation 
% This is not compulsary as any homogenous sample image hat can roughly show the shape of illumination can be used to find
% the white reference image
%
%
% the z plane for which the maximum correlation is observed between estimated white and reference white images is retained.
% the white image is then offsetted (its corresponding Dark is subtracted) and copied in the subfolder <WhiteReference> of the sample
% rootfolder to show that it has been specifically selected for the considered experiment
%The matlab corrcoeff function is used
%
% correlation coefficients are saved in a file called
% 'corr.txt' in the subfolder 'WhiteReference'
%
%
% expected input folder hierarchy:
%
%      >sampleFolder
%           > PosFolders or RoiFolders
%           > WhiteEstimate
%       >darkFolder
%       >darkFolder.smooth
%       >whiteFolder
%           > darkFolderforWhite
%           > darkFolderforWhite.smooth
%       >whiteFolder.smooth
%
%
% expected output folder hierarchy:
%
%      >sampleFolder
%           > PosFolders or RoiFolders
%           > WhiteEstimate
%           > WhiteReference
%                   > white after offset
%       >darkFolder
%       >darkFolder.smooth
%       >whiteFolder
%           > darkFolderforWhite
%           > darkFolderforWhite.smooth
%       >whiteFolder.smooth





%% use
% [zmax]=findWhiteReferenceImageTelemos

%% Comments
%   written for  proposal 20161050
%   adapted for proposal 20170043


%% Author
% MF Devaux
% INRA BIA
% PVPP

%% date
% 5 octobre 2017:
% 15 decembre 2017 : adapted to take into account the roi known from
% metadata
% 27 fevrier 2018 : comments details
% 4 septembre 2018: comments and check and naming of folders
% 12 mars 2019 : save white reference with the same size as white estimate
% 16 avril 2019 : include diagonals to check the relevance of white
% reference
% 20 mai 2019 : track folder 
% 27 mai 2019 : offset dark
%     4 juin 2019 : replace exist by isfolder or isfile

%% context variables
orig=pwd;           % returns the current directory

%% start

if nargin ~=0
    error('use:  [zmax]=findWhiteReferenceImageTelemos');
end


%% input
[genName,rootFolder,~,~]=metadataTelemos('root folder of TELEMOS images to process');
cd(rootFolder)

if isfolder('WhiteEstimate')
    cd('WhiteEstimate');
end

[filename,rreffluoEchfolder]=uigetfile({'*.tif'},'Estimated white fluorescence image from  sample','*.tif');

cd(rootFolder)
cd ..
    
[rfolderWhite]=uigetdir('.','FOLDER: Root folder of white images (preferably smoothed)');
cd(rfolderWhite)
if ~isfolder('Pos0')
    error('folder %s does not seem to be a TELEMOS folder')
end

cd ..

% [rfolderDarkforWhite]=uigetdir('.','Root folder of dark image for white images (preferably smoothed) - one image expected in the Pos0 subfolder');
% cd(rfolderDarkforWhite)
% if ~exist('Pos0','dir')
%     error('folder %s does not seem to be a TELEMOS folder')
% end
[ndark,rfolderDarkforWhite]=uigetfile('*.tif','TIF IMAGE: Dark image for white images');

cd(rootFolder)
if ~isfolder('WhiteReference')
    mkdir('WhiteReference');
end
cd('WhiteReference');
sfolder=pwd;

%% treatment
% read 'estimated white fluorescence image of sample
cd(rreffluoEchfolder)
imetesdt=imread(filename);
nl=size(imetesdt,1);
nc=size(imetesdt,2);

h27=figure(27);
set(h27,'units','normalized','position',[0.1 0.1 0.75 0.75])
subplot(2,2,1)
imshow(imetesdt,[prctile(imetesdt(:),0.1) prctile(imetesdt(:),99.9)])
title('raw white estimate');
hold on
plot([1 nc],[1 nl],'r','linewidth',2)

% plot of diagonal profile
p=improfile(imetesdt,[1 nc],[1 nl]);
p=p-min(p);
p=p/max(p);
figure(27)
subplot(2,2,2)
plot(p,'r','linewidth',2);
hold on
title('diagonal profiles')


% read ROI position
cd(rootFolder)
if isfile([genName '.roiAcq.txt'])
    roiPos=readDIV([genName '.roiAcq.txt']);
else
    error('no %s file find in folder %s',[genName '.roiAcq.txt'],genName);
end

dl=roiPos.d(1,2);                  % move in lines
dc=roiPos.d(1,1);               % move in column
if dl==0
    dl=1;
end
if dc==0
    dc=1;
end


% consider each possible white image
cd(rfolderWhite)
cd('Pos0');
% list of possible white images
list=dir('*.tif');

cc=zeros(length(list),1);


for i=1:length(list)
    display(list(i).name)
    
    % read image
    imw=imread(list(i).name);
    imw=imw(dl:(dl+nl-1),dc:(dc+nc-1));                         % consider reference image of the samesize than the size of the experiment considered
    
    pw=improfile(imw,[1 nc],[1 nl]);
    pw=pw-min(pw);
    pw=pw/max(pw);

    figure(27)
    subplot(2,2,2)
    plot(pw/max(pw)*max(p),'k','linewidth',1.2)
    plot(p,'r','linewidth',2);

    
    
   % correlation
  C=corrcoef(double(imetesdt(:)),double(imw(:)));
  
  cc(i)=C(1,2);
end

figure(27)
subplot(2,2,2)
hold off

% find z plane of maximal correlation coefficcient
zmax=find(cc==max(cc));
% figure of correlation coefficient according to z plane
figure(27)
subplot(2,2,3)
plot(cc,'linewidth',2);
xlabel('z plane','fontsize',14)
ylabel('correlation coeffcient','fontsize',14)
title('correlation coefficients')

% read reference white image
imw=imread(list(zmax).name);

% read dark image to offset white image
cd(rfolderDarkforWhite)
dark=imread(ndark);

% check for offset
tmp=double(imw)-double(dark);
% check for more than 1% of pixels with <0 values
if sum(sum(tmp<0))>size(imw,1)*size(imw,2)/100
    fdarkOffset=1;
    darkOffset=-round(mean(tmp(tmp<0)))+25;
else
    fdarkOffset=0;
    darkOffset=0;
end
% subtract background from white image
imb=double(imw)-double(dark)+double(darkOffset);
imwo=uint16(imb);

% profile of white image after backgournd correction
co=improfile(imwo(dl:(dl+nl-1),dc:(dc+nc-1)),[1 nc],[1 nl]);


co=double(co)/double(max(co));
seuil=0.2;
co(co<seuil)=seuil;
poc=double(p)./co;





%% save
% save white image after backgournd correction in <white folder>
cd(sfolder)
% selected white image
snameo=strcat('whiteReference.',filename);
imwo=imwo(dl:(dl+nl-1),dc:(dc+nc-1));

imwrite(imwo,snameo,'tif','compression','none');

if ~isfolder('track')
    mkdir('track')
end
cd('track')
sTrackFolder=pwd;

% selected white image before background correction
sname=[strrep(filename,'.tif','') '.WhiteIMage' num2str(zmax-1) '.tif'];
imwrite(imw,sname,'tif','compression','none');

% cross correlation results
ccr.d=cc;
ccr.v=char('corrcoeff');
ccr.i=(1:length(list))';

writeDIV(ccr,[sname 'corr.txt']);

% figure of diagonal profile of selected white image and estimated white
% image
figure(27)
subplot(2,2,4)
plot(co/max(co)*max(p),'m','linewidth',1.5)
hold on
plot(p,'k','linewidth',1.5)
ho=legend('white Reference','estimated white image');
hold off

xlabel('pixel','fontsize',14)
ylabel('intensity','fontsize',14)

set(ho,'location','eastoutside')
title('Diagonal profile')
%saveas(gcf,[sname '.DiagonalProfile.png'])
figure(27)
saveas(gcf,[sname '.DiagonalProfilesetCorrelation.png'])

figure(37)
plot(co/max(co)*max(p),'m','linewidth',1.5)
hold on
plot(p,'k','linewidth',1.5)
plot(poc,'r','linewidth',1.5)
hold off
ho=legend('white Reference','estimated white image','corrected estimated white image');
xlabel('pixel','fontsize',18)
ylabel('intensity','fontsize',18)
set(ho,'location','eastoutside')
title('With Diagonal profile after correction')
saveas(gcf,[sname,'DiagonalProfileCorr.png'])


    
%% matlab function tracking
cd ..

fid=fopen(strrep(snameo,'.tif','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS:  Find white reference  image in a z stack of reference images by correlation with estimated white image of sample to \r\n');
fprintf(fid,'_____________________________________________________________________________________________________________________________\r\n');

fprintf(fid,'\r\nReference white fluorescence image of sample:%s\r\n',filename);
fprintf(fid,'\t- Found in folder : %s\r\n',rreffluoEchfolder);

fprintf(fid,'\r\nWhite Images searched in folder: % s\r\n',rfolderWhite);

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Maximum correlation coefficient:%5:2f\r\n',cc(zmax));
fprintf(fid,'With white image:%s\r\n',list(zmax).name);

fprintf(fid,'\r\n');
fprintf(fid,'Saved as :%s\r\n', sname);
fprintf(fid,'In folder: %s\r\n',sTrackFolder);

fprintf(fid,'\r\n\r\n \r\n');
fprintf(fid,'White image after offset:\r\n');
fprintf(fid,'\t- using dark image for White:%s\r\n',ndark);
fprintf(fid,'\t- read in folder : %s\r\n\r\n',rfolderDarkforWhite);
if fdarkOffset
     fprintf(fid,'White images was offsetted after dark correction to avoid black saturation\r\n');
     fprintf(fid,'\t- offset %d\r\n',darkOffset);
end
fprintf(fid,'Offsetted White image:%s\r\n', snameo);
fprintf(fid,'\t- Saved in folder: %s\r\n',sfolder);


% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
