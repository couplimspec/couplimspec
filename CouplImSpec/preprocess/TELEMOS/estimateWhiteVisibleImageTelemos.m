function estimateWhiteVisibleImageTelemos
%% description
% find the white heterogneity of illumination fo visible image
% this function is specific of Telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    
%% input

%  nothing : interactive function

%% output
%   nothing

%% principe
%  NB: MICROMANAGER save images in a structured file folder architecture :
%
% root folder : name given by the user
%
%   subfolders roi(n)_tile1 : n folders for each selected roi
%           or pos(n) : n folders for each selected position
%
%   display_and_comments.txt : file describing the channels acquired 
%
% in each subfolder roi(n)_tile1 or Pos(n) :
%       images files with name img_00000000(n)_DM300_327-353_00(p).tif
%               n = number of time
%               p = z focal plane
%               DM300_327-353 = name of the channel
%       metadata.txt : files describing all metadata associated with the
%       acquisition : x, y, z position, camera settings .... etc.
%
% (Nb: depending on the date of acquisition, an extension fluo is found or
% not in the name of fluorescence image. This extension is given in
% contrast to the visible image.)
%
%
% IMAGE : all visible images in sub folders of the root folders are considered.
%
% IMPORTANT:
%       it is expected that in the folders at least several images cover the whole field of view 
%       illuminated by the synchrotron light.
%       IF IT IS NOT THE CASE, IT WON'T WORK
%
% BASIC : the max of all image is taken as estimated white reference for visible image
%
% WHAT IS DONE:
%            
%       1 - spurious pixels (three lines and columns around the image) are replaced 
%           by line and column number 4 and end-3 before filtering
%
%       2 - Filtering
% the estimated dark image is filtered :
%        opening to remove  white regions brighter than the low frequency signal that should correspond to  synchrotron light shape (should be small)
%        closing to remove black regions that remains (should be small to avoid synchrotron light shape deformation)
%        average filtering

% the image computed should be used  to
%     - correct visible images for intensities


%% use
% estimateWhiteVisibleImageTelemos

%% Comments
%   written for  Joel Passicousset PhD. IFPEN. proposal 20161050


%% Author
% MF Devaux
% INRA BIA
% PVPP

%% date
% 5 octobre 2017
% 17 mai 2019 for comments and review for the HyAm toolbox
%               subtraction of background
% 27 mai 2019 : offset dark 
%     4 juin 2019 : replace exist by isfolder or isfile
% 14 juin : close figure at the end


%% context variables
orig=pwd;           % returns the current directory

%% start

if nargin ~=0
    error('use:  estimateWhiteVisibleImageTelemos');
end


%% input
[genName,rootFolder,listDir]=getInfoTelemos('Root folder to process');

cd(rootFolder)
cd ..
[ndark,rfolderDarkforWhite]=uigetfile('*.tif','Dark image for visible white image');

% for saving white visible reference image
cd(rootFolder)
if ~isfolder('WhiteVisibleEstimate')
    mkdir('WhiteVisibleEstimate')
end
cd('WhiteVisibleEstimate')
sfolder=pwd;

sname=uiputfile({'*.tif'},'save  white reference  for visible images','whiteVisibleEstimate.tif');

%% treatment
h=figure(1);
set(h,'units','normalized');
set(h,'position',[0.1 0 0.8 0.8]);

% for each folder
cd(rootFolder)

nb=size(listDir,1);

for i=1:nb
    display(listDir(i,:))
    cd(rootFolder)

    cd(listDir(i,:))
    list=dir('*_nofilter_vis_*.tif');
    
    
    % read each visible image
    for j=1:length(list)
        
        ime=imread(list(j).name);
        
        if i==1 && j==1
            visible=ime;
        else
            visible=max(ime,visible);           % take the max
        end
    end
end


% show resulting image MAX at the end iof all iterations
figure(1)
subplot(1,2,1)
imshow(visible,[prctile(visible(:),0.05) prctile(visible(:),99.5)])
title('raw max image ')

% modifiy spurious pixels on the side of the images
imtmp=visible;
visible=visible(4:(end-3),4:(end-3));
imtmp(1:3,4:(end-3))=repmat(visible(1,:),3,1);
imtmp((end-2):end,4:(end-3))=repmat(visible(end,:),3,1);
imtmp(:,1:3)=repmat(imtmp(:,4),1,3);
imtmp(:,(end-2):end)=repmat(imtmp(:,end-3),1,3);
visible=imtmp;
clear imtmp;

% post treatment of image MAX
ok=0;
openSize=25;
closeSize=25;
aveSize=25;


while ~ok
    % opening to remove white particles
    tmp=imopen(visible,strel('square',openSize));
    
    % closing to remove dark particles
    tmp=imclose(tmp,strel('square',closeSize));
    
    % average smoothing
    f=fspecial('average',aveSize);
    tmp=imfilter(tmp,f,'symmetric');
    
    figure(1)
    subplot(1,2,2)
    imshow(tmp,[prctile(visible(:),0.05) prctile(tmp(:),99.5)])
    title('estimated white image ')

    
    ok=yesno('Ok for this estimated white image?');
    
    if ok
        visible=tmp;
    else
       defaultanswer={num2str(openSize),num2str(closeSize),num2str(aveSize)};
       tmax=floor(min(size(visible))/6)*2+1;
       uiwait(msgbox(['enter odd numbers between 1 and ' num2str(tmax) ' in the following dialog box'],'Filerting parameters','modal'));

       answer=inputdlg({'Remove bright details: enter opening size ',...
           'Remove dark details: enter closing size  ',...
           'Smooth image: enter average filtering size '},...
           'filtering parameters',1,defaultanswer);
        
       openSize=str2num(answer{1}); %#ok<*ST2NM>
       if openSize<=0
           openSize=1;                  % = no opening
       end
       openSize=floor(openSize/2)*2+1;
       closeSize=str2num(answer{2});
       if closeSize<=0
           closeSize=1;                 %  = no closing
       end
       closeSize=floor(closeSize/2)*2+1;
       
       aveSize=str2num(answer{3});
       if aveSize<=0
           aveSize=1;                   % no average
       end
       aveSize=floor(aveSize/2)*2+1;

    end
end

% read dark image to offset white image
cd(rfolderDarkforWhite)


%dark=imread(listdark(1).name);
dark=imread(ndark);

% read ROI position
cd(rootFolder)
if isfile([genName '.roiAcq.txt'])
    roiPos=readDIV([genName '.roiAcq.txt']);
else
    error('no %s file find in folder %s',[genName '.roiAcq.txt'],genName);
end

dl=roiPos.d(1,2);                  % move in lines
dc=roiPos.d(1,1);               % move in column
if dl==0
    dl=1;
end
if dc==0
    dc=1;
end
nl=size(visible,1);
nc=size(visible,2);

dark=dark(dl:(dl+nl-1),dc:(dc+nc-1));

% check for offset
tmp=double(visible)-double(dark);
% check for more than 1% of pixels with <0 values
if sum(sum(tmp<0))>nl*nc/100
    % then an offset is required so that background is not
    % strictly 0 : avoid saturation in dark
    fdarkOffset=1;
    darkOffset=-round(mean(tmp(tmp<0)))+5;
else
    fdarkOffset=0;
    darkOffset=0;
end

imb=double(visible)-double(dark)+double(darkOffset);
imwo=uint16(imb);



%% save
cd(sfolder);

imwrite(imwo,sname,'tif','compression','none');

figure(1)
close

%% matlab function tracking

fid=fopen(strrep(sname,'.tif','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS:  Find white reference image for visible images \r\n');
fprintf(fid,'______________________________________________________________________________________\r\n');


fprintf(fid,'\r\nProcessed root folder : %s\r\n',rootFolder);

fprintf(fid,'\r\nAll visible images in sub folders of the root folders are considered\r\n');

fprintf(fid,'\r\n');
fprintf(fid,'the White reference image of visible images is the max of all visible images acquired\r\n');
fprintf(fid,'\r\n');

fprintf(fid,'Modification of pixels: three lines and columns around the image are replaced by line and column number 4 and end-3 \r\n');

fprintf(fid,'\r\n\r\n');
fprintf(fid,'Size of opening to remove white regions:%d\r\n',openSize);
fprintf(fid,'Size of closing to remove dark regions:%d\r\n',closeSize);
fprintf(fid,'Size of average filtering:%d\r\n',aveSize);

fprintf(fid,'\r\n \r\n');
fprintf(fid,'White image after offset:\r\n');
fprintf(fid,'\t- using dark image for White:%s\r\n',ndark);
fprintf(fid,'\t- read in folder : %s\\Pos0\r\n\r\n',rfolderDarkforWhite);
if fdarkOffset
    fprintf(fid,'White images was offsetted after dark correction to avoid black saturation\r\n');
    fprintf(fid,'\t- offset %d\r\n',darkOffset);
end
fprintf(fid,'Final White visible image %s saved in folder:\r\n %s\r\n',sname,sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)




        
