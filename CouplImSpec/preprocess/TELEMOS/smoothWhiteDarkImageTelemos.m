  function smoothWhiteDarkImageTelemos
  

 %% description
    % smooth the Telemos White or Dark images
    
    % this function is specific of telemos images (DISCO line SOLEIL)
    % acquired using micromanager software linked to imageJ
    
    
    
%% input

    %  nothing interactive fonction
%% output
    
    %   nothing smoothed images are saved on disk 

%% principe
% NB: MICROMANAGER save images in a structured file folder architecture :
%
% root folder : name given by the user
%
%   subfolders roi(n)_tile1 : n folders for each selected roi
%           or pos(n) : n folders for each selected position
%
%   display_and_comments.txt : file describing the channels acquired 
%
% in each subfolder roi(n)_tile1 or Pos(n) :
%       images files with name img_00000000(n)_DM300_327-353_00(p).tif
%               n = number of time
%               p = z focal plane
%               DM300_327-353 = name of the channel
%       metadata.txt : files describing all metadata associated with the
%       acquisition : x, y, z position, camera settings .... etc.
%
% (Nb: depending on the date of acquisition, an extension fluo is found or
% not in the name of fluorescence image. this extension is given in
% contrast to the visible image.)
%
% IMAGES: function developped for smoothing dark and white images
%
% raw images are search in the PosO folder in the root directory
% all images (filters, t, z)  in the PosO are processed.
%
% nb: the case of rois is not taken into account
%
% WHAT IS DONE: images are filtered 
%
%    median filtering to remove spikes: current value fixed to : 9 pixels
%    average filtering to remoce noise: current value fixed to : 25 pixels
%
%    spurious pixels (three lines and columns around the image) are replaced
% by line and column number 4 and end-3 before filtering
%
% IMAGE SAVED: 
% 
% images are saved on disk on a rootFolder at the same level than the original RootFolder
% the name of the new folder is <rootFolder>_smooth
% "display and comments.txt" annd "metadata.txt" files are copied in the
% new folder structure.

%% use
% smoothWhiteDarkImageTelemos
    
%% Comments
    %   adatped from function developped for experiment proposal 20140308 
    
    
%% Author
    % MF Devaux & FJamme
    % INRA BIA
    % PVPP
    
%% date
    % 5 janvier 2017
    % 2 octobre 2017: adapted for Joel Passicousset PhD. IFPEN. proposal 20161050
    % 21 November 2017
    % 15 decembre 2017 : folder name smoothPos0
    % 3 septembre 2018 : comments and name folder 
    % 16 avril 2019 : track file
    % 21 mai 2019 : name of file
    % 3 juin 2019 : to smooth  images in a folder which is not a
    % micromanager folder
%     4 juin 2019 : replace exist by isfolder or isfile

%% context variables 
orig=pwd;           % returns the current directory
% size of median filtering
sizeMedFilt=9; 
% size of average filtering
sizeAverageFilt=25;

%% start

if nargin >0
    error('use: smoothWhiteDarkImageTelemos');
end


%% input
[rootFolder]=uigetdir('.','MicroManager Root folder of image to smooth');
cd(rootFolder)

otherFolder=0;
if  ~isfolder('Pos0')
    otherFolder=yesno(sprintf('folder %s does not seem to be a TELEMOS folder. Select image directly form this folder?',rootFolder));
    if ~otherFolder
        error('no PosO folder in %s root Folder',rootFolder);
    end
end

if otherFolder
    rfolder=pwd;
else
    cd ('Pos0')
    rfolder=pwd;
end

cd ..

% for saving smoothed images
if ~isfolder([rootFolder '_smooth'])
    mkdir([rootFolder '_smooth'])
end
cd([rootFolder '_smooth']);
sfolderRoot=pwd;
if ~otherFolder
    if ~isfolder('Pos0')
        mkdir('Pos0');
    end
    cd('Pos0');
end
sfolder=pwd;

%% treatement

cd(rfolder)

list=dir('*.tif');
nb=length(list);
for i=1:nb
    fprintf('%d: %s\r\n',i,list(i).name)
    im=imread(list(i).name);
    imtmp=im;
    
    % crop to remove spurious pixels
    im=im(4:(end-3),4:(end-3));
    
    % median filtering
    imf=medfilt2(im,[sizeMedFilt sizeMedFilt],'symmetric');
    
    % average filtering
    f=fspecial('average',sizeAverageFilt);
    imf=imfilter(imf,f,'symmetric');
    
    % manage spurious pixels
    
    imtmp(4:(end-3),4:(end-3))=imf;
    imtmp(1:3,4:(end-3))=repmat(imf(1,:),3,1);
    imtmp((end-2):end,4:(end-3))=repmat(imf(end,:),3,1);
    imtmp(:,1:3)=repmat(imtmp(:,4),1,3);
    imtmp(:,(end-2):end)=repmat(imtmp(:,end-3),1,3);
    imf=imtmp;
    % save
    cd(sfolder);
    imwrite(imf,list(i).name);
    
    cd(rfolder);
end

if ~otherFolder
    copyfile('metadata.txt',sfolder);
    cd ..
end

listtxt=dir('*.txt');
for i=1:length(listtxt)
    copyfile(listtxt(i).name,sfolderRoot);
end



%% matlab function tracking  
cd(sfolder);
cd ..;
nomtrack='smooth';
fid=fopen(strcat(nomtrack,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS: smooth image to generate dark and white images \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

if ~otherFolder
    fprintf(fid,'\r\nRaw files found in folder: %s\\Pos0\r\n',rootFolder);
else
    fprintf(fid,'\r\nRaw files found in folder: %s\r\n',rootFolder);
end
    
 
fprintf(fid,'\r\n Images are filtered: median filtering of size %dX%d to remove spikes\r\n',sizeMedFilt, sizeMedFilt);
fprintf(fid,'\r\n                    : average filtering of size %dX%d  for smoothing\r\n',sizeAverageFilt,sizeAverageFilt);
fprintf(fid,'\r\n Modification of pixels: three lines and columns around the image are replaced by line and column number 4 and end-3 \r\n');
fprintf(fid,'\r\n \r\n');
fprintf(fid,'smoothed images saved in folder: %s.smooth\r\n',rootFolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    