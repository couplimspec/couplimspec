function correctIntensities
%% description
% correct each recoreded images for dark background and white illumination
% inhomogeneities


%% input

%  nothing : interactive function

%% output
%   nothing

%% principe
% all images acquired  in the root Folder will be processed
% they are found in roi sub-directories
%
% the user is asked for the dark and white images adapated to the series.
%
% correction is the following : Imc = (Imraw-dark)/(white-dark)normalised
%
% after correction, a median filtering of size 3x3 is applied to remove
% white noise due to low signal on the edge of the imagesq
%
% corrected images are saved in a subfolder 'intensityCorrected' and the hieracrrchy
% of sub-folders created by micromanager is reformed for the corrected
% images
%
% metadata are copied together with the corection of the vsiible image
% see function corrigerWhiteVis
%
% expected input folder hierarchy:
%
%      >sampleFolder
%           > PosFolders or RoiFolders
%           > SampleWhiteImage
%           > White
%                   > white after offset
%       >darkFolder
%       >darkFolder.smooth
%       >whiteFolder
%           > darkFolderforWhite
%           > darkFolderforWhite.smooth
%       >whiteFolder.smooth
%
%
% expected output folder hierarchy:
%
%      >sampleFolder
%           > IntensityCorrected
%                   > PosFolders or RoiFolders
%           > PosFolders or RoiFolders
%           > SampleWhiteImage
%           > White
%                   > white after offset
%       >darkFolder
%       >darkFolder.smooth
%       >whiteFolder
%           > darkFolderforWhite
%           > darkFolderforWhite.smooth
%       >whiteFolder.smooth

% NB : managing dark cases : 
%      1-  when dark have been recorded as the sample with a micromanager
%       folder hierarchy
%      2- when drak are copied from the dark reference directly in a folder
%      with no micrmanager hierarchy
%      3 - when an offset is required to avoid 0 values

%% use
% correctIntensities

%% Comments
%   written for  Joel Passicousset PhD. IFPEN. proposal 20161050
% adapted for kevin Vidot (PhD PVPP INRA BIA) proposal 20170043
% adapted for lin proposal 20171197


%% Author
% MF Devaux
% INRA BIA
% PVPP

%% date
% 5 octobre 2017:
% 2 mars 2018
% 23 mars 2018
% 20 avril 2018 : folder for white
% 11 octobre 2018 : folder for reference image and correction of visible
% image
% 12 mar s2019 white image msut be of the same size than the images...
% 14 mai 2019: dark image can be in a floder that is not a micromanager folder
%               and images are listed according to channels
% 20 mai 2019 : offset of dark and reading dark using list channels
% reference
% 31 mai : take into account estimated dark with the same size of roi than
% the sample
%
%     4 juin 2019 : replace exist by isfolder or isfile
% 4 juillet 2019 : bugs for channels order when the visible image is not
% the last recorded image
%  23 septembre 2019 use deblank to avoid problems of various length of listchannels names

%% context variables
orig=pwd;           % returns the current directory

%% start

if nargin ~=0
    error('use:  correctIntensities');
end


%% input
[nameFolder,rootFolder,~,listChannel]=getInfoTelemos('root folder of TELEMOS images to process');
nbChannel=size(listChannel,1);

cd(rootFolder)

% type of micromanager structure
listdir=dir('Roi*');
if isempty(listdir)
    listdir=dir('Pos*');
    gsf='pos';
else
    gsf='roi';
end

% visible image
cd(listdir(1).name);
lvis=dir('*vis*.tif');
if isempty(lvis)
    okvis=0;
else
    okvis=1;
    
end


% dark images
cd(rootFolder)
cd ..
[rdarkFolder]=uigetdir('.','dark Images folder (preferably smoothed) ');
cd(rdarkFolder)
if ~isfolder('Pos0')
    otherDark=yesno(sprintf('folder %s does not seem to be a TELEMOS folder. Select dark image directly form this folder?',rdarkFolder));
    if ~otherDark
        error('Choose appropriate dark images before starting');
    end
else
    cd('Pos0')
    rdarkFolder=pwd;
end

% white reference
cd(rootFolder)
if isfolder('WhiteReference')
    cd('WhiteReference');
end
[fileWhite,rwhiteFolder]=uigetfile({'*.tif'},'white image for fluorescence','*.tif');


% correct white image if exist ?
if okvis
    okCorrVis=yesno('correct intensities of visible image ?');
    if okCorrVis
        cd(rootFolder)
        if isfolder('WhiteReferenceVis')
            cd('WhiteReferenceVis');
        else
            if isfolder('WhiteVisibleEstimate')
                cd('WhiteVisibleEstimate')
            end
        end
        [fileWhiteVis,rwhiteVisFolder]=uigetfile({'*.tif'},'white image for visible image','*.tif');
    end
else
    okCorrVis=0;
end

% save
cd(rootFolder)
if ~isfolder('intensityCorrected')
    mkdir('intensityCorrected')
end
cd('intensityCorrected')
sfolder=pwd;

%% read and initialisation
cd(rootFolder)
info=readDIV([nameFolder '.' gsf '.metadata.txt']);
ntime=unique(info.d(:,1));
if length(ntime)~=1
    error('check number of time for each pos');
end
nz=unique(info.d(:,3));
if length(nz)~=1
    error('check number of z for each pos');
end


%read position of roi in white image
pos=readDIV([nameFolder '.roiAcq.txt']);
yl=pos.d(1,2);
xc=pos.d(1,1);
if yl==0
    yl=1;
end
if xc==0
    xc=1;
end

% determine size of images to be processed
cd(rootFolder)
cd(listdir(1).name);
list=dir('*.tif');
imetesdt=imread(list(1).name);
nl=size(imetesdt,1);
nc=size(imetesdt,2);

% identify position of visible image in the list of channels
if okvis
    fnumVis=strfind(cellstr(listChannel),'_nofilter_vis');
    listChannelFluo=listChannel(1:(end-1),:);
    nch=1;
    for i=1:nbChannel
        if ~isempty(fnumVis{i})
            numVis=i;
        else
            listChannelFluo(nch,:)=listChannel(i,:);
            nch=nch+1;
        end
    end
else
    listChannelFluo=listChannel;
end

% dark images
% read dark image
cd(rdarkFolder)
listDark=dir('*.tif');
nbdark=length(listDark);
if nbdark ~= nbChannel
    error('number of dark differ from number of channels')
end

for i=1:nbdark
    nimdark=dir(['*_' deblank(listChannel(i,:)) '*.tif']);
    if length(nimdark)>1
        error('number of dark for channel %s is higher than 1 (%d)',listChannel(i,:),length(nimdark));
    end
    if isempty(nimdark)
        error('no dark for channel %s ',listChannel(i,:));
    end
    nimdark=nimdark.name;
    rim=imread(nimdark);
    if i==1
        dark=uint16(zeros(size(rim,1),size(rim,2),length(listDark)));
    end
    dark(:,:,i)=rim;
end

% compute dark adapted to the images to be processed
darkc=uint16(zeros(size(imetesdt,1),size(imetesdt,2),size(dark,3)));
if size(dark,1)==nl && size(dark,2)==nc
    darkc=dark;
else
    for i=1:nbdark
        darkc(:,:,i)=dark(yl:(yl+size(imetesdt,1)-1),xc:(xc+size(imetesdt,2)-1),i);
    end
end
% identify dark of fluorescent images
if okvis
    darkcFluo=darkc(:,:,1:(end-1));
    darkcFluo(:,:,1:(numVis-1))=darkc(:,:,1:(numVis-1));
    darkcFluo(:,:,numVis:end)=darkc(:,:,(numVis+1):end);
else
    darkcFluo=darkc;
end


% white image
% read white image
cd(rwhiteFolder)
imw=imread(fileWhite);

% WHITE IMAGE (should be directly of the proper size and offsetted for background) 
white=imw;
wh=double(white)/max(double(white(:)));

% in case of visible image : white image is NOT supposed to offsetted for
% background as it is supposed to be acquired EXCATLY with the same
% acquisition time than the sample : the dark for the sample can also be
% used for the visible white 
if okCorrVis
    cd (rwhiteVisFolder)
    imwv=imread(fileWhiteVis);
    whiteV=imwv;
    whiteV=whiteV-darkc(:,numVis);
    whv=double(whiteV)/max(double(whiteV(:)));
end

% initialisation : number of fluorescent image
if okvis
        nbfluo=size(listChannelFluo,1);
    else
        nbfluo=size(listChannel,1);
end

% of dark offset
fdarkOffset=zeros(1,nbfluo);
darkOffset=ones(1,nbfluo)*10;

%% treatment
% correct images
for i=1:length(listdir)
    display(listdir(i).name)
    cd(rootFolder);
    cd(listdir(i).name)
    
    rf=pwd;
    
    % create hireacrchy of sub-folders
    cd(sfolder)
    if ~isfolder(listdir(i).name)
        mkdir(listdir(i).name)
    end
    cd(listdir(i).name)
    sf=pwd;
    
    cd(rf)
    
    for c=1:nbfluo       % for each fluorescent channel
        fprintf('\t- %s\r',deblank(listChannelFluo(c,:)))
        cd(rootFolder);
        cd(listdir(i).name)
        list=dir(['img_*' deblank(listChannelFluo(c,:)) '*.tif']);
        nbim=length(list);
        
        for cpt=1:nbim
            %read
            cd(rf)
            ime=imread(list(cpt).name);
            % filter white noise
            ime=medfilt2(ime,[3 3 ],'symmetric');
            
            % subtract background
            % check for offset
            tmp=double(ime)-double(darkcFluo(:,:,c))+darkOffset(c);
            % check for more than 1% of pixels with <0 values
            if sum(sum(tmp<0))>size(ime,1)*size(ime,2)/100
                % then an offset is required so that background is not
                % strictly 0 : avoid saturation in dark
                tmp=tmp-darkOffset(c);
                if fdarkOffset(c)
                    checkDarkOffset=-round(mean(tmp(tmp<0)));
                    if abs(checkDarkOffset-darkOffset(c))>10
                        error('ATTENTION: new dark offset %d differ from the first darkOffset %d from more than 10 counts : check data before going on', checkDarkOffset,darkOffset(c));
                    end
                else
                    fdarkOffset(c)=1;
                    darkOffset(c)=-round(mean(tmp(tmp<0)));
                    
                    if darkOffset(c)>25
                        ok=yesno(sprintf('ATTENTION, dark offset of %d count necessary for channel %s. CONTINUE?',darkOffset(c),listChannel(c,:)));
                        if ~ok
                            error('ATTENTION: check data for dark offset before going on');
                        end
                    end
                end
            end
                
            imb=double(ime)-double(darkc(:,:,c))+double(darkOffset(c));
            
           
            % divide by white
            % for the first image, check for white image threshold of low
            % values
            if i==1 && c==1 && cpt==1
                ok=0;
                whGarde=wh;
                seuil=0.1;
                while ~ok
                    % remove low values of white image
                    wh(wh<seuil)=seuil;
                    imc=uint16(double(imb)./wh);
                    
                    h27=figure(27);
                    set(h27,'units','normalized','position',[0.05 0.45 0.85 0.5])
                    subplot(1,3,1)
                    imshow(imb,[prctile(imb(:),0.1) prctile(imb(:), 99.9)])
                    title('after background subtraction')
                    subplot(1,3,2)
                    imshow(imc,[prctile(imc(:),0.1) prctile(imc(:), 99.9)])
                    title('after white correction')
                    subplot(1,3,3)
                    cr=improfile(double(imb)/max(double(imb(:))),[1,nl],[1,nc]);
                    cw=improfile(wh,[1,nl],[1,nc]);
                    cc=improfile(double(imc)/max(double(imc(:))),[1,nl],[1,nc]);
 
                    plot(cr,'k','linewidth',2)
                    hold on
                    plot(cw,'r','linewidth',2)
                    plot(cc,'b','linewidth',2)
                    ylabel('normalised intensities');
                    legend('raw signal','white image','corrected signal')
                    hold off
                    title('diagonal profiles')
                    
                    
                    ok=yesno(sprintf('OK for this correction with threshold %4.2f for the white image?',seuil));
                    
                    if ~ok
                        seuil=inputNumber('choose a new threshold for the white image (>0 and <1)','Vmin',0,'Vmax',1,'def',0.1);
                        wh=whGarde;
                    end
                end
            else
                imc=uint16(double(imb)./wh);
            end
            
                        
            % save
            cd(sf)
            imwrite(imc,list(cpt).name);
        end
    end
    
    if okvis
        fprintf('\t- Visible images\r')
        cd(rf)
        lvis=dir('*vis*.tif');
        nbimvis=length(lvis);
        for v=1:nbimvis
            cd(rf)
            ime=imread(lvis(v).name);
        
            %    subtract background
            imb=ime-darkc(:,:,numVis);
        
            if okCorrVis
                % divide by white
                imc=uint16(double(imb)./whv);
                % filter noise due to low signal on the edge of the images
                imc=medfilt2(imc,[3 3 ],'symmetric');
            else
                imc=imb;
            end
        
            % save
            cd(sf)
            imwrite(imc,lvis(v).name);
        end
        
    end
    
    cd(rf)
    copyfile('metadata.txt',sf)
end

cd(rootFolder)
copyfile('display_and_comments.txt',sfolder)

%% matlab function tracking
cd(sfolder)
fid=fopen('correctIntensities.track.txt','w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS:  correction of images for intensity: subtract background and divide by white illumination to \r\n');
fprintf(fid,'______________________________________________________________________________________\r\n');


fprintf(fid,'\r\nProcessed root folder : %s\r\n',rootFolder);

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Rois position in white and dark images: \r\n');
fprintf(fid,'\t-Row position: %d\r\n\t-Col Position:%d\r\n',yl,xc);

fprintf(fid,'\r\nDark images found in folder:%s\r\n',rdarkFolder);
for i=1:length(listDark)
    fprintf(fid,'\t -  %s\r\n',listDark(i).name);
end

fprintf(fid,'\r\n');
fprintf(fid,'Images were offsetted after dark correction to avoid black saturation\r\n');
for c=1:nbfluo
    if fdarkOffset(c)
        fprintf(fid,'\t- Channel %s: offset %d\r\n',listChannel(c,:),darkOffset(c));
    else
        fprintf(fid,'\t- Channel %s: offset 10\r\n',listChannel(c,:));
    end
end

fprintf(fid,'\r\nWhite image:%s\r\n',fileWhite);
fprintf(fid,'Folder : %s\r\n\r\n',rwhiteFolder);
fprintf(fid,' White image is supposed to be preprocessed : smoothed and offseted for its own dark background: \r\n');
fprintf(fid,' White image is normalised  : smoothed and maximum set to 1\r\n');
fprintf(fid,' Values of white image below %4.2f are set to %4.2f\r\n',seuil,seuil);


fprintf(fid,'\r\n');
if okvis
    if okCorrVis
        fprintf(fid,'\r\nWhite image for  visible image:%s\r\n',fileWhiteVis);
        fprintf(fid,'Folder : %s\r\n\r\n',rwhiteVisFolder);
    else
        fprintf(fid,'Visible image not corrected for white inhomogeneities\r\n');
        fprintf(fid,'Visible image corrected for camera background\r\n');
    end
end

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Images were corrected as follow: \r\n');
fprintf(fid,'\t\tt (ImRaw(filter)-Dark(filter)) \r\n');
fprintf(fid,'\tImCorr = -----------------------------------\r\n');
fprintf(fid,'\t\t (ImWhiteSmoothOffsettedNormalisedTO1) \r\n');

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Images were filtered to remove white for noise due to low signal: \r\n');
fprintf(fid,'\t- Median filtering of size 3x3: \r\n');

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Corrected images saved in folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)

