function mspim=meanSpectrum(varargin)

 %% description
    % compute the average spectra of the spectral collection 
    % in the dso.mat file
    
%% input parameters can be
    % - no parameter
    % or
    % 1 - spectral data in dso format
    % 2 - save folder 
    
%% output
    
    % - nothing 
    % or
    % - the average spectrum vector as a dataset object

%% principle
    % - calculte the average spectrum of the data set
    %

%% use
    % meanSpectrum;
    
    % ims=readomnic;
    % gim=meanSpectrum(ims)
    
%% Comments, context
    % review for CouplimSpec Toolbox
    %
    % from moyspectres function
    % written for the PhD thesis of Fatma Allouche
    
    
%% Author
    % MF Devaux 
    % BIA - PVPP INRA Nantes
    
%% date
    % 3 avril 2014 : bug with imagemode
    % 13 fevrier 2014 for CouplImSpecToolbox
    % 5 mai 2011
    % 9 aout 2011 : save in sub-folder average

%% context variables 
orig=pwd; % returns the current directory

graph = 0 ;  % flag for drawing the average spectrum on a figure


%% input data
if nargin == 0
    % input image
    [fname,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

    cd(repl)
    dso=load(fname);
    ims=getfield(dso,char(fieldnames(dso)));
    clear dso;
   
    if ~exist('average','dir')
        mkdir('average')
    end;
    sfolder=strcat(repl,'average');
    graph = 1;
end;

if nargin >= 1
    % test si l'image en entr�e est un dataset or a file
    ims=varargin{1};
    if ischar(ims)
        fname=ims;
         % test du type de fichier
        point=strfind(fname,'.');
        point=point(length(point)-1);
        ext=fname((point+1):(length(fname)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end;
        dso=load(fname);
        ims=getfield(dso,char(fieldnames(dso)));
        clear dso;
    else if ~strcmp(class(ims),'dataset')
        error('give a data set as first input parameter');
        end;
    end;
    if ~exist('fname','var')
        fname=strcat(ims.name,'.dso.mat');
    end;
    repl=pwd;                               % reading folder
end;

if nargin == 1
    if ~exist('average','dir')
        mkdir('average')
    end;
    sfolder=strcat(repl,'average');
end;

if nargin == 2
    sfolder=varargin{2};
end;

if nargin >2
    disp('Wrong number of parameter')
    error('Usage: mspim=meanSpectrum(ims,sfolder');
end;

% test des param�tres de sortie
if  nargout >1
    error('wrong number of output parameter')
end;

%% Compute average spectrum

mspim=ims(1);
mspim.data=mean(ims.data);

mspim.label{1}=ims.name;

mspim.name=strcat(ims.name,'.average');

%% update imagemode field
if mspim.imagemode
    mspim.imagemode=0;
end;

%% update userdata field
% image : field has no more interest in average spectrum : field is removed
if isfield(mspim.userdata,'image')
    mspim.userdata=rmfield(mspim.userdata,'image');
end


%% save 
sname=strcat(mspim.name,'.dso.mat');
cd(sfolder)
save(sname,'mspim')

if graph
    plotdso(mspim,1,1,sfolder);
end;


%% matlab function tracking  

% 
fid=fopen(strrep(sname,'.dso.mat','.track.txt'),'w');

if fid==0
     errordlg('enable to open track file');
 end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Compute average spectrum from spectral dataset object \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nInput spectral image name: %s\r\n',fname);
fprintf(fid,'data folder: %s\r\n\r\n',repl);


fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'\r\nsave folder : %s \r\n',sfolder);


% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);


fclose(fid);


%% end

cd (orig)
    