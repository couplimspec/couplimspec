function varargout = SpatialCalibration(varargin)
% SPATIALCALIBRATION MATLAB code for SpatialCalibration.fig
%      SPATIALCALIBRATION, by itself, creates a new SPATIALCALIBRATION or raises the existing
%      singleton*.
%
%      H = SPATIALCALIBRATION returns the handle to a new SPATIALCALIBRATION or the handle to
%      the existing singleton*.
%
%      SPATIALCALIBRATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPATIALCALIBRATION.M with the given input arguments.
%
%      SPATIALCALIBRATION('Property','Value',...) creates a new SPATIALCALIBRATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SpatialCalibration_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SpatialCalibration_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SpatialCalibration

% Last Modified by GUIDE v2.5 19-Apr-2018 18:36:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @SpatialCalibration_OpeningFcn, ...
    'gui_OutputFcn',  @SpatialCalibration_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SpatialCalibration is made visible.
function SpatialCalibration_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SpatialCalibration (see VARARGIN)


% always start by loading an image
% always start by loading an image
[hfig,him,im,fname,rfolder,coord]=loadImage_sc;

handles.im=im;
handles.imorig=im;
handles.coord=coord.d;
handles.fname=fname;
handles.rfolder=rfolder;
handles.imaff=him;
handles.fig=hfig;

handles.imFlipH=0;
handles.imFlipV=0;


% Choose default command line output for SpatialCalibration
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SpatialCalibration wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = SpatialCalibration_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in sc_loadImage.
function sc_loadImage_Callback(hObject, eventdata, handles)
% hObject    handle to sc_loadImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% initialisation
handles.XdistPix=0;
handles.XdistMicron=0;
handles.XtPix=0;

handles.YdistPix=0;
handles.YdistMicron=0;
handles.YtPix=0;

set(findobj('tag','sc_textsaveCal'),'string',' not saved')
% initialisation of display
set(findobj('tag','sc_textFirstPoint'),'visible','off')
set(findobj('tag','sc_textSecondPoint'),'visible','off')
set(findobj('tag','sc_FirstPoint'),'string',['First Point']);
set(findobj('tag','sc_SecondPoint'),'string',['Second Point']);

set(findobj('tag','sc_textHpixSize'),'string','horizontal distance in �m');
set(findobj('tag','sc_textVpixSize'),'string','vertical distance in �m');

set(findobj('tag','sc_OKcropImage'),'value',0);

set(findobj('tag','cb_FlipH'),'String','not flipped');
set(findobj('tag','cb_FlipV'),'String','not flipped');
handles.imFlipH=0;
handles.imFlipV=0;

% load + display
% always start by loading an image
[hfig,him,im,fname,rfolder,coord]=loadImage_sc;

handles.im=im;
handles.imorig=im;
handles.coord=coord.d;
handles.fname=fname;
handles.rfolder=rfolder;
handles.imaff=him;
handles.fig=hfig;


% Update handles structure
guidata(hObject, handles);




% --- Executes on button press in sc_FirstPoint.
function sc_FirstPoint_Callback(hObject, eventdata, handles)
% hObject    handle to sc_FirstPoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(findobj('tag','sc_textFirstPoint'),'visible','on')

hf=figure(handles.fig);
hi=imshow(handles.im,[]);
hp=imscrollpanel(hf,hi);
api=iptgetapi(hp);
api.setMagnification(4);


[x1,y1,~]=impixel;
x1=x1(1);
y1=y1(1);

n1=str2double(inputdlg('First Point number'));

handles.P1=[n1 x1 y1];

api.setMagnification(api.findFitMag());

set(findobj('tag','sc_FirstPoint'),'string',['First Point: ',num2str(n1)]);
set(findobj('tag','sc_textFirstPoint'),'visible','off');


% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in sc_SecondPoint.
function sc_SecondPoint_Callback(hObject, eventdata, handles)
% hObject    handle to sc_SecondPoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(findobj('tag','sc_textSecondPoint'),'visible','on')

hf=figure(handles.fig);
hi=imshow(handles.im,[]);
hp=imscrollpanel(hf,hi);
api=iptgetapi(hp);
api.setMagnification(4);

[x2,y2,~]=impixel;
x2=x2(1);
y2=y2(1);

n2=str2double(inputdlg('Second Point number'));

handles.P2=[n2 x2 y2];

api.setMagnification(api.findFitMag());

set(findobj('tag','sc_SecondPoint'),'string',['Second Point: ',num2str(n2)]);
set(findobj('tag','sc_textSecondPoint'),'visible','off');


% Update handles structure
guidata(hObject, handles);


    




% --- Executes on button press in sc_CropImage.
function sc_CropImage_Callback(hObject, eventdata, handles)
% hObject    handle to sc_CropImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figure(handles.fig);
h = imrect(gca);
handles.apiX = iptgetapi(h);

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in sc_OKcropImage.
function sc_OKcropImage_Callback(hObject, eventdata, handles)
% hObject    handle to sc_OKcropImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of sc_OKcropImage

set(findobj('tag','sc_OKcropImage'),'value',1);

% get the position
pos = getPosition(handles.apiX) ;
handles.apiX.delete

im=handles.im;
im=im(pos(2):(pos(2)+pos(4)),pos(1):(pos(1)+pos(3)),:);

handles.imorig=handles.im;
handles.im=im;

figure(handles.fig);
handles.imaff=imshow(im,[]);


% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in sc_computeCal.
function sc_computeCal_Callback(hObject, eventdata, handles)
% hObject    handle to sc_computeCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


n1=handles.P1(1);
n2=handles.P2(1);
x1=handles.P1(2);
x2=handles.P2(2);

[a,b]=lineEq(handles.coord([n1 n2],1),[x1 x2]);

dc=[a b];

y1=handles.P1(3);
y2=handles.P2(3);
[a,b]=lineEq(handles.coord([n1 n2],2),[y1 y2]);

dl=[a b];


l7=handles.coord(7,2)*dl(1)+dl(2);
c7=handles.coord(7,1)*dc(1)+dc(2);

figure(handles.fig);
hold on

plot(c7,l7,'s','color','g')

Xtpix=abs(diff(handles.coord([n1 n2],1))/(x1-x2));
Ytpix=abs(diff(handles.coord([n1 n2],2))/(y1-y2));

set(findobj('tag','sc_textVpixSize'),'string',['Vertical pixel size: ',num2str(Ytpix),' �m'],'visible','on');

set(findobj('tag','sc_textHpixSize'),'string',['Horizontal pixel size: ',num2str(Xtpix),' �m'],'visible','on');

handles.Xtpix=Xtpix;
handles.Ytpix=Ytpix;

handles.dc=dc;
handles.dl=dl;


% Update handles structure
guidata(hObject, handles);




% --- Executes on button press in sc_saveCal.
function sc_saveCal_Callback(hObject, eventdata, handles)
% hObject    handle to sc_saveCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% tests
ok=1;

if (abs(handles.Xtpix-handles.Ytpix)>0.01)
    msgbox('Horizontal and vertical calibration differ. Check calibration please')
    ok=0;
end

if ok
    tpix=(handles.Xtpix+handles.Ytpix)/2;
    
    im=handles.im;
    im=im(:,:,2);
    
    im=imclose(im,strel('square',5));
    
    figure(handles.fig);
    imshow(im,[])
    
    oksaveim=yesno(['save image as '  strrep(handles.fname, '.tif', '.crop.tif') '?']);
    
    
    if oksaveim
        cd(handles.rfolder)
        %copyfile(handles.fname,strrep(handles.fname, '.tif', '.orig.tif'));
        
        imwrite(im,strrep(handles.fname, '.tif', '.crop.tif'));
        handles.sname=strrep(handles.fname, '.tif', '.crop.tif');
    end
    

    sname=strrep((handles.sname),'.tif','.size.txt');

    
    variable=char('nblig','nbcol','tpix','tlig','tcol');
    d.i=strrep(handles.sname,'.tif','');
    d.v=variable;
    d.d=[size(im,1) size(im,2) tpix tpix*size(im,1)  tpix*size(im,2)];
    
    cd(handles.rfolder)
    
    writeDIV(d,sname);
    
    if oksaveim
        d.i=strrep(handles.fname, '.tif','');
        d.v=variable;
        d.d=[size(handles.imorig,1) size(handles.imorig,2) tpix tpix*size(handles.imorig,1)  tpix*size(handles.imorig,2)];
        
        cd(handles.rfolder')
        
        writeDIV(d,strrep(handles.fname,'.tif','.size.txt'));
    end
    
    
    
    % convert stage cooordinate in pixel
cpix.d(:,1)=handles.coord(:,1)*handles.dc(1)+handles.dc(2);
if handles.imFlipH
    cpix.d(:,1)=abs(cpix.d(:,1)-size(handles.im,2)+1);
end


cpix.d(:,2)=handles.coord(:,2)*handles.dl(1)+handles.dl(2);
if handles.imFlipV
    cpix.d(:,2)=abs(cpix.d(:,2)-size(handles.im,1)+1);
end


cpix.i=(1:size(cpix.d,1))';
cpix.v=char('pixcol','pixline');

sname=strrep(handles.fname,'.imVideo.tif','.pixcoord.txt');
writeDIV(cpix,sname);

set(findobj('tag','sc_textsaveCal'),'string','SAVED');

 %% matlab function tracking
    
    fid=fopen(strrep(handles.fname,'.imVideo.tif','calibrate.track.txt'),'w');
    
    if fid==0
        errordlg('enable to open track file');
    end
    
    fprintf(fid,'\r\n%s\t',datestr(now,0));
    fprintf(fid,'Spatial Calibration \r\n');
    fprintf(fid,'__________________________________________________________________________\r\n');
    
   
    fprintf(fid,'\r\ninput file name: %s\r\n',handles.fname);
    fprintf(fid,'data folder: %s\r\n',handles.rfolder);
    
    fprintf(fid,'\r\n First point number : %d \r\n',handles.P1(1));
    fprintf(fid,'\r\n Second point number : %d \r\n',handles.P2(1));
    
    
    fprintf(fid,'\r\n Horizontal pixel size: %6.2f �m\r\n',handles.Xtpix);
    fprintf(fid,'\r\n vertical pixel size: %6.2f �m\r\n',handles.Ytpix);
    
      
    fprintf(fid,'\r\n Squared pixel considered\r\n');
    fprintf(fid,'\t -  Final pixel size : %6.2f �m \r\n',tpix);
    
    
    okcrop=get(findobj('tag','sc_OKcropImage'),'value');

    if okcrop
            fprintf(fid,'\r\n Video image cropped\r\n');
            fprintf(fid,'\r\n Cropped and filterred video image saved as %s\r\n',handles.sname);
    end
    
    fprintf(fid,'\r\nsaved calibration of video image as : %s \r\n',strrep((handles.sname),'.tif','.size.txt'));
    
    fprintf(fid,'\r\nsaved coordinates of points in pixels as : %s \r\n',sname);


    
    
    % save of function used
    fprintf(fid,'__________________________________________________________________________\r\n');
    info=which (mfilename);
    os=computer;        % return the type of computer used : windows, mac...
    switch os(1)
        case 'P'                        % for windows
            ind=strfind(info,'\');
        case 'M'                        % for Mac
            ind=strfind(info,'/');
        otherwise
            ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
    end
    
    repprog=info(1:(ind(length(ind))-1));
    fprintf(fid,'function name: %s ',mfilename);
    res=dir(info);
    fprintf(fid,'on %s \r\n',res.date);
    fprintf(fid,'function folder: %s \r\n',repprog);
    %fprintf(fid,'__________________________________________________________________________\r\n');
    
    fclose(fid);

end


% --- Executes on button press in pb_FlipH.
function pb_FlipH_Callback(hObject, eventdata, handles)
% hObject    handle to pb_FlipH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figure(handles.fig);

im=handles.im;
im=fliplr(im);
handles.imaff=imshow(im,[]);

handles.im=im;


if handles.imFlipH
    handles.imFlipH=0;
else
    handles.imFlipH=1;
end


if handles.imFlipH
    set(findobj('tag','cb_FlipH'),'String','Flipped');
else
    set(findobj('tag','cb_FlipH'),'String','not flipped');
end

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in pb_FlipV.
function pb_FlipV_Callback(hObject, eventdata, handles)
% hObject    handle to pb_FlipV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure(handles.fig);

im=handles.im;
im=flipud(im);
handles.imaff=imshow(im,[]);

handles.im=im;


if handles.imFlipV
    handles.imFlipV=0;
else
    handles.imFlipV=1;
end


if handles.imFlipV
    set(findobj('tag','cb_FlipV'),'String','Flipped');
else
    set(findobj('tag','cb_FlipV'),'String','not flipped');
end

% Update handles structure
guidata(hObject, handles);
