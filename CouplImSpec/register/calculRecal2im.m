function calculRecal2im
%% description
% project registrated images borders on a reference image
% all the registrated image in al folder are projected
%% input

    %  TODO 
    
%% output
    
    %   TODO

%% principe
    % TODO

%% use
    % calculRecal2im

%% Comments
    % modify with Dhivyaa Rajasundaram (Walltrac project)
    
    
    
%% Author
    % MF Devaux
    % INRA -BIA- PVPP
    
%% date
    % 17 mars 2014

%% context variables 

porig=pwd;

%% input

%% input
% read target image
[nomc,pathc]=uigetfile({'*.tif'},'target image:','*.tif');
cd(pathc)
imref=imread(nomc);
ngenref=strrep(nomc,'.tif','');


% read registrated image
[nom,pathr]=uigetfile({'*.tif'},'registrated image:','*.tif');
cd(pathr)
liste=dir(strcat('*',ngenref','*.register.txt'));
nbim=length(liste);

%% treatment
% reference image
figure('name','Reference image')
imshow(imref);

% loop for image
for i=1:nbim
    nomim=
gen='2f11mc2rfh1b1';

nomim=strcat(gen,'.sv.spi.ltf.norm.sum.recal.tif');
im=imread(nomim);

[ang,sca,t]=lireRecal(strrep(nomim,'.tif','.recal.txt'),'.')
T=matTransform(ang,sca,t)

imarecal=im;
d=[1 1 1 ;1 size(imarecal,1) 1 ; size(imarecal,2) size(imarecal,1) 1 ; size(imarecal,2) 1 1 ; 1 1 1 ];

T=matTransform(ang,sca,t)

cd .. 
cd('fondclair')
nomr=strcat(gen,'.tif');
imr=imread(nomr);

figure(1)
hold off
imshow(imr)
hold on

e=round(T*d')
plot(e(1,:),e(2,:),'r');

[angr,scar,tr]=lireRecal(strrep(nomr,'.tif','.recal.txt'),'.')

Tr=matTransform(angr,scar,tr)

cd ..
cd ..
cd ..
cd ('ref')
imf=imread('imRef.fh.tif');
figure(2)
hold off
imshow(imf)
hold on
imarecal=imr;
dr=[1 1 1 ;1 size(imarecal,1) 1 ; size(imarecal,2) size(imarecal,1) 1 ; size(imarecal,2) 1 1 ; 1 1 1 ];
er=round(Tr*dr')
plot(er(1,:),er(2,:),'r');

TT=Tr*T
et=round(TT*d')

plot(et(1,:),et(2,:),'g')


[angf,scarf,trf]=lireRecal('imRef.fh.recal.txt','.');
Tf=matTransform(angf,scarf,trf)
TT=Tf*Tr*T

imref=imread('imRef.tif');
figure(27)
imshow(imref)
hold on
et=round(TT*d')
plot(et(1,:),et(2,:),'g');

cd(porig)

