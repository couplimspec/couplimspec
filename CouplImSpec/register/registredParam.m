function [ig,jg,il,ic,maxcor]=registredParam(imref,imarecal,fact,ang)


 %% description
    % assess registrating parameters ( scale, rotation angle and translation) between two images
    %
%% input

    %  TODO 
    
%% output
    
    %   TODO

%% principe
    % TODO

%% use
    % [ig,jg,il,ic]=registredParam(imref,imarecal,fact,ang)

%% Comments
    % modify with Dhivyaa Rajasundaram (Walltrac project)
    
    
    
%% Author
    % MF Devaux
    % INRA -BIA- PVPP
    
%% date
    % 11 mars 2014
% 10 avril 2020

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=4
    error('use: [ig,jg,il,ic]=registredParam(imref,imarecal,fact,ang)');
end

%% no input

%% treatment
% assess cross correlation matrix with inital scale and rotation angle
% vectors
cc=crosscorrelationScaleRotMatrix(imref,imarecal,fact,ang);
figure
surf(fact,ang,cc','FaceColor','interp','EdgeColor','none','FaceLighting','phong')
xlabel('facteur');
ylabel('angle');

% find indices of scale and rotation angle corresponding to maximum correlation
[ig,jg]=find(cc==max(cc(:)));


% corresponding registrated image
figure('name','proposed regitrated image')      %TODO : improve....
imshow(imrotate(imresize(imarecal,1/fact(ig(1))),-ang(jg(1))))

% find translation vector
% rotate target image
tmpref=imrotate(imref,ang(jg(1)),'bilinear');
f1=size(tmpref,1);
f2=size(tmpref,2);
% scale registrating image
tmprecal=imresize(imarecal,1/fact(ig(1)),'bilinear');
t1=size(tmprecal,1);
t2=size(tmprecal,2);

% matlab cross correlation between the two temporary images (see matlab
% doc)
C = normxcorr2(tmprecal,tmpref);
C=C(t1:f1,t2:f2); 
% translation vector of sclaed regsitrating image into rotated target image
[i,j]=find(C==max(C(:)));
%  coordinates relative to the gravity center of the target image
ic=i-size(tmpref,1)/2;
jc=j-size(tmpref,2)/2;

maxcor=C(i,j);

% rotation matrix used to compute translation vector into target image BEFORE rotation
R=rotationMatd(-ang(jg(1)));

% compute translation vector 
rrc=R*[ic jc ]';
% translation vector expressed relatively to the image coordinate system (not the gravity center)
rr=rrc+size(imref)'/2;

% visualisation of the result
% fusion of registrating image to target image
voir=tmpref;
voir(:,:,2)=tmpref;
voir(:,:,3)=tmpref;
voir(i:(i+t1-1),j:(j+t2-1),2)=0.75*tmprecal+0.25*voir(i:(i+t1-1),j:(j+t2-1),2);
voir(i:(i+t1-1),j:(j+t2-1),3)=0.75*tmprecal+0.25*voir(i:(i+t1-1),j:(j+t2-1),3);

vv=imrotate(voir,-ang(jg(1)));

figure
%subplot(1,2,1)
imshow(vv)

% extreme points of registrating image in target image
d=[1 1 1 ;1 size(imarecal,1) 1 ; size(imarecal,2) size(imarecal,1) 1 ; size(imarecal,2) 1 1 ; 1 1 1 ];
t=ang(jg(1));
s=fact(ig(1));
T=[cosd(t)/s -sind(t)/s rr(2);sind(t)/s cosd(t)/s rr(1);0 0 1];
e=T*d';

figure
%subplot(1,2,2)
imshow(imref)
hold on

plot(e(1,:),e(2,:),'r','linewidth',3);

% output

il=rr(1);
ic=rr(2);

%% matlab function tracking  
% no funtion tracking

%% end
cd(orig)

