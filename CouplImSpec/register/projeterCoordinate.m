function projeterCoordinate

porig=pwd;

if nargin==0
    
    [image1,rep1]=uigetfile({'*.tif'},'image ref','*.tif');
    cd(rep1)
    imref=imread(image1);
    image1=strrep(image1,'.tif','.taille.txt');
    c1=lire(image1);
    
    cd(porig);
    
    [image2,rep2]=uigetfile({'*.tif'},'image � projeter','*.tif');
    cd(rep2)
    imarecal=imread(image2);
    image2=strrep(image2,'.tif','.taille.txt');
    c2=lire(image2);
    
    [nom,rep]=uigetfile({'*.txt'},'nom du fichier param�tre','*.recal*.txt');
    cd (rep)

    [angr,scar,tr]=lireRecal(nom,'.')   
    
    
end;

zimref=zeros(c1.d(1),c1.d(2));

Tr=matTransform(angr,scar,tr)

nl=c2.d(1);
nc=c2.d(2);

d=[1 1 1 ;1 nl 1 ; nc nl 1 ; nc 1 1 ; 1 1 1 ];

e=Tr*d';


roiindiv=roipoly(zimref,e(1,:),e(2,:));

[i,j]=ind2sub([nl nc],1:(nl*nc));
cp=[j' i' repmat(1,nl*nc,1)];

cpp=Tr*cp';
cppr=round(cpp(1:2,:));
indice=find(cppr(1,:)>0 & cppr(2,:)>0);
sel=cppr(:,indice);
io=i(indice);
jo=j(indice);

taille=5;
nl1=c1.d(1);
nc1=c1.d(2);
indice=find(sel(1,:)<(nc1-(taille-1)/2+1) & sel(1,:)>((taille-1)/2) & sel(2,:)<(nl1-(taille-1)/2+1) & sel(2,:)>((taille-1)/2))

figure
subplot(2,2,1)
imshow(imref);
subplot(2,2,2)
imshow(imarecal);
subplot(2,2,3);
imshow(roiindiv);
subplot(2,2,4)
imshow(imref)
hold on
plot(e(1,:), e(2,:),'r')
plot(sel(1,indice),sel(2,indice),'*g')

cop.d=[io(indice)' jo(indice)' sel(2,indice)' sel(1,indice)'];
cop.i=(1:size(cop.d,1))';
cop.v=char('ilIR','icIr','ilRaman','icRaman');

ecrire(cop,strcat(strrep(image2,'.taille.txt',''),'-',strrep(image1,'.taille.txt',''),'.coord'));

fusion(imref,imarecal,angr, scar,tr,c1,c2)

cd(porig);
