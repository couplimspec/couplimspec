function computeCoordinate2imRef
%% description
% calculate coordinate of pixels on a reference image
%% input

    %  reference image
    % coordinate of points in pixel value of the registrated original image
%% output

    %   coordinates of points in the reference images
%% principe
% coordinates of points are known in pixel values for an image registrated on 
% a target reference image% i.e. : there exist a - reference image : imref.tif 
% - <nomgen.pixcoord.txt nomgen.pixcoord.txt> file - <nomgen.imVideo.crop.TO.imRef.register.txt 
% nomgen.imVideo.crop.TO.imRef.register.txt> file

    % The register file contains the angle, scale and translation factors
    % of the registrated images. the values are used to compuet a
    % transformation matrix T
    
    % the coordinates of the points are computed using this transformation
    % matrix.
    
    % points are projected onto the reference image for validation and the
    % values are saved
%% use
% computeCoordinate2imRef
%% Comments
% developed for proposal SOLEIL 20150929 SMIS data
%% Author
% MF Devaux INRA -BIA- PVPP
%% date
% 7 mai 2018 5 novembre 2018 to manage name of spectra in the dso associated 
% file
% 7 decembre 2018:  also copy coordinates of orginal image and original
% video image if this option is selected

%% context variables

porig=pwd;
%% input

cd ..

% read target image
[nomc,pathc]=uigetfile({'*.tif'},'reference target image:','*.tif');
cd(pathc)
imref=imread(nomc);
ngenref=strrep(nomc,'.tif','');

cd(porig)

% read coordinate in registrated image
[coordFilename,pathr]=uigetfile({'*.txt'},'points coordinates in pixels for the registrated image : ','*.pixcoord.txt');

cd(pathr)
co=readDIV(coordFilename);
ngenreg=strrep(coordFilename,'.pixcoord.txt','');

% read param of registration
lreg=dir([ngenreg '*.TO.' ngenref '.register.txt']);

if isempty(lreg) || length(lreg)>1
    [nomReg,pathreg]=uigetfile({'*.register.txt'},'name of file containing the register parameters','*.register.txt'); 
    cd(pathreg);
else
    nomReg=lreg.name;
    pathreg=pwd;
    cd(pathreg)
end


[ang,sca,t]=readRegister(nomReg,'.');

% save
cd(pathr)
cd ..
sname=strrep(coordFilename,'.txt',['.To.' ngenref '.txt']);
[coordSfilename,paths]=uiputfile({'*.txt'},'save coordinate in reference image as : ',sname);

% save data associated to points
data=yesno('copy associated data files with coordinates ?');

if data
    cd (pathr)
    dataFilename=[ngenreg '*.dso.mat'];
    [dataFilename,pathrd]=uigetfile({'*.txt';'*.dso.mat'},'data file associated to points: ',dataFilename);
end
    
% copy original coordiante and video image 
videOrig=yesno('copy original pixel coordinate and video image ?');

if videOrig
    cd (pathr)
    [videoFilename,~]=uigetfile({'*.tif'},'video image file associated to points: ','*.tif');
end
    
% add to current figure
add=yesno('plot points to current figure ?');

if add
    coul='rgbcmy';
    listcoul={'red','green','blue','cyan','magenta','yellow'};
    [selec,ok]=listdlg('liststring',listcoul,'selectionmode','single','name','color of points','promptstring','select color of points');
    if ok
        coul=coul(selec);
    else
        coul='g';
    end
else
    coul='r';
end



% code for points
code=inputdlg('code of points','code of points',1,{'m01'});
code=char(code);
%% treatment

if ~add
    % reference image
    figure('name','Reference image')
    imshow(imref);
    hold on
end

% compute coordinates
% matrix of transformation
T=matTransform(ang,sca,t);
% homogeneous coordinates
d=[co.d(:,1),co.d(:,2),ones(size(co.d,1),1)];

e=round(T*d');

plot(e(1,:),e(2,:),'*','color',coul);

saveFig=yesno('save current figure ?');

% coordinates on reference image
cor.d=e(1:2,:)';
cor.v=co.v;
cor.i=[repmat([code '.'],size(co.i,1),1) co.i];

if data
    cd(pathrd)
    dso=loaddso(dataFilename);
    dso.label{1}=cor.i;
end

   

%% save

cd (paths)

writeDIV(cor,coordSfilename);

if saveFig
    saveas(gcf,strrep(coordSfilename,'.txt','.png'));
end

if data
    savedso(dso);
end

if videOrig
    cd(pathr)
    copyfile(coordFilename,paths)
    copyfile(videoFilename,paths);
end
    
%% matlab function tracking

cd (paths)
    
fid=fopen(strrep(coordSfilename,'.txt','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Compute points coordinates in reference image \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');


fprintf(fid,'\r\nreference image file name: %s\r\n',nomc);
fprintf(fid,'reference image folder: %s\r\n',pathc);

fprintf(fid,'\r\n\r\n');

fprintf(fid,'Points coordinates filename: %s\r\n',coordFilename);
fprintf(fid,'Found in folder: %s\r\n',pathr);

fprintf(fid,'\r\n\r\n');

fprintf(fid,'File of parameter of registration to reference image: %s\r\n',nomReg);

fprintf(fid,'\r\n\r\n');

fprintf(fid,'code of points: %s\r\n',code);


fprintf(fid,'\r\n\r\n');
fprintf(fid,'In folder: %s\r\n\r\n',paths);

fprintf(fid,'Points coordinates in reference image saved as: %s\r\n',coordSfilename);

if data
    fprintf(fid,'\r\nData file associated to points %s also saved \r\n',dataFilename);
    fprintf(fid,'File was found in folder %s \r\n',pathrd);
    fprintf(fid,'Label were modified to be the same as in the points coordinates file\r\n');
end
    
if videOrig
    fprintf(fid,'\r\noriginal video image %s also saved \r\n',videoFilename);
    fprintf(fid,'Original points coordinates also saved\r\n');
end

if saveFig
    fprintf(fid,'\r\n');

    if add
        fprintf(fid,'Reference image with projected points (colour %s) saved as %s\r\n',char(listcoul(selec)),strrep(coordSfilename,'.txt','.png'));
    else
        fprintf(fid,'Reference image with projected points saved as %s\r\n',strrep(coordSfilename,'.txt','.png'));
    end
end




% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);
cd(porig)