function [listenom]=decoderRegister(nom)

indice=strfind(nom,'.TO.');
tsep=4;

nb=length(indice);


listenom{1}=nom(1:(indice(1)-1));

suivant=indice(1)+tsep;

for i=2:nb
    listenom{i}=nom(suivant:(indice(i)-1));
    suivant=indice(i)+tsep;
end;

listenom{nb+1}=nom(suivant:end);