function registerFinal
%

 %% description
    % register two monochrome images
    
%% input

    %  nothing
    
%% output
    
    %  nothing

%% principe
    % interactive input
    % register according to the procedure developed during F Allouche PhD
    % and applied for Umea workshop


%% use
    % register
    
%% Comments
    % modify with Dhivyaa Rajasundaram (Walltrac project)
    
    
%% Author
    % MF Devaux
    % INRA -BIA- PVPP
    
%% date
    % 11 mars 2014

%% context variables 
pathd=pwd;           % returns the current directory
close all

%% start

if nargin >0
    error('use: register with no parameters');
end


%% input
% read registrating image
[nom,pathr]=uigetfile({'*.tif'},'registrating image:','*.tif');
cd(pathr)
imarecal=imread(nom);

figure('name','registrating image : start','windowstyle','docked')
imshow(imarecal);

% read target image
[nomc,pathc]=uigetfile({'*.tif'},'target image:','*.tif');
cd(pathc)
imref=imread(nomc);

figure('name','target')
imshow(imref);

% choose scale factor
ok=0;
while ~ok
    rep=inputdlg('input scale factor value from registrating image to target image');
    fact=str2num(char(rep));
    ok=1;
end
        
% choose angle
ok=0;
while ~ok
    rep=inputdlg('input rotation angle value from registrating image to target image');
    ang=str2num(char(rep));
    if ang>-360 && ang<360
        ok=1;
    else
        warndlg('value expected between -360 and +360 degrees');
    end
end
        


%% treatment
[il,ic,maxcor]=registredParamFinal(imref,imarecal,1/fact,ang);
    
 
%% saving registred image parameters
cd(pathr)   % file if recording if the folder of the registrating image
recal.i=nom;
recal.v=char('angle','scale','trow','tcol');

recal.d=[ang fact il ic];

sname=strcat(strrep(nom,'.tif',''),'.TO.',strrep(nomc,'.tif',''),'.register.txt');
writeDIV(recal,sname);

%% matlab function tracking  

fid=fopen(strcat(sname,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Regiter image\r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nregistrating image file name: %s\r\n',nom);
fprintf(fid,'data folder: %s\r\n',pathr);
fprintf(fid,'\r\ntarget image file name: %s\r\n',nomc);
fprintf(fid,'data folder: %s\r\n',pathc);

% registrating parameters: 
fprintf(fid,'\r\n\r\nProcess parameters:\r\n');
fprintf(fid,'\t- scale factor: %6.2f\r\n',fact);
fprintf(fid,'\t- rotation angle: %6.2f\r\n',ang);


% computed parameters: 
fprintf(fid,'\r\n\r\nFinal parameters:\r\n');
fprintf(fid,'\t- translation: row: %d column:%d\r\n',il,ic);
fprintf(fid,'\t- cross correlation coefficient: %6.2f\r\n',maxcor);
% 
fprintf(fid,'\r\n\r\nParameters file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',pathr);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end
cd(pathd)
