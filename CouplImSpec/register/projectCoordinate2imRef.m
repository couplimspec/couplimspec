function projectCoordinate2imRef
%% description
% project coordinate of pixels on a reference image
%% input

    %  TODO 
    
%% output
    
    %   TODO

%% principe
    % TODO

%% use
    % projectCoordinate2imRef

%% Comments
    % modify with Dhivyaa Rajasundaram (Walltrac project)
    
    
    
%% Author
    % MF Devaux
    % INRA -BIA- PVPP
    
%% date
    % 3 avril 2014

%% context variables 

porig=pwd;

   
%% input

% add to current figure
add=yesno('add to current figure ?');

if add
    coul='g';
else
    coul='r';
end;

% read target image
[nomc,pathc]=uigetfile({'*.tif'},'target image:','*.tif');
cd(pathc)
imref=imread(nomc);
ngenref=strrep(nomc,'.tif','');


% read registrated image
[~,pathr]=uigetfile({'*.tif'},'registrated image:','*.tif');
cd(pathr)
liste=dir(strcat('*',ngenref,'.register.txt'));
nbim=length(liste);


%% treatment
if ~add
    % reference image
    figure('name','Reference image')
    imshow(imref);
    hold on
end

% loop for image
for i=1:nbim
    tmp=decoderRegister(liste(i).name);
    nomim=strcat(tmp{1},'.tif');
    
    imarecal=imread(nomim);
    nomregister=strcat(strrep(nomim,'.tif',''),'.TO.',ngenref,'.register.txt');
    [ang,sca,t]=readRegister(nomregister,pathr);

    T=matTransform(ang,sca,t);

    d=[1 1 1 ;1 size(imarecal,1) 1 ; size(imarecal,2) size(imarecal,1) 1 ; size(imarecal,2) 1 1 ; 1 1 1 ];


    e=round(T*d');
    plot(e(1,:),e(2,:),coul);
end;

saveas(gcf,strcat(ngenref,'.projectRegistrated2imRef.png'));

cd(porig)

