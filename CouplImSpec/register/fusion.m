function vimref=fusion(imref,imarecal,T)



porig=pwd;

if nargin==0
    
    [image1,rep1]=uigetfile({'*.tif'},'reference image','*.tif');
    cd(rep1)
    imref=imread(image1);
       
    cd(porig);
    
    [image2,rep2]=uigetfile({'*.tif'},'registrated image','*.tif');
    cd(rep2)
    imarecal=imread(image2);
      
    [nom,rep]=uigetfile({'*.txt'},'registrating parameter file','*.register.txt');
    [angr,scar,tr]=readRegister(nom,'.');
    T=matTransform(angr,scar,tr);
    
    
end;

% construction d ela matrice de transformation

T=T';           % pour matalb function
T=maketform('affine',T);
[imar,xd,yd]=imtransform(imarecal,T,'nearest','xyscale',1,'fillvalues',double(max(imarecal(:))+1));
xd=round(xd);
yd=round(yd);

mask=zeros(size(imref));
mask(yd(1):yd(2),xd(:,1):xd(2))=imar<max(imar(:));
%mask(mask==max(imarecal(:))+1)
mask=logical(mask);

valg=imar(imar<max(imar(:)));
valgr=squeeze(ind2rgb(valg,jet(256)));
valgr=uint8(valgr*255);

if length(size(imref))==2
    vimref=uint8(zeros([size(imref) 3]));
    tmpf=imref;
else
    vimref=uint8(zeros(size(imref)));
end;


for i=1:3
    if length(size(imref))==3
        tmpf=imref(:,:,i);
    end;
    tmp=tmpf;
    tmp(mask)=uint8(0.65*double(tmpf(mask))+0.35*double(valgr(:,i)));
    vimref(:,:,i)=tmp;
end

figure
imshow(vimref)

cd(rep2)
imwrite(vimref,strcat(strrep(image2,'.tif',''),'-FD-',image1),'tif','compression','none')


cd(porig);
