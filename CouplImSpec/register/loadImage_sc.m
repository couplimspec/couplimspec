
%%______________ functions_______________________________________
% -- load image
function [hfig,him,im,fname,rfolder,coord]=loadImage_sc

[fname,rfolder]=uigetfile({'*.tif'},'name of image to calibrate','*.imVideo.tif');

cd(rfolder)
im=imread(fname);

if ~exist(strrep(fname,'.imVideo.tif','.coord.txt'),'file')
    errordlg('File of coordinates of spectra %s not found',strrep(fname,'.imVideo.tif','.coord.txt'));
else
    coord=readDIV(strrep(fname,'.imVideo.tif','.coord.txt'));
end


hfig=figure;

% show image
him=imshow(im);




