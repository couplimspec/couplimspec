function cc=crosscorrelationScaleRotMatrix(imref,imarecal,fact,ang)
%% description
    % compute a matrix of croo correlation coefficient for several values
    % of scales and rotation angles
    %
%% input

    %  TODO 
    
%% output
    
    %   TODO

%% principe
    % TODO

%% use
    % cc=crosscorrelationScaleRotMatrix(imref,imarecal,fact,ang)

%% Comments
    % modify with Dhivyaa Rajasundaram (Walltrac project)
    
    
    
%% Author
    % MF Devaux
    % INRA -BIA- PVPP
    
%% date
    % 11 mars 2014

%% context variables 
orig=pwd;           % returns the current directory

%% treatment
% initialisation
fi=0;
cc=zeros(length(fact),length(ang));
% scale factor loop
for f=fact
    fi=fi+1;
    fprintf('%d : ',fi);
    tmprecal=imresize(imarecal,1/f,'bilinear');

   if min(size(tmprecal))>10
        ai=0;
        % rotation angle loop
        for a=ang
            ai=ai+1;
            fprintf('%d ',ai);
            tmpref=imrotate(imref,a,'bilinear');
            
                 xdep=1;
                ydep=1;
                xSize=size(tmpref,1);
                ySize=size(tmpref,2);
 
            if size(tmprecal,1)<xSize && size(tmprecal,2)<ySize;
                t1=size(tmprecal,1);
                t2=size(tmprecal,2);
                tmpref=tmpref(xdep:(xdep+xSize-1),ydep:(ydep+ySize-1));
                f1=size(tmpref,1);
                f2=size(tmpref,2);
        
                C = normxcorr2(tmprecal,tmpref);
                C=C(t1:f1,t2:f2);
                [i,j]=find(C==max(C(:)));
               
                
                cc(fi,ai)=C(i,j);
            end
        end
   end
   fprintf('\r\n');
end
fprintf('\r\n');