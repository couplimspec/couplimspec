function IRcoordinates

close all
pIR=pwd;
pRaman='..\..\110120_Raman_SOLEIL\preProcessedDso';
cd (pRaman);
pRaman=pwd;
cd(pIR);

pPaired='..\..\pairedData';
cd (pPaired)
pPaired=pwd;
cd(pIR);


%% comments
% read IR spectral image
nimIR='2f11mc2rm';
dsoIR=loaddso(strcat(nimIR,'.dso.mat'));
simIR=dsoIR.imagedata;

% pixel coordinates are considered in the center of the pixel
% pixel size
pixRowSize=11.74;           % �m
pixColSize=19.26;           % �m

% number of pixel in raw and column
nbRow=25;
nbCol=10;

% meshgrid of coordinates
[rr ,cc]=meshgrid(1:nbRow,1:nbCol);
% row coordinates
rr=rr';
% column coordinates
cc=cc';

% pixel coordinates in micron
rrmicron=rr*pixRowSize-(pixRowSize/2);
crmicron=cc*pixColSize-(pixColSize/2);


% pixel coordinates in pixel unit of the IR image used for registration
% NB : - the raw infrared image had rectangular pixels (see pixel size)
%      - for registration, a synthetic IR image with squared pixel was computed 
%        (see IRSquarePixelImage.m function)
%      - the pixel size of the synthetic image was 6 �m
%       => resgitration with raman image was performed using 6�m IR pixels 
%      - for registration, the size of the pixels in �m is not taken into
%      account : the sacle factor if computed in pixel unit (not micron)
%
% as a consequence, we must consider the actual raw infrared pixel in
% pixel unit of synthetic IR image. This imply dividing the actual pixel
% coordinates in micron by the size of the synthetic IR pixel = 6
pixIRrSize=6;           % �m
rrmicron=rrmicron/pixIRrSize;
crmicron=crmicron/pixIRrSize;

% built the homogeneous coordinates matrix (see F Allouche PhD)
c(:,1)=reshape(rrmicron,nbRow*nbCol,1);
c(:,2)=reshape(crmicron,nbRow*nbCol,1);
c(:,3)=1;

IRWindow=20;

% name of infrared register image
nimIRReg=strcat(nimIR,'.sum.register');

% name of Raman files
genname='2f11mc2rfh1b';
endname='.sv.spi.ltf.norm';
nbfile=6;
num=1;
for i=1:nbfile
    nimRam=strcat(genname,num2str(i),endname);
    [IR2way, IRcoord, Ram3way, RamRange,num]= inRangeCoordinate(c,IRWindow,simIR,nimIRReg,pIR,nimRam,pRaman,num);
    
    % build the final paired data
    if i==1
        IR2wayPaired=IR2way;
        IRcoordPaired=IRcoord;
        Ram3wayPaired=Ram3way;
        RamRangePaired=RamRange;
    else
        IR2wayPaired=cat(1,IR2wayPaired,IR2way);
        IRcoordPaired=cat(1,IRcoordPaired,IRcoord);
        Ram3wayPaired=cat(1,Ram3wayPaired,Ram3way);
        RamRangePaired=cat(1,RamRangePaired,RamRange);
    end;
    nbPairedPixel(i)=size(IRcoordPaired,1);

end;
    



% dataset object
% infrared
dsoIR2way=dataset;

dsoIR2way.name=strcat(dsoIR.name,'.2way.paired2.',genname);

dsoIR2way.data=IR2wayPaired;
dsoIR2way.type='data';

dsoIR2way.label{1}=dsoIR.label{1}(IRcoordPaired(:,3),:);
dsoIR2way.label{2}=dsoIR.label{2};

dsoIR2way.axisscale{1}=(1:size(Ram3wayPaired,1))';
dsoIR2way.axisscale{2}=dsoIR.axisscale{2};

% userdata fields
dsoIR2way.userdata.acquisition=dsoIR.userdata.acquisition;
dsoIR2way.userdata.acquisition.coordStage.row=dsoIR.userdata.acquisition.coordStage.row(IRcoordPaired(:,1));

dsoIR2way.userdata.acquisition.coordStage.col=dsoIR.userdata.acquisition.coordStage.col(IRcoordPaired(:,2));

dsoIR2way.userdata.coordImOrig.row=IRcoordPaired(:,1);
dsoIR2way.userdata.coordImOrig.col=IRcoordPaired(:,2);
dsoIR2way.userdata.coordImOrig.id=1;
dsoIR2way.userdata.coordImOrig.image.imSize.row=dsoIR.imagesize(1);
dsoIR2way.userdata.coordImOrig.image.imSize.col=dsoIR.imagesize(2);

dsoIR2way.userdata.coordImOrig.image.imName=nimIR;
dsoIR2way.userdata.coordImOrig.image.imPathname=pIR;

cd(pPaired)
savedso(dsoIR2way);

% RAMAN
cd(pRaman)
nimRam=strcat(genname,num2str(1),endname);
dsoRam=loaddso(strcat(nimRam,'.dso.mat'));

dso3WayRam=dataset;
dso3WayRam.name=strcat(genname,'.3way.paired2.',dsoIR.name);

dso3WayRam.data=Ram3wayPaired;
dso3WayRam.type='data';

dso3WayRam.label{1}=dsoIR2way.label{1};
dso3WayRam.label{2}=num2str((1:size(Ram3wayPaired,2))');
dso3WayRam.label{3}=dsoRam.label{2};

dso3WayRam.axisscale{1}=(1:size(Ram3wayPaired,1))';
dso3WayRam.axisscale{2}=(1:size(Ram3wayPaired,2))';
dso3WayRam.axisscale{3}=dsoRam.axisscale{2};

dso3WayRam.description=dsoRam.description;


%userdata fields
dso3WayRam.userdata.acquisition=dsoRam.userdata.acquisition;

cd(pRaman)

p=1;
for i=1:6
    nimRam=strcat(genname,num2str(1),endname);
    dsoRam=loaddso(strcat(nimRam,'.dso.mat'));
    
    if isfield(dsoRam.userdata.acquisition,'visim')
        dso3WayRam.userdata.acquisition.visim.name{i}=dsoRam.userdata.acquisition.visim.name;
        dso3WayRam.userdata.acquisition.visim.pathname{i}=dsoRam.userdata.acquisition.visim.pathname;
        dso3WayRam.userdata.acquisition.visim.scale{i}=dsoRam.userdata.acquisition.visim.scale;
        dso3WayRam.userdata.acquisition.visim.angle{i}=dsoRam.userdata.acquisition.visim.angle;
        dso3WayRam.userdata.acquisition.visim.trow{i}=dsoRam.userdata.acquisition.visim.trow;
        dso3WayRam.userdata.acquisition.visim.tcol{i}=dsoRam.userdata.acquisition.visim.tcol;
        dso3WayRam.userdata.acquisition.visim.name{i}=dsoRam.userdata.acquisition.visim.name;
    end;

    coordImOrig.range{i}=RamRangePaired(p:(p+nbPairedPixel(i)-1),:);
    coordImOrig.id{i}=i;
    coordImOrig.image{i}.imSize.row=dsoRam.imagesize(1);
    coordImOrig.image{i}.imSize.col=dsoRam.imagesize(2);
    coordImOrig.image{i}.imName=nimRam;
    coordImOrig.image{i}.imPathname=pRaman;

    
end

dso3WayRam.userdata.acquisition.coordImOrig=coordImOrig;
%
cd(pPaired)
save(strcat(dso3WayRam.name,'.dso.mat'),'dso3WayRam');


cd(pIR)






