function register
%

 %% description
    % register two monochrome images
    
%% input

    %  nothing
    
%% output
    
    %  nothing

%% principe
    % interactive input
    % register according to the procedure developed during F Allouche PhD
    % and applied for Umea workshop


%% use
    % register
    
%% Comments
    % review for couplImSpecToolbox
    % modify with Dhivyaa Rajasundaram (Walltrac project)
    
    
%% Author
    % MF Devaux
    % INRA -BIA- PVPP
    
%% date
    % 1 avril 2014
    % 11 mars 2014
    % 17 octobre 2017

%% context variables 
pathd=pwd;           % returns the current directory
close all

%% start

if nargin >0
    error('use: register with no parameters');
end


%% input
% read registrating image
[nom,pathr]=uigetfile({'*.tif'},'registrating image:','*.tif');
cd(pathr)
imr=imread(nom);

% image size information
if ~exist(strrep(nom,'.tif','.size.txt'),'file')
    sizename=strcat(nom,'.size.txt');
else
    sizename=strrep(nom,'.tif','.size.txt');
end
imsize=readDIV(sizename);
tpixr=imsize.d(:,3);

figure('name','registrating image : start','windowstyle','docked')
imshow(imr);

% read target image
[nomc,pathc]=uigetfile({'*.tif'},'target image:','*.tif');
cd(pathc)
imc=imread(nomc);

% image size information
if ~exist(strrep(nomc,'.tif','.size.txt'),'file')
    sizename=strcat(nomc,'.size.txt');
else
    sizename=strrep(nomc,'.tif','.size.txt');
end
imsize=readDIV(sizename);
tpixc=imsize.d(:,3);


figure('name','target')
imshow(imc);
tpixRefPyr(1)=tpixc;
% pyramids
%target  image
imcp{1}=imc;
i=1;
while(min(size(imcp{i}))>50)
    i=i+1;
    imcp{i}=impyramid(imcp{i-1},'reduce');
    tpixRefPyr(i)=tpixc*2^(i-1);
    figure('name',strcat('target : pyramide ',num2str(i)),'windowstyle','docked')
    imshow(imcp{i});
end;

% registrating image 
imrp{1}=imr;
tpixRecalPyr(1)=tpixr;
i=1;
while(min(size(imrp{i}))>30)
    i=i+1;
    imrp{i}=impyramid(imrp{i-1},'reduce');
    tpixRecalPyr=tpixr*2^(i-1);
    figure('name',strcat('registrating : pyramide ',num2str(i)),'windowstyle','docked')
    imshow(imrp{i});
end;

% choose starting level of pyramids
ok=0;
while ~ok
    rep=input('registrating image: starting level of pyramide');
    nps=(rep);
    if nps>=1 && nps <=length(imrp)
        ok=1;
    end
end

imarecal=imrp{nps};
tpixRecal=tpixr*2^(nps-1);
npsInit=nps;


factTheoPyr=tpixRefPyr/tpixRecal;
factTheoPyr=1./factTheoPyr;

npc=find(factTheoPyr>=1);
npc=npc(end);
npcInit=npc;


% check initial values

imref=imcp{npc};
tpixRef=tpixc*2^(npc-1);


% starting multiplicative factor for registrating image to obtain the same resolution of target image
% 
factTheo=tpixRecal/tpixRef;


% ok=0;
% while ~ok
%     if factTheo<1           % not expected to perform a relevant search: higher level of pyramid selected
%         if npc>1
%             npc=npc-1;
%             imref=imcp{npc};                
%             tpixRef=tpixRef/2;
%             factTheo=tpixRecal/tpixRef;
%          else if nps<length(imrp)
%                 nps=nps+1;
%                 imarecal=imrp{nps};
%                 tpixRecal=tpixRecal*2;
%                 factTheo=tpixRecal/tpixRef;
%              else
%                 error('wrong starting level of pyramid');
%              end
%         end
%     else
%         ok=1;
%     end
% end;


% 1/factheo is considered to define bounds between 0 and 1 
bif=1/factTheo-0.20;
bsf=1/factTheo+0.2;
pasf=(bsf-bif)/20;
fact=bif:pasf:bsf;


% choose starting angle
ok=0;
while ~ok
    rep=inputdlg('starting rotation angle value from registrating image to target image');
    angTheo=str2num(char(rep));
    if angTheo>-360 && angTheo<360
        ok=1;
    else
        warndlg('value expected between -360 and +360 degrees');
    end
end
        
bia=angTheo-30;        %0;
bsa=angTheo+30;     %360;
pasa=3;
ang=bia:pasa:bsa;

%% treatment
% search loop
nbpas=10;
fini=0;
while ~fini
    [ig,jg,il,ic,maxcor]=registredParam(imref,imarecal,fact,ang);
    
    factTrouve=fact(ig(1));
    angTrouve=ang(jg(1));
    
    if npc>1
        npc=npc-1;
        imref=imcp{npc};
        fc=1/2;
    else
        fc=1;
    end;
    
    if nps>1
        nps=nps-1;
        imarecal=imrp{nps};
        fs=2;
    else
        fs=1;
    end
    
    factTrouve=factTrouve*fc*fs;
    pasf=pasf*fc*fs;
    bif=(factTrouve-2*pasf);
    bsf=(factTrouve+2*pasf);
    pasf=(bsf-bif)/nbpas;
    fact=bif:pasf:bsf;
    
    bia=angTrouve-2*pasa;
    bsa=angTrouve+2*pasa;
    pasa=(bsa-bia)/nbpas;
    ang=bia:pasa:bsa;
    
    %nbpas=ceil(nbpas/2);
    
    if fc==1 && fs==1
        if pasa>1
            fini=0;
        else
            fini=1;
        end
        
    end
end;

%% saving registred image parameters
cd(pathr)   % file if recording if the folder of the registrating image
recal.i=nom;
recal.v=char('angle','scale','trow','tcol');

recal.d=[angTrouve 1/factTrouve il ic];

sname=strcat(strrep(nom,'.tif',''),'.TO.',strrep(nomc,'.tif',''),'.register.txt');
writeDIV(recal,sname);

print(gcf,strcat(strrep(nom,'.tif',''),'.TO.',strrep(nomc,'.tif',''),'.register.png'),'-dpng');

%% matlab function tracking  

fid=fopen(strcat(sname,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Regiter image\r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nregistrating image file name: %s\r\n',nom);
fprintf(fid,'data folder: %s\r\n',pathr);
fprintf(fid,'\r\ntarget image file name: %s\r\n',nomc);
fprintf(fid,'data folder: %s\r\n',pathc);

% registrating parameters: 
fprintf(fid,'\r\n\r\nProcess parameters:\r\n');
fprintf(fid,'\t- registrating image starting level of pyramid: %d\r\n',npsInit);
fprintf(fid,'\t- target image starting level of pyramid: %d\r\n',npcInit);
fprintf(fid,'\t- initial scale factor: %6.2f\r\n',1/factTheo);
fprintf(fid,'\t- initial rotation angle: %6.2f\r\n',angTheo);


% computed parameters: 
fprintf(fid,'\r\n\r\nFinal parameters:\r\n');
fprintf(fid,'\t- scale factor: %6.2f\r\n',1/factTrouve);
fprintf(fid,'\t- rotation angle: %6.2f\r\n',angTrouve);
fprintf(fid,'\t- translation: row: %d column:%d\r\n',il,ic);
fprintf(fid,'\t- cross correlation coefficient: %6.2f\r\n',maxcor);
% 
fprintf(fid,'\r\n\r\nParameters file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',pathr);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end
cd(pathd)
