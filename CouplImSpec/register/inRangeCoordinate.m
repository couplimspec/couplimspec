function [IR2way, IRcoord, Ram3way, RamRange,num]= inRangeCoordinate(cIR,IRWindow,simIR,nimIR,pIR,nimRam,pRaman,num)

%%
% input
% cIR : infrared row and column coordinates of the images + column of 1 to
%       form the homogeneous coordinates required to compute the projection onto
%       the Raman image
% IRWindow : size of the small image in Raman pixel unit;
% simIR : 3-way spectral image : pixel x pixel x wavenumbers
% nimr : name of infrared spectral image (without dso.mat extension)
% pIR : path to IR data
% nimRam: name of RAMAN spectral image (without dso.mat extension)
% pRam : path to RAMAN data
% num : number of the current point

% output
% IR2way : 2-way IR data table of paired spectra
% IRcoord : matrix of row and column coordinate of paired IR spectra in the 
%           original 3way spectral image
% Ram3way : 3-way RAMAN data table of paired spectra : 
%               paired pixel x small image pixels x wavenumbers
% RamRange : min and max row and column coordinates of paired RAMAN pixel
%           in the original Raman image
% num : number of the current point

%%
% read register files
nameReg=strcat(nimIR,'.TO.',nimRam,'.sum.register.txt');
[ang,sca,t]=readRegister(nameReg,pIR);

% transformation matrix
T1=matTransform(ang,sca,t);

% compute Raman coordinates of IR pixels
cp1=T1*cIR';
% transpose result
cp1=cp1';

% project on images
cd(pRaman)
im=imread(strcat(nimRam,'.sum.tif'));
dso=loaddso(strcat(nimRam,'.dso.mat'));

figure
imshow(im,[])
hold on
plot(cp1(:,1),cp1(:,2),'g*')

% find pixels within the range of the image
% row
ff=find(cp1(:,1)>0 & cp1(:,1)< size(im,2));
cf=cp1(ff,:);
% column
fff=find(cf(:,2)>0 & cf(:,2)<size(im,1));

% raman coordinate of the infrared pixel
pixIrCoord(:,1)=round(cf(fff,2));
pixIrCoord(:,2)=round(cf(fff,1));

% number of infrared paired pixels
nbPixIr=size(pixIrCoord,1);

% small images
cd('smallImage')

sim=dso.imagedata;

Ram3way=zeros(nbPixIr,(IRWindow+1)^2,size(sim,3));
RamRange=zeros(nbPixIr,4);

for i=1:nbPixIr
    minr=pixIrCoord(i,1)-IRWindow/2;
    maxr=pixIrCoord(i,1)+IRWindow/2;
    minc=pixIrCoord(i,2)-IRWindow/2;
    maxc=pixIrCoord(i,2)+IRWindow/2;
    minr=max(1,minr);
    maxr=min(maxr,size(im,1));
    minc=max(1,minc);
    maxc=min(maxc,size(im,2));
    smallIm=im(minr:maxr,minc:maxc);
    figure(47)
    hold off
    imshow(im,[])
    hold on
    plot(cp1(ff(fff(i)),1),cp1(ff(fff(i)),2),'g*')
    if num<=9
        start='0';
    else
        start='';
    end
    if i<=9
        imwrite(smallIm,strcat(start,num2str(num),'-',nimRam,'0',num2str(i),'.tif'),'compression','none');
        saveas(gcf,strcat(start,num2str(num),'-',nimRam,'0',num2str(i),'.png'),'png');
    else
        imwrite(smallIm,strcat(start,num2str(num),'-',nimRam,num2str(i),'.tif'),'compression','none');
        saveas(gcf,strcat(start,num2str(num),'-',nimRam,num2str(i),'.png'),'png');
    end;
    smsim=sim(max(1,minr):min(maxr,size(im,1)),max(1,minc):min(maxc,size(im,2)),:);
    rsmsim=reshape(smsim,size(smsim,1)*size(smsim,2),size(smsim,3));
    nbPixRam=size(rsmsim,1);
    Ram3way(i,1:nbPixRam,:)=rsmsim;
    RamRange(i,:)=[ minr maxr  minc maxc];
    num=num+1;

end;

% infrared coordinate of the infrared pixel
nbRow=size(simIR,1);
nbCol=size(simIR,2);
[ic,jr]=ind2sub([ nbCol nbRow],ff(fff));
IRcoord(:,1)=jr;
IRcoord(:,2)=ic;

% find linear indices
linind=sub2ind([ nbRow nbCol ],jr,ic);

IRcoord(:,3)=linind;        % ff(fff);

IR2way=zeros(nbPixIr,size(simIR,3));

for i=1:nbPixIr
    IR2way(i,:)=simIR(jr(i),ic(i),:);
end;

figure
plot(IR2way')

seeram=zeros(nbPixIr,size(Ram3way,3));

for i=1:nbPixIr
    see=squeeze(Ram3way(i,:,:));
    seeram(i,:)=(mean(see(sum(see,2)>0,:)));
end

figure
plot(seeram')
set(gca,'xdir','reverse')

end




