function writeRegister(T,nom)
% focntion qui calcule et sauvegarde le fichier param�tre correspondant � une matrice de transformation
% affine d'images
%
%
% input : 
%       T : matrice de transformation en coordonnees homog�nes
%       nom : 'nom g�n�rique de sauvegarde (zsans recal)
%
% output : nothing
% 
% pricnipe : l'angle de rotation, le facteur d'achelle et le vecteur de trsnalation sont calcul�s
%           � partir de la matrice T
%           et les param�tres sont sauvegard�s dans le fichier recal
%           
%
% author : MF Devaux
%           BIA PVPP
%           30 septembre 2013
% 
%%
porig=pwd;

%% entree des param�tres
if nargin ~=2 
    error('uses :writeRegister(T,nom)');
end;

%% traitement
% ligne image=colonne matrice matlab et vice versa
% vecteur de translation
tl=T(2,3);
tc=T(1,3);

% facteur d'�chelle
sca= sqrt(T(1,1)^2+T(2,1)^2);

% cosinus et sinus de l'angle
ct=T(1,1)/sca;
st=T(2,1)/sca;

ang=atan2d(st,ct);

%% sauvegarde
param.d=[ang sca tl tc];
param.i=strcat(nom,'.inv');
param.v=char('angle','factEchelle','posl','posc');

nom=strrep(nom,'.txt','');
writeDIV(param,strcat(nom,'.register.txt'));

%%
cd (porig)


