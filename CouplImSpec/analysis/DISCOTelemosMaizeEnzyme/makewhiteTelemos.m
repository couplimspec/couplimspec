  function makewhiteTelemos
  

 %% description
    % create the white images of telemos from enzyme images
    
%% input

    %  nothing : more a script than a function...
    
%% output
    
    %   nothing whiteTelemos is saved on disk
    
    %% principe
    %HYPOTHESIS:
    %   the enzyme is largely visible. 
    % only large variations are searched for.
    %
    %
    % the first  image recorded for a sequence of enzymatic degradation in
    % channel 327-353 nm is considered idep
    
    % the camera black image is subtracted : Itemp=idep-IBkIm
 
    % the itemp is filtered => Ifilt
    %           top hat to remove spikes
    %               spikes are defined as peak less than 5x5 = sizeTopHat
    %                                                   with intensity >200
    %                                                   = seuilTopHat
    %
    %
    % the image lfilt is filtered => white
    %             in the frequency domain
    %             a value of 1=sigma of gaussian filtering applied on the
    %             whole image
    %
    % the white images is saved on disk

%% use
    % makewhiteTelemos
    
%% Comments
    %   for experiment SOLEIL 2014 proposal 20140308 
    
    
%% Author
    % MF Devaux
    % INRA BIA
    % PVPP
    
%% date
    % 17 janvier 2017

%% context variables 
orig=pwd;           % returns the current directory

% size of TopHat
 sizeTopHat=5;                                 
 seuilTopHat=200;
 
 
% window sigma for low frequency
WsizeLF=1;

% val min to added to white in order to avoid 0
Vmin=10;

%% start

if nargin >0
    error('use: makeImBlackCamera');
end


%% input
[rawfilename,rfolder]=uigetfile({'*.tif'},'Selec raw image of channel 327-353nm to generate white telemos image');
cd(rfolder)
im=imread(rawfilename);
[nl,nc]=size(im);
cd ..
ptmp=pwd;
[~,genFolder]=fileparts(ptmp);

[zfolder]=uigetdir(pwd,'Folder where camera black images have been saved');
cd(zfolder)
if exist(rawfilename,'file')
    bkim=imread(rawfilename);
else
    error('no images corresponding to camera black images seems to correspond %s',rawfilename);
end;

cd (rfolder)
cd ..
cd ..
cd ..
if ~exist('whiteImages','dir')
    mkdir('whiteImages');
end
cd('whiteImages');
if ~exist(genFolder,'dir')
    mkdir(genFolder);
end;
cd(genFolder);
[whitefilename,sfolder]=uiputfile({'*.tif'},'save white telemos image as','whiteTelemos.tif');


%% treatement

% remove camera black image 
imc=im-bkim;

% top hat to remove spike
imt=imtophat(imc,strel('square',sizeTopHat));
ims=uint16(zeros(size(imt)));
ims(imt>seuilTopHat)=imt(imt>seuilTopHat);

% strating image
imc=imc-ims;


% fft to estimate white image
% transformee de fourier de im
fim=fft2(imc);

% shift de la transformee de fourrier
sfim=fftshift(fim);

% gaussian filtering of fft
% filter
f=fspecial('gaussian',[nl nc],WsizeLF);
f=f/max(f(:));                                                      % keep value in the original range

%white image in the frequency domain
sfimf=sfim.*f;

% shift inverse
isfimf=ifftshift(sfimf);

% inverse fft
imft=ifft2(isfimf);
% recover real part of the signal
white=real(imft);
    
% convert white image and avoid too small values
white=uint16(white);
white(white<Vmin) = Vmin;

%% save
cd(sfolder);
imwrite(white,whitefilename,'tif','compression','none');

figure
surf(white,'FaceColor','interp',   'EdgeColor','none',  'FaceLighting','gouraud');
saveas(gcf,[strrep(whitefilename,'.tif,','') 'surf.png']);

figure
imshow(white,[]);
colormap('parula');
saveas(gcf,[strrep(whitefilename,'.tif,','') '.png']);


  
  %% matlab function tracking
  
  fid=fopen(strcat(whitefilename,'.track.txt'),'w');
  
  if fid==0
      errordlg('enable to open track file');
  end;
  
  fprintf(fid,'\r\n%s\t',datestr(now,0));
  fprintf(fid,'TELEMOS: Generate reference white image from experiment using fft transform\r\n');
  fprintf(fid,'__________________________________________________________________________\r\n');
  
  fprintf(fid,'\r\nInitial raw image IM %s found in folder: %s\r\n',rawfilename,rfolder);
  
  fprintf(fid,'\r\n \r\n');
  fprintf(fid,' camera Black images BKIM : %s/%s\r\n',rawfilename,zfolder);
  
  fprintf(fid,'\r\nProcessing of initial raw image :\r\n');
  fprintf(fid,'\t- subtraction of camera Black image: IMC=IM-BKIM \r\n');

  fprintf(fid,'\t- Top hat to remove spike => IMT :\r\n\t\t- size %dX%d to remove spikes',sizeTopHat, sizeTopHat);
  fprintf(fid,'\r\n\t\t- tophat threshold => IMS :  %d\r\n',seuilTopHat);
  
 
  
  fprintf(fid,'\r\n \r\n');
  fprintf(fid,'Processing to generate white Telemos image\r\n');
  fprintf(fid,'\t- gaussian filtering of fourier transform image: size %d\r\n',WsizeLF);
  fprintf(fid,'\t- value added to to avoid 0 in white image %d\r\n',Vmin);


  fprintf(fid,'\r\n \r\n');
  fprintf(fid,' save White image as %s in folder %s\r\n',whitefilename,sfolder);
  
  
  
  % save of function used
  fprintf(fid,'__________________________________________________________________________\r\n');
  info=which (mfilename);
  os=computer;        % return the type of computer used : windows, mac...
  switch os(1)
      case 'P'                        % for windows
          ind=strfind(info,'\');
      case 'M'                        % for Mac
          ind=strfind(info,'/');
      otherwise
          ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
  end;
  
  repprog=info(1:(ind(length(ind))-1));
  fprintf(fid,'function name: %s ',mfilename);
  res=dir(info);
  fprintf(fid,'on %s \r\n',res.date);
  fprintf(fid,'function folder: %s \r\n',repprog);
  %fprintf(fid,'__________________________________________________________________________\r\n');
  
  fclose(fid);
  
  %% end
  
  cd (orig)
    