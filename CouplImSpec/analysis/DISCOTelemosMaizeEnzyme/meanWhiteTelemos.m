  function meanWhiteTelemos
  

 %% description
    % average white images of telemos to assess a synthesis of all white
    % images
    
%% input

    %  nothing : more a script than a function...
    
%% output
    
    %   nothing whiteTelemos is saved on disk
    
    %% principe
    %whitetelemos images are recorded in separated files acording to original images.
    % 
    % the present function assess the sum of all images and then smooth the resulting images to obtain a a final "white "image
    %
    % the images is saved as uint16 after multiplying by a constant to obtain at least 1000 values to describe the shape of the white signal.
    %
    % the white images is saved on disk

%% use
    % meanWhiteTelemos
    
%% Comments
    %   for experiment SOLEIL 2014 proposal 20140308 
    
    
%% Author
    % MF Devaux
    % INRA BIA
    % PVPP
    
%% date
    % 6 f�vrier 2017

%% context variables 
orig=pwd;           % returns the current directory

% size of average filterig
 sizeAverage=25;                                 
  
% constant value to obtain at least 1000 values to save white image in uint16
cte=1000;


%% start

if nargin >0
    error('use: meanWhiteTelemos');
end


%% input
[rfolder]=uigetdir(pwd,'Folder where individual white images have been saved');
cd(rfolder)
ld=dir;
ld=ld([ld.isdir]');
ld=ld(3:end);
nbd=length(ld);

cd (rfolder)
if ~exist('meanwhiteTelemos','dir')
    mkdir('meanwhiteTelemos');
end
cd('meanwhiteTelemos');
sfolder=pwd;


%% treatement
%read and sum whitetelemos images
si=0;
for i=1:nbd
    cd(rfolder)
    if ~strcmp('meanwhiteTelemos',ld(i).name)
        cd(ld(i).name);
        if exist('whiteTelemos.tif','file');
            si=si+1;
            im=imread('whiteTelemos.tif');
            if si==1
                imt=double(im)/double(max(im(:)));
            else
                imt=imt+double(im)/double(max(im(:)));
            end;
            indice(si)=i; %#ok<NASGU>
        end;
    end;
end;

% average filtering
fsmooth=fspecial('average',[sizeAverage sizeAverage]);
    
imf=imfilter(imt,fsmooth,'symmetric');


% multiply by a constant and convert to uint16
imf=uint16(imf*cte);
    

%% save
cd(sfolder);
imwrite(imf,'meanWhiteTelemos.tif','compression','none');

figure
surf(imf,'FaceColor','interp',   'EdgeColor','none',  'FaceLighting','gouraud');
saveas(gcf,'meanWhiteTelemos.surf.png');

figure
imshow(imf,[]);
colormap('parula');
saveas(gcf,'meanWhiteTelemos.png');


  
  %% matlab function tracking
  
  fid=fopen('meanWhiteTelemos.track.txt','w');
  
  if fid==0
      errordlg('enable to open track file');
  end;
  
  fprintf(fid,'\r\n%s\t',datestr(now,0));
  fprintf(fid,'TELEMOS: Generate reference mean white image from a serie of white images\r\n');
  fprintf(fid,'__________________________________________________________________________\r\n');
  
  fprintf(fid,'\r\nInitial white telemos images %s found in subfolders of folder: %s\r\n','whiteTelemos.tif',rfolder);
  
  fprintf(fid,'\r\nSubfolder:  :\r\n');
 for i=1:si
      fprintf(fid,'\t - %s\r\n',ld(indice(i)).name);
  end
  
  
  fprintf(fid,'\r\nProcessing  :\r\n');
  fprintf(fid,'\t- sum of inidvidual whiteTelemos images \r\n');

  fprintf(fid,'\t- Average filtering of size %dX%d to remove spikes',sizeAverage);
  fprintf(fid,'\r\n\t- multiply by  %d to convert in uint16\r\n',cte);
  
 
  
   fprintf(fid,'\r\n \r\n');
  fprintf(fid,' save mean White image as %s in folder %s\r\n','meanWhiteTelemos.tif',sfolder);
  
  
  
  % save of function used
  fprintf(fid,'__________________________________________________________________________\r\n');
  info=which (mfilename);
  os=computer;        % return the type of computer used : windows, mac...
  switch os(1)
      case 'P'                        % for windows
          ind=strfind(info,'\');
      case 'M'                        % for Mac
          ind=strfind(info,'/');
      otherwise
          ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
  end;
  
  repprog=info(1:(ind(length(ind))-1));
  fprintf(fid,'function name: %s ',mfilename);
  res=dir(info);
  fprintf(fid,'on %s \r\n',res.date);
  fprintf(fid,'function folder: %s \r\n',repprog);
  %fprintf(fid,'__________________________________________________________________________\r\n');
  
  fclose(fid);
  
  %% end
  
  cd (orig)
    