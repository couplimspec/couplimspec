  function makeImBlackCamera40
  

 %% description
    % create the reference image of black signal comming from teh camera
    % for experiment SOLEIL 2014 proposal 20140308 
    
%% input

    %  nothing : more a script than a function...
    
%% output
    
    %   nothing image back camera is saved on disk

%% principe
    % Images are coming from experiment SOLEIL2014\DISCO\DISCO2\150206Acquisitions\2f13mb8ebl10_1\Pos0
    % channel 327-353 nm
    
    % images are filtered : median filtering of size 15x15 : remove spikes
    %                                    average filtering of large size
    %              
    % images are saved on disk

%% use
    % makeImBlackCamera
    
%% Comments
    %   for experiment SOLEIL 2014 proposal 20140308 
    
    
%% Author
    % MF Devaux
    % INRA BIA
    % PVPP
    
%% date
    % 21 mars 2017

%% context variables 
orig=pwd;           % returns the current directory

rfolder = 'D:\mfdevaux\projets\SOLEIL2014\DISCO\DISCO2\imageZEROCamera40\imDep';
sfolder='D:\mfdevaux\projets\SOLEIL2014\DISCO\DISCO2\imageZEROCamera40';

sizeMedFilt=15;     % size of median filtering
% size of average filtering
sizeAverageFilt=25;

%% start

if nargin >0
    error('use: makeImBlackCamera');
end


%% input
cd(rfolder)
list=dir('*.tif');

nb=length(list);

%% treatement
for i=1:nb
    
    fprintf('%d: %s\r\n',i,list(i).name)
    im=imread(list(i).name);
    
    % median filtering
    imf=medfilt2(im,[sizeMedFilt sizeMedFilt],'symmetric');
    
    % average filtering
    f=fspecial('average',sizeAverageFilt);
    imf=imfilter(imf,f,'symmetric');
    
    % save
    cd(sfolder);
    imwrite(imf,list(i).name);
    
    cd(rfolder);
end;


%% matlab function tracking  
cd(sfolder)
[~,nomtrack,~]=fileparts(sfolder);
fid=fopen(strcat(nomtrack,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS: Generate reference black camera image \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nRaw files found in folder: %s\r\n',rfolder);
 
fprintf(fid,'\r\n Images are filtered: median filtering of size %dX%d to remove spikes',sizeMedFilt, sizeMedFilt);
fprintf(fid,'\r\n                                  : average filtering of size %dX%d  for smoothing\r\n',sizeAverageFilt,sizeAverageFilt);

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Black camera images saved in folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    