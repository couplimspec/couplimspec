function ApplyMaizeEnzDegIntensity


%% description
% analysis of maize stem images during degradation acquired at synchrotron SOLEIL at the DISCO beamline using
% micromanager : extract intensity according to time and z
% proposal Marie-Fran�oise Devaux 20150969

%% input

%  nothing
% interactive function

%% output

%  nothing
% results are saved on disk

%% principe
%  the root folder contains several pos folder
% in each pos folder : two images channels sere recorded corresponding
%           - to the "enzyme autofluorescence" after excitation at 275 nm :
%               emission band pass filter 327-353 nm
%           - to the autofluorescence of phenolic compounds after
%                   excitation at 275 nm
%               emission band pass filer 420-480 nm
%
% at input : give the root folderand read the micromanager info
% for each pos folder :
%   read all tiff files in a multidimensionnal image im(x,y,z,t)
%   (NB : some image files were corrupted => test at file reading and
%   put 0 when no reading is possible)
%           preprocess images to remove spike : tophat removing =
%           opening of size 5 pixels
%           compute the sum image of the enzyme channel over z and time
%           interactively draw a line profile and a polygon region
%           recover the intensity = f(z,t) for
%                  whole image
%                  disk 3/4 of the image
%                  disk 1/2 of the image
%                  line profile
%                  polygon region
%           draw intensity evolution over time and z for each position
%           nb: here time and z= number of image
%           save the f(z,t) data in a text file

%



%% use
% MaizeEnzDegIntensity

%% Comments
%  micro manager save  images in a structured file folder
% architecture :
% root folder : name given by the user
%   subfolders roi(n)_tile1 : n folders for each selected roi
%           or pos(n) : n folders for each selected position
%   display_and_comments.txt : file describing the channels acquired in
%
%   in each subfolder roi(n)_tile1 or Pos(n) :
%       images files with name img_00000000(n)_CHANNEL_00(p).tif
%               n = number of time
%               p = z focal plane
%               CHANNEL = DM300_327-353 or  = DM300_420-480 name of the channel
%       metadata.txt : files describing all metadata associated with the
%       acquisition : x, y, z position, camera settings .... etc.

% function associated to PMEGelIntensity

%% Author
% MF Devaux
% INRA Nantes
% BIA-PVPP

%% date
% 1 septembre 2016
% 20 septembre 2016 fopr the choice of the channel to choose regions

%% context variables
orig=pwd;           % returns the current directory

%% start
close all

if nargin >1
    error('MaizeEnzDegIntensity ');
end


%% input
[nameFolder,rootFolder,listDir,~]=metadataTelemos;
nbPos=size(listDir,1);

% read general metadata
cd(rootFolder);
md=readDIV(strcat(nameFolder,'.pos.metadata'));
nbTime=unique(md.d(:,1));
nbZ=unique(md.d(:,3));
nl=md.d(1,6);
nc=md.d(1,7);

if nbPos>1
    [rfolder]=uigetdir(pwd,' Folder to process','Pos*');
    [~,rfolderPos]=fileparts(rfolder);
else
    rfolderPos='Pos0';
    cd(rfolderPos)
    rfolder=pwd;
end

[~,rfolderMask]=uigetfile({'*.tif'},'first mask','*.mask.tif');
cd(rfolderMask)
listMask=dir('*.mask.tif');
nbRegion=length(listMask);

% save result in folder
cd ..
cd ..
sfolder=uigetdir(pwd,'save file in folder');
cd(sfolder)
if ~exist(nameFolder,'dir')
    mkdir(nameFolder);
end;
cd(nameFolder)
if ~exist(rfolderPos,'dir')
    mkdir(rfolderPos)
end
cd(rfolderPos)
sfolder=pwd;


%% treatment

close all
cd(rootFolder);

mdp=readDIV(strcat(nameFolder,'.',rfolderPos,'.metadata'));

% check X and Y pos
x=unique(mdp.d(:,1));
if length(x)~=1
    error('check xposition')
end
y=unique(mdp.d(:,2));
if length(y)~=1
    error('check yposition')
end
z=unique(mdp.d(:,3));
if length(z)~=nbZ
    error('check zposition')
end


% read enzymes images files in the pos folder
cd(rfolder);
% list of file of the enzyme channel
enzChannel='DM300_327-353';
list=dir(['*' enzChannel  '*.tif']);
im=uint16(zeros(nl,nc,nbZ*nbTime));
imLue=1;
for j=1:length(list)
    try
        tim=imread(list(j).name,'tif');
    catch ME
        switch ME.identifier
            case 'MATLAB:imagesci:tiffmexutils:libtiffError'
                tim=uint16(zeros(nl,nc));
                imLue=0;
        end
    end;
    % pre-process image : median filtering of size 5
    if imLue
        tim=medfilt2(tim,[5 5 ],'symmetric');
    end;
    im(:,:,j)=tim;
end;

% read cell wall images files in the pos folder
% list of file of the phnelic compound channel
cwChannel='DM300_420-480';
list=dir(['*' cwChannel  '*.tif']);
cwim=uint16(zeros(nl,nc,nbZ*nbTime));
imLue=1;
for j=1:length(list)
    try
        tim=imread(list(j).name,'tif');
    catch ME
        switch ME.identifier
            case 'MATLAB:imagesci:tiffmexutils:libtiffError'
                tim=uint16(zeros(nl,nc));
                imLue=0;
        end
    end;
    % pre-process image : median filtering of size 5
    if imLue
        tim=medfilt2(tim,[5 5 ],'symmetric');
    end;
    cwim(:,:,j)=tim;
end;

% build image according to z and time
sim=reshape(im,size(im,1),size(im,2),nbZ,nbTime);
scwim=reshape(cwim,size(im,1),size(im,2),nbZ,nbTime);

affim=sum(im,3);
figure(1)
imshow(affim,[]);
hold on

% image size = int3/4
h34=imellipse(gca, [nl/8 nc/8 nl*3/4 nc*3/4]);
roi34=createMask(h34);
% and 1/2 of the image size int1/2
h12=imellipse(gca, [nl/4 nc/4 nl/2 nc/2]);
roi12=createMask(h12);

hold off
% save figure
cd(sfolder);
saveas(gcf,strcat(nameFolder,'.',rfolderPos,'.globalRegions.jpg'),'jpg');

% extract intensity values enzyme image and cell wall image
intTot=zeros(nbZ,nbTime);
int34=zeros(nbZ,nbTime);
int12=zeros(nbZ,nbTime);
cwintTot=zeros(nbZ,nbTime);
cwint34=zeros(nbZ,nbTime);
cwint12=zeros(nbZ,nbTime);

for ii=1:nbZ
    for ij=1:nbTime
        % enzyme
        tmp=sim(:,:,ii,ij);
        intTot(ii,ij)=mean2(tmp);
        int34(ii,ij)=mean2(tmp(roi34));
        int12(ii,ij)=mean2(tmp(roi12));
        % cell wall
        tmp=scwim(:,:,ii,ij);
        cwintTot(ii,ij)=mean2(tmp);
        cwint34(ii,ij)=mean2(tmp(roi34));
        cwint12(ii,ij)=mean2(tmp(roi12));
    end
end

% plot intensity according to time and z (NB: here time and z = number of
% image)
figure(2)
plot(reshape(intTot,1,length(intTot(:))),'r','linewidth',2);
set(gca,'fontsize',16);
hold on
plot(reshape(int34,1,length(intTot(:))),'g','linewidth',2);
plot(reshape(int12,1,length(intTot(:))),'b','linewidth',2);
xlabel('z and time','fontsize',18);
ylabel('average intensity','fontsize',18);
hl=legend('Enz: whole image','Enz: 3/4 dik','Enz: 1/2 disk','location','northeastoutside');
set(hl,'fontsize',16');
title(['enzyme images: ',rfolderPos],'fontsize',18);
hold off

% plot intensity according to time and z (NB: here time and z = number of
% image)
figure(3)
plot(reshape(cwintTot,1,length(intTot(:))),'-r','linewidth',2);
set(gca,'fontsize',16);
hold on
plot(reshape(cwint34,1,length(intTot(:))),'-g','linewidth',2);
plot(reshape(cwint12,1,length(intTot(:))),'-b','linewidth',2);
xlabel('z and time','fontsize',18);
ylabel('average intensity','fontsize',18);
hl=legend('cell wall: whole image','cell wall: 3/4 dik','cell wall: 1/2 disk','location','northeastoutside');
set(hl,'fontsize',16');
title(['cell wall images: ',rfolderPos],'fontsize',18);
hold off


for i=1:nbRegion
    
    figure(1)
    imshow(affim,[]);
    hold on
    cd(rfolderMask)
    mask=imread(listMask(i).name);
    contour=imsubtract(imdilate(mask,strel('square',5)),mask);
    [crc,crl]=find(contour);
    plot(crl,crc,'.r')
    
    
    nom=listMask(i).name;
    ii=strfind(nom,'.');
    code=nom((ii(2)+1):(ii(3)-1));
    
    codeID{i}=code;
    
    % extract intensity values
    intRegion=zeros(nbZ,nbTime);
    cwintRegion=zeros(nbZ,nbTime);
    for ii=1:nbZ
        for ij=1:nbTime
            tmp=sim(:,:,ii,ij);
            cwtmp=scwim(:,:,ii,ij);
            
            intRegion(ii,ij)=mean2(tmp(mask));
            cwintRegion(ii,ij)=mean2(cwtmp(mask));
            
        end
    end
    
    % plot intensity according to time and z (NB: here time and z = number of
    % image)
    figure(3+i)
    subplot(1,2,1)
    plot(reshape(intRegion,1,length(intRegion(:))),'r','linewidth',2);
    set(gca,'fontsize',16);
    xlabel('z and time','fontsize',18);
    ylabel('average intensity','fontsize',18);
    title(strcat(char(code),': Enzyme image'),'fontsize',18);
    subplot(1,2,2)
    plot(reshape(cwintRegion,1,length(cwintRegion(:))),'g','linewidth',2);
    set(gca,'fontsize',16);
    xlabel('z and time','fontsize',18);
    ylabel('average intensity','fontsize',18);
    title(strcat(char(code),': Cell wall image'),'fontsize',18);
    
    %% save
    cd(sfolder);
    figure(1)
    title([rfolderPos ' - ' code],'fontsize',18)
    imwrite(mask,strcat(nameFolder,'.',rfolderPos,'.',char(code),'.mask.tif'),'tif','compression','none');
    hold off
    
    saveas(gcf,strcat(nameFolder,'.',rfolderPos,'.',char(code),'.profim.jpg'),'jpg');
    
    % save intensity figure
    figure(3+i)
    saveas(gcf,strcat(nameFolder,'.',rfolderPos,'.',char(code),'.intensity.png'),'png');
    
    % save intensity values for each region selected with code
    d.d=[intRegion cwintRegion ];
    if nbZ<10
        d.i=strcat('z',num2str((1:nbZ)'));
    else
        d.i=strcat('z0',num2str((1:9)'));
        d.i=char(d.i,strcat('z',num2str((10:nbZ)')));
    end
    if nbTime<10
        sTime=num2str((1:nbTime)');
    else
        sTime=strcat('0',num2str((1:9)'));
        sTime=char(sTime,num2str((10:nbTime)'));
    end;
    
    % save intensity values : for each z : time values for enzyme and
    % cell wall images as concatenated vectors
    d.v=char(strcat('Enz',sTime),strcat('cw',sTime));
    writeDIV(d,strcat(nameFolder,'.',rfolderPos,'.',char(code),'.intensity.txt'));
    
    
end;




%% save
cd(sfolder);

% save global intensity figure
figure(2)
saveas(gcf,strcat(nameFolder,'.',rfolderPos,'.intensity.png'),'png');

d.d=[ intTot int34 int12 cwintTot cwint34 cwint12 ];

% save intensity values : for each z : time values for whole image, 3/4
% disk, 1/2 dik and line  : concatenated vectors
d.v=char(strcat('EnztTot',sTime),strcat('Enzt34',sTime),strcat('Enzt12',sTime),strcat('cwtTot',sTime),strcat('cwt34',sTime),strcat('cwt12',sTime));
writeDIV(d,strcat(nameFolder,'.',rfolderPos,'.intensity.txt'));

% save xpos, ypos,zpos  and time for each z
nmd.i=d.i;
nmd.v=char('xPos','yPos','zpos',strcat('t',sTime));
nmd.d(:,1)=repmat(x,nbZ,1);
nmd.d(:,2)=repmat(y,nbZ,1);
nmd.d(:,3)=z;
% time values in minute and second from time of saving the first image
% of the first position
% time values in minute and second from time of saving the first image
% of the first position
ttime=mdp.d(:,end);
ttime=reshape(ttime,nbZ,2,nbTime);
ttime=squeeze(ttime(:,1,:));


nmd.d(:,4:(nbTime+3))=ttime;

writeDIV(nmd,strcat(nameFolder,'.',rfolderPos,'.xyzt.txt'));


% save code region
codeID=char(codeID);
writeStringVec(codeID,strcat(nameFolder,'.',rfolderPos,'.code.txt'));




%% function tracking
tsname=strcat(nameFolder,'.track.txt');
fic=fopen(tsname,'w');
if fic==0
    error('unable to open %s for writing',tsname);
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'-\t extract intensity values from maize stem enzymatic degradation telemos DISCO images\r\n');
fprintf(fic,'__________________________________________________________________________\r\n');

fprintf(fic,'\r\n\r\n');

fprintf(fic,'root Folder : %s\r\n',rootFolder);
fprintf(fic,'Experiment name %s : \r\n',nameFolder);
fprintf(fic,'   %s\r\n',rfolderPos);

fprintf(fic,'makss foudn in folder  : %s\r\n',rfolderMask);

% region
fprintf(fic,' Code of region extracted for all positions:\r\n');

for i=1:size(codeID,1)
    fprintf(fic,'        - %s\r\n',codeID(i,:));
end
fprintf(fic,'\r\n\r\n');



fprintf(fic,'RESULTS\r\n\r\n');
fprintf(fic,'- save folder: \r\n\t- %s\r\n\r\n',sfolder);
fprintf(fic,'- Global regions intensity values : %s.Pos(n).intensity.txt \r\n',nameFolder);
fprintf(fic,'- Global regions intensity plot: %s.Pos(n).intensity.png\r\n\r\n',nameFolder);
fprintf(fic,'- localisation of global regions in the image: %s.Pos(n).globalRegions.jpg\r\n',nameFolder);
fprintf(fic,'- codes of selected region: %s.Pos(n).code.txt\r\n\r\n',nameFolder);
fprintf(fic,'- Line and polygons regions intensity values: %s.Pos(n).<code>.intensity.txt\r\n\r\n',nameFolder);
fprintf(fic,'- Line and polygons regions intensity plot: %s.Pos(n).<code>.intensity.png\r\n\r\n',nameFolder);
fprintf(fic,'- line and column coordinates of pixels for intensity profile : %s.Pos(n).coord.txt \r\n',nameFolder);
fprintf(fic,'- mask of polygon region for intensity profile : %s.Pos(n).<code>.mask.tif \r\n',nameFolder);
fprintf(fic,'- localisation of lines or polygons in the image: %s.Pos(n).<code>.profim.jpg\r\n',nameFolder);
fprintf(fic,'- localisation of lines or polygons in the image: %s.Pos(n).allLocalRegions.jpg\r\n',nameFolder);
fprintf(fic,'- associated x, y, z, time metadata : %s.Pos(n).xyzt.txt \r\n',nameFolder);


% save of function used
fprintf(fic,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fic,'function name: %s\r\n ',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);

fclose(fic);

%% end

cd (orig)
