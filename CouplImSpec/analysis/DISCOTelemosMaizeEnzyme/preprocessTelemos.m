function preprocessTelemos


%% description
% preprocessing of telemos images using black camera images and white
% estimated images

%% input

%  nothing : ..

%% output

%   nothing 

%% principe
%the root folder contains several pos folder
% in each pos folder : two images channels sere recorded corresponding
%           - to the "enzyme autofluorescence" after excitation at 275 nm :
%               emission band pass filter 327-353 nm
%           - to the autofluorescence of phenolic compounds after
%                   excitation at 275 nm
%               emission band pass filer 420-480 nm
%
%
% HYPOTHESIS:
%  the camera without any signal does not provie de 0 response
% the response is not homogenneous within the field of view
%M
% the shape of the synchrotron  illumination is not homogeneous within the whole
% filed of view.
%
% the deformation is estimated as : 
%
% camera : additive             => black signal
% synchrotron illumination : multiplicative         => white signal
%
% the black image and the white image have to be estimated or measured.
%
% pre-processing : 
%       - subtraction  of balck signal
%      -  spike elimination using top hat transformation
%      -  median filtering of size 3
%      -  division be the white signal

%% use
% preprocessTelemos

%% Comments
%   developed for experiment SOLEIL 2014 proposal 20140308


%% Author
% MF Devaux
% INRA BIA
% PVPP

%% date
%20 janvier 2017

%% context variables
orig=pwd;           % returns the current directory

% raw image
%sizeMedFilt=3;     % size of median filtering
%size of gaussain filtering
sigmaGaussFilt=0.5;
sizeGaussFilt=3;

% size of TopHat
 sizeTopHat=5;                                 
 seuilTopHat=200;

%% start

if nargin >0
    error('use: preprocessTelemos');
end


%% input
[nameFolder,rootFolder,listDir,~]=metadataTelemos;
nbPos=size(listDir,1);
cd(rootFolder)

if nbPos>1
    [rfolder]=uigetdir(' Folder to process','Pos*');
    [~,rfolderPos]=fileparts(rfolder);
else
    rfolderPos='Pos0';
    cd(rfolderPos)
    rfolder=pwd;
end

cd(rootFolder)
channels=readStringVec(strcat(nameFolder,'.listChannels.txt'));
nbc=size(channels,1);

cd ..
cd ..
[zfolder]=uigetdir(pwd,'Folder where camera black images have been saved');
cd(zfolder);
cd ..

[wfname,wfolder]=uigetfile({'*.tif'},'White images ','whiteTelemos.tif');
cd(wfolder)
white=imread(wfname);
white=double(white)/max(double(white(:)));

cd (rootFolder)
cd ..
cd ..
[sfolder]=uigetdir('.','save corrected images in folder');
cd(sfolder)
if ~exist(nameFolder,'dir')
    mkdir(nameFolder)
end
cd(nameFolder)
srootFolder=pwd;
if ~exist(rfolderPos,'dir')
    mkdir(rfolderPos)
end
cd(rfolderPos)
sfolder=pwd;
cd (rootFolder)
if exist('display_and_comments.txt','file')
    copyfile('display_and_comments.txt',srootFolder);
else
    warning('no display_and_comments.txt file in folder %s',rootFolder);
end;



%% treatement

% read camera black image
disp('read camera black image...')
cd(zfolder)
listk=dir('*.tif');
nbk=length(listk);
for i=1:nbk
    fprintf('%d ',i);
    bkim(:,:,i)=imread(listk(i).name);
end
fprintf('\n');

% process files for each channels
cd(rfolder);
for i=1:nbc
    disp('--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------')
    disp('read image...')
    disp(channels(i,:))
    list=dir(['*' channels(i,:) '*.tif']);
    nb=length(list);
    
  
    for j=1:nb
        fprintf('%d ',j);
        im=imread(list(j).name);
        
        % filter image
        % remove camera black image 
        imc=im-bkim(:,:,j);
        
        % top hat to remove spike
        imt=imtophat(imc,strel('square',sizeTopHat));
        ims=uint16(zeros(size(imt)));
        ims(imt>seuilTopHat)=imt(imt>seuilTopHat);
        
        % strating image
        imc=imc-ims;
        
        % gaussian filtering
        imc=imgaussfilt(imc,sigmaGaussFilt,'FilterSize',[sizeGaussFilt sizeGaussFilt],'padding','symmetric');
        
        
        % divide by white image
        imc=uint16(double(imc)./white);
        
        
        % save
        cd(sfolder);
        imwrite(imc,strcat('c',list(j).name),'tif','compression','none');
        
        cd(rfolder)
        
    end
    fprintf('\n');
end;

% save metadat.txt in the corrected image folder
cd (rootFolder)
cd(rfolderPos)
if exist('metadata.txt','file')
    copyfile('metadata.txt',[srootFolder,'\',rfolderPos]);
else
    warning('no metadata.txt file in folder %s',[rootFolder,'\',rfolderPos]);
end;

%% matlab function tracking
cd(sfolder)
[~,nomtrack,~]=fileparts(sfolder);
fid=fopen(strcat(nomtrack,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'TELEMOS: Correction of telemos image for background and intensityt\r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nInitial raw image IMfound in folder: %s\r\n',rfolder);

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Camera Black images BKIM found in folder: %s\r\n',zfolder);
fprintf(fid,'White telemos image WHIM : %s found in folder %s\r\n',wfname,wfolder);
fprintf(fid,'\t- white telemos image is divided by the max: values are ranging from 0 to 1.');

fprintf(fid,'\r\n \r\n');
fprintf(fid,'Processing to generate Telemos image corrected for intensities\r\n');
fprintf(fid,'\t- subtraction of camera Black image: IMC=IM-BKIM \r\n');

fprintf(fid,'\t- Top hat to remove spike => IMT :\r\n\t\t- size %dX%d to remove spikes',sizeTopHat, sizeTopHat);
fprintf(fid,'\r\n\t\t- tophat threshold => IMS :  %d\r\n',seuilTopHat);
fprintf(fid,'\t- subtraction of tophat image: IMC=IMC-IMS \r\n');
  
fprintf(fid,'\r\n\t- gaussian filtering of size %dX%d  - sigma %6.2f = smoothing to remove noise => IMC\r\n',sizeGaussFilt,sizeGaussFilt,sigmaGaussFilt);
 

fprintf(fid,'\t- division by white image : IMC=IMC/WHIM\r\n');



fprintf(fid,'\r\n \r\n');
fprintf(fid,'Save preprocessed images in uint16 in folder %s\r\n',sfolder);



% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
