  function MaizeEnzDegIntensitySummary
  

 %% description
    % anlysis of maize stem images during degradation acquired at synchrotron SOLEIL at the DISCO beamline using
    % micromanager : summarize intensity values extracted by
    % MaizeEnzDegIntensity function
    % proposal Marie-Fran�oise Devaux 20150969
    
    
%% input

    %  nothing 
    % interactive function
    
%% output
    
    %  nothing
    % results are saved on disk

%% principe
    %  the root folder contains several pos folder
    % in each pos folder : only one image channel was recorded corresponing
    % to the "enzyme autofluorescence" after excitation at 275 nm :
    % emission band pass filter 327-353 nm
    %
    % at input : give the root folderand read the micromanager info
    % for each pos folder : 
    %   read all tiff files in a multidilensionnal image im(x,y,z,t)
    %   (NB : some image files were corrupted => test at file reading and
    %   put 0 when no reading is possible)
    %           preprocess images to remove spike : tophat removing =
    %           opening of size 5 pixels
    %           compute the sum image over z and time
    %           interactively draw a line profile
    %           recover the intensity = f(z,t) for
     %                  whole image
     %                  disk 3/4 of the image
     %                  disk 1/2 of the image
     %                  line profile
    %           draw intensity evolution over time and z for each position
    %           save the f(z,t) data in a text file
     
     % 
    
    

%% use
    % MaizeEnzDegIntensitySummary
    
%% Comments
    %  micro manager save  images in a structured file folder
    % architecture :
    % root folder : name given by the user
    %   subfolders roi(n)_tile1 : n folders for each selected roi
    %           or pos(n) : n folders for each selected position
    %   display_and_comments.txt : file describing the channels acquired in
    %   
    %   in each subfolder roi(n)_tile1 or Pos(n) : 
    %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
    %               n = number of time
    %               p = z focal plane
    %               DM300_327-353 = name of the channel
    %       metadata.txt : files describing all metadata associated with the
    %       acquisition : x, y, z position, camera settings .... etc.
    
    
%% Author
    % MF Devaux
    % INRA Nantes
    % BIA-PVPP
    
%% date
    % 2/09/2016

%% context variables 
orig=pwd;           % returns the current directory

%% start
close all

if nargin >1
    error('MaizeEnzDegIntensitySummary');
end


%% input
[rFolder]=uigetdir(pwd,'Folder where intensity data have been saved');

cd(rFolder);
listxyzt=dir('*.xyzt.txt');


if isempty(listxyzt)
    error('no xyzt files in folder %s',rFolder);
end;

nb=length(listxyzt);

if ~exist('summary','dir')
        mkdir('summary')
end;


%% treatment
% for each pos intensity file
for i=1:nb
    dt=readDIV(listxyzt(i).name);
     
    di=readDIV(strrep(listxyzt(i).name,'xyzt.txt','intensity.txt'));
    
    if i==1
        % time values
        T=dt.d(1,4:end);        % in second
        T=round(T/60);          % in minute
        nbT=length(T);
        
        % generic name
        sname=strrep(listxyzt(i).name,'.xyzt.txt','');
    end;
    
    % extract intensity for whole images enzyme
    itote=di.d(:,1:nbT);
    nbz(i)=size(itote,1);
    stote=sum(itote)/nbz(i);
    % for 3/4 dik enzyme
    i34e=di.d(:,(nbT+1):2*nbT);
    s34e=sum(i34e)/nbz(i);
    % for 1/2 disk enzyme
    i12e=di.d(:,(2*nbT+1):3*nbT);
    s12e=sum(i12e)/nbz(i);
    
    ginte.d=[stote;s34e ;s12e];
    
    
    % extract intensity for whole images cell wall
    itotp=di.d(:,(3*nbT+1):4*nbT);
    stotp=sum(itotp)/nbz(i);
     % for 3/4 dik cell wall
    i34p=di.d(:,(4*nbT+1):5*nbT);
    s34p=sum(i34p)/nbz(i);
% for 1/2 diskcell wall
    i12p=di.d(:,(5*nbT+1):6*nbT);
    s12p=sum(i12p)/nbz(i);

    gintp.d=[stotp;s34p; s12p];
   
    
       
    % for all measures
    list=dir(strrep(listxyzt(i).name,'xyzt.txt','*.intensity.txt'));
    nbl=length(list);
    for j=1:nbl
        di=readDIV(list(j).name);
        
        code=strrep(list(j).name,sname,'');
        code=strrep(code,'.intensity.txt','');
        code=strrep(code,'.','');
        
        % extract intensity for images enzyme
        irege=di.d(:,1:nbT);
        srege.d(j,:)=sum(irege)/nbz(i);
        
              
        % extract intensity for  images cell wall
        iregp=di.d(:,(nbT+1):2*nbT);
        sregp.d(j,:)=sum(iregp)/nbz(i);
        
        if j==1
            srege.i=code;
            sregp.i=code;
        else
            srege.i=char(srege.i,code);
            sregp.i=char(sregp.i,code);
        end;
    end;
            
        
end

ginte.i=char('Itot','I34','I12');
gintp.t=ginte.i;

ginte.v=T';
gintp.v=ginte.v;

srege.v=T';
sregp.v=T';

map=colormap(colorcube(size(srege.i,1)+1));
map=map(1:(end-1),:);         % not white !

cd('summary')

figure
subplot(1,2,1)
plot(ginte.v,ginte.d);
xlabel('Time (min)','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(ginte.i)
title('global intensity: enzyme image')
subplot(1,2,2)
plot(gintp.v,gintp.d);
xlabel('Time (min)','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(ginte.i)
title('global intensity: cell wall image')
saveas(gcf,[sname '.globalInt.png'],'png');

figure
plot(srege.v,srege.d(1,:),'color',map(1,:));
hold on
for i=2:size(srege.i,1)
    plot(srege.v,srege.d(i,:),'color',map(i,:));
end
xlabel('Time (min)','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
hl=legend(srege.i);
set(hl,'location','northeastoutside');
title('local intensity: enzyme image')
saveas(gcf,[sname '.localIntEnz.png'],'png');

figure
plot(sregp.v,sregp.d(1,:),'color',map(1,:));
hold on
for i=2:size(srege.i,1)
    plot(sregp.v,sregp.d(i,:),'color',map(i,:));
end
xlabel('Time (min)','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
hl=legend(srege.i);
set(hl,'location','northeastoutside');
title('local intensity: cell wall image')
saveas(gcf,[sname '.localIntCW.png'],'png');

%% save
    
       
writeDIV(ginte,strcat(sname,'.globalIntEnz.txt'));
writeDIV(gintp,strcat(sname,'.globalIntCW.txt'));

writeDIV(srege,strcat(sname,'.localIntEnz.txt'));
writeDIV(sregp,strcat(sname,'.localIntCW.txt'));

    
 


    
%% function tracking
tsname=strcat(sname,'.summary.track.txt');
fic=fopen(tsname,'w');
if fic==0
    error('unable to open %s for writing',tsname);
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'___________________________________________________________________________\r\n');
fprintf(fic,'-\t summarise intensity values extracted from maize stem section during enzymatic degradation\r\\n');
fprintf(fic,'-\tDISCO synchrotron images : enzyme and cell wall visualisation\r\n');
fprintf(fic,'___________________________________________________________________________\r\n');

fprintf(fic,'\r\n\r\n');

fprintf(fic,'Folder: %s\r\n',pwd);
fprintf(fic,'\r\n\r\n');

fprintf(fic,' summarise and draw  intensity values according to Time for enzyme and cell wall images:\r\n');
fprintf(fic,'\r\n\r\n');
fprintf(fic,'        - global intensity measures:%s.globalIntEnz.txt and %s.globalIntCW.txt + png figures\r\n',sname,sname);
fprintf(fic,'       -  regions measures: %s.localIntEnz.txt and %s.localIntCW.txt\r\n',sname,sname);
fprintf(fic,'\r\n\r\n');



% save of function used
fprintf(fic,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fic,'function name: %s\r\n ',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fic);

%% end

cd (orig)
    