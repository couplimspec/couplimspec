  function PMEGelIntensitySummary
  

 %% description
    % anlysis of PME Gel images acquired at synchrotron SOLEIL at the DISCO beamline using
    % micromanager : summarize intensity values extracted by
    % PMEGelIntenisty.m fucntion
    % proposal Estelle Bonnin 20150365
    
%% input

    %  nothing 
    % interactive function
    
%% output
    
    %  nothing
    % results are saved on disk

%% principe
    %  the root folder contains several pos folder
    % in each pos folder : only one image channel was recorded corresponing
    % to the "enzyme autofluorescence" after excitation at 275 nm :
    % emission band pass filter 327-353 nm
    %
    % at input : give the root folderand read the micromanager info
    % for each pos folder : 
    %   read all tiff files in a multidilensionnal image im(x,y,z,t)
    %   (NB : some image files were corrupted => test at file reading and
    %   put 0 when no reading is possible)
    %           preprocess images to remove spike : tophat removing =
    %           opening of size 5 pixels
    %           compute the sum image over z and time
    %           interactively draw a line profile
    %           recover the intensity = f(z,t) for
     %                  whole image
     %                  disk 3/4 of the image
     %                  disk 1/2 of the image
     %                  line profile
    %           draw intensity evolution over time and z for each position
    %           save the f(z,t) data in a text file
     
     % 
    
    

%% use
    % PMEGelIntensitySummary
    
%% Comments
    %  micro manager save  images in a structured file folder
    % architecture :
    % root folder : name given by the user
    %   subfolders roi(n)_tile1 : n folders for each selected roi
    %           or pos(n) : n folders for each selected position
    %   display_and_comments.txt : file describing the channels acquired in
    %   
    %   in each subfolder roi(n)_tile1 or Pos(n) : 
    %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
    %               n = number of time
    %               p = z focal plane
    %               DM300_327-353 = name of the channel
    %       metadata.txt : files describing all metadata associated with the
    %       acquisition : x, y, z position, camera settings .... etc.
    
    
%% Author
    % MF Devaux
    % INRA Nantes
    % BIA-PVPP
    
%% date
    % 30/08/2016

%% context variables 
orig=pwd;           % returns the current directory

%% start
close all

if nargin >1
    error('PMEGelIntensitySummary');
end


%% input
[rFolder]=uigetdir(pwd,'Folder where intensity data have been saved');

cd(rFolder);
list=dir('*.intensity.txt');

if isempty(list)
    error('no intensity files in folder %s',rfolder);
end;

nb=length(list);


%% treatment
% for each pos intensity file
for i=1:nb
    di=readDIV(list(i).name);
    dt=readDIV(strrep(list(i).name,'intensity','xyzt'));
    
    if i==1
        % time values
        T=dt.d(1,4:end);        % in second
        T=round(T/60);          % in minute
        nbT=length(T);
        
        % generic name
        sname=strrep(list(i).name,'.intensity.txt','');
        sname=strrep(sname,'Pos0','');
    end;
    
    % extract intensity for whole images 
    itot=di.d(:,1:nbT);
    stot.d(:,i)=sum(itot);
    nbz(i)=size(itot,1);

    % for 3/4 dik
    i34=di.d(:,(nbT+1):2*nbT);
    s34.d(:,i)=sum(i34);

    % for 1/2 disk
    i12=di.d(:,(2*nbT+1):3*nbT);
    s12.d(:,i)=sum(i12);

    % for line
    iline=di.d(:,(3*nbT+1):4*nbT);
    sline.d(:,i)=sum(iline);

    
    % x value
    px(i)=unique(dt.d(:,1));
end

% x values are always <0
px=-px;
px=px-px(1);

stot.v=px';
s34.v=px';
s12.v=px';
sline.v=px';

% Time
stot.i=T';
s34.i=T';
s12.i=T';
sline.i=T';


figure
plot(stot.v,stot.d/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(stot.i))
title('whole image')
saveas(gcf,[sname '.tot.png'],'png');

figure
plot(s34.v,s34.d/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(s34.i))
title('3/4 disk')
saveas(gcf,[sname '.d34.png'],'png');

figure
plot(s12.v,s12.d/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(s12.i))
title('1/2 disk')
saveas(gcf,[sname '.d12.png'],'png');

figure
plot(sline.v,sline.d/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(sline.i))
title('line')
saveas(gcf,[sname '.line.png'],'png');

figure
plot(stot.v,stot.d(1:3:end,:)/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(stot.i(1:3:end)))
title('whole image')
saveas(gcf,[sname '.tot.ext.png'],'png');

figure
plot(s34.v,s34.d(1:3:end,:)/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(s34.i(1:3:end)))
title('3/4 disk')
saveas(gcf,[sname '.d34.ext.png'],'png');

figure
plot(s12.v,s12.d(1:3:end,:)/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(s12.i(1:3:end)))
title('1/2 disk')
saveas(gcf,[sname '.d12.ext.png'],'png');

figure
plot(sline.v,sline.d(1:3:end,:)/nbz(i));
xlabel('x position','fontsize',18)
ylabel('average intensity','fontsize',18)
set(gca,'fontsize',16)
legend(num2str(sline.i(1:3:end)))
title('line')
saveas(gcf,[sname '.line.ext.png'],'png');




    %% save
writeDIV(stot,strcat(sname,'.tot.txt'));

writeDIV(s34,strcat(sname,'.d34.txt'));
writeDIV(s12,strcat(sname,'.d12.txt'));
writeDIV(sline,strcat(sname,'.line.txt'));
    
 


    
%% function tracking
tsname=strcat(sname,'.summary.track.txt');
fic=fopen(tsname,'w');
if fic==0
    error('unable to open %s for writing',tsname);
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'___________________________________________________________________________\r\n');
fprintf(fic,'-\t summarise intensity values extracted from PME gel synchrotron images according to XPosition\r\n');
fprintf(fic,'___________________________________________________________________________\r\n');

fprintf(fic,'\r\n\r\n');

fprintf(fic,'Folder: %s\r\n',pwd);

fprintf(fic,' summarise and draw  intensity values according to XPosition for:\r\n');
fprintf(fic,'        - whole image:%s.tot.txt and png\r\n',sname);
fprintf(fic,'       -  disk 3/4 of the image%s.d34.txt and png\r\n',sname);
fprintf(fic,'       -  disk 1/2 of the image%s.d12.txt and png\r\n',sname);
fprintf(fic,'        - line profile%s.line.txt and png\r\n',sname);
fprintf(fic,'\r\n\r\n');



% save of function used
fprintf(fic,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fic,'function name: %s\r\n ',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fic);

%% end

cd (orig)
    