  function PMEGelIntensity
  

 %% description
    % anlysis of PME Gel images acquired at synchrotron SOLEIL at the DISCO beamline using
    % micromanager : extract intensity according to time and z
    % proposal Estelle Bonnin 20150365
    
%% input

    %  nothing 
    % interactive function
    
%% output
    
    %  nothing
    % results are saved on disk

%% principe
    %  the root folder contains several pos folder
    % in each pos folder : only one image channel was recorded corresponing
    % to the "enzyme autofluorescence" after excitation at 275 nm :
    % emission band pass filter 327-353 nm
    %
    % at input : give the root folderand read the micromanager info
    % for each pos folder : 
    %   read all tiff files in a multidilensionnal image im(x,y,z,t)
    %   (NB : some image files were corrupted => test at file reading and
    %   put 0 when no reading is possible)
    %           preprocess images to remove spike : tophat removing =
    %           opening of size 5 pixels
    %           compute the sum image over z and time
    %           interactively draw a line profile
    %           recover the intensity = f(z,t) for
     %                  whole image
     %                  disk 3/4 of the image
     %                  disk 1/2 of the image
     %                  line profile
    %           draw intensity evolution over time and z for each position
    %           nb: here time and z= number of image
    %           save the f(z,t) data in a text file
     
     % 
    
    

%% use
    % PMEGelIntensity
    
%% Comments
    %  micro manager save  images in a structured file folder
    % architecture :
    % root folder : name given by the user
    %   subfolders roi(n)_tile1 : n folders for each selected roi
    %           or pos(n) : n folders for each selected position
    %   display_and_comments.txt : file describing the channels acquired in
    %   
    %   in each subfolder roi(n)_tile1 or Pos(n) : 
    %       images files with name img_00000000(n)_DM300_327-353_00(p).tif
    %               n = number of time
    %               p = z focal plane
    %               DM300_327-353 = name of the channel
    %       metadata.txt : files describing all metadata associated with the
    %       acquisition : x, y, z position, camera settings .... etc.
    
    
%% Author
    % MF Devaux
    % INRA Nantes
    % BIA-PVPP
    
%% date
    % 22/08/2016

%% context variables 
orig=pwd;           % returns the current directory

%% start
close all

if nargin >1
    error('PMEGelIntensity');
end


%% input
[nameFolder,rootFolder,listDir,~]=metadataTelemos;
nbPos=size(listDir,1);

% read general metadata
cd(rootFolder);
md=readDIV(strcat(nameFolder,'.pos.metadata'));
nbTime=md.d(:,1);
nbZ=md.d(:,3);
nl=md.d(1,6);
nc=md.d(1,7);

% save result in folder
cd .. 
cd ..
sfolder=uigetdir(pwd,'save file in folder');
cd(sfolder)
if ~exist(nameFolder,'dir')
    mkdir(nameFolder);
end;
cd(nameFolder)
sfolder=pwd;


%% treatment
% for each pos sub-folder
for i=1:nbPos
    cd(rootFolder);

    mdp=readDIV(strcat(nameFolder,'.',listDir(i,:),'.metadata'));
    
    % check X and Y pos
    x=unique(mdp.d(:,1));
    if length(x)~=1
        error('check xposition')
    end
    y=unique(mdp.d(:,2));
    if length(y)~=1
        error('check yposition')
    end
     z=unique(mdp.d(:,3));
    if length(z)~=nbZ
        error('check zposition')
    end
    
    % read files in the pos folder
    cd(listDir(i,:));
    list=dir('*.tif');
    im=uint16(zeros(nl,nc,nbZ(i)*nbTime(i)));
    imLue=1;
    for j=1:length(list)
        try
            tim=imread(list(j).name,'tif');
        catch ME
            switch ME.identifier
                case 'MATLAB:imagesci:tiffmexutils:libtiffError'
                    tim=uint16(zeros(nl,nc));
                    imLue=0;
            end
            % pre-process image : tophat of size 5
        end;
        if imLue
            tim=medfilt2(tim,[5 5 ],'symmetric');
        end;
        im(:,:,j)=tim;
    end;
    
    % build image according to z and time
    sim=reshape(im,size(im,1),size(im,2),nbZ(i),nbTime(i));

%     if exist('f1','var')
%         close(f1);
%     end
    figure(1);
    imshow(sum(im,3),[]);
    title(listDir(i,:),'fontsize',18)
    
    % compute intensity profile for each z and time image
    % considering the whole image = intTot
    % considering the center of the image : disk of diameter 3/4 of the
    % image size = int3/4
    h34=imellipse(gca, [nl/8 nc/8 nl*3/4 nc*3/4]);
    roi34=createMask(h34);
    % and 1/2 of the image size int1/2
    h12=imellipse(gca, [nl/4 nc/4 nl/2 nc/2]);
    roi12=createMask(h12);
    % and considering the line intLine
    % interactive  line  selection for intensity profile from the sum of
    % images over z and time
    if i==1
        trace=1;
        newline=0;
    else
        hold on
        plot(cl,cc,'r','linewidth',2)
        hold off
        keep=yesnoMF('keep this line to extract intensity profile?',[0.5 0.75]);
        if keep
            trace=0;
        else
            trace=1;
            newline=1;
        end;
    end
    
    if trace
        hm=msgbox('draw a line to extract intensity profile');
        waitfor(hm);
        [cl,cc,~]=improfile;
        
    end;
    
        
    % extract intensity values
    intTot=zeros(nbZ(i),nbTime(i));
    int34=zeros(nbZ(i),nbTime(i));
    int12=zeros(nbZ(i),nbTime(i));
    intLine=zeros(nbZ(i),nbTime(i));
    for ii=1:nbZ(i)
        for ij=1:nbTime(i)
            tmp=sim(:,:,ii,ij);
            intTot(ii,ij)=mean2(tmp);
            int34(ii,ij)=mean2(tmp(roi34));
            int12(ii,ij)=mean2(tmp(roi12));
            intLine(ii,ij)=mean(improfile(tmp,cl,cc,length(cl)));
        end
    end
    
    % plot intensity according to time and z (NB: here time and z = number of
    % image)
    figure(2)
    plot(reshape(intTot,1,length(intTot(:))),'r','linewidth',2);
    set(gca,'fontsize',16);
    hold on
    plot(reshape(int34,1,length(intTot(:))),'g','linewidth',2);
    plot(reshape(int12,1,length(intTot(:))),'b','linewidth',2);
    plot(reshape(intLine,1,length(intTot(:))),'k','linewidth',2);
    xlabel('z and time','fontsize',18);
    ylabel('average intensity','fontsize',18);
    legend('whole image','3/4 dik','1/2 disk','line profile','location','northeastoutside');
    set(gco,'fontsize',16');
    title(listDir(i,:),'fontsize',18);
    hold off

    

    %% save
    cd(sfolder);
    figure(1)
    % draw line on sum image save line selection as jpg image
    if newline
        % re-display image without the old line
        imshow(sum(im,3),[]);
        title(listDir(i,:),'fontsize',18)
        imellipse(gca, [nl/8 nc/8 nl*3/4 nc*3/4]);
        imellipse(gca, [nl/4 nc/4 nl/2 nc/2]);
    end;
    hold on
    plot(cl,cc,'r','linewidth',2)
    hold off
    
    saveas(gcf,strcat(nameFolder,'.',listDir(i,:),'.profim.jpg'),'jpg');
    
    % save intensity figure
    figure(2)
    saveas(gcf,strcat(nameFolder,'.',listDir(i,:),'.intensity.png'),'png');

    d.d=[intTot int34 int12 intLine];
    d.i=strcat('z',num2str((1:nbZ(i))'));
    if nbTime<10
        sTime=num2str((1:nbTime(i))');
    else
        sTime=strcat('0',num2str((1:9)'));
        sTime=char(sTime,num2str((10:nbTime(i))'));
    end;

    % save intensity values : for each z : time values for whole image, 3/4
    % disk, 1/2 dik and line  : concatenated vectors
    d.v=char(strcat('tTot',sTime),strcat('t34',sTime),strcat('t12',sTime),strcat('tLine',sTime));
    writeDIV(d,strcat(nameFolder,'.',listDir(i,:),'.intensity.txt'));

    % save coordinate of line profile
    co.d=[cl cc];
    co.i=(1:length(cl))';
    co.v=char('line','col');
    
    writeDIV(co,strcat(nameFolder,'.',listDir(i,:),'.profcoord.txt'));
    
    % save xpos, ypos,zpos  and time for each z
    nmd.i=d.i;
    nmd.v=char('xPos','yPos','zpos',strcat('t',sTime));
    nmd.d(:,1)=repmat(x,nbZ(i),1);
    nmd.d(:,2)=repmat(y,nbZ(i),1);
    nmd.d(:,3)=z;
    % time values in minute and second from time of saving the first image
    % of the first position
    if i==1
        time0=mdp.d(1,(end-5):end);
    end;
    res=datevec(datenum(mdp.d(:,(end-5):end))-datenum(time0));
    % time in second taking into account hour, minutes and second
    ttime=(res(:,4)*3600+ res(:,5)*60+res(:,6));
    nmd.d(:,4:(nbTime(i)+3))=reshape(ttime,nbZ(i),nbTime(i));
    
    writeDIV(nmd,strcat(nameFolder,'.',listDir(i,:),'.xyzt.txt'));
    
end;

    
%% function tracking
tsname=strcat(nameFolder,'.track.txt');
fic=fopen(tsname,'w');
if fic==0
    error('unable to open %s for writing',tsname);
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'-\t extract intensity values from PM gel synchrotron images\r\n');
fprintf(fic,'__________________________________________________________________________\r\n');

fprintf(fic,'\r\n\r\n');

fprintf(fic,'root Folder : %s\r\n',rootFolder);
fprintf(fic,'Experiment name %s : \r\n',nameFolder);
for i=1:nbPos
    fprintf(fic,'   %s\r\n',listDir(i,:));
end;


fprintf(fic,' preprocess images to remove spike : median filtering of size 5x5 pixels\r\n');
fprintf(fic,'\r\n\r\n');

fprintf(fic,' recover the intensity = f(z,t) for:\r\n');
fprintf(fic,'        - whole image\r\n');
fprintf(fic,'       -  disk 3/4 of the image\r\n');
fprintf(fic,'       -  disk 1/2 of the image\r\n');
fprintf(fic,'        - line profile\r\n');
fprintf(fic,'\r\n\r\n');

fprintf(fic,'RESULTS\r\n\r\n');
fprintf(fic,'- save folder: \r\n\t- %s\r\n\r\n',sfolder);
fprintf(fic,'- intensity values : %s.Pos(n).intensity.txt \r\n',nameFolder);
fprintf(fic,'- associated x, y, z, time metadata : %s.Pos(n).xyzt.txt \r\n',nameFolder);
fprintf(fic,'- line and column coordinates of pixels for intensity profile : %s.Pos(n).coord.txt \r\n',nameFolder);
fprintf(fic,'- location of profile in the image: %s.Pos(n).profim.jpg\r\n',nameFolder);
fprintf(fic,'- intensity plot: %s.Pos(n).intensity.png\r\n\r\n',nameFolder);


% save of function used
fprintf(fic,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fic,'function name: %s\r\n ',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);

fclose(fic);

%% end

cd (orig)
    