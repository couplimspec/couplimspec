function reponse=yesnoMF(question,pos)
%% description 
% fonction ask a yes-no question and return 0 or 1
% 
%% input
% the question
%
%% output
%  0 or 1 depending on the answer
%
%% principle
%   just string compare test and return 0 or 1 
%   to avoid retyping it every time in fonctions
%
%% USE
%   continue=yesno('do you want to continue ?')
%
%% comment
%   review for couplimspec Toolbox
%
%% Author
% MF Devaux
% BIA-PVPP
%% date
% 25 mars 2014 : couplimSpec
% 4 f�vrier 2014% fonction ouinon adapt�e pour couplImSpec
% le 14 octobre 2002


%% start
% test parameters
if (nargin~=2)
    error('use : rep=yesnoMF(question,[Xpos, Ypos])')
end;

%% treatment

rep=MFquestdlg(pos,question,'','yes','no','yes');
    
if strcmp(rep,'yes')
   reponse=1;
else
   reponse=0;
end;

%% no save
%% no tracking
%% end