function varargout = PCAgui(varargin)
% Principal Component Analysis using Graphical User Interface
%
% Call PCAgui_calc to compute 
%
% 
% Usage :
%  PCAgui
%
% Auteur : MF Devaux
%			  INRA
%             BIA- PVPP
%              13 mar s2018
%
% from acpgui
%

% PCAGUI M-file for PCAgui.fig
%      PCAGUI, by itself, creates a new PCAGUI or raises the existing
%      singleton*.
%
%      H = PCAGUI returns the handle to a new PCAGUI or the handle to
%      the existing singleton*.
%
%      PCAGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PCAGUI.M with the given input arguments.
%
%      PCAGUI('Property','Value',...) creates a new PCAGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before acpgui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PCAgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PCAgui

% Last Modified by GUIDE v2.5 13-Mar-2018 14:49:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PCAgui_OpeningFcn, ...
                   'gui_OutputFcn',  @PCAgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PCAgui is made visible.
function PCAgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PCAgui (see VARARGIN)

% r�pertoire d'origine
handles.porig=pwd;

%logo=imread('D:\mfdevaux\outils_logiciels\matlab.dep\acp\Logoinra.gif');
%axes(handles.logo)
%imshow(logo,[])


% Choose default command line output for PCAgui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PCAgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PCAgui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_princ.
function load_princ_Callback(hObject, eventdata, handles)
% hObject    handle to load_princ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% read data
% either DIV or dso files can be uploaded
[nom,rep]=uigetfile({'*.txt','*.dso.mat'},'Data / Donn�es principales');

% extract file extension
[~,name,ext] = fileparts(nom);
switch ext
    case '.txt'
        filetype='DIV';
    case '.mat'
        [~,~,ext] = fileparts(name);
        if strcmp(ext,'.dso')
            filetype='DSO';
        else
            error('expected *.dso.mat file');
        end
    otherwise
           error('expected *.dso.mat or *.txt (DIV)  file');
end

cd(rep)

switch filetype
    case 'DIV'
        d=readDIV(nom);
        d.n=nom;
    case 'DSO'
        dso=loaddso(nom);
        d.d=dso.data;
        d.i=dso.label{1};
        d.v=dso.label{2};
        d.n=nom;
end

d.r=rep;
handles.d=d;

cd(rep);

set(findobj('tag','nom_princ'),'String',d.n);
if length(nom>35)
   set(findobj('tag','nom_princ'),'fontsize',8);
end

set(findobj('tag','nom_sup'),'String','');
if isfield(handles,'ds')
    handles=rmfield(handles,'ds');
end
set(findobj('tag','valpro'),'string','')

guidata(handles.figure1,handles)


% --- Executes on button press in load_sup.
function load_sup_Callback(hObject, eventdata, handles)
% hObject    handle to load_sup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% 
% lecture de donn�es suppl�mentaires

% read data
% either DIV or dso files can be uploaded
[nom,rep]=uigetfile({'*.txt','*.dso.mat'},'Supplementary Data / Donn�es suppl�mentaires');

% extract file extension
[~,name,ext] = fileparts(nom);
switch ext
    case '.txt'
        filetype='DIV';
    case '.mat'
        [~,~,ext] = fileparts(name);
        if strcmp(ext,'.dso')
            filetype='DSO';
        else
            error('expected *.dso.mat file');
        end
    otherwise
           error('expected *.dso.mat or *.txt (DIV)  file');
end

switch filetype
    case 'DIV'
        ds=readDIV(nom);
    case 'DSO'
        dso=loaddso(nom);
        ds.d=dso.data;
        ds.i=dso.label{1};
        ds.v=dso.label{2};
end


ds.r=rep;
d=handles.d;

handles.ds=ds;

set(findobj('tag','nom_sup'),'string',ds.n);
if length(ds.n>25)
   set(findobj('tag','nom_sup'),'fontsize',8);
end


if ~strcmp(ds.v,d.v)
    errordlg('variables differ from principal to supplementary data/les variables des donn�es principales et suppl�mentaires diff�rent')
    set(findobj('tag','nom_sup'),'String','');
    clear ds;
    clear handles.ds;
end



guidata(handles.figure1,handles)


% --- Executes on button press in demarrer.
function demarrer_Callback(hObject, eventdata, handles)
% hObject    handle to demarrer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% analyse en composantes principales
%

scale=logical(get(findobj('string','scale'),'Value'));

sauver_fig=logical(get(findobj('string','save_figures'),'Value'));

if isfield(handles,'ds')
    PCAguicalc(handles.d,scale,sauver_fig,handles.ds);
else
    PCAguicalc(handles.d,scale,sauver_fig);
end




cd(handles.porig)


% --- Executes on button press in scale.
function scale_Callback(hObject, eventdata, handles)
% hObject    handle to scale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of scale

% = modifie la valeur de scale (vrai ou faux)

% --------------------------------------------------------------------
function aide_Callback(hObject, eventdata, handles)
% hObject    handle to aide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% petit message pour indiquer la marche � suivre !

message={'load data/donn�es principales','load supplementary data/donn�es suppl�mentaires (option)',...
        'select scale to perfom a standardised PCA', 'clic <START>'};
msgbox(char(message),'Principal Component Ananlysis - Analyse en Composantes Principales','help') 


% --- Executes on button press in acpsauverfig.
function acpsauverfig_Callback(hObject, eventdata, handles)
% hObject    handle to acpsauverfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of acpsauverfig


