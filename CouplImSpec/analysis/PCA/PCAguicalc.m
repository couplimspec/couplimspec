function PCAguicalc(nom,scale,sauver,nomsup)
% Analyse en composantes principales du tableau de donn�es
% de nom g�n�rique <nom>
%
% la fonction acpguicalc a �t� d�velopp�e pour etre appel�e depuis la fonction interactive acpgui
% elle est �crite en prenant en compte les dimensions du tableau des individus principaux 
% une tansposition du tableau est effectu�e si le nombre de variable est
% grand (>50) et sup�rieur au nombre d'individus.
%
% la fonction a �t� d�velopp�e pour correspondre aux formats de donn�es
% utilis�s � l'inra de nantes et pour obtenir les graphiques de travail usuels
% d'une analyse en composantes principales
%
% Le fichier de d�part doit contenir les noms des variables et des individus
% 	fichier de type txt excel ou .d .i .v en 3 fichiers
%
% Scale : option de normalisation des variables 1= oui, 0 = non (d�faut)
% nom des donn�es suppl�mentaires (optionnel)
%
% les r�sultats sont sauvegard�s dans 3 fichiers r�sultats 
% de noms g�n�riques :
% 		<nom>.co pour les coordon�es des individus
%		<nom>.vp pour les loadings ou vecteurs propres
%		<nom>.vl pour les valeurs propres
%
% Le programme constitue automatiquement les graphiques de :
% 		la d�croissance des valeurs propres
%		les cartes des composantes 1 et 2 
%								   3 et 4
%		les profils des variables pour les composantes 1, 2, 3, 4 si les variables sont des nombres
%       les cartes des variables et les cercles des corr�lations si les variables sont des chaines de caract�re
%
% 
% USAGE : acpguicalc('nomfic') pour une acp non norm�e
%
%       ou
%
%        acpguicalc('nomfic',1) pour une acp norm�e (variables h�t�rog�nes)
%
% Version du 18 mars 2004
% Version du 3 mai 2005 pour la sauvegarde des figures
%
% Auteur : MF Devaux
%		   BIA - PV                 (URPOI-Parois)
% D'apr�s acp.m

% fermeture des fenetre acp existentes
h=findobj('name','Analyse en Composantes Principales : Valeurs propres');
close(h);
h=findobj('name','Analyse en Composantes Principales : Composantes 1 et 2');
close(h);
h=findobj('name','Analyse en Composantes Principales : Composantes 3 et 4');
close(h);
h=findobj('name','Analyse en Composantes Principales : Profils des variables');
close(h);
h=findobj('name','Analyse en Composantes Principales : Variables : Composantes 1 et 2');
close(h);
h=findobj('name','Analyse en Composantes Principales : Variables : Composantes 3 et 4');
close(h);
h=findobj('name','Analyse en Composantes Principales : cercle des corr�lations : Composantes 1 et 2');
close(h);
h=findobj('name','Analyse en Composantes Principales : cercle des corr�lations : Composantes 3 et 4');
close(h);

% test du nombre de param�tres
if (nargin==1)
    scale=0;
    sup=0;
    sauver=0;
else
    if (nargin==2)
        sup=0;
        sauver=0;
    else
        if (nargin==3)
            sup=0;
        else
            if (nargin==4)
                sup=1;
            else
              error('USE : PCAguicalc(<nom>,flag <0/1> de normalisation,flag <0/1> de sauvegarde des figures, <nomsup> (optionnel)')
            end
        end
    end  
end

% lecture des donn�es
if ischar(nom)
    don=lire(nom);
else
    don=nom;
    if isfield(don,'n')
        nom=don.n;
    else
        nom='nonD�fini';
    end
end
nom=strrep(nom,'.txt','');

if sup
    if ischar(nomsup)
        dons=lire(nomsup);
    else
        dons=nomsup;
        if isfield(dons,'n')
            nomsup=dons.n;
        else
            nomsup='nonD�finiS';
        end
    end
    if ~strcmp(dons.v,don.v)
        errordlg('ATTENTION : les noms des variables des 2 fichiers diff�rent');
        error('')
    end
    nomsup=strrep(nomsup,'.txt','');
end
    

% centrage de la matrice de donn�es
cdon=don.d-repmat(mean(don.d),size(don.d,1),1);
if sup
    cdons=dons.d-repmat(mean(don.d),size(dons.d,1),1);
end

if scale
    %normalisation des variables = division par l'�cart-type des variables
    et=sqrt(var(cdon));
    cdon=cdon*diag(1./et);
    if sup
        cdons=cdons*diag(1./et);
    end
end

% analyse en composantes principales
%
% renvoie : 
%   coord=coordonn�es
%   vecpro : vecteur propres (loadings)
%   valpro : valeurs propres

%calcul de la matrice de variance covariance V
if (size(don.d,2)>50)&&(size(don.d,2)>size(don.d,1))
    % transposition du calcul pour diagonaliser une matrice plus petite
    % cas o� le nombre de variable est plus grand que le nombre d'individu
    % et > � 50
    transp=1;
    V=cdon*cdon';           % V est de dimension nind x nind
else
    % pas de transposition
    transp=0;
    V=cdon'*cdon;           % V est de dimension nvar x nvar
end

% division par la nombre d'individu-1 pour avoir une covariance
V=V/(size(don.d,1)-1);

% diagonalisation de la matrice de variance covariance � l'aide de la fonction matlab svd (singluar value
% decomposition. A ce stade coord=valpro car la matrice V est sym�trique
% et vl est la matrice diagonale desvaleurs propres
[vecpro,vl_svd,coord]=svd(V);

% constitution du tableau des valeurs propres <valpro>
% premi�re colonne : valeur propre
% deuxi�me colonne : pourcentage d'inertie
% troisi�me colonne : pourcentage cumul�
valpro(:,1)=diag(vl_svd);
valpro(:,2)=valpro(:,1)*100/sum(valpro(:,1));
for i=1:size(valpro,1)
    valpro(i,3)=sum(valpro(1:i,2));
end
clear vl;

% affichage des valeurs propres dans le bloc de texte de la fenetre acp
vl_aff=sprintf('num�ro valeurs propres %% d''inertie %% cumul�\r\n\r\n');
for i=1:min(length(valpro),20)
    vl_aff=char(vl_aff,sprintf('     %2d         %8.2f        \t%5.2f\t   %6.2f',i,valpro(i,:)));
end

set(findobj('tag','valpro'),'string',vl_aff)

% choix du nombre de composantes sauvegard�es
nbval=0;
while~nbval
    reponse=inputdlg('nombre de composantes retenues','choix du nombre de composantes',1,{num2str(min(10,size(don.d,2)))});
    nbval=str2num(char(reponse)); 
end


% calcul des coordonn�es ou des vexteurs propres
if transp
    % dans ce cas, les vecteurs propres doivent etre recalcul�s � partir de
    % coord
    
    vecpro=cdon'*coord(:,1:nbval);
   
    % normalisation des vecteurs propres : afin que vecpro*vecpro= matrice
    % identit�. les nbval premiers sont retenus
    for i=1:nbval
        vecpro(:,i)=vecpro(:,i)/sqrt(sum(vecpro(:,i).*vecpro(:,i)));
    end
else
    % les vecteurs propres sont un r�sultat direct de la d�composition en
    % valeurs singuli�res de la matrice de variance covariance
    % les nbval premiers sont retenus
    vecpro=vecpro(:,1:nbval);
end
    
% calcul des coordonn�es � partir des vectuers propres
coord=cdon*vecpro;


% donnees suppl�mentaires
if sup
    % calcul des coordonn�es des individus suppl�mentaires
    coords=cdons*vecpro;
end



%sauvegarde des r�sultats
[gensauve,rep_sauve]=uiputfile(strcat(nom,'.acp.txt'),'sauvegarde des r�sultats');
gensauve=strrep(gensauve,'.acp.txt','');
cd(rep_sauve);
%coordonn�ees
co.d=coord;
co.i=don.i;
co.v=char('cp1');
for i=2:size(coord,2)
   co.v=char(co.v, sprintf('cp%d',i));
end
writeDIV(co,sprintf('%s.co',gensauve));
if sup
    cos.d=coords;
    cos.i=dons.i;
    cos.v=co.v;
    writeDIV(cos,sprintf('%s.%s.cos',gensauve,nomsup));
end

% vecteurs propres
vp.d=vecpro;
vp.i=don.v;
vp.v=co.v;
writeDIV(vp,sprintf('%s.vp',gensauve));

% valeurs propres
vl.d=valpro;
vl.v=char({'valpro', '%inertie' '%cumul�e'});
vl.i=char('cp1');
for i=2:size(valpro,1)
   vl.i=char(vl.i, sprintf('cp%d',i));
end
writeDIV(vl,sprintf('%s.vl',gensauve));

if (fopen(sprintf('%s.acp.txt',gensauve),'w')==-1)
    error('probl�me d''ouverture en �criture du fichier %s.acp.txt ',gensauve);
end
% fichier texte 
fic=fopen(sprintf('%s.acp.txt',gensauve),'w');
fprintf(fic,'%s\r\n\r\n',date);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n');
fprintf(fic,'-                              ANALYSE EN COMPOSANTES PRINCIPALES                              -\r\n');
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n');

if scale
    fprintf(fic,'\r\nanalyse centr�e et norm�e\r\n');
else
    fprintf(fic,'\r\nanalyse centr�e\r\n');
end

fprintf(fic,'\r\nr�pertoire : %s\r\n',pwd);

fprintf(fic,'\r\nfichier : %s\r\n',nom);
if sup
    fprintf(fic,'\r\nfichier de donn�es suppl�mentaires : %s\r\n',nomsup);
end

fprintf(fic,'\r\nnombre d''individus : %d\r\n',size(don.d,1));
fprintf(fic,'nombre de variables : %d\r\n',size(don.d,2));
if sup
    fprintf(fic,'\r\nnombre d''individus suppl�mentaires : %d\r\n',size(dons.d,1));
end

fprintf(fic,'\r\n------------------------------------------------------------------------------------------------\r\n');
fprintf(fic,'Variance des premi�res composantes principales\r\n');
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n');
fprintf(fic,'\r\nComposante   Valeurs propres     %% de variance  %% cumul�\r\n');
for i=1:min(20,size(vl.d,1))
    fprintf(fic,'   %2d\t\t   % 9.3f\t      %5.2f\t\t %6.2f\r\n',i,vl.d(i,1),vl.d(i,2),vl.d(i,3));
end


fprintf(fic,'\r\n\r\n\r\n------------------------------------------------------------------------------------------------\r\n');
fprintf(fic,'r�pertoire de sauvegarde des r�sultats : %s\r\n',rep_sauve);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n');
fprintf(fic,'\r\nFichiers sauvegard�s :\r\n\r\n');
fprintf(fic,'\t- composantes principales (ou scores) : ');
fprintf(fic,'%s.co\r\n',gensauve);
if sup
    fprintf(fic,'\t- donn�es suppl�mentaires : composantes principales (ou scores) : ');
    fprintf(fic,'%s.%s.cos.txt\r\n',gensauve,nomsup);
end
fprintf(fic,'\r\n\t- fichiers des donn�es vecteurs propres (ou loadings) : ');
fprintf(fic,'%s.vp.txt\r\n',gensauve);
fprintf(fic,'\t- fichiers des valeurs propres (ou eigenvalues) : ');
fprintf(fic,'%s.vl.txt',gensauve);

% sauvegarde du programme utilis�
prog=which('PCAguicalc.m');
prop=dir(prog);
dateprog=prop.date(1:11);

fprintf(fic,'\r\n\r\n\r\n------------------------------------------------------------------------------------------------\r\n');
fprintf(fic,'programme de calcul : %s du %s\r\n',prog,dateprog);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n\r\n\r\n\r\n');

fclose(fic);
fclose all;


% graphe des valeurs propres
h=figure('numbertitle','off','name','Analyse en Composantes Principales : Valeurs propres');
figure(h);
subplot(1,1,1);
plot(valpro(:,2));
xlabel('num�ro de composantes principales');
ylabel(' pourcentage d''inertie');
title(sprintf('%s : valeurs propres',nom));
if sauver
    saveas(h,strcat(gensauve,'.vl.tif'),'tif');
end

% cartes des composantes 1 et 2
h=figure('numbertitle','off','name','Analyse en Composantes Principales : Composantes 1 et 2');
figure(h);
subplot(1,1,1);
plot(coord(:,1),coord(:,2),'.w');
text(coord(:,1),coord(:,2),don.i,'color','k','fontsize',7);
if sup
    text(coords(:,1),coords(:,2),dons.i,'color','b','fontsize',7);
end
axis auto        
xlabel(sprintf('composante principale 1 (%5.2f %%)',valpro(1,2)));
ylabel(sprintf('composante principale 2 (%5.2f %%)',valpro(2,2)));
title(nom);
if sup
    title(sprintf('%s - %s',nom,nomsup));
end
if sauver
    saveas(h,strcat(gensauve,'.cp12.tif'),'tif');
end

if (nbval>=4)
    % cartes des composantes 3 et 4
    h=figure('numbertitle','off','name','Analyse en Composantes Principales : Composantes 3 et 4');
    figure(h);
    subplot(1,1,1);
    plot(coord(:,3),coord(:,4),'.w');
    text(coord(:,3),coord(:,4),don.i,'fontsize',7);
    if sup
        text(coords(:,3),coords(:,4),dons.i,'color','b','fontsize',7);
    end
    axis auto 
    xlabel(sprintf('composante principale 3 (%5.2f %%)',valpro(3,2)));
    ylabel(sprintf('composante principale 4 (%5.2f %%)',valpro(4,2)));
    title(nom);
    if sup
        title(sprintf('%s - %s',nom,nomsup));
    end
    if sauver
        saveas(h,strcat(gensauve,'.cp34.tif'),'tif');
    end
end
        
% profils des variables
if (sum(size(str2num(don.v))))>0 
    h=figure('numbertitle','off','name','Analyse en Composantes Principales : Profils des variables');
    figure(h);
    % les variables sont des nombres
    for i=1:min(4,nbval)
       subplot(2,2,i);
       plot(str2num(don.v),vecpro(:,i),'k'); %#ok<*ST2NM>
       hold on;
       plot([min(str2num(don.v)) max(str2num(don.v))],[0 0],':k');
       xlabel(' ');
       ylabel('unit� arbitraire');
       minx=min(str2num(don.v));
       maxx=max(str2num(don.v));
       miny=min(vecpro(:,i))-0.03*abs(min(vecpro(:,i)));
       maxy=max(vecpro(:,i))+0.03*abs(max(vecpro(:,i)));
       axis([minx maxx miny maxy]);
	
       title(sprintf('%s : profil %d',nom,i),'fontsize',7);
       hold off;
    end
    if sauver
        saveas(h,strcat(gensauve,'.vp.tif'),'tif');
    end
else
    h=figure('numbertitle','off','name','Analyse en Composantes Principales : Variables : Composantes 1 et 2');
    figure(h);

    % les variables sont des chaines de caract�res : carte et cercle des corr�lations
    subplot(1,1,1);
    plot(vecpro(:,1),vecpro(:,2),'.w');
    text(vecpro(:,1),vecpro(:,2),don.v,'fontsize',7);
    xlabel(sprintf('composante principale 1 (%5.2f %%)',valpro(1,2)));
    ylabel(sprintf('composante principale 2 (%5.2f %%)',valpro(2,2)));
    title(nom);
    if sauver
        saveas(h,strcat(gensauve,'.vp12.tif'),'tif');
    end
    if (nbval>=4)
        h=figure('numbertitle','off','name','Analyse en Composantes Principales : Variables : Composantes 3 et 4');
        figure(h);
        subplot(1,1,1);
        plot(vecpro(:,3),vecpro(:,4),'.w');
        text(vecpro(:,3),vecpro(:,4),don.v,'fontsize',7);
        xlabel(sprintf('composante principale 3 (%5.2f %%)',valpro(3,2)));
        ylabel(sprintf('composante principale 4 (%5.2f %%)',valpro(4,2)));
        title(nom);
        if sauver
            saveas(h,strcat(gensauve,'.vp34.tif'),'tif');
        end
    end
    % cercles des corr�lations
    if scale
        h=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corr�lations : Composantes 1 et 2');
        figure(h);
        subplot(1,1,1);
        plot(sqrt(valpro(1,1))*vecpro(:,1),sqrt(valpro(2,1))*vecpro(:,2),'.w');
        text(sqrt(valpro(1,1))*vecpro(:,1),sqrt(valpro(2,1))*vecpro(:,2),don.v,'fontsize',7);
        xlabel(sprintf('composante principale 1 (%5.2f %%)',valpro(1,2)));
        ylabel(sprintf('composante principale 2 (%5.2f %%)',valpro(2,2)));
        hold on;
        cercle;
        plot([-1 1],[0 0],':k');
        plot([0 0 ],[-1 1],':k');
        title(nom);
        hold off;
        if sauver
            saveas(h,strcat(gensauve,'.vpc12.tif'),'tif');
        end

        if (nbval>=4)
            h=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corr�lations : Composantes 3 et 4');
            figure(h);
            subplot(1,1,1);
            plot(sqrt(valpro(3,1))*vecpro(:,3),sqrt(valpro(4,1))*vecpro(:,4),'.w');
            text(sqrt(valpro(3,1))*vecpro(:,3),sqrt(valpro(4,1))*vecpro(:,4),don.v,'fontsize',7);
            xlabel(sprintf('composante principale 3 (%5.2f %%)',valpro(3,2)));
            ylabel(sprintf('composante principale 4 (%5.2f %%)',valpro(4,2)));
            hold on;
            cercle;
            plot([-1 1],[0 0],':k');
            plot([0 0 ],[-1 1],':k');
            title(nom);
            hold off;
            if sauver
                saveas(h,strcat(gensauve,'.vpc34.tif'),'tif');
            end
        end
    else
        cc=corrcoef([cdon,coord]);
        cc=cc(1:size(cdon,2),(size(cdon,2)+1):(size(cdon,2)+size(coord,2)));
        h=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corr�lations : Composantes 1 et 2');
        figure(h);
        subplot(1,1,1);
        plot(cc(:,1),cc(:,2),'.w');
        text(cc(:,1),cc(:,2),don.v,'fontsize',7);
        xlabel(sprintf('composante principale 1 (%5.2f %%)',valpro(1,2)));
        ylabel(sprintf('composante principale 2 (%5.2f %%)',valpro(2,2)));
        hold on;
        cercle;
        plot([-1 1],[0 0],':k');
        plot([0 0 ],[-1 1],':k');
        title(nom);
        hold off;
        if sauver
            saveas(h,strcat(gensauve,'.vpc12.tif'),'tif');
        end

	
        if (nbval>=4)
            h=figure('numbertitle','off','name','Analyse en Composantes Principales : cercle des corr�lations : Composantes 3 et 4');
            figure(h);
            subplot(1,1,1);
            plot(cc(:,3),cc(:,4),'.w');
            text(cc(:,3),cc(:,4),don.v,'fontsize',7);
            xlabel(sprintf('composante principale 3 (%5.2f %%)',valpro(3,2)));
            ylabel(sprintf('composante principale 4 (%5.2f %%)',valpro(4,2)));
            hold on;
            cercle;
            plot([-1 1],[0 0],':k');
            plot([0 0 ],[-1 1],':k');
            title(nom);
            hold off;
            if sauver
                saveas(h,strcat(gensauve,'.vpc34.tif'),'tif');
            end
        end
    end
end

