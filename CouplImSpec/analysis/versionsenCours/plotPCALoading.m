  function h=plotPCALoading(x,vp,num,titre)

 %% description
    % plot PCA loadings
    
%% inputh=plotPcaLoading(vp,num,titre)
    % x : variable en x
    %  vp : matrix of laodings as column vectors
    %  num = number of laodings to plot
    % titre=title
    
    
    
%% output
    
    %   h : handles of resulting figure

%% principe
    % plot each loading in a separate figure
    % had the zero lines to the plot


%% use
    % h=plotPCALoading(x,vp,num,titre))

    
%% Comments
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 24 June 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=4 && nargin ~=3 
    error('use: h=plotPcaLoading(x,vp,num,titre)');

end


if nargin==3
     titre='';
end



% check errors

if max(num)>size(vp,2)
    error('wrong number of loadings');
end
    

%% treatement
% each loading in a separate figure
for i=1:length(num)
    h(i)=figure('name',[titre ': loading ' num2str(num(i))]);
 	plot(x,vp(:,i),'LineWidth',2);
	hold on
	plot([x(1) x(end)],[0 0 ],'--k','LineWidth',2);
	xlabel('')
	ylabel('arbitrary unit','FontSize',16,'FontWeight','bold')
	title([titre ': loading ' num2str(num(i))],'FontSize',16,'FontWeight','bold')
	hold off
end;



%% matlab function tracking  
% no tracking
 
%% end

cd (orig)
    