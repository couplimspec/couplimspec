  function dso3=twoWay2threeWay(dso2,simSize)

 %% description
    % convert 2 way dso image data table into a three way data table
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% input

    %  dso2 : dso image 2 way data table
    %  simSize : side size of the small image
    %
    % or nothing
    
%% output
    
    %   dso3 : dso image 3 way data table

%% principle
    % in a dso image, the two way data table correspond to an unfolded
    % three-way multispectral image
    % the size of the multispectral image in pixel is given by the
    % dso.imagesize field
    %
    % the small images will be built from the three-way image
    %
    % small image are supposed to be squared small image
    %
    % index of all the top left pixel of the small image are determined
    % using the meshgrid function
    % 
    % the three way data table is computed and saved in a subfolder called
    % 3waydso
    %


%% use
    % dso3=twoWay2threeWay(dso2,simSize)
   
%% Comments
    % written for Dhivyaa Rajasundaram stay in Nantes (Walltrac project)
    
    
%% Author
    % MF Devaux and D Rajasundaram
    % BIA-PVPP INRA Nantes
    
%% date
    % 7 april 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if (nargin ~=2) && (nargin~=0)
    error('use: dso3=twoWay2threeWay(dso2,simSize) or dso3=twoWay2threeWay');
end


%% input
if nargin == 0
    [fname,pathname]=uigetfile({'*.dso.mat'},'name of 2 way dso file to convert','*.dso.mat');
    cd(pathname)
    
    dso2=loaddso(fname);
    
    ok=0;
    while ~ok
        rep=inputdlg('size of the small image (odd number please)','size of small image',1,{'5'});
        simSize=str2double(char(rep));
        if simSize>0
            if (simSize-floor(simSize/2)*2)==1
                ok=1;
            end
        end
    end;
end

%% treatement
% size of multispectral image in the 2way dso2
imSize=dso2.imagesize;
% multispectral image
spim=dso2.imagedata;
% number of spectral variable
nbsp=size(spim,3);

% top left coordinates of small image in multispectral image
%row
tlR=1:simSize:imSize(1);
nbRow=length(tlR);
% column
tlC=1:simSize:imSize(2);
nbCol=length(tlC);

% Topleft pixel coordinates
tlRow=meshgrid(tlR,tlC)';
tlCol=meshgrid(tlC,tlR);


% number of small image
nbsim=nbRow*nbCol;
data3=zeros(nbsim,simSize^2,nbsp);

% find linear indices of pixels in each small image
k=0;
for i=1:(nbRow-1)
    for j=1:(nbCol-1)
        k=k+1;
        % small spectral image
        sspim=spim(tlRow(i,j):(tlRow(i,j)+simSize-1),tlCol(i,j):(tlCol(i,j)+simSize-1),:);
        
        
        % 3-way data table: unfold small image
        data3(k,:,:)=reshape(sspim,simSize*simSize,nbsp);

    end;
    % last column
    k=k+1;
    
    sspim=spim(tlRow(i,nbCol):(tlRow(i,nbCol)+simSize-1),tlCol(i,nbCol):imSize(2),:);


    % 3-way data table: unfold small image
    nbcolsspim=imSize(2)-(nbCol-1)*simSize;
    data3(k,1:simSize*nbcolsspim,:)=reshape(sspim,simSize*nbcolsspim,nbsp);

end;

% last row
for j=1:(nbCol-1)
    k=k+1;
      % small spectral image
    sspim=spim(tlRow(nbRow,j):imSize(1),tlCol(nbRow,j):(tlCol(nbRow,j)+simSize-1),:);


    % 3-way data table: unfold small image
    nbrowsspim=imSize(1)-(nbRow-1)*simSize;
    data3(k,1:nbrowsspim*simSize,:)=reshape(sspim,nbrowsspim*simSize,nbsp);
end;
% last row -last column
k=k+1;
% small spectral image
sspim=spim(tlRow(nbRow,nbCol):imSize(1),tlCol(nbRow,nbCol):imSize(2),:);


% 3-way data table: unfold small image
data3(k,1:nbrowsspim*nbcolsspim,:)=reshape(sspim,nbrowsspim*nbcolsspim,nbsp);

% built dso3dso=dataset;
dso3=dataset;
dso3.name=strcat(dso2.name,'3way',num2str(simSize));
dso3.author=dso2.author;
dso3.data=data3;
dso3.type='image';
dso3.imagesize=[nbRow nbCol];
dso3.axisscale{1}=1:size(data3,1);
dso3.axisscale{2}=(1:simSize^2);
dso3.axisscale{3}=dso2.axisscale{2};


dso3.title{1}='pixel';
dso3.label{1}=num2str(dso3.axisscale{1}');

dso3.label{3}=dso2.label{2};
dso3.title{3}='Wavenumber';

dso3.label{2}=num2str((1:simSize^2)');

dso3.title{2}='Small image Pixel';

dso3.description=dso2.description;

dso3.userdata=dso2.userdata;



%% save 
if ~exist('3waydso','dir')
    mkdir('3waydso')
end

cd('3waydso')
sfolder=pwd;
save(strcat(dso3.name,'.dso.mat'),'dso3');

%% matlab function tracking  

fid=fopen(strcat(dso3.name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'convert 2 way dso into 3 way dso file \r\n');
fprintf(fid,'_______________________________________\r\n');

fprintf(fid,'\r\ninput dso file name: %s\r\n',fname);
fprintf(fid,'data folder: %s\r\n',pathname);

fprintf(fid,'\r\n\r\n');
fprintf(fid,'Small image size: %d\r\n\r\n',simSize);
 
fprintf(fid,'\r\nsaved file name : %s.dso.mat \r\n',dso3.name);
fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    