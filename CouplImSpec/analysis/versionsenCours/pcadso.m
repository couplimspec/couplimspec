  function pcadso(dso)

 %% compute the pca of a dso
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% input

    %   dso : dso data table
 
    %
    % or nothing
    
%% output
    
     %  nothing

%% principle
    % 
    % centred pca
    % TODO


%% use
    % pcadso(dso)
    
%% Comments
    % written for Dhivyaa Rajasundaram stay in Nantes (Walltrac project)
    
    
%% Author
    % MF Devaux and D Rajasundaram
    % BIA-PVPP INRA Nantes
    
%% date
    % 16 april 2014 : 

%% context variables 
orig=pwd;           % returns the current directory

%% start

if (nargin ~=1) && (nargin~=0)
    error('use: pcadso(dso) or pcadso');
end


%% input
if nargin == 0
    [fname,pathname]=uigetfile({'*.dso.mat'},'name of dso file to analyse','*.dso.mat');
    cd(pathname)
    
    dso=loaddso(fname);
    

end

%% treatement
% mean
m=mean(dso.data);

% centring
X=double(dso.data)-repmat(m,size(dso,1),1);

% variance covariance
V=X'*X;

% svd
[U,s,v]=svd(V);

% scores
c=X*U;

% inertia
v(:,1)=diag(s);
v(:,2)=v(:,1)*100/sum(v(:,1));
for i=1:size(v,1)
    v(i,3)=sum(v(1:i,2));
end;
clear s;

nbsco=min(20,size(c,2));

% built scores dataset;
sco=dataset;
sco.name=strcat(dso.name,'.pcaScores');
sco.author='PCADSO';
sco.data=c(:,1:nbsco);
if dso.imagemode
    sco.type='image';
    sco.imagesize=dso.imagesize;
else
    sco.type='data';
end

sco.axisscale{1}=dso.axisscale{1};
sco.axisscale{2}=(1:nbsco)';

sco.title{1}=dso.title{1};
sco.label{1}=dso.label{1};

sco.label{2}=strcat('cp',num2str((1:nbsco)'));
sco.title{2}='principal component';

sco.description=dso.description;

sco.userdata=dso.userdata;


% built loading dataset;
loa=dataset;
loa.name=strcat(dso.name,'.pcaLoadings');
loa.author='PCADSO';
loa.data=U(:,1:nbsco)';
loa.type='data';

loa.axisscale{1}=(1:nbsco)';
loa.axisscale{2}=dso.axisscale{2};

loa.title{1}='loadings';
loa.label{1}=sco.label{2};

loa.label{2}=dso.label{2};
loa.title{2}=dso.title{2};

loa.description=dso.description;

loa.userdata=dso.userdata;

% built mean dataset;
mea=dataset;
mea.name=strcat(dso.name,'.pcaMean');
mea.author='PCADSO';
mea.data=m;
mea.type='data';

mea.axisscale{1}=1;
mea.axisscale{2}=dso.axisscale{2};

mea.title{1}='PCAmean';
mea.label{1}='PCAmean';

mea.label{2}=dso.label{2};
mea.title{2}=dso.title{2};

mea.description=dso.description;

mea.userdata=dso.userdata;

% eigenvalues
vl.d=v;
vl.v=char({'eigenvalues', '%inertia' '%cumulatedintertia'});
vl.i=char('cp1');
for i=2:size(v,1)
   vl.i=char(vl.i, sprintf('cp%d',i));
end;
writeDIV(vl,sprintf('%s.pcaEigenvalues',dso.name));

%% save 
sfolder=pwd;
savedso(sco)
savedso(loa)
savedso(mea)

%% matlab function tracking  

fid=fopen(strcat(dso.name,'.pcadso.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'PCA DSO \r\n');
fprintf(fid,'___________________________________\r\n');

fprintf(fid,'\r\ninput dso file name: %s\r\n',fname);
fprintf(fid,'data folder: %s\r\n',pathname);


fprintf(fid,'\r\nsaved file name : %s \r\n',strcat(dso.name,'.pca* '));
fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    