function largePCAdso
%

%% DESCRIPTION
% PCA od a serie of images contaiend in a single folder
% The PCA is mean centered
%
%% INPUT
%  nothing
%
%% OUTPUT
% name of folder : <FOLDNAME>
% New subfolder : largePCAloadings
%   dso files : PCA loadings in the form loadings x variables :
%                                   <FOLDNAME>.PCAloadings.dso.mat
%                                   and their graph as png files
%                   .mean of PCA
%                                      <FOLDNAME>.PCAmean.dso.mat
%                   variance-covariance matrix as a DIV file
%                                       <FOLDNAME>.PCAVcov.txt
%                   number of pixels in each image as a DIV file
%                                       <FOLDNAME>.PCAnbPixel.txt
%                   eigenvalue and % of variance as a DIV file
%                                       <FOLDNAME>.PCAeigenvalues.txt
%
%% PRINCIPE
% TODO
%
%% USE
%               largePCAdso
%
%% COMMENTS
% adapted from the version developed by Mathias Corcel
%
%
%% AUTHOR
% MF Devaux
% BIA - PVPP Nantes
%
%
%% DATE
% 11 mars 2019
% 8 aout 2019 : consider any syntax name for rois : means that consistence
% with image name is not checked
% 19 octobre 2020 again remove test on mask name

%% context variables
orig=pwd;           % returns the current directory

%% start
rfolderRoi='rois';
if nargin~=0
    error('use largePCA')
end

%% input
[~,rfolder]=uigetfile({'*.dso.mat'},'name of first dso images : ','*.dso.mat');
cd( rfolder)
imList=dir('*.dso.mat');
nbim=length(imList);
[ngen]=fileparts(rfolder);
[~,ngen]=fileparts(ngen);

if ~exist('largePCAloadings','dir')
    mkdir('largePCAloadings')
end
cd('largePCAloadings')
sfolder=pwd;

cd(rfolder)
wroi=yesno('with roi ?');
if wroi
    [~,rfolderRoi]=uigetfile({'*.tif'},'name of first roi: ','*.tif');
    cd( rfolderRoi)
    imRoiList=dir('*.tif');
    nbr=length(imRoiList);
    if nbr~=nbim
        error('number of ROI differ from number of images');
    end
 
end

%% treatement
% Preallocation
fprintf('Initialisation\r\n');
cd(rfolder)
dso = loaddso(imList(1).name); % load the first image to get number of columns
X = zeros(dso.size(1,2),dso.size(1,2));
Ni = zeros(1,nbim);
N = 0;
S = zeros(1,dso.size(1,2));

for i = 1:nbim
    fprintf('%d of %d: %s\r\n\tload dso...',i,nbim,imList(i).name)
    cd(rfolder)
    dso = loaddso(imList(i).name);
    if wroi
        cd(rfolderRoi)
        fprintf('\tread mask...')
     %   maskName=dir(['*' dso.name '*.tif']);
        
%         if length(maskName)>1
%             error('more than one  roi image found with generic name %s: problem may occur',dso.name);
%         end
%         if isempty(maskName)
%             error('no roi image found with generic name %s',dso.name);
%         end
        
%        maskName=maskName.name;
            
        maskName=imRoiList(i).name;
        disp(maskName);
        mask = imread(maskName);
        mask = reshape(mask,size(dso.data,1),[]);
        Xi = dso(mask);
        cd ..
    else
        Xi=dso;
    end
    Xi = Xi.data;
    Xi = double(Xi);
    
    fprintf('\tcompute local mean and local variance covariance\r\n')
    % Calcul covar
    Ni(i) = size(Xi,1);
    Si = sum(Xi);
    % Sum of squares
    X = X + Xi'*Xi;
    % Number of rows
    N = N + Ni(i);
    % Sum for each column
    S = S + Si;
    clear Xi  Si
    
end

fprintf('eigenvalues, eigenvectors....\r\n')
% Mean for each column
M = S / N;
% Covariance matrix C
C = (X - N * (M' * M)) / (N - 1);
% Calculate eigenvalues D and eigenvectors V
[V,D,~] = svd(C);
D = diag(D);
Vari = D*100/sum(D);
Varcumul = cumsum(Vari);


% graphe des valeurs propres
h=figure('numbertitle','off','name','Large PCA  : Eigenvalues');
figure(h);
subplot(1,3,1);
plot(Vari);
xlabel('Principal Component');
ylabel(' % of variance');
title(sprintf('Large PCA : eigenvalues'));
subplot(1,3,2);
plot(log10(Vari));
xlabel('Principal Component');
ylabel(' % of variance');
title(sprintf('Large PCA : log10(eigenvalues)'));
subplot(1,3,3);
plot(Varcumul);
xlabel('Principal Component');
ylabel(' cumulated % of variance');
title(sprintf('Large PCA : eigenvalues'));

nbCP=inputNumber('number of principal component saved on disk(=number of loadings)');

fprintf('save results...\r\n');
% create datasets and DIV files
% mean spectra
moy=dso(1,:);
moy.data=M;
moy.label{1}='mean vector';
moy.userdata=dso.userdata;
moy.userdata.process.Npixel=N;
moy.userdata.process='largePCAmean';
moy.name=[ngen '.mean'];

if isfield(moy.userdata,'method')
    moy.userdata.acquisition.method=moy.userdata.method;
    moy.userdata=rmfield(moy.userdata,'method');
end

% loadings
vp=dso(1:nbCP,:);
vp.data=V(:,1:nbCP)';
netot=size(V,2);
if size(V,2)<10
    tmp=strcat('pc',num2str((1:netot)'));
else
    if size(V,2)<100
        tmp=strcat('pc0',num2str((1:9)'));
        tmp=char(tmp,strcat('pc',num2str((10:netot)')));
    else
        if size(V,2)<1000
             tmp=strcat('pc00',num2str((1:9)'));
            tmp=char(tmp,strcat('pc0',num2str((10:99)')));
            tmp=char(tmp,strcat('pc',num2str((100:netot)')));
        else
             tmp=strcat('pc000',num2str((1:9)'));
            tmp=char(tmp,strcat('pc00',num2str((10:99)')));
            tmp=char(tmp,strcat('pc0',num2str((100:999)')));
            tmp=char(tmp,strcat('pc',num2str((1000:netot)')));
        end
    end
end
vp.label{1}=tmp(1:nbCP,:);
vartot=tmp;
clear tmp;

vp.userdata=moy.userdata;
vp.userdata.process=';largePCAloadings';
vp.name=[ngen '.loadings'];


% eigenvalues
ed.d=[D Vari Varcumul];
ed.i=vartot;
ed.v=char('eigenvalue','%var','%varcumul');



% number of pixel
np.d=Ni';
np.i=char(imList.name);
np.i=np.i(:,1:(end-8));
np.v='nbPixel';

% variance covariance (sum of square)
covv.d=X;
covv.i=moy.label{2};
covv.v=moy.label{2};


%% save
cd(sfolder);
savedso(moy)
plotdso(moy,'linewidth',2);

savedso(vp);
for i=1:min(10,nbCP)
    plotdso(vp(i,:),1,0,'.','linezero','linewidth',2);
    title([ngen '.loading' num2str(i)],'fontsize',18);
    ylabel('arbitrary units','fontsize',18);
    saveas(gcf,[ngen '.loading' num2str(i) '.png']);
end


writeDIV(ed,[ngen '.largePCAeigenvalues']);

writeDIV(np,[ngen '.largePCAnbPixel']);

writeDIV(covv,[ngen, '.largePCASumSquare']);



%% matlab function tracking
fid=fopen('largePCAdso.track.txt','w');

if fid==0
    errordlg('unable to open track file');
end
% %s for str   %d for double    No [] for paths
fprintf(fid,'%s\t',datestr(now,0));
fprintf(fid,'\r\nPCA of series of images');
fprintf(fid,'\r\n__________________________________________________________________________');
fprintf(fid,'\r\n');
fprintf(fid,'\r\nAll images from a folder are used for Principal Component Analysis\r\n');

fprintf(fid,'\r\nComputing steps:');
fprintf(fid,'\r\n\t - for each image');
fprintf(fid,'\r\n\t\t - read each image');
fprintf(fid,'\r\n\t\t - increment sum of values of each variable');
fprintf(fid,'\r\n\t\t - increment sum of squared values for variance covariance matrix');
fprintf(fid,'\r\n\t - after all image reading: ');
fprintf(fid,'\r\n\t - compute mean for each variable');
fprintf(fid,'\r\n\t - considering that the data matrix is mean centered: assess covariance matrix from sum of square, mean spectra and number of pixel');
fprintf(fid,'\r\n\t - Decomposition of the matrix to obtain the eigenvalues and eigenvectors');
fprintf(fid,'\r\n');

fprintf(fid,'\r\nInput folder : %s',rfolder);
fprintf(fid,'\r\n');

fprintf(fid,'\r\nResults folder %s',sfolder);
fprintf(fid,'\r\n');
fprintf(fid,'\r\n - number of pixels in each image: %s',[ngen, '.largePCAnbPixel.txt']);
fprintf(fid,'\r\n - Mean spectrum: %s',[moy.name '.dso.mat']);
fprintf(fid,'\r\n - sum of square for variance covanriance matrix calculation: %s',[ngen, '.largePCASumSquare.txt']);
fprintf(fid,'\r\n');
fprintf(fid,'\r\n - loadings (eigenvectors): %s',[vp.name '.dso.mat']);
fprintf(fid,'\r\n -eigenvalues and percentage of variance: %s',[ngen, '.largePCAeigenvalues.txt']);


fprintf(fid,'\r\n');
fprintf(fid,'\r\nList of all images used :');
for i = 1:nbim
    fprintf(fid,'\r\n - %s',imList(i).name);
end
fprintf(fid,'\r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
fprintf(fid,'__________________________________________________________________________\r\n');
fclose(fid);

% end

cd (orig)
