function [PCAmodel] = largePCA
%
%% DESCRIPTION
% PCA on a serie of images contaiend in a single folder
% Each pixel is normalized (sum of all variables = 1)
% The PCA is mean centered
% 
%% INPUT
% no input
% 
%% OUTPUT
% New subfolder : PCA
% .mat file : PCAmodel, which have all variables organised in a structure
% array
% Subsubfolder : Loadings, with images of loadings
%
%% PRINCIPE
% 
% 
%% USE
% use: largePCA
% User will be asked to select an image (randomly) inside the folder
% containing the serie of images
%
%% COMMENTS
% TODO : validateattributes
%
%% AUTHOR
% Mathias Corcel
% IATE Montpellier / BIA-PVPP Nantes
% review : MF Devaux
%   BIA-PVPP INRA Nantes
%
%% DATE
% 21/04/2015 : start
% 03/06/2015 : TODO : validateattributes
% 11/08/2015 : Changement de la m�thode de normalisation des images
% 30/10/2015 : firts review MFD
% 31/01/2020: new modifications still to be completly evaluated

%% context variables
orig=pwd;           % returns the current directory

%% start


%% input
% input file list
[~,rfolder]=uigetfile({'*.dso.mat'},'name of first dataset multispectral image','*.dso.mat');
ext='.dso.mat';

% input file list to process
cd(rfolder);
imList=dir(['*' ext]);
nbim=length(imList);

% roi folder
[~,genName]=fileparts(pwd);
roiFolder=strcat(genName,'.roi');
if ~exist(roiFolder,'dir')
    ROI=0;
else
    ROI=1;
end
if ROI
    cd(roiFolder);
    roiFolder=pwd;
end

% save

cd(rfolder)
cd ..
sfolder=strcat(genName,'.largePCA');
if ~exist(sfolder,'dir')
    mkdir(sfolder)
end
cd(sfolder)
sfolder=pwd;
cd ..

% sfolderscores=strcat(genNAme,'.largePCAscores');
% if ~exist(sfolderscores,'dir')
%     mkdir(sfolderscores)
%     cd(sfolderscores)
%     sfolderscores=pwd;
%     cd ..
% end;



%% treatement
% Preallocation
%load(imList.Name{1});
%cd(rfolder)
%dso = loaddso(imList.Name{1},imList.Path{1}); % load a first image to get number of columns
%dso=loaddso(imList(1).name);
% sumX2 = zeros(dso.size(1,2),dso.size(1,2)); 
% nbp = 0;
% sumx = zeros(1,dso.size(1,2));
%nbim = size(imList.Name,1);

for i = 1:nbim
    cd(rfolder)
    disp(imList(i).name)
    fprintf('\tLoad dso...')
    %cd(imList.Path{i})
    %load(imList.Name{i});
    %dso = loaddso(imList.Name{i},imList.Path{i});
    dso=loaddso(imList(i).name);
    
    if i==1
        % pre-allocation
        sumX2 = zeros(dso.size(1,2),dso.size(1,2)); 
        nbp = 0;
        sumx = zeros(1,dso.size(1,2));
    end
    
    %dso.data = dso.data./repmat(sum(dso.data,2),1,12);
    %cd('Masks')
    if ROI
        fprintf('\tRead mask image...')
        cd(roiFolder)
        roiName=strrep(imList(i).name,'.dso.mat','.roi.tif');
        %mask = imread(strcat(dso.name,'_mask.tif'));
        mask=imread(roiName);
        %mask = reshape(mask,size(dso.data,1),[]);
        dso = dso(mask);
    end
    
    fprintf('\tCompute cumulative sums...\r\n')

    Xi = dso.data;
   % cd ..
    % Calcul covar
    nbpi = size(Xi,1);
    sumxi = sum(Xi);
    sumXi2=double(Xi)'*double(Xi);
    
    % Multiplication of variables
    sumX2 = sumX2 + sumXi2 ; 
    % Number of rows
    nbp = nbp + nbpi; 
    % Sum for each column
    sumx = sumx + sumxi; 
    
    clear Xi nbpi sumxi sumXi2
    
end


disp('SVD...')

% Mean spectra
meanS = sumx / nbp; 
% Find covariance matrix C
COV = (sumX2 - nbp * (meanS' * meanS)) / (nbp - 1);
% Calculate eigenvalues D and eigenvectors V
[loading,eigValue,~] = svd(COV);
eigValue = diag(eigValue);
Var = eigValue*100/sum(eigValue);
Varcumul=zeros(size(eigValue,1),1);
for i = 1:size(eigValue,1)
    Varcumul(i,1) = sum(Var(1:i));
end


% number of compoenent collected  TODO : ask for....
nbPC=size(eigValue,1);

if nbPC>20
    if Varcumul(20)<99
        ind=find(Varcumul>99);
        nbPC=ind(1);
    else
        nbPC=20;
    end
end

%% save
disp('Save...')
cd(sfolder)

% loadings as dso
dsoLoadings=dso(1:size(loading,2),:);
dsoLoadings.name=strcat(genName,'.largePCA.loadings');
dsoLoadings.type='data';

dsoLoadings.data=loading';
dsoLoadings=dsoLoadings(1:nbPC,:);

% name of component
if nbPC<10
    PCName=[repmat('PC',nbPC,1),num2str((1:nbPC)')];
else
    PCName=[repmat('PC0',9,1),num2str((1:9)')];
    PCName=[PCName;[repmat('PC',nbPC-9,1),num2str((10:nbPC)')]];
end

dsoLoadings.label{1}=PCName;
dsoLoadings.axisscale{1}=(1:nbPC);
dsoLoadings.title{1}='Principal Components';

savedso(dsoLoadings);

% mean spectra as dso
dsoMean=dsoLoadings(1,:);

dsoMean.name=strcat(genName,'.largePCA.mean');

dsoMean.data=meanS;

dsoMean.title{1}='mean spectra';

savedso(dsoMean);


% eigenvalues as DIV file

eig.d=[eigValue Var Varcumul];

eig.v=char('eigenValue','%Var','%cumVar');

% name of component
nb=size(eig.d,1);
if nb<10
    EigName=[repmat('PC',nb,1),num2str((1:nb)')];
else
    if nb <100
    EigName=[repmat('PC0',9,1),num2str((1:9)')];
    EigName=[EigName;[repmat('PC',nb-9,1),num2str((10:nb)')]];
    else
        EigName=[repmat('PC00',9,1),num2str((1:9)')];
        EigName=[EigName;[repmat('PC0',90,1),num2str((10:99)')]];
        EigName=[EigName;[repmat('PC',nb-99,1),num2str((100:nb)')]];
    end
end
eig.i=EigName;

writeDIV(eig,strcat(genName,'.largePCA.eigenValue'));



% save 
%PCAmodel = struct;
%PCAmodel.datafolder = imList.Path{1};
%PCAmodel.covariance = C;
%PCAmodel.eigvectors = loading;
%PCAmodel.variance_percent = Var;
%PCAmodel.variance_cumul = Varcumul;
%PCAmodel.variables_labels = dso.label{2};

PCAmodel.cumul.nbp = nbp;
PCAmodel.cumul.sumx = sumx;
PCAmodel.cumul.sumX2 = sumX2;

save('PCAmodel.cumul.mat','PCAmodel')

% a = 'PCA';
% mkdir(a)
% cd(a)
% save('PCAmodel','PCAmodel')

%% graphs
% eigenvalue
figure
subplot(1,1,1)
plot(eig.d(:,2),'b','linewidth',2)
xlabel('index of pincipal component')
ylabel(' % of variance')
title(sprintf('%s: Large PCA eigenvalues',genName))
saveas(gcf,strcat(genName,'.largePCA.eigenValue.png'),'png');

%loadings
for i=1:nbPC
    figure
    dsoli=dsoLoadings(i,:);
    h=plotdso(dsoli,0,0);
    hold on
    % get handles to the line drawn by plot
    g=get(h,'children'); 
    set(g.Children,'linewidth',2)
 
    % get x axis limits to drawn the zero line
    xl=get(gca,'Xlim');

    plot(xl,[0 0],'k');
    ylabel('arbitrary units','fontsize',16);
    title(PCName(i,:))
    saveas(gcf,strcat(genName,'.largePCA.Loading',PCName(i,3:end),'.png'),'png');
end
    

% a = 'Loadings';
% mkdir(a)
% cd(a)
% for i = 1:size(eigValue,1)
%     plot(loading(:,i))
%     a = strrep(strcat('PC',num2str(i),' (',num2str(Var(i)),char(37),')'),'.',',');
%     legend(a) 
%     set(gca,'FontSize',12,'fontWeight','bold');
%     set(gca, 'XLim',[0 size(dso.label{2},1)+1], 'XTick',0:size(eigValue,1)+1,'XTickLabel', [blanks(4);dso.label{2};blanks(4)]);
%     h=get(gca,'Children');
%     set(h,'linewidth',4,'Marker','.');
%     t = get(gca,'Title');
%     set(t,'FontSize',20,'fontWeight','bold');
%     tx = get(gca,'Xlabel');
%     set(tx,'FontSize',18,'fontWeight','bold');
%     ty = get(gca,'Ylabel');
%     set(ty,'FontSize',18,'fontWeight','bold');
%     title(a);
%     hold on
%     plot([0 size(eigValue,1)+1],[0 0],'k--')
%     print(a,'-djpeg')
%     savefig(a)
%     close
% end
% cd ..

%% matlab function tracking
fid=fopen('largePCA.track.txt','w');

if fid==0
    errordlg('unable to open track file');
end;
% %s for str   %d for double    No [] for paths
fprintf(fid,'%s\t',datestr(now,0));
fprintf(fid,'\r\nPCA with multiple images');
fprintf(fid,'\r\n__________________________________________________________________________');
fprintf(fid,'\r\n');

fprintf(fid,'\r\nCentred Principal Component Analysis\r\n');

fprintf(fid,'\r\nInput folder : %s',rfolder);
fprintf(fid,'\r\n');
fprintf(fid,'\r\nList of all images used :');
for i = 1:nbim
    fprintf(fid,'\r\n - %s',imList(i).name);
end
fprintf(fid,'\r\n');

fprintf(fid,'\r\nTotal number of pixels: %d\r\n',nbp);
fprintf(fid,'Number of variables : %d\r\n',size(dso,2));

fprintf(fid,'\r\n------------------------------------------------------------------------------------------------\r\n');
fprintf(fid,'Variance of the first principal components\r\n');
fprintf(fid,'------------------------------------------------------------------------------------------------\r\n');
fprintf(fid,'\r\nComponent\t\tEigenValue\t\t%% de variance\t\t%% cumul�\r\n');
for i=1:min(20,nbPC)
    fprintf(fid,'   %2d\t\t   % 9.3f\t      %5.2f\t\t %6.2f\r\n',i,eig.d(i,1),eig.d(i,2),eig.d(i,3));
end;


fprintf(fid,'\r\n\r\n\r\n------------------------------------------------------------------------------------------------\r\n');
fprintf(fid,'save result : %s\r\n',rfolder);
fprintf(fid,'------------------------------------------------------------------------------------------------\r\n');

fprintf(fid,'\r\n\t- Loadings : ');
fprintf(fid,'%s.mat\r\n',dsoLoadings.name);
fprintf(fid,'\t- Eigenvalues : ');
fprintf(fid,'%s.txt',strcat(genName,'.largePCA.eigenValue'));

fprintf(fid,'\r\n');
fprintf(fid,'\r\nCumulated sums and numbers saved in mat file : PCAmodel.cumum.mat');
fprintf(fid,'\r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
fprintf(fid,'__________________________________________________________________________\r\n');
fclose(fid);

%% end

cd (orig)
    