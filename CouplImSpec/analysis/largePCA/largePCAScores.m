function largePCAScores
%
%% DESCRIPTION
% Project a list of images on components obtained by large principal component
% analysis 
%
%% INPUT
% nothing : interactive function
%
%% OUTPUT
% Nothing : save on disk
%
%% PRINCIPE
%  scores are computed as double and converted to single to build the
%  dso.mat scores matrix in order to stay to a reasonnable data set size
%  in case of ROIS, the 0 values outside to the ROIS are put to 0 score
%  values
%  
%  scores are also saved as jpg images in unit8. In this case, for each scores,
%  the limits are streched to between 5 and 250.
%  a linear stretching is applied
%
%% USE
% largePCAScores
% should be launched after largePCAdso
%
%% COMMENTS
%
%% AUTHOR
% Mathias Corcel
% IATE Montpellier / BIA-PVPP Nantes
% review by MF Devaux
% INRA - BIA- PVPP
%
%% DATE
% 19/05/2015 : start
% 03/06/2015 : TODO : validateattributes
%  10/ novembre 2015 MFD
% 8 aout 2019 : consider any syntax name for rois : means that consistence
% with image name is not checked
% 22 aout 2019 : add option no rois
% 11 fevrier 2020 : remove png images ?
% 19 octobre 2020 again remove test on mask name
% 17 novembre 2020 : save the general min and max of the images : geston of
% name of rois using a generic name...
% 27 novembre 2020 : minmax measured for all the colelction. min and max
% pixels put for image representation

%% context variables
orig=pwd;           % returns the current directory

maxuint8=250;
minuint8=5;

%% start
close all

%% input
[~,rfolder]=uigetfile({'*.dso.mat'},'name of first dso images : ','*.dso.mat');
cd( rfolder)
imList=dir('*.dso.mat');
nbim=length(imList);

wroi=yesno('with roi ?');
if wroi
    [genname,start]=genericName(imList(1).name);
    lgen=length(genname);
    if exist('rois','dir')
        cd('rois')
    end
    defname=['*',genname,'*.tif'];
    [~,rfolderRois]=uigetfile({'*.tif'},'name of first roi images : ',defname);
    cd(rfolderRois)
    imListRoi=dir('*.tif');
    nbimRoi=length(imListRoi);
    if nbimRoi~=nbim
        error('number of rois differ from number of image');
    end
    
    cd(rfolder)
    zoomRois=yesno('rois zoom ?');
    if zoomRois
        if exist('roisZoom','dir')
            cd('roisZoom')
        end
        [~,rfolderRoisZoom]=uigetfile({'*.tif'},'name of first zoomed roi images : ',defname);
    end
else
    zoomRois=0;
end

nb=inputNumber('nombre de composantes � dessiner ?');

% read
cd(rfolder)
if ~exist('largePCALoadings','dir')
    error('largePCALoadings folder not exist')
end
cd('largePCALoadings')
rfolderLoad=pwd;

% loadings and mean
cd(rfolderLoad)
lvp=dir('*.loadings.dso.mat');
if length(lvp)>1 || isempty(lvp)
    error('pb loading file');
end

loadings=loaddso(lvp.name);
loadings=loadings.data';


lmoy=dir('*.mean.dso.mat');
if length(lmoy)>1 || isempty(lmoy)
    error('pb moy file');
end

meanS=loaddso(lmoy.name);
meanS=meanS.data;

lvl=dir('*.largePCAeigenvalues.txt');
if length(lvl)>1 || isempty(lvl)
    error('pb vl file');
end

vl=readDIV(lvl.name);

% save
cd(rfolder)
if ~exist('largePCAScores','dir')
    mkdir('largePCAScores')
end
cd('largePCAScores')
sfolder=pwd;

if ~isfolder('imagesComponent')
    mkdir('imagesComponent')
end
cd('imagesComponent')
sfolderimages=pwd;


%% treatement
% initialisation 
% label for principal Component
lab=strcat('cp',num2str((1:nb)'));

factor=4;           % to define the general  min and max to represent the images
% min and max to represent the images
minCo=-factor*sqrt(vl.d(1:nb,1))';
maxCo=factor*sqrt(vl.d(1:nb,1))';

% edge for histogram computation
nbpas=5000;
stepCo=(maxCo*5-minCo*5)/nbpas;
edgeh=zeros(nb,nbpas+3);
for i=1:nb
    edgeh(i,:)=[minCo(i)*6 minCo(i)*5:stepCo(i):maxCo(i)*5 maxCo(i)*6];
end

% histograms 
histo=cell(nb,1);

% linear conversion to save scores as uint8 images between 0 255
a8=repmat((maxuint8-minuint8),1,length(minCo))./(maxCo-minCo);
b8=repmat(minuint8,1,length(a8))-minCo.*a8;

% select the number of laodings
loadings=loadings(:,1:nb);

for i = 1:nbim
    disp([num2str(i) ' of ' num2str(nbim)])
    disp(imList(i).name)
 
    disp('load dso')
    cd(rfolder)
    dso = loaddso(imList(i).name);
    
    dso.data = double(dso.data);
    
    
    % ROIS
    if wroi
        cd(rfolderRois)
        disp('read ROI')
        genname=imList(i).name(start:(start+lgen-1));
        maskName=ROIname(genname);
 
        disp(maskName);
        mask = imread(maskName);
    end
    
    Xi = dso.data;
    
    % centering
    Xi=Xi-repmat(meanS,size(Xi,1),1);
    
    % in case of ROIS,put outside pixels to 0
    if wroi
        Xi(~mask(:),:)=0;
    end
    
    % projection on loadings
    Ci=Xi*loadings;
    
    
    clear Xi
    
    % building resulting scores dso
    % initialisation 
    co=dso(:,1:nb);
    clear dso
    
    % conversion to single
    co.data=single(Ci);        
    co.label{2}=lab;
    co.name=[co.name '.co'];
 
    % save
    cd(sfolder)
    disp('save score')
    savedso(co);
    
    % MIn and Max scores
    if i==1
        minico=min(co.data);
        maxico=max(co.data);
        minmax.v=co.label{2};
    else
        minico=min([co.data;minico]);
        maxico=max([co.data;maxico]);
    end
    
    % conversion to uint8
    Ci8=uint8(Ci.*repmat(a8,size(Ci,1),1)+repmat(b8,size(Ci,1),1));
    Ci8(1,:)=0;
    Ci8(end,:)=255;
    
    co8=co;
    co8.data=Ci8;

    clear Ci8
     
    % represent as images
    % load zoomRois
    if zoomRois
        cd(rfolderRoisZoom);
        maskName=dir(['*' genname '*.tif']);
        if length(maskName)>2
            error('more than two  roi image found with generic name %s: problem may occur',genname);
        end
        if isempty(maskName)
            error('no roi image found with generic name %s',genname);
        end
        lmask=maskName;
        nbm=length(lmask);
        maskzoom=cell(nbm);
        coz8=cell(nbm);
        for k=1:nbm
            maskzoom{k} = imread(lmask(k).name);
            [maskrectangle,masksize]=maskBoundingBox(maskzoom{k});
            
            coz8{k}=co8(maskrectangle(:),:);
            coz8{k}.imagesize=masksize;
        end
    end
    
    cd(sfolderimages)

    for j=1:size(co8,2)
        if i==1
            figure(1)
            imshow(co8.imagedata(:,:,j),[])
            title(['cp ' num2str(j) ' : '  imList(i).name],'fontsize',14)
            colorbar('ticks',0:25:255,'ticklabels',round(((0:25:255)-b8(j))/a8(j)/10)*10)
            saveas(gcf,[imList(i).name '.cp ' num2str(j) 'colorbar.png'  ],'png');
        end
        
        imwrite(co8.imagedata(:,:,j),[imList(i).name '.cp ' num2str(j) '.jpg'  ],'jpg');
    
        if zoomRois
            for k=1:nbm
                imwrite(coz8{k}.imagedata(:,:,j),[strrep(imList(i).name,'.dso.mat','') '.' strrep(lmask(k).name,'.tif','') '.cp ' num2str(j) '.jpg'  ],'jpg');
            end
        end
    end
    
    
    clear co
    clear co8
    clear coz8
    
    % build global grey level histogramms of pixel for each components for
    % quantile evaluation
    if wroi
        Ci=Ci(mask(:),:);
    end
    
%     minilocal=min(Ci);
%     maxilocal=max(Ci);
    
    for j=1:nb
%         if minilocal(j)<edgeh(j,1)
%             edgeh(j,1)=minilocal(j);
%         end
%         if maxilocal(j)>edgeh(j,end)
%             edgeh(j,end)=maxilocal(j);
%         end
        before=sum(Ci(:,j)<edgeh(j,1));
        histo{j}.d(i,:)=histcounts(Ci(:,j),edgeh(j,:));
        after=sum(Ci(:,j)>edgeh(j,end));
        histo{j}.d(i,1)=histo{j}.d(i,1)+before;
        histo{j}.d(i,end)=histo{j}.d(i,end)+after;

    end
    
end

cd(sfolder)
for i=1:nb
    edgeh(i,1)=min(minico(i),edgeh(i,1));
    edgeh(i,end)=max(maxico(i),edgeh(i,end));
end
centreBin=movmean(edgeh',2)';
centreBin=centreBin(:,2:end);
for i=1:nb
    histo{i}.v=centreBin(i,:);
    histo{i}.i=char(imList.name);
    writeDIV(histo{i},strcat('CP.histo',num2str(i)))
end

minmax.d=[minico; maxico];
minmax.i=char('min','max');
writeDIV(minmax,'CP.minmax');


%% matlab function tracking
fid=fopen('largePCAScores.track.txt','w');

if fid==0
    errordlg('unable to open track file');
end
% %s for str   %d for double
fprintf(fid,'%s\t',datestr(now,0));
fprintf(fid,'\r\nLargePCAdso: Compute principal component images');
fprintf(fid,'\r\n__________________________________________________________________________');
fprintf(fid,'\r\n');
fprintf(fid,'\r\nModel used :');
fprintf(fid,'\r\n');
fprintf(fid,'\r\n - %s',rfolderLoad);
fprintf(fid,'\r\n');
fprintf(fid,'\r\nScores are computed as double but stored in uint16 after normalisation with min and max of each cmponents');
fprintf(fid,'\r\n');
fprintf(fid,'\r\nInput folder : %s', rfolder);
fprintf(fid,'\r\n');
if wroi
    fprintf(fid,'\r\nRoi folder : %s', rfolderRois);
    fprintf(fid,'\r\n');
end
fprintf(fid,'\r\nList of all images used :');
for i = 1:nbim
    fprintf(fid,'\r\n - %s',imList(i).name);
end
fprintf(fid,'\r\n');
% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
fprintf(fid,'__________________________________________________________________________\r\n');
fclose(fid);

% end

cd (orig)

end

function maskName=ROIname(genname)

    maskName=dir(['*' genname '*.tif']);
    if length(maskName)>1
        error('more than one  roi image found with generic name %s: problem may occur',currentgenname);
    end
    if isempty(maskName)
        error('no roi image found with generic name %s',currentgenname);
    end

    maskName=maskName.name;
end

function [maskrectangle,masksize]=maskBoundingBox(mask)
    
    bb=regionprops(mask>0,'boundingbox');
    bb=bb.BoundingBox;
    
    ib=max((floor(bb(2))-50),1);
    ih=min((floor(bb(2))+50+bb(4)),size(mask,1));
    jb=max(1,(floor(bb(1))-50));
    jh=min((floor(bb(1))+50+bb(3)),size(mask,2));
    
    maskrectangle=false(size(mask));
    maskrectangle(ib:ih,jb:jh)=1;
    
    masksize=[ih-ib+1 jh-jb+1];
end

