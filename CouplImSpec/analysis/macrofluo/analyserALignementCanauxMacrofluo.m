  function analyserALignementCanauxMacrofluo
%

 %% description
    % nalayse du fichier produit par la focntion testerALignementCanauxMacrofluo
    
    
%% input

    %  interactive 
    
%% output
    
    %   save a data file with indicator opf shift between  filter blocd

%% principe
    % find all templates from image 1 : ua
    % calculate shifts between lines/columns measures with the
    % original ones
    % shift over 10 are not considered (occur when template could not be
    %   relevantly found in the target image)
    % the result is the number of timevalues of 0, +-1, +-2, +-3, etc...
    % were found
    % the procedure is repeated for the four images
    
%% use
    % analyserALignementCanauxMAcrofluo
    
    
%% Comments
    % 
    
    
%% Author
    % MF devaux
    % BIA - PVPP
    
%% date
    % 24 janvier 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: analyserALignementCanauxMacrofluo');
end


%% input
% select dso file from folder 
[name,rfolder]=uigetfile({'*.txt'},'macrofluo test resulta file',strcat('*.testAlignementCanauxMacrofluo.txt'));
cd (rfolder)
d=lire(name);
    
%save result file
sname=strrep(name,'test','analyse');
[sname,sfolder]=uiputfile({'*.txt'},'save result file',sname);


%% treatement
nborim=max(d.d(:,2));          % number of original images

ming=11;
maxg=-11;

% loop for each original image
for i=1:nborim
    % selec template from the current image
    iorig=find(d.d(:,2)==i);
    
    dcur=d.d(iorig,:);
    
    % line position analysis
    ij=1;
    mins=11;
    maxs=-11;
    for j=5:2:11
        % shift vector)
        shift=dcur(:,3)-dcur(:,j);
        % indice of "reasonable" shift = <10
        is=(abs(shift)<10);
        shift=shift(is);
        ior=iorig(is);
        
        % values of shift : 
        vshift=unique(shift);
        mins=min(min(vshift),mins);
        maxs=max(max(vshift),maxs);
        
        % histogram of vshift values
        if length(vshift)>1
            nshift=hist(shift,vshift);
        else
            nshift=length(shift);
        end;
        
        % save
        rshift{i,ij,1}={vshift,nshift};
        
        ij=ij+1;
    end
    
    % column position analysis
    ij=1;
    for j=6:2:12
        % shift vector)
        shift=dcur(:,4)-dcur(:,j);
        % indice of "reasonable" shift = <10
        is=(abs(shift)<10);
        shift=shift(is);
        ior=iorig(is);
        
        % values of shift : 
        vshift=unique(shift);
        mins=min(min(vshift),mins);
        maxs=max(max(vshift),maxs);
        
        % histogram of vshift values
        if length(vshift)>1
            nshift=hist(shift,vshift);
        else
            nshift=length(shift);
        end;
               
        
        % save
        rshift{i,ij,2}={vshift,nshift};
        
        ij=ij+1;
    end
    
    ming=min(mins,ming);
    maxg=max(maxs,maxg);
    
end;

% reshape all data to form a usual data table
r.v=(ming:maxg)';
r.d=zeros(32,length(r.v));
j=1;
for i=1:nborim
    for ij=1:4
        %line
        r.i(j,:)=strcat(num2str(i),'-',num2str(ij),'-lig');
        temp=rshift{i,ij,1};
        vshift=temp{1};
        nshift=temp{2};
        r.d(j,vshift-ming+1)=nshift;
        j=j+1;
        %column
        r.i(j,:)=strcat(num2str(i),'-',num2str(ij),'-col');
        temp=rshift{i,ij,2};
        vshift=temp{1};
        nshift=temp{2};
        r.d(j,vshift-ming+1)=nshift;
        j=j+1;
    end;
end;

% find max mvalue for each item of the data table
nbp=size(r.d,1);

for i=1:nbp
    tt=find(r.d(i,:)==max(r.d(i,:)))+ming-1;
    tp(i)=tt(1);
end;
t.d=reshape(tp,2,16)';
t.v=char('lig','col');
t.i=['UA-UA';'UA-UB';'UA-BL';'UA-VE';...
    'UB-UA';'UB-UB';'UB-BL';'UB-VE';...
    'BL-UA';'BL-UB';'BL-BL';'BL-VE';...
    'VE-UA';'VE-UB';'VE-BL';'VE-VE'];


%% save 

ecrire(r,sname);
ecrire(t,strrep(sname,'.txt','.resume.txt'));

%% matlab function tracking  

fid=fopen(strrep(sname,'.txt','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end; 

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Analyse macrofluo image alignment test file\r\n');
fprintf(fid,'_____________________________________________\r\n');
fprintf(fid,'\r\ninput file name: %s \r\n',name);
fprintf(fid,'data folder: %s\r\n',rfolder);
 
fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'\r\nsaved file name summary : %s \r\n',strrep(sname,'.txt','.resume.txt'));
fprintf(fid,'data folder: %s\r\n',sfolder);

fprintf(fid,'\r\nBrief description: \r\n');
fprintf(fid,'\r\n\t - calculate the histogram of shifts between individual images');
fprintf(fid,'\r\n\t - bothe in line and columns');
fprintf(fid,'\r\n\r\n');


% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    

