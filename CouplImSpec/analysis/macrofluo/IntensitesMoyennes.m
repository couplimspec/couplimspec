function IntensitesMoyennes

 %% description
    % Compuite average grey level of each channel
   
   
    
%% input

    %  nothing
    
%% output
    
    %  nothing average intensity profiles are saved on disk on  asingle file

%% principe
    % all the image of teh folder are analysed
    % images are considered as 12 channels fluorescence images
    % the average intansity of each channel is computed 
    
%% use
    % IntensitesMoyennes
    
%% Comments
    % for Joel Passicousset PhD
    % session 2 : macrofluoresncence time were set separately for each
    % filter
     % applied after exposure time correction
    
%% Author
    % MF Devaux and Joel Passicousset
    % INRA Nantes BIA-PVPP
    % IFPEN
    
%% date
    % 20 juin 2016

%% context variables 
orig=pwd;           % returns the current directory


%% start

if nargin >1
    error('use: intensitesMoyennes');
end


%% input
% image folder
[~,rfolder]=uigetfile({'*.tif'},'first image to be analysed','*.tif');
cd(rfolder)
listim=dir('*.tif');
nbim=length(listim);



%% treatement
% initialisation

% intensity data
IntMoy.d=uint16(zeros(nbim,12));


for i=1:nbim
     cd(rfolder)
     nom=listim(i).name;        % image name
     disp(nom);
     info=imfinfo(nom);
     nim=length(info);
    
    % read the tiff image
    for j=1:nim
        fprintf('%d ',j);
        tmp=imread(nom,j);
        if j==1
            im=tmp;
       else
            im(:,:,j)=tmp;
        end
    end
    fprintf('\r\n');
  
    % maximum intensities of each channel
    mesInt=zeros(1,12);
    for j=1:12
        mesInt(j)=mean2(im(:,:,j));
    end
         
    IntMoy.d(i,:)=uint16(mesInt);
    
end
        
    
% save intenisty data
if ~exist('IntensitesMoyennes','dir')
    mkdir('IntensitesMoyennes')
    
end

cd('IntensitesMoyennes')
sfolder=pwd;

IntMoy.i=char(listim.name);
IntMoy.v=['VE-B';'VE-G';'VE-R';'BL-B';'BL-G';'BL-R';'UB-B';'UB-G';'UB-R';'UA-B';'UA-G';'UA-R'];

ordre=[10 11 12 7 8 9 4 5 6 1 2  3];

IntMoy.v=IntMoy.v(ordre,:);
IntMoy.d=IntMoy.d(:,ordre);

writeDIV(IntMoy,'MeanInt');

                           

%% fucntion tracking
% remplissage d'un fichier trace pour indiquer les t�ches effectu�es
% actualisation du fichier trace.txt
fic=fopen('IntensitesMOyennes.track.txt','w');
if fic==0
    errordlg('probl�me d''�criture du fichier <track.txt>');
end;

fprintf(fic,'\r\n\r\n%s\r\n',datestr(now,0));
fprintf(fic,'__________________________________________________________________________\r\n');
fprintf(fic,'-\t Calcul des intensit�s moyennes des 12 canaux - MACROFLUO\r\n');
fprintf(fic,'__________________________________________________________________________\r\n');

fprintf(fic,'\r\n\r\n');


fprintf(fic,'image folder : %s\r\n',rfolder);
fprintf(fic,'avregae intensities saved in folder : %s\r\n',sfolder);
fprintf(fic,'average intensities  saved in file : MeanInt.txt\r\n');

fprintf(fic,'\r\n\r\n');

% save of function used
fprintf(fic,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fic,'function name: %s ',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);
%fprintf(fic,'__________________________________________________________________________\r\n');

fclose(fic);

%% end

cd (orig)