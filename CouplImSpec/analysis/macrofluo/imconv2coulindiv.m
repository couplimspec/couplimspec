  function imconv2coulindiv
  %% description
    % conversion d'images pour cr�er les images couleurs infividuelle
    % affecter une r�solution en dpi (dot per inch) pour X et Y 
    % en fonction de la taille du pixel r�elle (en �m en g�n�ral))
    % s'applique aux images tiff provenant du logiciel NIS de Nikon
    
%% input

    %  nothing
    
%% output
    
    %   nothing

%% principe
    % Objectif : 
    %  permettre l'affichage d'images dans powerpoint ou word de mani�re �
    %  afficher les images couleur individuelles de chaque condition de fluorescence
    %  et � respecter la vraie taille des objets sans avoir � rechercher
    %  manuellement le grandissement relatif � appliquer. Apr�s
    %  application du programme, il suffira d'inclure les images. Leur
    %  taille � l'�cran et dans le document seront comparables.
    % de plus les images finales sont de taille environ 1000 pour leur plus
    % petite dimension de mani�re � r�duire la taille des fichiers avec
    % images import�es
    
    % ordre des images au macroflmuo en enregistrement multispectral
    
    %   1   canal bleu excitation vert
    %   2   canal vert excitation vert
    %   3   canal rouge excitation vert
    %   4   canal bleu excitation bleu
    %   5   canal vert excitation bleu
    %   6   canal rouge excitation bleu
    %   7   canal bleu excitation Uv 350
    %   8   canal vert excitation Uv 350
    %   9   canal rouge excitation Uv 350
    %   10  canal bleu excitation Uv 340
    %   11  canal vert excitation Uv 340
    %   12  canal rouge excitation Uv 340
    
    % il faut donc remettre les canaux dans l'ordre pour extraire les
    % images couleur
    
    % 
    % pour chaque image, les m�ta-donn�es des images tiff sont lues par 
    % la fonction matlab imfinfo.m.
    % dans le champ UnkownTags, la deuxi�me valeur correspond � la taille
    % du pixel (d�termin�e exp�rimentalement le 28 f�vrier 2013)
    %
    % ATTENTION : on suppose que la taille est donn�es en micron
    %
    % A partir de cette valeur, une r�solution en dot per inch est
    % d�termin�e de mani�re � ce que cette valeur soit comprise entre 300
    % et 1000. 
    % NB : cette valeur a �t� choisie pour que les images inclues
    % automatiquement dans word ou powerpoint soient environ de 5-10 cm de
    % hauteur.
    
    % pour cela, pour chaque image,  plusieurs valeurs de facteur sont test�es 
    % selon le principe : 
    %               res(facteur)= 25400 / taille_pixel / facteur    
    % la valeur de facteur retenue est celle qui permet d'avoir res compris
    % entre 300 et 1000.
    % res correspond � la r�solution en dpi, 25400 �tant la taille d'un pouce en micron.
    % si plusisuers valeurs sont possible, la plus petites valeurs de dpi
    % est retenue
    
    % l'image est ensuite sauvegard�e avec cette r�solution
    
    % Les valeurs de grandissements correspondantes en affichage de d�part sur papier
    % et sans redimensionnement sont calcul�es : 
    %               taillimage en affichage / taille ech r�elle
    % avec : taillimage en affichage = nbpixel/dpi*25400  (en micron)
    %        taillechreelle = nbpixel * taillpixel        (en micron)
    
    % les tailles de pixel, les tailles des champs de vue en �m et les dpi r�sultants sont
    % enregistr�es dans le fichier taillim.txt du r�pertoire de d�part
    % on enregistre �galement les tailles affich�es en cm
    
    % toutes les images d'un m�me r�pertoire sont trait�es 
    

%% use
    % imconv2coulindiv
 
    
%% Comments
    % 
    
    
%% Author
    % MF Devaux 
    % BIA-PVPP
    
%% date
    % 5 mars 2013
    % 23 janvier 2014 : autre redimensionnement et �criture directe en jpg
    % des images "pour voir"

%% context variables 
orig=pwd;           % returns the current directory

% position des images couleur
% exc 543 nm (vert)
ver=3;
veg=2;
veb=1;

% exc 488 nm (bleu)
blr=6;
blg=5;
blb=4;

% exc 350 nm (uv b)
ubr=9;
ubg=8;
ubb=7;

% exc 340 nm (uv a)
uar=12;
uag=11;
uab=10;

% position de la valeur de taille de pixel dans le champ UnknownTags des
% m�tadonn�es retourn�es par la fonction imfinfo
postpix=2;

% valeurs de facteur test�es
facteur=[1 5 10 50 100 500 1000];
tailleid=1000;

% taille du pouce en micron
tinch=25400;


%% start

if nargin >0
    error('pas de param�tres pour cette fonction. \r\nuse:  % imconv2visu ');
end


%% input

% open first file from folder 
[~,rfolder]=uigetfile({'*.tif'},'nom de la premi�re image � traiter','*.tif');
cd (rfolder)
% liste de simages � traiter
liste=dir('*.tif');
nbim=length(liste); % nombre d'images

% r�pertoire de sauvegarde cr�e dans le r�pertoire contenant
% les images � traiter
if ~exist('voirimcoulindiv','dir')
    mkdir('voirimcoulindiv')
end
cd('voirimcoulindiv');
sfolder=pwd;

if ~exist('imcoulindiv','dir')
    mkdir('imcoulindiv')
end
cd('imcoulindiv');
sfolder2=pwd;


%% treatement
% boucle sur les images
for i=1:nbim
    % r�pertoire de lecture des images
    cd(rfolder)
    % nom de l'image
    nomim=liste(i).name;
    display(nomim)
    
    % lecture de l'image 
    [im]=imread_tiff_multi(nomim);
    
    % taille de l'image et calcul du facteur de r�duction de taille des
    % images
    taillim=size(im(:,:,1));
    %redim= min(round(taillim/100))/10;
    redim=max(taillim)/tailleid;
    
    % lecture des m�ta donn�es 
    md=imfinfo(nomim);
    % taille du pixel � partir de la premi�re image de la s�rie
    tpix=md(1).UnknownTags(postpix).Value;
    
    indice=0;
    
    while ~indice
        % d�termination de la valeur en dpi : test des diff�rents facteurs
        tdpi=zeros(length(facteur),1);
        for j=1:length(facteur)
            tdpi(j)=round(tinch/tpix/facteur(j));
        end;

        % recherche de la valeur de dpi comprise entre 300 et 1000
        indice=tdpi>=300 & tdpi<=1000;
        
        % si on ne trouve pas de bonne valeur de dpi : on r�duit le champ
        % de recherche du facteur
        if ~indice
            nfacteur=find((tdpi-650)<0);
            deb=nfacteur(1);
            pas=(facteur(deb-1)-facteur(deb))/3;
            facteur=facteur(deb):pas:facteur(deb-1);
        end;
    end;

    dpi=tdpi(indice);
    dpi=dpi(length(dpi));
    

    % sauvegarde des images individuelles avec la nouvelle r�solution
    cd(sfolder)
    % image excitation vert 545 nm
    display('vert 545 nm')
    im2w=imresize(im(:,:,[ver,veg,veb]),1/redim)*4;
    imwrite(im2w,strrep(nomim,'.tif','.ve.tif'),'tif','Resolution',[dpi dpi],'compression','none');
    %cd(sfolder2)
    %im2w=im(:,:,[ver,veg,veb])*4;
    %imwrite(im2w,strrep(nomim,'.tif','.ve.tif'),'tif','Resolution',[dpi dpi],'compression','none');
    %cd(sfolder);
    
    % image excitation bleu 488 nm 
    display('bleu 488 nm')
    im2w=imresize(im(:,:,[blr,blg,blb]),1/redim)*4;
    imwrite(im2w,strrep(nomim,'.tif','.bl.tif'),'tif','Resolution',[dpi dpi],'compression','none');
%     cd(sfolder2)
%     im2w=im(:,:,[blr,blg,blb])*16;
%     imwrite(im2w,strrep(nomim,'.tif','.bl.tif'),'tif','Resolution',[dpi dpi],'compression','none');
%     cd(sfolder);

    % image excitation uv b 350 nm
    display('UV 450 nm')
    im2w=imresize(im(:,:,[ubr,ubg,ubb]),1/redim)*4;
    imwrite(im2w,strrep(nomim,'.tif','.ub.tif'),'tif','Resolution',[dpi dpi],'compression','none');
%     cd(sfolder2)
%     im2w=im(:,:,[ubr,ubg,ubb])*16;
%     imwrite(im2w,strrep(nomim,'.tif','.ub.tif'),'tif','Resolution',[dpi dpi],'compression','none');

%     % image excitation uv a 340 nm
 %   cd(sfolder);
    display('UV 340 nm')
    im2w=imresize(im(:,:,[uar,uag,uab]),1/redim)*4;
    imwrite(im2w,strrep(nomim,'.tif','.ua.tif'),'tif','Resolution',[dpi dpi],'compression','none');
%     cd(sfolder2)
%     im2w=im(:,:,[uar,uag,uab])*16;
%     imwrite(im2w,strrep(nomim,'.tif','.ua.tif'),'tif','Resolution',[dpi dpi],'compression','none');
%     cd(sfolder);
    
    % enregistrement des tailles de pixels, de champ de vue et valeurs de dpi
    d.d(i,1)=tpix;                  % taille du pixel en micron
    d.d(i,2)=tpix*size(im,1);       % taille du champ de vue en micron en X
    d.d(i,3)=tpix*size(im,2);       % taille du champ de vue en micron en Y
    d.d(i,4)=dpi;
    
    % enregistrement des tailles affich�es en cm et du grandissement �
    % l'affichage
    d.d(i,5)=size(im,1)/dpi*2.54;   % taille de l'image en X affich�e en cm   
    d.d(i,6)=size(im,2)/dpi*2.54;   % taille de l'image en Y affich�e en cm  
    % grandissement initial de l'affichage
    d.d(i,7)=d.d(i,5)*10*1000/d.d(i,2);
     
end;
    
%% save*
% fichiers des tailles
d.i=char(liste.name);
d.v=char('tailpix','taillimX','taillimY','dpi','taillimaffX','taillimaffY','grandissement');

cd(sfolder)
ecrire(d,'taillim');
    
   
%% matlab function tracking  
fid=fopen('imconv2coulindiv.track.txt','w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t\r\n',datestr(now,0));
fprintf(fid,'sauvegarde des images couleur indivieulle de chaque conditions d''excitation du macrofluo\r\n');
fprintf(fid,'_________________________________________________________________________________________\r\n');

fprintf(fid,'\r\nr�pertoire de lecture des images : %s\r\n',rfolder);
fprintf(fid,'\r\nr�pertoire de sauvegarde des images : %s \r\n',sfolder);

fprintf(fid,'\r\nsauvegarde des images excitation dans le vert 545 nm avec l''extension .ve.tif\r\n')
fprintf(fid,'sauvegarde des images excitation dans le bleu 488 nm avec l''extension .bl.tif\r\n')
fprintf(fid,'sauvegarde des images excitation dans l''UV 350 nm avec l''extension .ub.tif\r\n')
fprintf(fid,'sauvegarde des images excitation dans l''UV 340 nm avec l''extension .ua.tif\r\n')

fprintf(fid,'\r\nATTENTION : les valeurs de niveaux de gris ont �t� multipli�es par 16 pour affichage\r\n');
fprintf(fid,'\r\nATTENTION : on suppose que les tailles de pixel trouv�es sont en micron\r\n');

fprintf(fid,'\r\n\r\nImages converties : \r\n');
for i=1:nbim
    fprintf(fid,'\r\n%s \r\n',liste(i).name);
end

fprintf(fid,'\r\n\r\nfichier des tailles de pixel, champs de vue r�el (�m) et affich� (cm), valeur de dpi, grandissement initial de l''affichage : \r\n');
fprintf(fid,'\t - %s\r\n','taillim.txt');

% save of function used
fprintf(fid,'\r\n\r\n__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    