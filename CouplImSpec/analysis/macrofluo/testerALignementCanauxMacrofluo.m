  function testerALignementCanauxMacrofluo
%

 %% description
    % test si les images du macrofluo de NAntes sont lign�es ou non
    
%% input

    %  interactive 
    
%% output
    
    %   save a data file with position of template in the four images of
    %   macrofluo

%% principe
    % work on dataset
    % calculate the max of the three RGB channels => four images
    % representative of each filter
    % select at random 100 squared template 
        % in one of the four images : at random
        % of a random size ranging between 200 to 500  pixels wide
    % calculate the cross correlation
    % calculate the coordinates in the images
    % save template
    % save result for all templates in a txt file
    
%% use
    % testerALignementCanauxMAcrofluo
    
    
%% Comments
    % 
    
    
%% Author
    % MF devaux
    % BIA - PVPP
    
%% date
    % 23 janvier 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >0
    error('use: testerALignementCanauxMacrofluo');
end


%% input
% select dso file from folder 
[name,rfolder]=uigetfile({'*.dso'},'macrofluo dso image','*.dso');
cd (rfolder)
tmp=load(name);
dso=getfield(tmp,char(fieldnames(tmp)));
clear tmp;
if ~strcmp(dso.userdata.method,'MACROFLUO')
    error('dso %s does not seem to be a macrofluo image',name);
end
    
%save result file
sname=strrep(name,'dso.mat','');
[sname,sfolder]=uiputfile({'*.txt'},'save result file',[sname,'testAlignementCanauxMacrofluo.txt']);


%% treatement
% extract multivariate image from dso
if dso.imagemode
    sim=dso.imagedata;
else
    error('dso %s is not an image',name);
end

if size(sim,3)~=12
    error('function written for a 12 channel image : ua, ub, bl and ve. It should be adapted for anything else...')
end

% assess max image of each individual image
ua=sim(:,:,1:3);
ub=sim(:,:,4:6);
bl=sim(:,:,7:9);
ve=sim(:,:,10:12);

maxim{1}=max(ua,[],3);
maxim{2}=max(ub,[],3);
maxim{3}=max(bl,[],3);
maxim{4}=max(ve,[],3);

% size of max images
nl=size(ua,1);
nc=size(ua,2);

% go to save folder
cd(sfolder)

% template loop
for i=1:100
    disp(i);
    
    % select one of the four image at random
    nim=randi(4,1);
    im=maxim{nim};
    
    % select size of template
    stemp=200+randi(300,1);
    
    % select initial position of template
    ilt=randi(nl-stemp,1);
    ict=randi(nc-stemp,1);
    
    % save initial image number and position
    res1=[stemp,nim,ilt,ict];
    
    % select template
    temp=im(ilt:(ilt+stemp-1),ict:(ict+stemp-1));
    
    % save template
    if i<=9
        imwrite(temp,strcat(strrep(name,'.dso.mat',''),'00',num2str(i),'.tif'),'tif','compression','none');
    else
        if i<=99
              imwrite(temp,strcat(strrep(name,'.dso.mat',''),'0',num2str(i),'.tif'),'tif','compression','none');
        else
              imwrite(temp,strcat(strrep(name,'.dso.mat',''),num2str(i),'.tif'),'tif','compression','none');
        end
    end


    % calculate the location of template in the four image : based on cross
    % correlation
    for j=1:4
        cc=normxcorr2(temp,maxim{j});
        cc=cc(stemp:nl,stemp:nc);
        [il,ic]=find(cc==max(cc(:)));
        
        % save result
        res1=[res1, il,ic];
    end
    res.d(i,:)=res1;
end
    


%% save 
res.i=(1:100)';

res.v=char('sizeTemp','imOrig','ilOrig','icOrig','il1','ic1','il2','ic2','il3','ic3','il4','ic4');

ecrire(res,sname);


%% matlab function tracking  

fid=fopen(strrep(sname,'.txt','.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end; 

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Test macrofluo image alignment\r\n');
fprintf(fid,'_______________________________\r\n');

fprintf(fid,'\r\ninput file name: %s \r\n',name);
fprintf(fid,'data folder: %s\r\n',rfolder);
 
fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

fprintf(fid,'\r\nBrief description: \r\n');
fprintf(fid,'\r\n\t - calculate the max of the three RGB channels of each individual macrofluo images');
fprintf(fid,'\r\n\t\t - 1,2,3 : UA filter in BGR order');
fprintf(fid,'\r\n\t\t - 4,5,6 : UB filter in BGR order');
fprintf(fid,'\r\n\t\t - 7,8,9 : BL filter in BGR order');
fprintf(fid,'\r\n\t\t - 10,11,12 : VE filter in BGR order');
fprintf(fid,'\r\n\t - select at random 100 squared template:');
fprintf(fid,'\r\n\t\t - in one of the four images : at random');
fprintf(fid,'\r\n\t\t - of a random size ranging between 200 to 500  pixels wide');
fprintf(fid,'\r\n\t - find template in each of the 4 max images');
fprintf(fid,'\r\n\t\t - calculate cross correlation');
fprintf(fid,'\r\n\t\t - find the coordinates of max correlation in the images');
fprintf(fid,'\r\n\t - save template');
fprintf(fid,'\r\n\t - save result for all templates in a txt file');
fprintf(fid,'\r\n\r\n');

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    