  function testerALignementCanaux
%

 %% description
    % test si les images du macrofluo de NAntes sont lign�es ou non
    
%% input

    %  interactive 
    
%% output
    
    %   save a data file with position of template in the four images of
    %   macrofluo

%% principe
    % work on dataset
    % calculate the max of the three RGB channels => four images
    % representative of each filter
    % select at random 100 squared template 
        % in one of the four images : at random
        % of a random size ranging between 200 to 500  pixels wide
    % calculate the cross correlation
    % calculate the coordinates in the images
    % save result for all templates in a txt file
    
%% use
    % testerALignementCanaux
    
    
%% Comments
    % 
    
    
%% Author
    % MF devaux
    % BIA - PVPP
    
%% date
    % 23 janvier 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: testerALignementCanaux');
end


%% input


%% treatement


%% save 


%% matlab function tracking  

fid=fopen(strcat(name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Import data set object from LAbspec *.spc map file \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

% fprintf(fid,'\r\ninput file name: %s%s\r\n',name,ext);
% fprintf(fid,'data folder: %s\r\n',pathname);
% 
% fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
% fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    