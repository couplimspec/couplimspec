  function imCoulEtIndiv2
%
%% DESCRIPTION
% Creates new images for easier  and manipulation of multispectral images
% from the macroscope
%
%% INPUT
% 
%% OUTPUT
%
%% PRINCIPE
% Creates a subfolder with images of mean values from all R/G/B channels
% And a second subfolder with images of each filter
% All images are resized, the resolution is halved
% 
%% USE
% 
%% COMMENTS
%
%% AUTHOR
% Mathias Corcel
% IATE Montpellier / BIA-PVPP Nantes
%
%% DATE
% 19/05/2015 : cr�ation de la fonction

%% context variables 
orig=pwd;           % returns the current directory

%% start

%% input
[~, Path]=uigetfile({'*.tif'},'nom de l''image','*.tif'); % Manual selection of an image from the folder
cd(Path)
list=dir('*.tif'); % Listing of all images in original folder

% r�pertoire de sauvegarde cr�e dans le r�pertoire contenant
% les images � traiter
if ~exist('imCouleur','dir')
    mkdir('imCouleur')
end
cd('imCouleur');
sfolderCouleur=pwd;

cd ..



% r�pertoire de sauvegarde cr�e dans le r�pertoire contenant
% les images � traiter
if ~exist('imCoulIndiv','dir')
    mkdir('imCoulIndiv')
end
cd('imCoulIndiv');
sfolderIndiv=pwd;

cd ..




%% treatement
[a,b] = size(list);
for j=1:a
    cd(Path)

    % Get the number of channels
    r=imfinfo(list(j).name);
    nbim=length([r.FileSize]); 
    
    disp(list(j).name)
    
    if nbim == 9   % Only treat images with 12 channels, ignores brightfield images
        
        % Import and resize the image
        tmp=imread(list(j).name,'tif');
        [nblig,nbcol]=size(tmp);
        clear tmp;
        imtmp = uint16(zeros(nblig,nbcol));
        im = imresize(imtmp, 1);
        im = repmat(im,1,1,nbim);
        for i=1:nbim
            fprintf('%d',i)
            imtmp=imread(list(j).name,i);
            im(:,:,i) = imresize(imtmp, 1);
        end
        fprintf('\r\n');
        
        % Mean image
       % imR = cat(3,im(:,:,3),im(:,:,6),im(:,:,9),im(:,:,12));
        imR = cat(3,im(:,:,3),im(:,:,6),im(:,:,9));
        imR = mean(imR,3)/16383; % Mean of red channels
       % imG = cat(3,im(:,:,2),im(:,:,5),im(:,:,8),im(:,:,11));
       % imG = cat(3,im(:,:,5),im(:,:,8),im(:,:,11));
        imG = cat(3,im(:,:,2),im(:,:,5),im(:,:,8));
        imG = mean(imG,3)/16383; % Mean of green channels
        %imB = cat(3,im(:,:,1),im(:,:,4),im(:,:,7),im(:,:,10));
        imB = cat(3,im(:,:,4),im(:,:,7));
        imB = mean(imB,3)/16383; % Mean of blue channels 
        
        imRGB = cat(3,imR,imG,imB);
        imRGB = im2uint8(imRGB);
        
        %imRGB = uint8(cat(3,imR,imG,imB)*255);
        %imRGB = im2uint8(imRGB);
        %imRGB = imadjust(imRGB, [min(imRGB(:)) min(imRGB(:)) min(imRGB(:)); max(imRGB(:)) max(imRGB(:)) max(imRGB(:))],[]); % Adjust to scale on uint16
        
        % Images of each filters
        fact=65536/16384;
       % imG2A = im(:,:,[3 2 1])*fact;
       % imG2A = im2uint8(imG2A);
       % imB2A = im(:,:,[6 5 4])*fact;
        imB2A = im(:,:,[3 2 1])*fact;
        imB2A = im2uint8(imB2A);
       % imUV1A = im(:,:,[9 8 7])*fact;
        imUV1A = im(:,:,[6 5 4])*fact;
        imUV1A = im2uint8(imUV1A);
       % imUV2A = im(:,:,[12 11 10])*fact;
        imUV2A = im(:,:,[9 8 7])*fact;
        imUV2A = im2uint8(imUV2A);
        
        % Set path to first subfolder
        cd('imCouleur')
        
        % Write mean image
        disp('image couleur moyenne')
        imwrite(imRGB , strcat('c', list(j).name),'compression','none')
        
        cd ..
        
        % Set path to second subfolder
        cd('imCoulIndiv')
        disp('images couleur individuelles')
        % Write filter images
   %     imwrite(imG2A, [strcat('c', list(j).name(1:end-4), 'VE.tif')],'tif','compression','none')
        imwrite(imB2A, [strcat('c', list(j).name(1:end-4), 'BL.tif')],'compression','none')
        imwrite(imUV1A, [strcat('c', list(j).name(1:end-4), 'UB.tif')],'compression','none')
        imwrite(imUV2A, [strcat('c', list(j).name(1:end-4), 'UA.tif')],'compression','none')
        
        % Back to original folder for next image
        cd(Path)
        
        clear im imtmp imR imG imB imRGB imG2A imB2A imUV2A imUV1A nbcol nbim nblig r
    end
end


%% save

%% matlab function tracking

%% end
cd (orig)
    