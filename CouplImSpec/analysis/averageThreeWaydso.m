  function dso2=averageThreeWaydso(dso3)

 %% compute the average 2 way dso data table from a three way dso data table
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% input

    %   dso3 : dso image 3 way data table
 
    %
    % or nothing
    
%% output
    
     %  dso2 : dso image 2 way data table

%% principle
    % 
    % take into account the fact that some stacks have missing values (==0)
    %


%% use
    % dso2=averageThreeWaydso(dso3)
    
%% Comments
    % written for Dhivyaa Rajasundaram stay in Nantes (Walltrac project)
    
    
%% Author
    % MF Devaux and D Rajasundaram
    % BIA-PVPP INRA Nantes
    
%% date
    % 15 april 2014 : take into account data and image mode
    % 7 april 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if (nargin ~=1) && (nargin~=0)
    error('use: dso2=averageThreeWaydso(dso3) or dso2=averageThreeWaydso');
end


%% input
if nargin == 0
    [fname,pathname]=uigetfile({'*.dso.mat'},'name of 3 way dso file to average','*.dso.mat');
    cd(pathname)
    
    dso3=loaddso(fname);
    

end

%% treatement
% size of the three way data table
s1=size(dso3.data,1);
s2=size(dso3.data,2);
% number of spectral variable
nbsp=size(dso3.data,3);

% loop according to the first size
for i=1:s1
    % recover the data
    data=squeeze(dso3.data(i,:,:));
    
    
    % check for 0 values
    fnonzero=find(~sum(data,2)==0);
    
    % average
    mdata(i,:)=mean(data(fnonzero,:));
end;
    
    % 


% built dso3dso=dataset;
dso2=dataset;
dso2.name=strcat(dso3.name,'Average');
dso2.author=dso3.author;
dso2.data=mdata;
if dso3.imagemode
    dso2.type='image';
    dso2.imagesize=dso3.imagesize;
else
    dso2.type='data';
end

dso2.axisscale{1}=dso3.axisscale{1};
dso2.axisscale{2}=dso3.axisscale{3};

dso2.title{1}='pixel';
dso2.label{1}=dso3.label{1};

dso2.label{2}=dso3.label{3};
dso2.title{2}='Wavenumber';

dso2.description=dso3.description;

dso2.userdata=dso3.userdata;



%% save 
if ~exist('average','dir')
    mkdir('average')
end

cd('average')
sfolder=pwd;
savedso(dso2)
%% matlab function tracking  

fid=fopen(strcat(dso2.name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'compute average of 3 way dso file \r\n');
fprintf(fid,'___________________________________\r\n');

fprintf(fid,'\r\ninput dso file name: %s\r\n',fname);
fprintf(fid,'data folder: %s\r\n',pathname);


fprintf(fid,'\r\nsaved file name : %s.dso.mat \r\n',dso2.name);
fprintf(fid,'data folder: %s\r\n',sfolder);

% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    