function [vp,vl,co]=acpimdsocalc(dso,nbcp,scale)
% Analyse en composantes principales du tableau de donn�es dso
% Analyse centr�e
%
%% input
%   tableau de don�nes au format dso
%   nbcp : number of components to compute
%   Scale : option de normalisation des variables 1= oui, 0 = non (d�faut)
%   
%
%
%% output 
%   vp : vecteurs propres au format div
%   vl : tableau  des valeurs propres au format div
%   co : composantes pricniaples au format dso
%
%% principe 
%   la fonction acpimdsocalc a �t� d�velopp�e pour etre appel�e depuis la fonction interactive acpimdsogui
%   elle est �crite en prenant en compte les dimensions du tableau des
%   donn�es
%   une tansposition du tableau est effectu�e si le nombre de variable est
%   grand (>50) et sup�rieur au nombre d'individus.
%
%   la fonction a �t� d�velopp�e pour obtenir les graphiques de travail usuels
%   d'une analyse en composantes principales d'images multivari�es
%
%   Le programme constitue automatiquement les graphiques de :
%		les cartes des composantes 1 et 2 
%								   3 et 4
%       les images composantes principales de 1 � 4 si le tableau dso est
%       une image
%		les profils des variables pour les composantes 1, 2, 3, 4 si les variables sont des nombres
%       les cartes des variables et les cercles des corr�lations si les variables sont des chaines de caract�re
%
% 
%% USAGE : [vp,vl,co]=acpimdsocalc(dso,nbcp,scale)
%
%% comments
% d'apr�s acpguicalc
%
%% Auteur : MF Devaux
%		   BIA - PVPVV
%
%% date
%   13 septembre 2011


%% context variables 

%% start
% test du nombre de param�tres
if nargin ==0 || nargin >3
    error('USAGE : [vp,vl,co]=acpimdsocalc(dso,<nbcp>,<scale>)')
end;
if ~strcmp(class(dso),'dataset')
    error('enter dataset object as first argument')
end;
if (isempty(scale))
    scale=0;
end;
if nargin == 1
    nbcp=min([10,size(dso.data)]);
end;


%% input
% included variables
don=dso.data(:,dso.include{2});

%% treatment
% centrage de la matrice de donn�es
cdon=double(don)-repmat(mean(don),size(don,1),1);

% sacle
if scale
    %normalisation des variables = division par l'�cart-type des variables
    et=sqrt(var(cdon));
    cdon=cdon*diag(1./et);
end;

% analyse en composantes principales
%
% renvoie : 
%   coord=coordonn�es
%   vecpro : vecteur propres (loadings)
%   valpro : valeurs propres

%calcul de la matrice de variance covariance V
if (size(don,2)>50)&&(size(don,2)>size(don,1))
    % transposition du calcul pour diagonaliser une matrice plus petite
    % cas o� le nombre de variable est plus grand que le nombre d'individu
    % et > � 50
    transp=1;
    V=cdon*cdon';           % V est de dimension nind x nind
else
    % pas de transposition
    transp=0;
    V=cdon'*cdon;           % V est de dimension nvar x nvar
end;

% division par la nombre d'individu-1 pour avoir une covariance
V=V/(size(don,1)-1);

% diagonalisation de la matrice de variance covariance � l'aide de la fonction matlab svd (singluar value
% decomposition. A ce stade coord=vecpro car la matrice V est sym�trique
% et vl est la matrice diagonale desvaleurs propres
[vp.d,vl,coord]=svd(V);

% constitution du tableau des valeurs propres <valpro>
% premi�re colonne : valeur propre
% deuxi�me colonne : pourcentage d'inertie
% troisi�me colonne : pourcentage cumul�
valpro(:,1)=diag(vl);
clear vl;
valpro(:,2)=valpro(:,1)*100/sum(valpro(:,1));
for i=1:size(valpro,1)
    valpro(i,3)=sum(valpro(1:i,2));
end;
vl.d=valpro;
vl.v=char({'valpro', '%inertie' '%cumul�e'});
vl.i=char('cp1');
for i=2:size(valpro,1)
   vl.i=char(vl.i, sprintf('cp%d',i));
end;
clear valpro;

% calcul des vecteurs propres
if transp
    % dans ce cas, les vecteurs propres doivent etre recalcul�s � partir de
    % coord
    vp.d=cdon'*coord;
    % normalisation des vecteurs propres : afin que vecpro*vecpro= matrice
    % identit�. les nbval premiers sont retenus
    for i=1:size(vp.d,2);
        vp.d(:,i)=vp.d(:,i)/sqrt(sum(vp.d(:,i).*vp.d(:,i)));
    end;
else
    % les vecteurs propres sont un r�sultat direct de la d�composition en
    % valeurs singuli�res de la matrice de variance covariance
    % les nbval premiers sont retenus
end;
vp.d=vp.d(:,1:nbcp);
vp.i=dso.label{2};

if nbcp<10
    vp.v=[repmat('cp',nbcp,1) num2str((1:nbcp)')];
else if nbcp<100
        vp.v=[repmat('cp0',9,1) num2str((1:9)')];
        vp.v=char(vp.v,[repmat('cp',(nbcp-9),1) num2str((10:nbcp)')]);
    else
        vp.v=[repmat('cp00',9,1) num2str((1:9)')];
        vp.v=char(vp.v,[repmat('cp0',(90),1) num2str((10:99)')]);
        vp.v=char(vp.v,[repmat('cp',(nbcp-99),1) num2str((100:nbcp)')]);
    end
end


%% calcul des coordonn�es � partir des vecteurs propres
% dso: data set object
co=dataset;
co.name=strcat(dso.name,'.cp');
co.author=dso.author;

co.data=cdon*vp.d;

co.axisscale{1}=dso.axisscale{1};
co.axisscale{2}=1:nbcp;

co.title{1}='pixel';
co.title{2}='composante principale';

co.type='image';
co.imagemode=1;
co.imagesize=dso.imagesize;

co.label{1}=dso.label{1};
co.label{2}=vp.v;

userdata=dso.userdata;
if ~isfield(userdata,'treatment')
    userdata.treatment='acpimdso';
else
    userdata.treatment{length(userdata.treatment)+1}=char('acpimdso');
end;
co.userdata=userdata;


