function varargout = acpimdsogui(varargin)
%% principal component analysis of dso multivariate image 
%
%
%% input

    %  nothing : GUI
    
%% output
    
    % 

%% principe
    % 
    % 

%% use
    % acpimdsogui
    
%% Comments
    % cadre TH�se Fatma Allouche
% 
%
% 
    
%% Author
    %  MF Devaux
    % BIA -PVPP
    
%% date
    % 5 mai  2011
    
%%
%ACPIMDSOGUI M-file for acpimdsogui.fig
%      ACPIMDSOGUI, by itself, creates a new ACPIMDSOGUI or raises the existing
%      singleton*.
%
%      H = ACPIMDSOGUI returns the handle to a new ACPIMDSOGUI or the handle to
%      the existing singleton*.
%
%      ACPIMDSOGUI('Property','Value',...) creates a new ACPIMDSOGUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to acpimdsogui_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      ACPIMDSOGUI('CALLBACK') and ACPIMDSOGUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in ACPIMDSOGUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help acpimdsogui

% Last Modified by GUIDE v2.5 20-Sep-2011 16:13:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @acpimdsogui_OpeningFcn, ...
                   'gui_OutputFcn',  @acpimdsogui_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before acpimdsogui is made visible.
function acpimdsogui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for acpimdsogui
handles.output = hObject;

% logo

%logo=imread('\\grandlieua\pvpp\04_outils_logiciels\matlab\mfd\matadim2.tif');
%axes(handles.logo)
%imshow(logo,[])

% r�pertoire de d�part
porig=pwd;

% stockage des donn�es en m�moire
set(handles.figure1, 'UserData', {porig});

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes acpimdsogui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = acpimdsogui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_don.
function load_don_Callback(hObject, eventdata, handles)
% hObject    handle to load_don (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% r�cuperation des donn�es
data=get(handles.figure1,'UserData');
if length(data)==1;
    porig=data{1};
else
    rfolder=data{3};
    porig=data{5};
    clear data;
    cd(rfolder)
end;

% lecture de l'image
disp('lecture de l''image....')
[fname,rfolder]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');
ext='.dso.mat';
nomgen=strrep(fname,ext,'');
cd(rfolder)
dso=load(fname);
im=getfield(dso,char(fieldnames(dso)));
clear dso;


% affichage de l'image somme des intensit�s
imorig=sum(im.imagedata,3);
set(handles.figure1,'CurrentAxes',handles.imsum)
imshow(imorig,[]);

ho=findobj('Tag','nomgen_acpim');
set(ho,'string',nomgen);

% mise � jour de la liste des images
h=findobj('Tag','sel_var_acpim');
affiche=strcat(num2str((1:size(im.data,2))'),' : ',im.label{2});
set(h,'string',affiche);
set(h,'max',size(im.data,2));
set(h,'value',1:size(im.data,2));

% stockage des donn�es en m�moire
set(handles.figure1, 'UserData', {im,nomgen,rfolder,imorig,porig});
zoom out

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% r�cup�rartion des don�nes
data=get(handles.figure1,'UserData');
im=data{1};
nomgen=data{2};
rfolder=data{3};
porig=data{5};

h=findobj('Tag','sel_var_acpim');
selvar=get(h,'value');

im.include{2}=selvar;

% nombre de composantes � calculer
nbcp=str2num(get(handles.acpimdso_nb_cp,'string'));
if isempty(nbcp)
    rep=inputdlg('enter the number of components to compute','nbcp',1,{'10'});
    nbcp=str2num(char(rep));
    set(handles.acpimdso_nb_cp,'string',num2str(nbcp))
end;

scale=logical(get(findobj('string','scale_acpimdso'),'Value'));


[vp,vl,dsosco]=acpimdsocalc(im,nbcp,scale);

% affichage des valeurs propres dans le bloc de texte de la fenetre acp
textvl=sprintf('num�ro valeurs propres %% d''inertie %% cumul�\r\n\r\n');
for i=1:min(size(vl.d,1),20)
    textvl=char(textvl,sprintf('     %2d         %8.2f        \t%5.2f\t   %6.2f',i,vl.d(i,:)));
end;

set(findobj('tag','valpro_acpimdso'),'string',textvl)

% make dso for loadings
dsoload=im(1:nbcp,:);
dsoload.name=strcat(dsoload.name,'.vp');
dsoload.data=vp.d';
dsoload.imagemode=0;
dsoload.label{1}=vp.v;
dsoload.axisscale{1}=(1:nbcp)';
dsoload.title{1}='principal component';
dsoload.include{2}=1:size(dsosco,2);



% graphiques
[coa,hvp,hco]=acpimdsograph(dsoload,dsosco,vl,im,scale);


%% sauvegarde des r�sultats
cd(rfolder)
if ~exist('acpim','dir')
    mkdir('acpim');
end;
cd('acpim');
repsauve=pwd;

gensauve=nomgen;

% save results
% components
save(strcat(gensauve,'.co.dso.mat'),'co');
for i=1:min(4,size(coa,3))
    imwrite(coa(:,:,i),strcat(gensauve,'.cp',num2str(i),'.tif'),'tif','compression','none');
end;
imwrite(coa(:,:,1:3),strcat(gensauve,'.cp123.tif'),'tif','compression','none');
if size(coa,3)>=4
    imwrite(coa(:,:,2:4),strcat(gensauve,'.cp234.tif'),'tif','compression','none');
end;

% veceturs propres
ecrire(vp,strcat(gensauve,'.vp.txt'));
%valeurs propres
ecrire(vl,strcat(gensauve,'.vl.txt'));

%graphiques
saveas(hco(1),strcat(gensauve,'.cp12.tif'),'tif');
if size(coa,3)>=4
    saveas(hco(2),strcat(gensauve,'.cp34.tif'),'tif');
end;
if length(hvp)==1
    saveas(hvp(1),strcat(gensauve,'.vp.tif'),'tif');
else
    saveas(hvp(1),strcat(gensauve,'.vp12.tif'),'tif');
    saveas(hvp(2),strcat(gensauve,'.vp34.tif'),'tif');
end;

%  summary  and track
if (fopen(sprintf('%s.acp.txt',gensauve),'w')==-1)
    error('probl�me d''ouverture en �criture du fichier %s.acp.txt ',gensauve)
end;

% fichier texte 
fic=fopen(sprintf('%s.acp.txt',gensauve),'w');
fprintf(fic,'%s\r\n\r\n',date)
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n');
fprintf(fic,'-                              ANALYSE EN COMPOSANTES PRINCIPALES : DSO                            -\r\n');
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n');

if scale
    fprintf(fic,'\r\nanalyse centr�e et norm�e\r\n');
else
    fprintf(fic,'\r\nanalyse centr�e\r\n');
end;

fprintf(fic,'\r\nr�pertoire : %s\r\n',rfolder);

fprintf(fic,'\r\nfichier : %s\r\n',nomgen);

fprintf(fic,'\r\nnombre d''individus : %d\r\n',size(im.data,1));
fprintf(fic,'nombre de variables : %d\r\n',size(im.data,2));


fprintf(fic,'\r\n------------------------------------------------------------------------------------------------\r\n')
fprintf(fic,'Variance des premi�res composantes principales\r\n');
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n')
fprintf(fic,'\r\nComposante   Valeurs propres     %% de variance  %% cumul�\r\n');
for (i=1:min(20,size(vl.d,1)))
    fprintf(fic,'   %2d\t\t   % 9.3f\t      %5.2f\t\t %6.2f\r\n',i,vl.d(i,1),vl.d(i,2),vl.d(i,3));
end;


fprintf(fic,'\r\n\r\n\r\n------------------------------------------------------------------------------------------------\r\n')
fprintf(fic,'r�pertoire de sauvegarde des r�sultats : %s\r\n',repsauve);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n')
fprintf(fic,'\r\nFichiers sauvegard�s :\r\n\r\n');
fprintf(fic,'\t- composantes principales (ou scores) - format dso : ')
fprintf(fic,'%s.co.dso.mat\r\n',gensauve)

fprintf(fic,'\r\n\t- fichiers des donn�es vecteurs propres (ou loadings) - format div : ')
fprintf(fic,'%s.vp.txt\r\n',gensauve)
fprintf(fic,'\t- fichiers des valeurs propres (ou eigenvalues) - format div : ')
fprintf(fic,'%s.vl.txt',gensauve)


% save function used
fprintf(fic,'\r\n------------------------------------------------------------------------------------------------\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fic,'function name: %s \r\n',mfilename);
res=dir(info);
fprintf(fic,'on %s \r\n',res.date);
fprintf(fic,'function folder: %s \r\n',repprog);

% sauvegarde du programme de calcul utilis�
prog=which('acpimdsocalc.m');
prop=dir(prog);
dateprog=prop.date(1:11);

fprintf(fic,'\r\n\r\n\r\n------------------------------------------------------------------------------------------------\r\n')
fprintf(fic,'programme de calcul : %s du %s\r\n',prog,dateprog);
fprintf(fic,'------------------------------------------------------------------------------------------------\r\n\r\n\r\n\r\n')

fclose(fic);
fclose all

% retour au r�pertoire de d�part
cd(porig)







% --- Executes during object creation, after setting all properties.
function sel_var_acpim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sel_var_acpim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






% --- Executes during object creation, after setting all properties.
function acpimdso_nb_cp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acpimdso_nb_cp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in scale_acpimdso.
function scale_acpimdso_Callback(hObject, eventdata, handles)
% hObject    handle to scale_acpimdso (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of scale_acpimdso


% --- Executes on button press in acpimdso_drawroi.
function acpimdso_drawroi_Callback(hObject, eventdata, handles)
% hObject    handle to acpimdso_drawroi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% choix de la zone d'interet---------------------------------------------------
data=get(handles.figure1,'UserData');
im=data{1};
imorig=data{4};
nomgen=data{2};
rfolder=data{3};
porig=data{5};

figure('Numbertitle', 'off','name','S�lection d''une zone d''int�r�t') 
imaff=ajusterimage(imorig);
imshow(imaff);

% s�lection de la zone d'int�r�t
[zone,x,y]=roipoly(imaff);

hf=findobj('Tag','imsum');
figure(hf)
hold on
plot(x,y,'r','linewidth',2);


% save Roi
if ~exist('roi','dir')
    mkdir('roi')
end;
cd('roi')

imwrite(zone,strcat(nomgen,'.roi.tif'),'tif','compression','none')

% handles
% stockage des donn�es en m�moire
set(handles.figure1, 'UserData', {im,nomgen,rfolder,imorig,porig,zone});


% Update handles structure
guidata(hObject, handles);




% --- Executes on button press in acpimdso_load_roi.
function acpimdso_load_roi_Callback(hObject, eventdata, handles)
% hObject    handle to acpimdso_load_roi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~exist('roi','dir')
    warndlg('pas de r�pertoire ''roi'', s�lection interactive de roi')
    acpimdso_drawroi_Callback(hObject, eventdata, handles)
end;

data=get(handles.figure1,'UserData');
im=data{1};
imorig=data{4};
nomgen=data{2};
rfolder=data{3};
porig=data{5};

cd(rfolder)
cd('roi')
if ~exist(strcat(nomgen,'.roi.tif'),'file')
    warndlg('pas de ROI de nom g�n�rique %s',nomgen)
    acpimdso_drawroi_Callback(hObject, eventdata, handles)
end;
    
zone=imread(strcat(nomgen,'.roi.tif'));

axes(handles.imsum)
plot(x,y,'r','linewidth',2);

% handles
% stockage des donn�es en m�moire
set(handles.figure1, 'UserData', {im,nomgen,rfolder,imorig,porig,zone});


% Update handles structure
guidata(hObject, handles);
