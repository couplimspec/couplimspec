function  gpe=acp_init_classifim(tabim,zone,cp)
% r�alise l'initialisation de la s�paration de la population initiale en deux groupes
% Le r�sultat est une initialisation de partition : les donn�es doivent ensuite �tre
% trait�e par la m�thode d'agr�gation autour des centres mobiles pour obtenir 
% les groupes effectifs.
%
% param�tres d'entr�e
% tabim : matrice de donn�es dont il faut r�aliser la division en deux groupes
% zone : image binaire d�finissant la zone s�lectionn�e sur l'image en fond clair
% cp : num�ro de la composante principale prise en compte pour initialiser
% les deux groupes
%
% param�tres de sortie
% gpe : matrice logique/binaire qui d�finit les 2 groupes de la matrice
% tabim
%
% Usage :
%       im=lire_sim('monimage');
%       nlig=size(im,1)
%       ncol=size(im,2);
%       nbim=size(im,4);
%       zone=roipoly(im(:,:,:,1));
%       for i=1:nbim
%           tmp=im(:,:,:,i);
%           tabim(:,:,i)=tmp(zone);
%       end;
%       tabim=double(tabim);
%
%       gpe=acp_init_classifim(tabim,zone,1);
%
% Auteur: S�verine Muret
%				URPOI-MicMac
% Version du 12/04/00
% 
% revue par MF Devaux et B Bouchet
%   URPOI-Parois
%   23 septembre 2003
% version du 16 septembre 2004 pour la prise en compte de la composante cp

if (nargin~=3)&(nargin~=2)
    errordlg('USAGE : gpe=acp_init_classifim(tabim,zone,<cp : optionnel>);');
    return;
end;

if nargin==2
    cp=1;
end;

npix=size(tabim,1);

% initialisation de la classification en deux groupes
% ACP centr�e
moy=mean(tabim);	 % calcul des moyennes des variables
Totale=(double(tabim)'*double(tabim))/npix-(moy'*moy);	 % calcul de la matrice variance-covariance
[co,vl,vp]=svd(Totale); 	% calcul des param�tres de l'ACP
co=double(tabim)*vp-repmat(moy,npix,1)*vp;	 % calcul des composantes principales

% Affichage de la premi�re composante principale
% initialisation d'une matrice de z�ros de dimension de l'image �tudi�e
vzo=zeros(size(zone));
% repr�sentation dans une image des pixels de la zone d'int�r�t
vzo(zone)=co(:,cp);
% affichage 
figure('numbertitle','off','name','premi�re composante principale, (analyse partielle)','units','normalized','position',[0 0 0.45 0.45]);
subplot(1,2,1)
imshow(vzo,[]);

subplot(1,2,2)
plot(1:size(tabim,2),vp(:,cp));
hold on
plot([1 size(tabim,2)],[0 0 ],':k');
xlabel('num�ro d''ordre de l''image')
ylabel('unit� arbitraire')
hold off

% 2 groupes : groupes initiaux 
gpe(co(:,cp)>0)=1;	 % d�termination du groupe 1
gpe(co(:,cp)<0)=2;	 % d�termination du groupe 2
gpe=gpe';            % on veut que gpe soit un vecteur colonne
