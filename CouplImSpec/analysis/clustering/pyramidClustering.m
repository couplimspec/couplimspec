function [nC_conserve,gpe_conserve, trB, trBWm1]= pyramidClustering(initCentre)
%% description
    % multiscale clustering of spectral images
    
%% input

    %  imc : image couleur 
    
%% output
    
    %   im : image monochrome de meem type que l'image d'entree

%% principe
    % les images sont converties en monochrome selon 3 possibilites
        % choix d'un des trois canaux RGB
        % utilisation de la fonction rgb2gray de matlab : rgb2gray converts RGB values to grayscale values by forming a weighted sum of the R, G, and B components:

                    % 0.2989 * R + 0.5870 * G + 0.1140 * B 

        % calcul de l'image en niveaux de gris a partir de la formule
                    % 0.3333 * R + 0.3333 * G + 0.3333 * B 


%% use
    % [dso]= readspc('filename.spc'); return dataset "dso" of spc map.
    % dso= readomnic;
    % dso is a dataset object from eigenvector research see http://www.eigenvector.com/software/dataset.htm
    
%% Comments
    % 
    
    
%% Author
    % 
    
%% date
    % 

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: [dso_read]= readspc(filename)');
end


%% input


if nargin~=0
    errordlg('pas de param�tres pour cette fonction. Usage : classifim')
end;


% fermeture de toutes les fenetres en cours
close all;

%r�pertoire de d�part
porig=pwd;

% carte de couleur pour affichage
load('mamap.mat');

nomlist={'H06Tafg.crop11.7.mat','H06Tafg.crop11.6.mat','H06Tafg.crop11.5.mat','H06Tafg.crop11.4.mat','H06Tafg.crop11.3.mat','H06Tafg.crop11.2.mat','H06Tafg.crop11.dso.mat'};



% !!!!!!!!!% load H06 pyramide
load(nomlist{1});

% get data as table of selected pixels
d=tmp.pyr;
m=tmp.mask;
zoom=tmp.zoom;
d=Imspec2Tab(d,m);
clear tmp;

% PCA
% standardised principal components
 [cos,et,u,moy]=PCAforKmeans(d);
 

cdgn=KmeansHinitCentre(cos,10);

nb=size(cdgn,1);


for i=2:nb
    % k-means
    [nC,gpe]=KmeansH(cos(:,1:5),cdgn(1:i,1:5));

    [pcentre,map,indice]=showKmeansHresult(gpe,cos(:,1:5),m,nC,u(:,1:5),moy,et(1:5),mamap(1:(max(gpe)+1),:),zoom,[1 1 1])


    [W,inert,nogimax]=inertie(cos(:,1:5),gpe,1000);

    gpe_conserve(:,i-1)=gpe;
    nC_conserve{i-1}=pcentre;

    [Crit]=criteres(cos(:,1:5),gpe,W,inert);

    trB(i-1)=Crit(1);
    trBWm1(i-1)=Crit(3);
    
end;

% 
figure(100)
subplot(1,2,1)
plot(2:nb,trB)
title('trace between')
subplot(1,2,2)
plot(2:nb,trBWm1)
title('trace between *  within-1')

% 
