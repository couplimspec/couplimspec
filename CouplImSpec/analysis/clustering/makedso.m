  function dso=makedso(tab,id,name,dsorig)

 %% description
    % use dsorig structure to generate a new dso for the tab matrix
    
%% input
    % tab : usual 2D matrix with the same number of variables than dsorig
    % id : label for tab lines
    % name : name for the dso
    % dsorig : dataset object from eigenvector
        
    
%% output
    %  dso : with tab as data table

%% principe
    % affect each dso field to the tab

%% use
    % dso=makedso(tab,id,name,dsorig)
    
%% Comments
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 24 June 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=4
    error('use: dso=makedso(tab,id,name,dsorig)');
end


%% treatement
% check consistency
if size(tab,2)~=size(dsorig.data,2)
    error('the number of variable of matrix differ from the one expected in the dso');
end

dso=dsorig(1:size(tab,1),:);

% name of dso
dso.name=name;

% type
dso.type='data';

% data
dso.data=tab;

% label
dso.label{1}=id;



%% matlab function tracking  
% no tracking
 
%% end

cd (orig)
    