function [newCenter,gpe]=KmeansH(data,initCenter,varargin)
%
%% DESCRIPTION
% Kmeans function homemade for Mathias PhD
%
%% INPUT
% data : 2D matrix with lines to be clustered and column as variables
% initCenter : initial center of the groups
%
% other arguments :
%   'distance' + value = 'euclidean'                usual distance
%   (default)
%                                     'normeuclidean'       variables are
%                                                                         normalised by dividing by the standard deviation
%                                      ' mahalanobis'
%
%   'numberOfChange' + value : number of points that change of groups
%                                                           (default=1% of the total number)
%
%   'percentOfChange' + value : percentage of points that change of groups
%                                                           (default=1% of the total number)
%

%
%% OUTPUT
% newCenter : final centers of groups
% gpe : label of groupes of each line

%% PRINCIPE
% lines are affected to groups using the distance selected
% the group with minimum distance is retained.
% New centers are computed
% the procedure is iterated until no change occurs between groups, i.e.
% less than the numberOfChange

%% USE
%
% myData=loaddso(''mydata');
% data=myData.data;
% initcenter=data(1:10,:);
% [newCenter,gpe]=KmeansH(data,initCenter,distance,'euclidean','numberOfChange',10)
%
%
%% COMMENTS
%  adapted from the old function nuee_dyn
% Auteur: S�verine Muret
%				URPOI-MicMac
% Version du 12/04/00
% revue le 22 septembre 2003
% MF Devaux et B Bouchet
%   URPOI-Parois
%
% for Mathias Corcel PhD

%% AUTHOR
% MF Devaux
% INRA - BIA- PVPP
%
%% DATE
%
%  19 novembre 2015 MFD

%% context variables
porig=pwd;

distList={'euclidean','seuclidean','mahalanobis'};

% default values
distanceSelec='euclidean';
percentofchange=0.01;

%% start
% test of parameters
if nargin<2
    error('USE :  [newCenter,gpe]=KmeansH(data,initCenter,''distance'',''euclidean/normeuclidean/mhalanobis'',''numberOfChange'',n)');
else
    validateattributes(data,{'numeric'},{'2d'});
    validateattributes(initCenter,{'numeric'},{'2d'});
end;

i=1;
while i<=length(varargin)
    switch lower(varargin{i})
        case 'distance'
            if nargin==i
                error('name of distance expected after ''distance'' argument')
            end;
            validatestring(lower(varargin{i+1}),distList);
            distanceSelec=lower(varargin{i+1});
            i=i+2;
        case 'numberofchange'
            if nargin==i
                error(' stop value : number of points that change of groups expected after ''numberofchange'' argument')
            end;
            if isnumeric(varargin{i+1})
                numberofchange=varargin{i+1};
            else
                error('expected sotp value :  number of points that change of groups ');
            end;
            i=i+2;
        case 'percentofchange'
            if nargin==i
                error(' stop value : percent of points that change of groups expected after ''numberofchange'' argument')
            end;
            if isnumeric(varargin{i+1})
                percentofchange=varargin{i+1};
            else
                error('expected stop value :  percent of points that change of groups ');
            end;
            i=i+2;
        otherwise
            error('invalid arguments %s',varargin{i})
    end;
end;

if ~exist('numberofchange','var')
    %numberofchange=round(percentofchange*size(data,1)/100);
    numberofchange=round(percentofchange*size(data,1));
end;

%% treatment
% variables n�cessaires
nind=size(data,1); 	    % nombre de lignes de grpe
ngpe=size(initCenter,1); 	    % nombre de grpe
nvar=size(data,2); 	% nombre de colonnes de tableau

% standard deviation in case of normalised distance
if strcmp(distanceSelec,'seuclidean') ||strcmp(distanceSelec,'mahalanobis')
    et=std(data);
end


% initialisation des matrices et des variables
distance=zeros(nind,ngpe);
newCenter=zeros(ngpe, nvar);
critere=numberofchange+1;
iter=0;

% initialisation des groupes �tape n�cessaire pour Mahalanobis
switch distanceSelec
    case {'euclidean','mahalanobis'}
        for i=1:ngpe
            distance(:,i)=sum(((data-repmat(initCenter(i,:),nind,1))').^2)';
        end;
    case 'seuclidean'
        for i=1:ngpe
            distance(:,i)=sum(((data-repmat(initCenter(i,:),nind,1))*diag(1./et)).^2,2);
        end;
end;
%  affectation des individus aux centres de gravit�
[~,gpeold]=min(distance,[],2);
gpeactuel=gpeold;

%  calcul des centres de gravit�s
for i=1:ngpe
    newCenter(i,:)=mean(data(gpeold==i,:));
end;

% ieration while d�finissant les groupes
while (abs(critere)>numberofchange)
    
    % mise � jour du compteur iter
    iter=iter+1;
    
    % affichage du nombre d'it�ration
    display(sprintf('It�ration %d\n',iter))
    
    %  calcul des distances aux centres de gravite
    switch distanceSelec
        case 'euclidean'
            for i=1:ngpe
                distance(:,i)=sum(((data-repmat(newCenter(i,:),nind,1))').^2)';
            end;
        case 'seuclidean'
            for i=1:ngpe
                distance(:,i)=sum(((data-repmat(newCenter(i,:),nind,1))*diag(1./et)).^2,2);
            end;
        case 'mahalanobis'
            garde=ones(ngpe,1);
            for i=1:ngpe
                if sum(gpeactuel==i)>nvar
                    distance(:,i)=mahal(data*diag(1./et),data(gpeactuel==i,:)*diag(1./et));
                else
                   garde(i)=0;
                end;
            end;
            distance=distance(:,logical(garde));
            ngpe=size(distance,2);
    end;
    
    
    %  affectation des individus aux centres de gravit�
    [~,gpeactuel]=min(distance,[],2);
        
    
    %  calcul des centres de gravit�s
    for i=1:ngpe
        newCenter(i,:)=mean(data(gpeactuel==i,:));
    end;
    
    % calcul du crit�re de fin
    critere=length(find((gpeactuel-gpeold)));
    
    % mise � jour des groupes
    gpeold=gpeactuel;
    
end

% groupes finaux
gpe=gpeactuel;


%  calcul des centres de gravit�s
for i=1:ngpe
    newCenter(i,:)=mean(data(gpe==i,:));
end;

%% save nothing on disk

%% matlab function tracking
%no tracking

%% end
cd(porig)