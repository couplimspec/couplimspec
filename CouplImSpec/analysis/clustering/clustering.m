function clustering

 %% description
    % clustering of dso images
    
%% input
    % nothing 
    % interactive input
     
    
%% output
    
    % file saved on disk

%% principe
    % the dso image is displayed
    % a region of interest is selected
    % clustering is applied on teh pixel of the region of interest
    
    % clustering : a hierarchical k-means procedure is used as refred to
    % below
    % after the clustering 1000 pixels the closest to the gravity center of
    % the group are selected ans saved as representative pixels of the
    % group
    
    % Several region of interest can be used for one image. in this case,
    % the soft has to be lauched as many time as required to have the
    % result of all region of interest
    % final clustering will be obtained by merging all resulting selected
    % pixels
    
%% use
    % clustering
    
    
%% Comments
    % fonction de segmentation d'images multivari�es par classification
% automatique selon la proc�dure mise au point par S�verine Muret
%
% proc�dure d'analyse pour des images multispectrales provenant du
% microscope confocal de Nantes. Le microscope permet d'enregistrer 20
% images diff�rentes : 1 image en fond clair et 19 images en fluorescence.
% Ces images sont num�rot�es de 1 � 20 selon un protocole d'acquisition
% d�crit dans le rapport de S�verine Muret, l'image 1 �tant l'image fond
% clair.
% L'image fond clair n'est pas prise en compte dans les �tapes de
% classification
%
% USAGE : classifim
%
% auteur : MF Devaux et B Bouchet
%          d'apr�s S�verine Muret, stage ENITIAA septembre 2000
%          14 septembre 2004
%

    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 23 juin 2014


%% context variables 

%% start

if nargin~=0
    errordlg('pas de param�tres pour cette fonction. Usage : clustering')
end;

question=0;

% fermeture de toutes les fenetres en cours
close all;

%r�pertoire de d�part
porig=pwd;

% carte de couleur pour affichage
pointcouleur=[1 0 0; 0 1 0 ; 0 0 1; 1 1 0; 1 0 1; 0 1 1 ;  0.5 0.5 0.5; .75 0 .5; .5 .75 0 ; .5 0 .75; 0 .5 .75; 0 .75 .5; .5 .5 0; .5 0 .5; 0 .5 .5 ; .5 .25 0; .5 0 .25; .25 .5 0; .25 0 .5; 0 .25 .5 ; 0. .5 .25];
pointcarte=[0 0 0 ;1 0 0; 0 1 0 ; 0 0 1; 1 1 0; 1 0 1; 0 1 1 ; 0.5 0.5 0.5; .75 0 .5; .5 .75 0 ; .5 0 .75; 0 .5 .75; 0 .75 .5;.5 .5 0; .5 0 .5; 0 .5 .5; .5 .25 0; .5 0 .25; .25 .5 0; .25 0 .5; 0 .25 .5 ; 0. .5 .25];
mapim=[0 0 0;repmat(pointcouleur,100,1)];
pointcarte=repmat(pointcarte,100,1);

%% input
% r�pertoire des donn�es et premier fichier
% input image
[nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

% read image
cd(repl)
ims=loaddso(nom);
nomgen=strrep(nom,'.dso.mat','');

sim=ims.imagedata;

% image charactersitics
nl=size(sim,1);
nc=size(sim,2);
% nombre d'images dans la sequence
nim=size(sim,3);

% sauvegarde
repsauve='cluster';
if ~exist(repsauve,'dir')
    mkdir(repsauve);
end
cd(repsauve)
repsauve=pwd;

% choose region of intererst ---------------------------------------------------
figure('Numbertitle', 'off','name','Region of interest') 
% average intensity
sumim=mean(ims.imagedata,3);
sumim=uint8(sumim/max(sumim(:))*255);
imshow(sumim,[]);
% test if some region of interest have already been used for this image
% only the case with less than 10 roi is considered
nbr=0;
trouve=1;
while trouve
    listroi=dir(strcat(nomgen,'.roi',num2str(nbr+1),'.tif'));  
    if isempty(listroi)
        trouve=0;
    else
        nbr=nbr+1;
    end
end;
if nbr>=1
    ficzone=yesno('load region of interest from file ?');
else
    ficzone=0;
end
if ficzone
    [nomficzone, pathzone]=uigetfile({'*.roi*.tif'},'image of region of interest',strcat(nomgen,'.roi1.tif'));
    cd(pathzone)
    zone=imread(nomficzone);
else
    % select region of interest
    zone=roipoly(sumim);
    % save region of interest
    nomroi=strcat(nomgen,'.roi',num2str(nbr+1),'.tif');
    imwrite(zone,nomroi,'tif','compression','none');                        % sous la forme d'une image
end;


%% treatment
% show selected region of interest
hroi=imroishow(sumim,zone,'selected ROI');

% pixel selected as a vector
vecvzo=reshape(zone,nl*nc,1);
% data table of teh selected pixels
tabim=ims.data(vecvzo,:);

npix=size(tabim,1);                     % nombre of pixel selected


% first cluster into two groups : 
% initialise groups
[gpe,co,vp]=initGpe(tabim,zone,1);
% variables of vp
vid=[repmat('cp',size(vp,2),1) num2str((1:size(vp,2))')];
nom=strcat(ims.name,'.roiloadings');
vp=makedso(vp',vid,nom,ims);

xlab='Principal Component 1';
ylab='Principal Component 2';
titre='ROI Principal Component Analysis';
figure(hroi);
scatterPlotCluster(co,1,2,ones(npix,1),'k',xlab,ylab,titre);
plotdsoPCALoading(vp,1:4,titre);



% classification en deux groupes
gpe = nuees_dyn(gpe,double(tabim));	% application de la m�thode des centres mobiles

% stockage du r�sultat dans un tableau pour comparaison finale
gpe_conserve(:,1)=gpe;

% affichage des groupes trouv�s : carte ACP
titre=['ROI Principal Component Analysis: ' num2str(2) ' groups'];
scatterPlotCluster(co,1,2,gpe,pointcarte(1:2,:),xlab,ylab,titre);


% affichage : image
vzo(zone)=gpe;
imroishow(vzo,zone,[num2str(2) ' groups']);


% inertie de chaque groupe et s�lection du num�ro de sous-groupe 
% dont l'inertie totale est la plus forte : nogimax
% le groupe dont les niveux de gris est la plus faible est �limin� du
% clasement
m1=mean(mean(tabim(gpe==1,:)));
m2=mean(mean(tabim(gpe==2,:)));
if m1<m2
    [W,inert,nogimax]=inertie(double(tabim),gpe,1);
else
    [W,inert,nogimax]=inertie(double(tabim),gpe,2);
end;
% variance totale, Within et Between
Vtot=(double(tabim)'*double(tabim))/npix-(mean(tabim)'*mean(tabim));	 % calcul de la variance totale
Wtot=zeros(nim,nim);		% initialisation de Wintra totale
% calcul de Wintra totale
for i=1:2
   Wtot=Wtot + W(:,:,i)*sum(gpe==i)/npix;
end;
Btot=Vtot-Wtot;					% calcul de la variance inter ou between

% calcul des crit�res : 
% premier crit�re : trace de B
Crit1(1)=trace(Btot);	% attention l'indice correspond � ngpe -1
% deuxi�me crit�re : log(determinant(T)/determinant(W))
Crit2(1)=log(det(Vtot)/det(Wtot));
% troisi�me crit�re : trace BW-1
Crit3(1)=trace(Btot*inv(Wtot));
% quatri�me crit�re : C_index= (trace(Wtot)-min(inert))/(max(inert)-min(inert))
Crit4(1)=(trace(Wtot)-min(inert))/(max(inert)-min(inert));
% boucle while d�finissant le nombre de groupes 
% initialisation du compteur no : nombre de groupe 
no=2;

% variables utiles
zoneactuelle=zone;	% zone d'int�ret en cours

if question
    continuer=ouinon('voulez vous continuer ?');
else
    continuer=1;
end;

while continuer			  
   
   % mise � jour du compteur no
   no=no+1;
   
   % classification du tableau consid�r� 
   zoneactuelle(zone)=(gpe==nogimax);
   vecvzo=reshape(zoneactuelle,nl*nc,1);


    tabim2=ims.data(vecvzo,:);

      
   % initialisation de la division du groupe bas�e sur la seconde
   % composante principale 2
   gpe2=acp_init_classifim(tabim2,zoneactuelle,2);
   clear tabim2
    
   gpetmp(gpe2==1)=nogimax;
   gpetmp(gpe2==2)=no;
   
   gpe(gpe==nogimax)=gpetmp;
   
   clear gpetmp;
   
   % classification en no groupes
   gpe = nuees_dyn(gpe,double(tabim));	% application de la m�thode des centres mobiles


   % stockage du r�sultat dans un tableau pour comparaison finale
   gpe_conserve(:,no-1)=gpe;
	
   % affichage des groupes trouv�s sur la carte d'ACP
   if question
       graphe=findobj('name','Analyse en Composantes Principales : CP 1 et 2');
       figure(graphe)
   else
       figure('numbertitle','off','name',sprintf('Analyse en Composantes Principales : CP 1 et 2 : %d groupes',no),'units','normalized','position',[0 1 0.45 0.45]);
   end;
   
   plot(co(gpe==1,1),co(gpe==1,2),'.','color',pointcarte(1,:))
   hold on
   for i=2:no
	   plot(co(gpe==i,1),co(gpe==i,2),'.','color',pointcarte(i,:))
   end;
   xlabel('composante principale 1')
   ylabel('composante principale 2')
   hold off
   if question
       graphe=findobj('name','Analyse en Composantes Principales : CP 3 et 4');
       figure(graphe)
   else
       figure('numbertitle','off','name',sprintf('Analyse en Composantes Principales : CP 3 et 4 : %d groupes',no),'units','normalized','position',[0.5 0.5 0.45 0.45]);
   end;
   plot(co(gpe==1,3),co(gpe==1,4),'.','color',pointcarte(1,:))
   hold on
   for i=2:no
	   plot(co(gpe==i,3),co(gpe==i,4),'.','color',pointcarte(i,:))
   end;
   xlabel('composante principale 3')
   ylabel('composante principale 4')
   hold off
	
   
   % affichage des groupes sur l'image
   vzo(zone)=gpe;
   figure('numbertitle','off','name',['classification en ' num2str(no) ' groupes'],'units','normalized','position',[0.5 0 0.5 0.5]);
   imshow(vzo,mapim);
   
   % inertie de chaque groupe et s�lection du num�ro de sous-groupe 
   % dont l'inertie totale est la plus forte : nogimax
   for j=1:no
       m(j)=mean(mean(tabim(gpe==j,:)));
   end;
   nel=find(m==min(m));
   [W,inert,nogimax]=inertie(double(tabim),gpe,nel);
   
   	
   % variance totale, Within et Between
   Vtot=(double(tabim)'*double(tabim))/npix-(mean(tabim)'*mean(tabim));	 % calcul de la variance totale
   Wtot=zeros(nim,nim);		% initialisation de Wintra totale
   % calcul de Wintra totale
   for i=1:no
       Wtot=Wtot + W(:,:,i)*sum(gpe==i)/npix;
   end;
   Btot=Vtot-Wtot;					% calcul de la variance inter ou between
	
   % calcul des crit�res : 
   % premier crit�re : trace de B
   Crit1(1,no-1)=trace(Btot);	% attention l'indice correspond � ngpe -1
   % deuxi�me crit�re : log(determinant(T)/determinant(W))
   Crit2(1,no-1)=log(det(Vtot)/det(Wtot));
   % troisi�me crit�re : trace BW-1
   Crit3(1,no-1)=trace(Btot*inv(Wtot));
   % quatri�me crit�re : C_index= (trace(Wtot)-min(inert))/(max(inert)-min(inert))
   Crit4(1,no-1)=(trace(Wtot)-min(inert))/(max(inert)-min(inert));

    if question || no>10
        continuer=yesno('voulez vous continuer ?');
    else
        continuer=1;
    end;

%     if no>25
%         continuer=0;
%     end;
%     
end;

% affichage des crit�res qualit�s------------------------------------------------

if no>=4
	% graphique des crit�res
	% premier crit�re : trace de B
	figure('Numbertitle','off','name','crit�res')
	subplot(2,4,1)
	plot(2:no,Crit1);	% attention l'indice correspond � ngpe -1
	axis([2 no min(Crit1) max(Crit1)]);
	title('trace(Bn)')
	
	% graphe de la d�riv�e
	subplot(2,4,5)
	plot(3:no,log(Crit1(2:(no-1))-Crit1(1:(no-2))))
	axis([3 no min(log(Crit1(2:(no-1))-Crit1(1:(no-2)))) max(log(Crit1(2:(no-1))-Crit1(1:(no-2))))]);
	title('log(Bn-Bn-1)')
	
	% deuxi�me crit�re : log(determinant(T)/determinant(W))
	subplot(2,4,2)
	plot(2:no,Crit2);	% attention l'indice correspond � ngpe -1
	axis([2 no min(Crit2) max(Crit2)]);
	title('log(|T|/|W|)')
	
	% graphe de la d�riv�e
	subplot(2,4,6)
	plot(3:no,Crit2(2:(no-1))-Crit2(1:(no-2)))
	axis([3 no min(Crit2(2:(no-1))-Crit2(1:(no-2))) max(Crit2(2:(no-1))-Crit2(1:(no-2)))]);
	title('d�riv�e')
	
	% troisi�me crit�re : trace BW-1
	subplot(2,4,3)
	plot(2:no,Crit3);	% attention l'indice correspond � ngpe -1
	axis([2 no min(Crit3) max(Crit3)]);
	title('trace(BW-1)')
	
	% graphe de la d�riv�e
	subplot(2,4,7)
	plot(3:no,Crit3(2:(no-1))-Crit3(1:(no-2)))
	axis([3 no min(Crit3(2:(no-1))-Crit3(1:(no-2))) max(Crit3(2:(no-1))-Crit3(1:(no-2)))]);
	title('d�riv�e')
	
	% quatri�me crit�re : C_index= (trace(Wtot)-min(inert))/(max(inert)-min(inert))
	subplot(2,4,4)
	plot(2:no,Crit4);	% attention l'indice correspond � ngpe -1
	axis([2 no min(Crit4) max(Crit4)]);
	title('C_index')
end;

% choix du nombre de groupe � conserver ------------------------------------
nof=inputdlg('Donner le nombre de groupe retenu : ');
nof=str2num(char(nof));

% segmentation de l'image enti�re -----------------------------------------
% groupes retenus pour les pixels de la zone d'int�ret
gpe=gpe_conserve(:,nof-1);
% image segment�e de la zone d'int�ret
vzo(zone)=gpe;

%  calcul des centres de gravit�
for (i=1:nof)
  	cdg(i,:)=mean(tabim(gpe==i,:));
end;

% s�lection des pixels les plus proches du centre de gravit�
% 1000 pixels par groupe sont s�lectionn�s
% distance
for (i=1:nof)
  distance(:,i)=sum(((double(tabim)-repmat(cdg(i,:),size(tabim,1),1))').^2)';
end;

% rang du tableau des distances
[Y,I]=sort(distance);
%affectation aux groupes
[mini,gpim]=min(distance,[],2);

% constitution du tableau des points s�lectionn�s dans l'image
%tpix=tabim(I(1:1000,1),:);

%for i=2:nof   
   % concat�nation
 %  tpix=[tpix;tabim(I(1:1000,i),:)];
%end;

% r�cup�ration des indices des pixels de la zone d'int�ret
[iz,jz]=find(zone);

% r�cup�ration des indices des pixels s�lectionn�s et affichage sur l'image
% fond clair des indices s�lectionn�s
% constitution du tableau des pixels s�lectionn�s
figure('Numbertitle', 'off','name','pixels s�lectionn�s') 
imshow(sumim,[]);
hold on
fin=0;
for i=1:nof
    % calcul du nombre de pixel les plus proches du centre de gravit�
    nps=min(1000,sum(gpim==i));
    % indices dans le tableau tabim
    Is=I(1:nps,i);
    is=iz(Is);          % indices lignes (colonne pour les images)
    js=jz(Is);          % indices colonnes (ligne pour les images)
    %affichage dans l'image fond clair
    plot(js,is,'.','color',mapim(i,:))
    deb=fin+1;
    fin=deb-1+nps;
    gpeselec(deb:fin,:)=i;
    % individu(deb:fin,1:2)=[is,js];
    tpix(deb:fin,:)=tabim(I(1:nps,i),:);
end;

clear distance

% calcul des distances de tous les pixels aux centres de gravit�
for (i=1:nof)
  distance(:,i)=sum(((double(ims.data)-repmat(cdg(i,:),nl*nc,1))').^2)';
%  distance(:,i)=sum(((double(reshape(im(:,:,:,2:nb),size(im,1)*size(im,2),nb-1))-repmat(cdg(i,:),size(im,1)*size(im,2),1))').^2)';
end;

%  affectation des pixels aux centres de gravit�
[mini,gpim]=min(distance,[],2);

%constitution de l'image enti�re segment�e
segim=reshape(gpim,nl,nc);

% visualistion de l'image enti�re segment�e
figure('numbertitle','off','name',['classification en ' num2str(nof) ' groupes']);
imshow(segim,mapim);
	
	
% sauvegarde des r�sultats -------------------------------------------------
cd(repsauve)

nomsauve=strcat(nomgen,'.clust',num2str(nof),'.tif');

% sauvegarde des crit�res
h=findobj('name','crit�res');
nomfic=strrep(nomsauve,'.tif','.criteres.tif');
saveas(h,nomfic,'tif');


% sauvegarde de l'image de la zone d'int�ret segment�e
nom=strcat(strrep(nomsauve,'.tif','.roi'),num2str(nbr+1),'.tif');
imwrite(vzo,mapim(1:(max(max(vzo))+1),:),nom,'tif','compression','none');

% sauvegarde de l'image enti�re segment�e
imwrite(segim,mapim(1:(max(max(vzo))+1),:),nomsauve,'tif','compression','none');


% sauvegarde de la carte des composantes principales en couleur
% affichage des groupes trouv�s
graphe=findobj('name','Analyse en Composantes Principales : CP 1 et 2');
figure(graphe)
plot(co(gpe==1,1),co(gpe==1,2),'.','color',pointcarte(1,:))
hold on
for i=2:no
   plot(co(gpe==i,1),co(gpe==i,2),'.','color',pointcarte(i,:))
end;
xlabel('composante principale 1','FontSize',16,'FontWeight','bold')
ylabel('composante principale 2','FontSize',16,'FontWeight','bold')
hold off
% sauvegarde
nomfic=strrep(nomsauve,'.tif','.cp12.tif');
saveas(graphe,nomfic,'tif')

% affichage
graphe=findobj('name','Analyse en Composantes Principales : CP 3 et 4');
figure(graphe)
plot(co(gpe==1,3),co(gpe==1,4),'.','color',pointcarte(1,:))
hold on
for i=2:no
   plot(co(gpe==i,3),co(gpe==i,4),'.','color',pointcarte(i,:))
end;
xlabel('composante principale 3','FontSize',16,'FontWeight','bold')
ylabel('composante principale 4','FontSize',16,'FontWeight','bold')
hold off
% sauvegarde
nomfic=strrep(nomsauve,'.tif','.cp34.tif');
saveas(graphe,nomfic,'tif')


% sauvegarde des pixels s�lectionn�s
nvar=size(tpix,2);
tpix(:,nvar+1)=gpeselec;
nomfic=strcat(nomgen,'.selec.tif');
imwrite(tpix,nomfic,'tif');

%tpix.d=tpix;
%nvar=size(tpix.d,2);
%nind=size(tpix.d,1);
%tpix.d(:,nvar+1)=gpeselec;              % num�ro de groupe des pixels s�lectionn�s
%tpix.d(:,(nvar+2):(nvar+3))=individu;            % coordonn�es des pixels s�lectionn�s
%tpix.i=(1:nind)';
%tpix.v=char(num(2:length(num),:),'gpe','x','y');

%ecrire(tpix,nomgen);

% sauvegarde de l'image des pixels s�lectionn�s
h=findobj('name','pixels s�lectionn�s') ;
nomfic=strrep(nomsauve,'.tif','.pselec.tif');
saveas(h,nomfic,'tif');


cd(porig)    

