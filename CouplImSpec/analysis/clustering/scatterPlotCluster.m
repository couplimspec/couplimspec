  function h=scatterPlotCluster(co,i,j,part,coul,xlab,ylab,titre,leg)

 %% description
    % plot a scatter plot of two column vectors with some color for a partition of
    % samples
    
%% input

    %  co : matrix od data
    %  i : number of column : in x
    % j : number of column : in y
    % part : vector of sample class
    % vector of color as vector of char or matrix od color (RGB)
    % xlab=xlabel
    % ylab=ylabel
    % titre=title
    % leg=legend
    
    
%% output
    
    %   h : handle of resulting figure

%% principe
    % scatter plot with points of given color


%% use
    % h=scatterPlotCluster(co,i,j,part,coul,xlab,ylab,titre)

    
%% Comments
    % for clustering
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 24 June 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=8 && nargin ~=5 && nargin ~=9
    error('use: h=scatterPlotCluster(co,i,j,part,coul,xlab,ylab,titre,leg)');

end

if length(part)~=size(co,1)
        error('number of lines in first matrix differ from length or partition vector');
end
if size(part,2)~=1
    error('vector expected as partition of samples ');
end;

if nargin==5
    xlab='';
    ylab='';
    titre='';
end



% check errors
ngpe=max(part);
if ischar(coul)
    if length(coul)~=ngpe
        error('number of color differ from number of groups');
    end
else
    if size(coul,1)~=ngpe
        error('number of color differ from number of groups');
    end
end
    
if nargin==9
    Pleg=1;
    if size(leg,1)~=ngpe
        error('wrong number of legend');
    end;
else
    Pleg=0;
end;



%% treatement

h=figure('name',titre,'windowstyle','docked');
plot(co(:,i),co(:,j),'.w')
hold on
for k=1:ngpe
    hg(i)=plot(co(part==k,i),co(part==k,j),'.','color',coul(k,:));
end

xlabel(xlab,'fontsize',18);
ylabel(ylab,'fontsize',18);
title(titre,'fontsize',18);

if Pleg
    hl=legend(hg,leg);
    set(hl,'fontsize',16);
end




%% matlab function tracking  
% no tracking
 
%% end

cd (orig)
    