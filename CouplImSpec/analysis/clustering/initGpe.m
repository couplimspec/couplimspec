function  [gpe,co,vp]=initGpe(tabim,zone,cp)
%% description
% reaelise l'initialisation de la separation de la population initiale en deux groupes
% Le resultat est une initialisation de partition : les donnees doivent ensuite �tre
% traitee par la methode d'agregation autour des centres mobiles pour obtenir 
% les groupes effectifs.
%
%% parametres d'entree
% tabim : matrice de donnees dont il faut realiser la division en deux groupes
% zone : image binaire definissant la zone selectionnee sur l'image en fond clair
% cp : numero de la composante principale prise en compte pour initialiser
% les deux groupes
%
%% parametres de sortie
% gpe : matrice logique/binaire qui definit les 2 groupes de la matrice
% tabim
% co : matrice de composantes principales
% vp= matrices des vecteurs propres
%
%% principle
% a centred PCA is performed on the data
% the first group correspond to the positive value for the component
% considered
% the second group correspond to the negative values

%% Use
%       gpe=initGpe(tabim,zone,cp);

%% Author
%   MF Devaux
%   BIA - PVPP
%% date
%   24 june 2014

%% comment from
%
% Auteur: Severine Muret
%				URPOI-MicMac
% Version du 12/04/00
% 
% revue par MF Devaux et B Bouchet
%   URPOI-Parois
%   23 septembre 2003
% version du 16 septembre 2004 pour la prise en compte de la composante cp


% start 
if nargin>3
    error('USe : gpe=initGpe(tabim,roi(optionnel),<Nocp : optionnel>);');
end;

if nargin<=2
    cp=1;
end;

if nargin==1
    image=0;
else
    image=1;
end;

%% treatment

npix=size(tabim,1);

% ACP centree
moy=mean(tabim);	 % calcul des moyennes des variables

Totale=(double(tabim)'*double(tabim))/npix-(moy'*moy);	 % calcul de la matrice variance-covariance

[~,~,vp]=svd(Totale); 	% SVD

co=double(tabim)*vp-repmat(moy,npix,1)*vp;	 % calcul des composantes principales

% 2 groupes : groupes initiaux 
gpe(co(:,cp)>0)=1;	 % determination du groupe 1
gpe(co(:,cp)<0)=2;	 % determination du groupe 2
gpe=gpe';            % on veut que gpe soit un vecteur colonne

% if image...
if image
    % Affichage de la premiere composante principale
    % initialisation d'une matrice de zeros de dimension de l'image etudiee
    vzo=zeros(size(zone));
    % representation dans une image des pixels de la zone d'inter�t
    vzo(zone)=co(:,cp);
    % affichage
    h=imroishow(vzo,zone,'first principal component');
end;


