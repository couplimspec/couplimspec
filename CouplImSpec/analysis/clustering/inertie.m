function [W,inert,nogimax]=inertie(tab,gpe,no)
% calcule les matrices d'inerties de chaque groupe et d�termine le num�ro
% de groupe d'inertie max
% ayant une inertie totale la plus forte
%
% param�tres d'entr�e :
% tab : matrice � tester
% gpe : tableau logique binaire d'affectation aux groupes
% no  : num�ro du groupe ou des groupes �limin�s du classement
%
% param�tres de sortie
% W : tableau 3D des matrices d'inertie intragroupes :
%				var X var X groupe
% inert : vecteur des inerties selon la composante s�lectionn�e
% nogimax : num�ro du groupe d'inertie maximum
% 
% Auteur: S�verine Muret
%				URPOI-MicMac
% Version du 13/04/00
% revue par MF Devaux et B Bouchet
%   URPOI-Parois
%   le 23 septembre 2003
%   version du 16 septembre 2004
%   pour le choix du ou des groupes � �liminer de la classification

% message d'erreur
if (nargin~=2)&&(nargin~=3)&&(nargin ~=4)
	errordlg('USAGE : inertie(tab,gpe) ou inertie(tab,gpe,no)');
    return;
end;

% variables utiles
ngpe=max(gpe);       % nombre de groupe
% for i=1:ngpe
%     nig(i)=sum(gpe==i);
% end;

if nargin==2
    no=0;
end;
    
% calcul de l'inertie des groupes
for (i=1:ngpe)
 	%  calcul des centres de gravit�s
    cdg(i,:)=mean(tab(gpe==i,:));
    % calcul de la matrice variance-covariance
    W(:,:,i)=(tab(gpe==i,:)'*tab(gpe==i,:))/(sum(gpe==i))-(cdg(i,:)'*cdg(i,:)); 
    % calcul de l'inertie des groupes
    inert(i)=trace(W(:,:,i));
end;


% D�termination des valeurs propres de chaque groupe

for i=1:ngpe
	[U,S,V]=svd(W(:,:,i));
	l1(i)=S(1,1)/sum(diag(S));		% pourcentage d'inertie de la premi�re valeur propre
end;

for i=1:length(no)
    l1(i)=0;
end;

%l1(nig<=nind)=0;

nogimax=find(l1==max(l1));	% choix du pourcentage d'inertie le plus fort

