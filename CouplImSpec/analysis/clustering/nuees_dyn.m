function [newCenter,gpe]=KmeansH(data,initCenter,varargin)
%
%% DESCRIPTION
% Kmeans function homemade for Mathias PhD
% 
%% INPUT
% data : 2D matrix with lines to be clustered and column as variables
% initCenter : initial center of the groups
%
% other arguments : 
%   'distance' + value = 'euclidian'                usual distance
%   (default)
%                                     'normeuclidian'       variables are
%                                                                         normalised by dividing by the standard deviation
%                                      ' mahalanobis'
%
%   'numberOfChange' + value : number of points that change of groups 
%   ' 
% 
%% OUTPUT
% Nothing
%
%% PRINCIPE
% 
% 
%% USE
% Build a model with bigPCA
% List the images to project with selectList
% load both in Matlab workspace
% use : dsoScores(PCAresults,selectList)
%
%% COMMENTS
%
%% AUTHOR
% Mathias Corcel
% IATE Montpellier / BIA-PVPP Nantes
% review by MF Devaux
% INRA - BIA- PVPP
%
%% DATE
% 19/05/2015 : start
% 03/06/2015 : TODO : validateattributes
%  10/ novembre 2015 MFD

%% context variables% calcule les groupes apr�s classification hi�rarchique descendante
% � partir des groupes initialis�s dans grpe
% gpe : matrice bool�enne repr�sentant les groupes finaux
% grpe : matrice bool�enne de d�part
% tableau : matrice de donn�es du d�part
% seuil : param�tre optionnel d�finissant la pr�cision de l'affectation
% au groupe : quand le crit�re d�passe le seuil, les it�rations stoppent
% 
% Auteur: S�verine Muret
%				URPOI-MicMac
% Version du 12/04/00
% revue le 22 septembre 2003
% MF Devaux et B Bouchet
%   URPOI-Parois
%
% message d'erreur
if (nargin~=2)&&(nargin~=3)
	errordlg('Il faut entrer deux ou trois param�tres � cette fonction : usage : gpe=nuees_dyn(grpe,tableau,<optionnel : seuil>);');
    return;
end;

if nargin==2
    seuil=10;
end;

% variables n�cessaires
nind=size(grpe,1); 	    % nombre de lignes de grpe
ngpe=max(grpe); 	    % nombre de grpe
nvar=size(tableau,2); 	% nombre de colonnes de tableau

%  calcul des centres de gravit�s
for (i=1:ngpe)
  	cdg(i,:)=mean(tableau(grpe==i,:));
end;

% initialisation des matrices et des variables
gpeold=zeros(nind,1);
critere=seuil+1;
iter=0;

% boucle while d�finissant les groupes
while (abs(critere)>seuil)
   
   % mise � jour du compteur iter
   iter=iter+1;
   
   % affichage du nombre d'it�ration
   display(sprintf('It�ration %d\n',iter))

   %  calcul des distances aux centres de gravite
   for (i=1:ngpe)
      distance(:,i)=sum(((tableau-repmat(cdg(i,:),nind,1))').^2)';
   end;
   
   %  affectation des individus aux centres de gravit�
   [mini,gpeactuel]=min(distance,[],2);
      
   %  calcul des centres de gravit�s
   for (i=1:ngpe)
      cdg(i,:)=mean(tableau(gpeactuel==i,:));
   end;

   % calcul du crit�re de fin
   critere=length(find((gpeactuel-gpeold)))
   
   % mise � jour des groupes
   gpeold=gpeactuel;
   
end

% groupes finaux
gpe=gpeactuel;
  
   
   
