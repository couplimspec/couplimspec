  function h=imroishow(im,roi,titre)

 %% description
    % show the best view of a roi: discard black regions around the roi
    
%% input
    % im : grey level original image
    % binary roi of the same size than the roi
    % titre : optional title
        
    
%% output
    %   h : handles of resulting figure

%% principe
    % search for the rectangle that surround the roi
    % show only this sub image
    

%% use
    % h=imroishow(im,roi)

    
%% Comments
    
    
%% Author
    % MF Devaux
    % BIA-PVPP
    
%% date
    % 24 June 2014

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=2 && nargin ~=3
    error('use: h=imroishow(im,roi,title)');
end

if nargin==2
    titre='';
end;


%% treatement
% search for rectangle
[line, col]=find(roi);
lmin=min(line);
lmax=max(line);
cmin=min(col);
cmax=max(col);

% subimage roi
sroi=roi(lmin:lmax,cmin:cmax);

% subimage im
sim=im(lmin:lmax,cmin:cmax);


% imshow
tempim=sim;
vzo=zeros(size(sroi));
vzo(sroi)=tempim(sroi);
h=figure('name',titre,'windowstyle','docked');
imshow(vzo,[]);
title(titre,'fontsize',18)

%% matlab function tracking  
% no tracking
 
%% end

cd (orig)
    