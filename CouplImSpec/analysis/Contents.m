% ANALYSIS
%
% Files
%   averageThreeWaydso - compute the average 2 way dso data table from a three way dso data table
%   pcascoresdso       - compute the pca of a dso
%   plotdsoPCALoading  - plot PCA loadings
%   twoWay2threeWay    - convert 2 way dso image data table into a three way data table

