  function code=extractCode
 %% description
    % extract code from sample name in a dso or txt file
    
%% input

    %  nothing
%% output
    
    %   code : vector of char array of the codes
%% principe
    % the user must give : 
    %       - the beginning of the code in the sample name
    %       - the length of the code
    
    % works only if codes are of the same length and at the same place in
    % the sample name.
    
    % search for unique char arrays even in samples are not in the
    % alphabetic order


%% use
    % code=extractCode
    
    
%% Comments
    % review for couplImSPec toolbox
    
    
%% Author
    % Marie-Francoise Devaux
    % BIA - PVPP
    
%% date
    % 4 avril 2014
    % 14 f�vrier 2014
    % 26 aout 2013

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: code=extractCode');
end


%% input
% file name
[filename, rep] = uigetfile({'*.txt';'*.dso.mat'},'name of data file'); 
[~,~,ext]=fileparts(filename);
ext=strrep(ext,'.','');
cd(rep)

switch ext
    case 'txt'
        don=readDIV(filename);
        sampleName=don.i;
    case 'mat'
        don=loaddso(filename);
        if isa(don,'dataset')
            sampleName=char(don.Label{1});
        else
            error('dataset expected')
        end
    otherwise
        error('txt or dso.mat files expected')
end;
        
% position of the code in the sample names
prompt=['Location of code''s first character in sample names: ex: ' sampleName(1,:)];
titre='Code location';
dep = inputNumber(prompt,'def',1,'vmin',1,'vmax',size(sampleName,2),'title',titre);

% Length of the code in the sample names
prompt=['Length of code: ex: ' sampleName(1,:)];
titre='Code''s length';
lcode = inputNumber(prompt,'def',1,'vmin',1,'vmax',size(sampleName,2),'title',titre);

% name for saving file
[sfilename, reps] = uiputfile({'*.txt'},'name of code file','code.txt'); 


%% treatement
ni=sampleName(:,dep:(dep+lcode-1));
nb=length(ni);

code{1}=ni(1,:);
j=1;

for i=2:nb
    if ~strcmp(ni(i,:),code{j})
        j=j+1;
        code{j}=ni(i,:);
    end;
end

% check for unicity of the codes
nb=length(code);
tmp=sort(code);
clear code;

code=tmp{1};
j=1;
for i=2:nb
    if ~strcmp(tmp{i},code(j,:))
        j=j+1;
        code=char(code,tmp{i});
    end;
end

%% save 
cd(reps)
if ~strfind(sfilename,'.txt')
    sfilename=strcat(sfilename,'.txt');
end;
writeStringVec(code,sfilename);

%% matlab function tracking  

fid=fopen(strcat(strrep(sfilename,'.txt',''),'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\r\n',datestr(now,0));
fprintf(fid,'__________________________________________________________________________\r\n');
fprintf(fid,'\r\n               create codes from sample names \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput file name: %s\r\n',filename);
fprintf(fid,'input file folder: %s\r\n',rep);

fprintf(fid,'\r\ncode file name: %s \r\n',sfilename);
fprintf(fid,'code folder: %s\r\n',reps);

fprintf(fid,'\r\nLocation of the codes in sample names: %d\r\n',dep);
fprintf(fid,'\r\nLength of the codes: %d\r\n',lcode);


% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
   

