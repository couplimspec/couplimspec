  function code=findCode(vecChar)

 %% description
    % find code in a vector of char defined by a position
    % similar to unique for numbers
    
%% input
    %  vecChar : vector of character array
    
%% output
    %   code : char array of the code

%% principle
    % check the unique strings in the vector of char and return tham as
    % codes


%% use
    % d=readDIV;
    % vecChar=d.i(2:3);
    % code=findCode(vecChar)
    
    
%% Comments
    % review for couplimSpec Toolbox
    
    
%% Author
    %  MF Devaux
    % INRA Nantes
    % BIA-PVPP
    
%% date
    % 25 mars 2014
    % 13 aout 2013
    % 14 decembre 2018 : replace length by size


%% start
if nargin >1
    error('use: code=findCode(vecChar)');
end


%% treatment
nb=size(vecChar,1);

code{1}=vecChar(1,:);
j=1;

for i=2:nb
    if ~sum(strcmp(vecChar(i,:),code))
        j=j+1;
        code{j}=vecChar(i,:); %#ok<AGROW>
    end;
end

code =char(code);

%% matlab function tracking  
% no function tracking
%% end


    