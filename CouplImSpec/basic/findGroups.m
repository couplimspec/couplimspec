  function groupe=findGroups(label,code,dep)
%

 %% description
    % attribute numbers to groups depending on codes
    
%% input

    % label : cell or char array of label
    % code : vector of cell or char array of code of groupes
    % dep : starting number for the groups
    
%% output
    
    %  gpe : vector of number corresponding to the groups from 1 to ngpe.
    %  

%% principe
    % each label is scree,ed for each group
    % the number return is the number of the code in the code vector
    % when no code s are found within some label, 0 is returned as group
    %  number
    
%% use
    % gpe=findGroups(label,code,dep)
    
%% Comments
    % review for couplImSpec Toolbox
    
    
%% Author
    % MF Devaux
    % BIA - INRA Nantes
    
    
%% date
    % 14 f�vrier 2014
    % 8 novembre 2013
    % 14 decembre 2018 : remove ;

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin ~=3
    error('use: gpe=findGroups(label,code,dep)');
end

%% test inputs
if iscellstr(label) %#ok<*ISCLSTR>
    label=char(label);
else
    if ~ischar(label)
        error('char array of cellstring array expected for individual labels')
    end
end

if ischar(code)
    code=cellstr(code);
else
    if ~iscellstr(code)
        error('char array of cellstring array expected for group codes')
    end
end


%% treatement
% number of groups
ngpe=size(code,1);

% find groups -----------------------------------------------
% initialisation
groupe=zeros(size(label,1),1);

for i = 1:ngpe
    groupe(strcmp(code{i},cellstr(label(:,dep:(dep-1+size(code{i},2))))))=i;
end

indice=find(groupe==0);
if indice
    'ATTENTION : some label were not assigned to a group' %#ok<NOPRT>
end

%% no function tracking

%% end

cd (orig)
    