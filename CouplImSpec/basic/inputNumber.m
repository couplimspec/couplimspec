  function valNum=inputNumber(varargin)
%

 %% description
    % return the number from a dialog window  
    
%% input

    %  first argument must be the prompt
    % optional arguments : 
    %   - 'def' : default value                 (default = nothing)
    %   - 'Vmin' : authorised minimum value     (default = nothing)
    %   - 'Vmax' : authorised maximum value     (default = nothing)
    %   - 'Title' : title of the widow          (default = nothing)
    
%% output
    
    %   valNum : returne the answer as a double

%% principe
    % use inpudlg with error checking
    % 

%% use
    % factMin=20;
%     vMin=1;
%     vMax=99;
% 
%     prompt = {'taille du facteur f � appliquer pour estimer l''intensit� des minimax relatifs (entre 1 et 99 %) :(Imax-Imin)/f'};
%     dlg_title = 'suppression des minimas';
    
%       f = inputNumber(prompt,'def',factMin,'vmin',vMin,'vmax',vMax,'title',dlg_title);

%% Comments
    % review for CouplImSpec toolbox
    
    
%% Author
    % Marie-Fran�oise Devaux
    % BIA-PVPP
    
%% date
    % 14 f�vrier 2014
    % 26 Aout 2013

%% context variables 

%% start

if nargin <1
    error('use: number = inputNumber(prompt,<''def'',xx>,<''Vmin'',xx>,<''vmax'',xx>,<''title'',''xx''>');
end

%% input 

prompt=varargin{1};

i=2;
while i<=nargin
    switch lower(varargin{i})
        case 'def'
            i=i+1;
            if isnumeric(varargin{i})
                def={num2str(varargin{i})};
            else
                error('numeric value expected for ''Def'' parameter')
            end
         case 'vmin'
            i=i+1;
            if isnumeric(varargin{i})
                vMin=varargin{i};
            else
                error('numeric value expected for ''Vmin'' parameter')
            end
         case 'vmax'
            i=i+1;
            if isnumeric(varargin{i})
                vMax=varargin{i};
            else
                error('numeric value expected for ''Vmax'' parameter')
            end            
        case 'title'
            i=i+1;
            if ischar(varargin{i})
                dlgTitle=varargin{i};
            else
                error('char array expected for ''Title'' parameter')
            end               
        otherwise
            error('wrong parameter. <''Def''>,<''Vmin''>,<''Vmax''>,<''Title''> expected');
    end
    i=i+1;
end;

if ~exist('dlgTitle','var')
    dlgTitle='';
end;

if ~exist('def','var')
    def={''};
end;

if exist('vMin','var')
    testMin=1;
else
    testMin=0;
end;

if exist('vMax','var')
    testMax=1;
else
    testMax=0;
end;
    
%% treatement

% defaulf values
num_lines = 1;
options.Resize='on';

suite=0;
while ~suite
    reponse = inputdlg(prompt,dlgTitle,num_lines,def,options);
    valNum=str2num(reponse{1}); %#ok<ST2NM>
    
    okMin=0;
    if testMin
        if valNum>=vMin
            okMin=1;
        end
    else
        okMin=1;
    end;
    
    okMax=0;
    if testMax
        if valNum<=vMax
            okMax=1;
        end
    else
        okMax=1;
    end;
    
    
    if okMin && okMax
        suite=1;
    end;
end;


%% matlab function tracking  
% no tracking
%% end


    