function don=alphaOrder(nom,nomsauve)
% classement des individus dans l'ordre alphab�tique des noms
% 			lecture du fichier de d�part soit .i .d et .v soit txt ou fda
%			ecriture 
%					dans nomsauve.txt si cet argument est pass� 
%			sinon dans nom.txt
%
%	la fonction renvoie �galemnt le tableau class� dans la m�moire
%
% USAGE :
%	 ordrab('nomgen')
% ou
%	 ordrab('nomdep','nomsauvegardre')
%
% AUTEUR : MF Devaux
%			  URPOI MicMac
% Version du 30/8/2000
% 

% if (nargin~=1)&(nargin~=2)
%    display('nombre de param�tres incorrect')
% 	error('USAGE : alphaOrder(<nom>) ou alphaOrder(<nomdep>, <nomres>)');
% end;

% image folder
[nom,rfolder]=uigetfile({'*.txt'},'file to be put in alphabetic order','*.txt');
cd(rfolder)

% lecture des donn�es
dep=readDIV(nom);

% classement des noms d'individus et r�cup�ration des indices de d�part
[don.i,indice]=sortrows(dep.i);

%classement du tableau de donn�es dans le m�m ordre
don.d=dep.d(indice,:);

%recopie des variables
don.v=dep.v;

[nomsauve,sfolder]=uiputfile({'*.txt'},'saveas',nom);

cd(sfolder)


writeDIV(don,nomsauve);
