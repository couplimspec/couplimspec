 function   coordImOrig=catCoordImOrig(coordImOrig1,coordImOrig2)


 %% description
    % concatenate couplimSpec coordImOrig field for userdata dso field
    
    % dso is a dataset object from eigenvector research see
    % http://www.eigenvector.com/software/dataset.htm
    
%% input
    % coordImOrig1 and coordImOrig2 : dso userdata filed to be concatenated
        
%% output
    
    % resulting coordImOrig userdata field 

%% principe
    %
    % dso.userdata.coordImOrig contain information concerning the coordinate of pixels in 
    % the image from which they were extracted. The information is required
    % for reprenting pixel scores in original image or other registrated
    % images
    % subfields of coordImOrig are: 
    % row: [1x41 double]					row coordinate (unit pixel)
    % col: [1x49 double]					column coordinate (unit pixel)
    % id: [1x49 integer]					number of the original image in
                                            % the following list image
    % image: [1x1 struct]					list of original images
    % 
    % >> dso.userdata. coordImOrig.image    contain fields:
    % imSize: [1x1 struct]					image size
    % imName: 'xxx.tif'					image name
    % imPathname: �xxxxxx�					 image pathname
    % 
    % >> dso.userdata. coordImOrig.image.imSize			contain fields:
    % row: 18							number of rows
    % col: 38							number of columns
    % 


%% use
     %  coordImOrig=catCoordImOrig(coordImOrig1,coordImOrig2)

    
%% Comments
    % written for couplimSpecToolbox
    % to be used in catdso function

    
    
%% Author
    % MF Devaux
    % BIA PVPP - INRA  Nantes
    
%% date
    % 3 avril 2014

 %% treatment
 % concatenate row and column coordinate values
 coordImOrig.row=[coordImOrig1.row ;coordImOrig2.row];
 coordImOrig.col=[coordImOrig1.col ;coordImOrig2.col];
            
 
 % control image number 
 if OkImageNumber(coordImOrig1.Id) 
     nmax1=max(coordImOrig1.Id);
 else
     error('invalid number of original image');
 end;
 
 if OkImageNumber(coordImOrig2.Id)
     nmax2=max(coordImOrig2.Id);
     coordImOrig2.Id=coordImOrig2.Id+nmax1;
 else
     error('invalid number of original image');
 end;

coordImOrig.Id=[coordImOrig1.Id coordImOrig2.Id];
nmax=nmax1+nmax2;

% image field
coordImOrig.image.name=coordImOrig1.image.name;
coordImOrig.image.name{(nmax1+1):nmax}=coordImOrig2.image.name;

coordImOrig.image.pathname=coordImOrig1.image.pathname;
coordImOrig.image.pathname{(nmax1+1):nmax}=coordImOrig2.image.pathname;


coordImOrig.imSize.row=[coordImOrig1.imSize.row ;coordImOrig2.imSize.row];
coordImOrig.imSize.col=[coordImOrig1.imSize.col ;coordImOrig2.imSize.col];

%% no function tracking
%% end
 
 end
            
            

%% sub functions
function flag=OkImageNumber(imNumber)

%% check if the imNumber range from 1 to N with a 1 increment.
% If not : if may reveal a mistake in the image name.

%% initialisation
flag=1;

%% treatment

number=unique(imNumber);

% start from 1 ?
if min(number)~=1
    flag=0;
end

% is the increment  = 1 ?
diffNumber=unique(diff(number));

if length(diffNumber)~=1 || diffNumber~=1
    flag=0;
end;
  
end







