function selecSpec

%% description
% hyperpsectral images : select spectra on the basis of their maximum value

%% input parameters can be
% - no parameter
% 

%% output
% - nothing
% 

%% principle
% two cases are treated
%   1- all spectra with maximum absorbance value under a given threshold
% value are removed from the dataset object
%   2 - all spectra with maximum value over a given threshold are removed
%
% the threshold is given for a given range of wavenumbers
% 
% pixel coordinates are also modified
%
% specra and coordinates are saved as dso and DIV files respectively in the
% same folder chosen by the user

%% use
%   selecSpec;
% or
%   [sim, coord]=selecSpec; 
% or
%   [sim, coord]=selecSpec(ims.filename,maxThreshold,sfolder,<coord.filename>'



%% Comments, context
% written for the proposal SOLEIL 20150929


%% Author
% MF Devaux
% BIA - PVPP INRA Nantes

%% date
% 16 october 2018
% 5 novembre 2018 to manage also coordinates
% 20 decembre to take into account both low intensity and high intensity in
% some regions of the spectra
% plus : only interactive function

%% context variables
orig=pwd; % returns the current directory



%% input data
if nargin~=0 
    disp('Wrong number of parameter')
    error('Usage: selecSpec');
end


% input image
[nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

cd(repl)
ims=loaddso(nom);

%choose threshold for max value
maxthr=0.1;                 % value generally defined for FTIR spectra
vMin=0;                     % positive value required
prompt = {'threshold value of maxima of spectra:'};
dlg_title = 'MAXIMA THRESHOLD ';
maxthr = inputNumber(prompt,'def',maxthr,'vmin',vMin,'title',dlg_title);

% choose wavenumber range
wholeSpectra=yesno('apply threshold on the whole spectra');

if ~wholeSpectra
    prompt = {'Enter minimum value','Enter maximum value'};
    dlg_title = 'Wavenumber Range';
    dims = [1 35];
    definput = {'1200','1300'};
    answer = inputdlg(prompt,dlg_title,dims,definput);
    minww=str2double(answer{1});
    maxww=str2double(answer{2});
    minw=find(abs(ims.axisscale{2}-minww)<3);
    maxw=find(abs(ims.axisscale{2}-maxww)<3);
    if minw>maxw
        tmp=minw;
        minw=maxw;
        maxw=tmp;
    end
else
    minw=1;
    maxw=length(ims.axisscale{2});
end

choixEl=yesno(sprintf('Yes to eliminate spectra BELOW the threshold value %6.2f\r\nNo to eliminate spectra OVER the threshold value',maxthr));

% coordinate
flagCoord=yesno('associate coordinates files ?');
if flagCoord
    [nomCoord,replCoord]=uigetfile({'*.pixcoord.txt'},'name of file contaning coordinate of spectra:','*.pixcoord.txt');
    cd(replCoord)
    coord=readDIV(nomCoord);
end

% save
[sname,sfolder]=uiputfile({'*.mat'},'save file as',strrep(nom,'.dso.mat','.selsp.dso.mat'));
sname=strrep(sname,'.dso.mat','');



%

%% selec the sub-set of spectra
% region of wavenumbers :

spectre=ims.data(:,minw:maxw);

if choixEl
    ispectre=max(spectre,[],2)>maxthr;
else
    ispectre=max(spectre,[],2)<maxthr;
end


sim=ims(ispectre,:);                        % initialisation of the resulting dataset object
sim.name=sname;
sim.type='data';

if flagCoord
    % select corresponding coordinates
    coord.d=coord.d(ispectre,:);
    coord.i=coord.i(ispectre,:);
end

%% save
% dso
cd(sfolder);
savedso(sim,sname,sfolder);
if flagCoord
    writeDIV(coord,strcat(sname,'.pixcoord.txt'));
end

%% matlab function tracking
%TDO : coordinate
fid=fopen(strcat(sim.name,'.track.txt'),'w');

if fid==0
    errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Selec spectra that show a maximum value over a given threshold \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'data folder: %s\r\n\r\n',repl);
fprintf(fid,'Input spectral image name: %s.mat\r\n',ims.name);
if flagCoord
    fprintf(fid,'Associated spectra coordinates filename: %s\r\n\r\n',nomCoord);
end

fprintf(fid,'Initial number of spectra: %d\r\n\r\n',size(ims,1));

fprintf(fid,'Maxima threshold: %8.4f \r\n\r\n',maxthr);

if choixEl
    fprintf(fid,'Spectra BELOW the threshold value are eliminated\r\n');
else
    fprintf(fid,'Spectra OVER the threshold value are eliminated\r\n');
end

if wholeSpectra
    fprintf(fid,'Maximum threshold applied on Whole spectra\r\n');
else
    fprintf(fid,'Maximum threshold applied on the spectral region: \r\n');
    fprintf(fid,'\t[%6.2f - %6.2f] %s\r\n\r\n',minww,maxww,ims.userdata.acquisition.unit);
end


fprintf(fid,'Number of selected spectra: %d\r\n\r\n',size(sim,1));

fprintf(fid,'saved file name : %s \r\n',sname);
if flagCoord
    fprintf(fid,'Associated spectra coordinates filename: %s\r\n\r\n',strcat(sname,'.pixcoord.txt'));
end
fprintf(fid,'save folder : %s \r\n\r\n',sfolder);



% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);


fclose(fid);

%% end
cd (orig)

end


