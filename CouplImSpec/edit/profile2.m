function varargout = profile2(varargin)

 %% extract spectra from spectral image along a profile.
    
% PROFILE2 M-file for profile2.fig
%      PROFILE2, by itself, creates a new PROFILE2 or raises the existing
%      singleton*.
%
%      H = PROFILE2 returns the handle to a new PROFILE2 or the handle to
%      the existing singleton*.
%
%      PROFILE2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROFILE2.M with the given input arguments.
%
%      PROFILE2('Property','Value',...) creates a new PROFILE2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before profile_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to profile2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help profile2

% Last Modified by GUIDE v2.5 19-Aug-2013 10:32:56

    
%% input

    %  nothing : GUI
    
%% output
    
    % 

%% principe
    % 
    % 

%% use
    % profile2
    
%% Comments
    % cadre THese Fatma Allouche
% d'apres profile2.m
% Date : 28 janvier 2008
% AUteur : marie-Fran�oise devaux
%   BIA-PV
%
% modification du programme pour prendre en compte le fait que les images
% spectrales (ou multivariees) ne sont pas seulement avec fond clair
% une selection des images a prendre en compte pour le trace du profil est
% incluse
% Il est demande de donner un code pour les pixels selectionnes : unique
% pour l'image
% modification pour prise en compte de l'existanece potentielle d'une image fond clair dasn le userdata du dso
% modification pour choix du plan ou de la longueur d'onde a afficher
    
%% Author
    %  MF Devaux
    % BIA -PVPP
    
%% date
    % 31 mars 2014 : remove image coordinate : linear indice are recorded
    % as pixel axisccale : image size is required and is diffucult to
    % record. The information if found in the file name + in the coordxy
    % file. Coordxy file should be renamed RCcoord in  a future version
    % 5 mai  2011
    % 11 mai 2012
    % 13 aout 2013
    


%% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @profile2_OpeningFcn, ...
                   'gui_OutputFcn',  @profile2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% --- Executes just before profile2 is made visible.
function profile2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to profile2 (see VARARGIN)

% Choose default command line output for profile2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% UIWAIT makes profile2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%% --- Outputs from this function are returned to the command line.
function varargout = profile2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%% --- Executes on button press in load_prof.
function load_prof_Callback(hObject, eventdata, handles)
% hObject    handle to load_prof (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recuperation des donnees
data=get(handles.figure1,'UserData');
if ~isempty(data)
    prof_clear_Callback(hObject, eventdata, handles);
    rfolder=data{3};
    api=data{27};
    clear data;
    ho=findobj('tag','aff_min_profile');
    set(ho,'Value',0);
    ho=findobj('tag','aff_max_profile');
    set(ho,'Value',1);
    ho=findobj('tag','zoom_profile');
    minZoom=0.1;
    maxZoom=20;

    v=(1-minZoom)/(maxZoom-minZoom);
    set(ho,'Value',v);

    cd(rfolder)
end;

% lecture de l'image
disp('lecture de l''image....')
[fname,rfolder]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');
ext='.dso.mat';
nomgen=strrep(fname,ext,'');
cd(rfolder)
im=loaddso(fname);

% choix de l'image a afficher
[imorig,imTitle,listcanaux,npossInit,exc]=choixImage(im);

% affichage de la liste des images dans la listbox
ho=findobj('Tag','listCanaux');
set(ho,'string',listcanaux);

if exist('api','var')
    api.replaceImage(imorig,[])
else
    set(handles.figure1,'CurrentAxes',handles.image_profile)
    hIm=imshow(imorig,[]);
    hSP = imscrollpanel(handles.figure1,hIm);
    set(hSP,'Units','normalized','Position',[0.05 .15 0.55 .70])
    hMagBox = immagbox(handles.figure1,hIm);
    pos = get(hMagBox,'Position');
    set(hMagBox,'Position',[0 0 pos(3) pos(4)])
    imoverview(hIm)
    
    api = iptgetapi(hSP);
end;

api.setMagnification(api.findFitMag())


ho=findobj('Tag','nomgen_prof');
set(ho,'string',sprintf('%s - %s',nomgen,imTitle));



data{1}=im;                     % image spectrale
data{2}=nomgen;                 % nom de l'image
data{3}=rfolder;                % dossier de lecture de l'image
data{4}=imorig;                 % image affichee
data{5}=listcanaux;             % liste des images affichables
data{6}=npossInit;              % debut de la liste des canaux de l'image dans la liste des images affichables
data{7}=exc;                    % longeuurs d'onde d'excitation  si elles existent

data{27}=api;                   % controle de la figure 
    
set(handles.figure1, 'UserData', data);

% Update handles structure
guidata(hObject, handles);


%% --- Executes on button press in tracer_prof_profils.
function tracer_prof_profils_Callback(hObject, eventdata, handles)
% hObject    handle to tracer_prof_profils (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recuperation des donnees
data=get(handles.figure1,'UserData');
im=data{1};
nomgen=data{2};
rfolder=data{3};
imorig=data{4};
if ~isempty(data{8})
    coord=data{8};
    cx=coord(:,1);
    cy=coord(:,2);
    data{8}=[];
end;
if ~isempty(data{9})
    precode=data{9};
    data{9}=[];
end;

if length(data)>=28
    hl=data{28};
    nbl=length(hl);
else
    nbl=0;
end;


% choix de la ligne d'interet
set(handles.figure1,'CurrentAxes',handles.image_profile);
zoom off
if ~exist('cx','var')
    [cx,cy,~]=improfile;
    % mettre les indices en entier
    cx=round(cx);
    cy=round(cy);
    cx(cx==0)=1;
    cx(cx>size(imorig,2))=size(imorig,2);
    cy(cy==0)=1;
    cy(cy>size(imorig,1))=size(imorig,1);
end;

% affichage de la ligne selectionnee dans l'image
nbl=nbl+1;
hold on
hl(nbl)=plot(cx,cy,'r','LineWidth',2);
hold off


% selection des indices lineaires
indice=sub2ind(size(imorig),cy,cx);

% selection des spectres
ims=im(indice);
ims.type='data';

if exist('precode','var')
    def=precode;
else
    def='';
end;
code=inputdlg('code des pixels selectionnees','code des pixels',1,{def});
ims.name=strcat(ims.name,'.',code);

% constitution des noms d'individus a partir des codes
ims.label{1}=repmat(code,length(indice),1);

% inclusion des coordonnees dans le champ axiscale
% ims.axiscale{1,1} correspond au numero du spectra dans l'image de depart
%ims.axisscale{1,2}=cx;
%ims.axisscale{1,3}=cy;

% sauvegarde des resultats
sname=strcat(ims.name,'.dso.mat');
[sname,sfolder]=uiputfile({'*.dso.mat'},'nom de sauvegarde',sname);

cd(sfolder)
save(sname,'ims')

% fichier des coordonnees
xy.d(:,1)=cx;
xy.d(:,2)=cy;
xy.i=char(repmat(code,length(cx),1));
xy.v=char('cx','cy');
writeDIV(xy,strcat(ims.name,'.profxy.txt'))

% affichage des profils selectionnees sur une nouvelle figure
plotdso(ims,1,1,sfolder);

% sauvegarde des images produites : image avec la position de la ligne
% et figure des profils extraits
% profile2
h=figure;
imshow(imorig,[])
hold on
plot(cx,cy,'r','LineWidth',2);

hold off
title(ims.name);
nomfic=strcat(ims.name,'.profim.jpg');
saveas(gcf,nomfic,'jpg')
close(h);

data{28}=hl;

set(handles.figure1, 'UserData', data);


%% matlab function tracking  

fid=fopen(strcat(sname,'.track.txt'),'a');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'select spectra from a profile line in the spectral image \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\ninput file name: %s.dso.mat\r\n',im.name);
fprintf(fid,'data folder: %s\r\n',rfolder);

fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'data folder: %s\r\n',sfolder);

fprintf(fid,'\n coordinates of selected pixels in image : %s.profxy.txt\r\n',nomgen);
fprintf(fid,' code of selected pixels : %s\r\n',char(code));

fprintf(fid,'\n graphs\n \t - spectra of selected pixels : %s.sp.tif\r\n',ims.name);
fprintf(fid,'\t - position in image : %s.profim.jpg\r\n',ims.name);



% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end;

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

% retour au repertoire de depart
%cd(rfolder)



%% --- Executes on button press in load_coordprof_profile.
function load_coordprof_profile_Callback(hObject, eventdata, handles)
% hObject    handle to load_coordprof_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% chargement d'un fichier de coordonnees de pixels dans le but de calculer
% des profils sur une autre image. Par exemple : avant et apres
% standardisation des niveaux de gris

% recuperation des donnees
data=get(handles.figure1,'UserData');
if length(data)>=7
    im=data{1};
    nomgen=data{2};
    rfolder=data{3};
    imorig=data{4};
    listcanaux=data{5};
    npossInit=data{6};
    exc=data{7};
   
else
    load_prof_Callback(hObject, eventdata, handles);
    
end;

% chargement du fichier des coordonnees : 
[nom_coord,nom_rep_coord] = uigetfile({'*profxy.txt'},'nom du fichier des coordonnees');
cd(nom_rep_coord)

xy=readDIV(nom_coord);                  % fichier des coordonnees x et y

% recuperation du code des points aa partir du nom du fchier
[posprofxy]=findstr(nom_coord,'.profxy.txt');
if ~isempty(posprofxy)
    posp=findstr(nom_coord(1:posprofxy),'.');
    if ~isempty(posp)
        l=length(posp);
        precode=nom_coord((posp(l-1)+1):(posprofxy-1));
    end;
end;
    
data{8}=xy.d;
if exist('precode','var')
    data{9}=precode;
end;
set(handles.figure1, 'UserData', data);

tracer_prof_profils_Callback(hObject, eventdata, handles)

% retour au repertoire de depart
%cd(rfolder)







%% --- Executes on button press in prof_clear.
function prof_clear_Callback(hObject, eventdata, handles)
% hObject    handle to prof_clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recuperation des donnees
data=get(handles.figure1,'UserData');


if length(data)>=28
    hl=data{28};
    
    nbl=length(hl);
    
    for i=1:nbl
        set(hl(i),'visible','off')
    end;
end;

% 


%%
function image_profile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prof_clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% recuperation des donnees

%% 
function [imorig,imTitle,listcanaux,npossInit,exc]=choixImage(im)

if isfield(im.userdata, 'brightfieldimage')
    listcanaux={'brightfield','sum of intensity'};
    nposs=2;
else
    listcanaux={'sum of intensity'};
    nposs=1;
end;
npossInit=nposs;
exc=[];
if strcmp(im.userdata.acquisition.method,'CONFOCAL')
    % trouver les longueurs d'onde d'excitation
    excem=im.label{2};
    exct=str2num(excem(:,1:3)); %#ok<*ST2NM>
    exc=unique(exct);
    
    
    for i=1:length(exc)
        listcanaux{i+nposs}=['sum of intensity for excitation ',num2str(exc(i))];
    end;
    
    nposs=nposs+length(exc);
end;

if strcmp(im.userdata.acquisition.method,'MACROFLUO')
    % trouver les longueurs d'onde d'excitation
    excem=im.label{2};
    exct=excem(:,1:2);
    exc=findCode(exct);
    
    
    for i=1:size(exc,1)
        listcanaux{i+nposs}=['sum of intensity for excitation: ',exc(i,:)];
    end;
    
    nposs=nposs+size(exc,1);
end;
    
% cas general
canaux=char(im.label{2});
for i=1:size(canaux,1);
    listcanaux{i+nposs}=['channel: ',canaux(i,:)];
end;


if isfield(im.userdata, 'brightfieldimage')
    imorig=im.userdata.brightfieldimage;
else
    imorig=sum(im.imagedata,3);
end;
numCanal=1;
imTitle=listcanaux{numCanal};

%%
function imorig=selImage(im,numCanal,npossInit,exc)

if numCanal==1 && npossInit==2
    imorig=im.userdata.brightfieldimage;
end
if (numCanal==1 && npossInit==1) || (numCanal==2 && npossInit==2)
    imorig=sum(im.imagedata,3);
end
if ~exist('imorig','var')
    nposs=npossInit;
    switch im.userdata.acquisition.method
        case'CONFOCAL'
            if numCanal<=(nposs+length(exc))
                excem=im.label{2};
                exct=str2num(excem(:,1:3));
                numExc=numCanal-nposs;
                indIm=exct==exc(numExc);
                imorig=sum(im.imagedata(:,:,indIm),3);
            else
                nposs=nposs+length(exc);
                numCanal=numCanal-nposs;
                imorig=im.imagedata(:,:,numCanal);
            end;
        case 'MACROFLUO'
            if numCanal<=(nposs+size(exc,1));
                numExc=numCanal-nposs;
                indIm=((numExc-1)*3+1):numExc*3;
                imorig=sum(im.imagedata(:,:,indIm),3);
            else
                nposs=nposs+size(exc,1);
                numCanal=numCanal-nposs;
                imorig=im.imagedata(:,:,numCanal);
            end;
        otherwise
            numCanal=numCanal-nposs;
            imorig=im.imagedata(:,:,numCanal);
    end
end

%% --- Executes on selection change in listCanaux.
function listCanaux_Callback(hObject, eventdata, handles)
% hObject    handle to listCanaux (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listCanaux contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listCanaux


% recuperation des donnees
data=get(handles.figure1,'UserData');
im=data{1};
nomgen=data{2};
rfolder=data{3};
imorig=data{4};
listcanaux=data{5};
npossInit=data{6};
exc=data{7};
api=data{27};


ho=findobj('Tag','listCanaux');
numCanal=get(ho,'Value');

fZoom=api.getMagnification();
pos=api.getVisibleLocation();

% recalcul de l'image a afficher
imorig=selImage(im,numCanal,npossInit,exc);

imTitle=listcanaux{numCanal};

api.replaceImage(imorig,[])
api.setMagnification(fZoom);
api.setVisibleLocation(pos);

ho=findobj('tag','aff_min_profile');
set(ho,'Value',0);
ho=findobj('tag','aff_max_profile');
set(ho,'Value',1);

ho=findobj('Tag','nomgen_prof');
set(ho,'string',sprintf('%s - %s',nomgen,imTitle));

data{4}=imorig;
set(handles.figure1, 'UserData', data);


% --- Executes during object creation, after setting all properties.
function listCanaux_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listCanaux (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function aff_min_profile_Callback(hObject, eventdata, handles)
% hObject    handle to aff_min_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
data=get(handles.figure1,'UserData');
api=data{27};
imorig=data{4};

minIm=min(imorig(:));
maxIm=max(imorig(:));

fZoom=api.getMagnification();
pos=api.getVisibleLocation();

v=get(hObject,'Value');

affMin=v*(maxIm-minIm)+minIm;

ho=findobj('tag','aff_max_profile');
v=get(ho,'Value');
affMax=v*(maxIm-minIm)+minIm;

if affMin>affMax
    affMin=affMax;
    v=(affMin-minIm)/(maxIm-minIm);
    set(hObject,'Value',v);
end;

api.replaceImage(imorig,[affMin affMax])

api.setMagnification(fZoom);
api.setVisibleLocation(pos);

% --- Executes during object creation, after setting all properties.
function aff_min_profile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to aff_min_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',0);

% --- Executes on slider movement.
function zoom_profile_Callback(hObject, eventdata, handles)
% hObject    handle to zoom_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
data=get(handles.figure1,'UserData');
api=data{27};

v=get(hObject,'Value');

minZoom=0.1;
maxZoom=20;

fZoom=v*(maxZoom-minZoom)+minZoom;

api.setMagnification(fZoom);



% --- Executes during object creation, after setting all properties.
function zoom_profile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zoom_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

minZoom=0.1;
maxZoom=20;

v=(1-minZoom)/(maxZoom-minZoom);

set(hObject,'Value',v);




% --- Executes on slider movement.
function aff_max_profile_Callback(hObject, eventdata, handles)
% hObject    handle to aff_max_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
data=get(handles.figure1,'UserData');
api=data{27};
imorig=data{4};

minIm=min(imorig(:));
maxIm=max(imorig(:));

fZoom=api.getMagnification();
pos=api.getVisibleLocation();

v=get(hObject,'Value');
affMax=v*(maxIm-minIm)+minIm;


ho=findobj('tag','aff_min_profile');
v=get(ho,'Value');
affMin=v*(maxIm-minIm)+minIm;

if affMin>affMax
    affMax=affMin;
    v=(affMax-minIm)/(maxIm-minIm);
    set(hObject,'Value',v);
end;

api.replaceImage(imorig,[affMin affMax])

api.setMagnification(fZoom);
api.setVisibleLocation(pos);

% --- Executes during object creation, after setting all properties.
function aff_max_profile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to aff_max_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

set(hObject,'Value',1);


% --- Executes on button press in resetZoom_profile.
function resetZoom_profile_Callback(hObject, eventdata, handles)
% hObject    handle to resetZoom_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


data=get(handles.figure1,'UserData');
api=data{27};

api.setMagnification(api.findFitMag())
fZoom=api.getMagnification();

minZoom=0.1;
maxZoom=20;

v=(fZoom-minZoom)/(maxZoom-minZoom);

ho=findobj('tag','zoom_profile');
set(ho,'Value',v);






% --- Executes on button press in resetMinMaxProf.
function resetMinMaxProf_Callback(hObject, eventdata, handles)
% hObject    handle to resetMinMaxProf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=get(handles.figure1,'UserData');
api=data{27};
imorig=data{4};


fZoom=api.getMagnification();
pos=api.getVisibleLocation();

api.replaceImage(imorig,[])

api.setMagnification(fZoom);
api.setVisibleLocation(pos);

ho=findobj('tag','aff_min_profile');
set(ho,'Value',0);
ho=findobj('tag','aff_max_profile');
set(ho,'Value',1);


% --- Executes during object creation, after setting all properties.
function logoProfile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to logoProfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate logoProfile

logoMF=imread('logo/couplimspec.tif');
axes(hObject)
imshow(logoMF,[])
