function sim=selecVar(varargin)

 %% description
    % select a range of wavelength in a spectral image
    
%% input parameters can be
    % - no parameter
    % or
    % ims - spectral image in dso format
    % lo - vector of two values indicating the
    %                               wavelengths or wavenumbers that delimitate the band
    % sfolder - save folder 
    
%% output
    
    % - nothing 
    % or
    % sim - data set object with a reduce wavelength

%% principle
    %
    % update data set with the slected range of wavelength

%% use
    % selecvar;
    
    % ims=readomnic;
    % sim=selecvar(ims,[1200 800])
    
%% Comments, context
    % written for the PhD thesis of Fatma Allouche
    
    
%% Author
    % MF Devaux 
    % BIA - PVPP INRA Nantes
    
%% date
    % 15 mars 2011*
    % 6 mars 2014
    % 8 f�vrier 2018 to reverse x axis for infarred and raman spectra
    % 21 novembre 2019 : polypheme

%% context variables 
orig=pwd; % returns the current directory



%% input data
if nargin == 0
    % input image
    [nom,repl]=uigetfile({'*.dso.mat'},'name of spectral image','*.dso.mat');

    cd(repl)
    load(nom);
    ims=dso;
    clear dso;

    %choose wavelengths
    [lo,indicelo]=choix_param_selecvar(ims);
    
    [sname,sfolder]=uiputfile({'*.mat'},'nom de sauvegarde du fichier r�sultat',strrep(nom,'.dso.mat','.sv.dso.mat'));
    sname=strrep(sname,'.dso.mat','');
     
end

if nargin >= 1
    % test si l'image en entr�e est un dataset or a file
    ims=varargin{1};
    if ischar(ims)
        nom=ims;
         % test du type de fichier
        point=strfind(nom,'.');
        point=point(length(point)-1);
        ext=nom((point+1):(length(nom)));
        if ~strcmp(ext,'dso.mat')
             error('format .dso.mat attendu')
        end
        load(nom)
        ims=dso;
        clear dso;
    else if ~strcmp(class(ims),'dataset')
        error('give a data set as first input parameter');
        end
    end

    repl=pwd;                               % reading folder
end

if nargin == 1
    %choose wavelengths
    [lo,indicelo]=choix_param_selecvar(ims);
    sfolder=repl;
    sname=strcat(ims.name,'.sv');
end

if nargin >= 2
    lo=varargin{2}; 
    if ~strcmp(class(lo),'double')
        error('Wrong wavelength or wavenumber (real value expected)');
    end
    if length(lo)~=2
        error('Wrong number of wavelength or wavenumber (2 expected)');
    end
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    if lo(1)<min(lot)|| lo(1)>max(lot)
        error('Wrong wavelength value 1: must be between %s and %s',min(lot),max(lot))
    end
    if lo(2)<min(lot)|| lo(2)>max(lot)
        error('Wrong wavelength value 2: must be between %s and %s',min(lot),max(lot))
    end
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));
    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));
    
end

       
if nargin == 2
    sfolder=repl;
    sname=strcat(ims.name,'.sv');
end

if nargin == 3
    sfolder=varargin{3};
    if ~exist(sfolder,'dir')
        error('%s directory not found',sfolder);
    end
    sname=strcat(ims.name,'.sv');
end

if nargin >3
    disp('Wrong number of parameter')
    error('Usage: sim=selecvar(ims, wavelength or wavevenumber,sfolder');
end

% test des param�tres de sortie
if  nargout >1
    error('wrong number of output parameter')
end
% 
%% selec the sub-set of variables

       
% correction de ligne de base des spectres pour la bande consid�r�e
if indicelo(1)<indicelo(2)      % cas fluorescence (set longueur d'onde)
    i1=indicelo(1);
    i2=indicelo(2);
else                            % cas IR ou Ramand avec nombre d'onde invers�s dans les graphiques
                                % mais dans le sens croissant dans la matrice des donn�es
    i1=indicelo(2);
    i2=indicelo(1);
end

sim=ims(:,i1:i2);                        % initialisation of the resulting dataset object

if ~exist('sname','var')
    sim.name=strcat(sim.name,'.sv');
else
    sim.name=sname;
end

%% plot imagedata sum 

if strcmp(sim.type,'image')
    h=figure(27);
    set(h,'windowstyle','docked');
    area=sum(sim.imagedata,3);

    imagesc(sim.userdata.acquisition.coordStage.col,sim.userdata.acquisition.coordStage.row,area);
    colormap(jet)
    title(sim.name,'Fontsize',12);
    set(gca,'dataAspectRatio',[1 1 1])
end


%% save 
% dso
cd(sfolder);
savedso(sim,sname,sfolder);

%% matlab function tracking  

fid=fopen(strcat(sim.name,'.track.txt'),'w');

if fid==0
     errordlg('enable to open track file');
end

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,'Selec var from a spectral image \r\n');
fprintf(fid,'__________________________________________________________________________\r\n');

fprintf(fid,'\r\nInput spectral image name: %s.mat\r\n',ims.name);
fprintf(fid,'data folder: %s\r\n\r\n',repl);

fprintf(fid,'selection of %s between %d and %d %s \r\n',lower(ims.title{2}),round(lo(1)), round(lo(2)),ims.userdata.acquisition.unit);

fprintf(fid,'\r\nsaved file name : %s \r\n',sname);
fprintf(fid,'\r\nsave folder : %s \r\n',sfolder);


% save of function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
os=computer;        % return the type of computer used : windows, mac...
switch os(1)
    case 'P'                        % for windows
        ind=strfind(info,'\');                          
    case 'M'                        % for Mac
        ind=strfind(info,'/');
    otherwise
        ind=strfind(info,'/');      % for UNIX, Linux (to be checked)
end

repprog=info(1:(ind(length(ind))-1));
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);


fclose(fid);

%% end
cd (orig)

end

%% sub - functions
function [lo,indicelo]=choix_param_selecvar(ims)

   % selection of the wavelengths 
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(1)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        if st==0
            st=st+1;
        end
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.userdata.acquisition.method,'INFRARED') || strcmp(ims.userdata.acquisition.method,'RAMAN') 
            set(gca,'Xdir','reverse');
        end
        xlabel([ims.title{2} ' (' ims.userdata.acquisition.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,y]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=0;
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(round(x))});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in selecvar',ims.title{2});
                else
                    ok=0;
                end
            end
        end
        hold off
    end
    lo(1)=x;
    clear x
    clear y
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        if st==0
            st=1;
        end
        plot(lot,ims.data(1:st:nbs,:)); 
         if strcmp(ims.userdata.acquisition.method,'INFRARED') || strcmp(ims.userdata.acquisition.method,'RAMAN') 
            set(gca,'Xdir','reverse');
        end
        xlabel([ims.title{2} ' (' ims.userdata.acquisition.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x,y]=ginput(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=0;
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(round(x))});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in selecvar',ims.title{2});
                else
                    ok=0;
                end
            end
        end
        hold off
    end
    lo(2)=x;
    clear x
    clear y                
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));

    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));

    clear silo
end
    