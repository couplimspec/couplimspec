function removeSpectra
%
%% description
% remove spectra with strange baseline
%
%% input
%           pas de param�tres
%
%% output
%           nothing : data set saved on disk
%
%% principe
%
%  compare spectra and baseline and select yes or no to keep the spectra
%
%% Use :
%        removeSpectra
%
%
%% Auteur :
%           MArie-Fran�oise Devaux et Sylvie Durand
%           INRA - BIA - PVPP
%
%% Date : 12 mars 2010
%       23 f�vrier 2018
%
%

%% context variables
porig=pwd;

%% input

if nargin~=0 
    error('use:  removeSpectra)');
end


[nom,rfolder]=uigetfile({'*.mat'},'nom du fichier des spectres de d�part : ','*.dso.mat');
cd(rfolder)



[nomcoord,rfoldercoord]=uigetfile({'*.txt'},'nom du fichier des coordonnees spectres de d�part : ','*.coord.txt');
cd(rfoldercoord)



snom=strcat(strrep(nom,'.dso.mat',''),'.sp.dso.mat');


[~,sfolder]=uiputfile({'*.mat'},'save resulting file as dso: ',snom);






%% treatment

cd(rfolder)

ims=loaddso(nom);

imsb=loaddso(strrep(nom,'.dso.mat','.base.dso.mat'));

baseline=loaddso(strcat(ims.name,'.baseline.dso.mat'));

cd(rfoldercoord)

coord=readDIV(nomcoord);

h=figure;
set(h,'units','normalized','position',[0.2 0.1 0.7 0.8]);


garde=logical(zeros(size(ims,1)));

for i=1:size(ims,1)
    plot(ims.axisscale{2},ims.data(i,:))
    hold on
    plot(ims.axisscale{2},baseline.data(i,:))
    hold off
    title(ims.label{1}(i,:))
    set(gca,'xdir','reverse');
    ylim([min(ims.data(:)) max(ims.data(:))]);
    ok=yesno('on le garde ?');
    garde(i)=ok;
end

ims=ims(garde,:);
ims.name=strcat(ims.name,'.sp');

imsb=imsb(garde,:);
imsb.name=strcat(imsb.name,'.sp');

baseline=baseline(garde,:);
baseline.name=strrep(baseline.name,'baseline','sp.baseline');

co.d=coord.d(garde,:);
co.i=coord.i(garde,:);
co.v=coord.v;



%% sauvegarde des r�sultats
cd(sfolder)
savedso(ims);
savedso(imsb);
savedso(baseline);

writeDIV(co,strcat(ims.name,'.coord.txt'));


%% trace de l'ex�cution du programme

    
    fic=fopen(strcat(strrep(ims.name,'.dso.mat',''),'.track.txt'),'w');
    if fic==0
        errordlg('probl�me d''�criture du fichier %s',strcat(strrep(ims.name,'.dso.mat',''),'.track.txt'));
    end
    
    fprintf(fic,'%s\r\n',datestr(now,0));
    fprintf(fic,'__________________________________________________________________________\r\n');
    fprintf(fic,'\t\t selec spectra from baseline correction\r\n\r\n');
    fprintf(fic,'Read folder :\r\n\t- %s\r\n',rfolder);
    fprintf(fic,'\r\nFile : %s\r\n',nom);
    fprintf(fic,'\r\n\r\n');
    
     fprintf(fic,'Read folder for initial coordinate:\r\n\t- %s\r\n',rfoldercoord);
    fprintf(fic,'\r\nFile : %s\r\n',nomcoord);
    fprintf(fic,'\r\n\r\n');
    
   
    fprintf(fic,'\r\n\r\n');
    fprintf(fic,'save file :%s \r\n',ims.name);
    fprintf(fic,'in folder: %s \r\n',sfolder);
    
    % sauvegarde du programme utilis�
    fprintf(fic,'__________________________________________________________________________\r\n');
    info=which(mfilename);
    repprog=fileparts(info);
    fprintf(fic,'Programme : %s ',mfilename);
    res=dir(info);
    fprintf(fic,'du %s\r\n',res.date);
    fprintf(fic,'\r\nR�pertoire du programme : %s \r\n',repprog);
    
    fclose(fic);
    
    
    %% fin
    cd(porig)
