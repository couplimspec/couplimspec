function don=selecGpe(nom,ncode,dep,no)
%% description
% s�lection d'un sous ensemble de donn�es selon un crit�re de nom des
% individus

%% input
% nom : nom du fichier de d�part
% ncode : nom du fichier des codes de groupe
% dep : position de d�part du code dans les noms d'individus
% no : num�ro du groupe s�lectionn�

% output
% don : fichier r�sultat des donn�es s�lectionn�es

%% principle
% le choix se fait en fonction d'un code positionn� au meme point de d�part
% pour tous les individus
% le fichier est sauvegard� sous le nom nom.code.txt ou <code> est le code
% du groupe s�lectionn�
%
%% comment
% review for couplImSpecToolbox
%
%% AUTHOR
% Auteurs : M-F Devaux 
% BIA-PVPP
% URPOI-Paroi
% version du 30/06/2003
%
%% date
% 14 f�vrier 2014
%

%%start
% r�pertoire d'origine
porig=pwd;

if nargin==0
    % lecture du fichier de d�part
    [don,nom,repertoire]=readDIV;
    cd(repertoire)
    % lecture des codes de groupe
    code=readStringVec;
    % choix des groupes
    ok=0;
    while ~ok
        [no,ok] = listdlg('ListString',code)
    end;
    nbg=length(no);   % nombre de groupes retenus
    % lecture du num�ro de d�part dans les noms des individus
    message='donner le num�ro de d�part des codes de groupes ';
    for i=1:nbg
        message =[message '<' code(no(i),:) '> '];
    end;
    dep = inputdlg([message '(exemple : ' don.i(1,:) ' )'],'position des codes dans les noms d''individus' );
    dep=str2num(char(dep));
else
    if nargin ==4
        don=lire(nom);
        if ~isstruct(don)
            errordlg(['pas de fichier de nom g�n�rique <' nom '>']);
            return;
        else
            code=lire_vec_char(ncode);
            if ~code
                errordlg(['pas de fichier de nom g�n�rique <' ncode '>']);
                return;
            else
                if (dep<=0)| (dep>size(don.i,2))
                    errordlg(['valeur de position des codes dans les noms d''individu : <' num2str(dep) '> : non valide']);
                    return;
                else
                    nbg=length(no);   % nombre de groupes retenus
                    for i=1:nbg
                        if (no(i)<0)|(no(i)>size(code,1))
                            errordlg(['num�ro de groupe s�lectionn� : <' num2str(no(i)) '> : non valide']);
                            return;
                        end;
                    end;
                end;
            end;
        end;
    else
        errordlg('usage <don=selecgpe> ou <don=selecgpe(nom,ncode,dep,no)>')
        return;
    end;
end;

% s�lection des individus du groupe ou des groupes retenus

% indice des individus s�lectionn�s
indice=0;
for i=1:nbg
    indice=[indice ; strmatch(deblank(code(no(i),:)),don.i(:,dep:(dep-1+size(deblank(code(no(i),:)),2))))];
end;

indice=indice(2:length(indice));

if length(indice)==0
    message='pas de nom d''individu contenant le(s) code(s) ';
    for i=1:nbg
        message =[message '<' code(no(i),:) '> '];
    end;
    errordlg(message)
    return;
end;

% s�lection
res.d=don.d(indice,:);
res.i=don.i(indice,:);
res.v=don.v;

% sauvegarde
don=res;
clear res;

for i=1:nbg
    nom=strcat(strrep(nom,'.txt',''),'.',code(no(i),:));
end;
writeDIV(don,nom);

cd(porig);
    



