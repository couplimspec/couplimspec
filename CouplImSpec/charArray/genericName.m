  function [genname,start]=genericName(fname)
 %% description
    % define generic name in a char array
    
%% input
    %  fname : char array , i.e. : filename
    
%% output
    %   genname : char
    %   start : strating index of genname in fname
    
%% use
    % [fname,rfolder]=uigetfile({'*.dso.mat'},'name of first dso images : ','*.dso.mat');
    % [genname,start]=genericName(fname)
    
%% Comments
    % written to manage series of filename and their rois
    
%% Author
    % MF Devaux
    % BIA - PVPP
    
%% date
    % 17 novembre 2020

%% context variables 
orig=pwd;           % returns the current directory

%% start

if nargin >1
    error('use: [genname,start]=genericName(fname)');
end


%% treatement

% generic name
ln=length(fname);
i=1;
while ln-i-3>=3
    listenom{i}=fname(1:(ln-i-3)); %#ok<AGROW>
    i=i+1;
end
listenom{i}='other';
[selection ]=listdlg('liststring',listenom,'selectionmode','single','initialvalue',3,'name','generic name','promptstring','file generic name');
genname=char(listenom(selection));
if strcmp(genname,'other')
    genname=char(inputdlg('file generic name','generic name',1,{fname(1:(ln-4))}));
end


start=strfind(fname,genname);
if length(start)>1
    start=inputNumber(sprintf('more than one position for generic Name. Please, give starting indice of generic name in %s',filename));
end

%% matlab function tracking  
% no function tracking
%% end

cd (orig)
