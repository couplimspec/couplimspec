function [indice,message]=comparerLabel(lab1,lab2)

 %% description
    % compare two lists of char arrays: find indice of each lab2 string in lab1 
    
%% input

    %  lab1 and lab2 : the two vector of char arrays. Could be char or cell
    %  arrays
    %  lab2 must be os smaller dimension than lab1
    
%% output
    
    %   indice : indice of lab2 string found in lab1

%% principe
    % compare the two tables until the end of lab2
    % return the values of corresponding indices  found in lab1
    
%% use
    %  indice=comparerLabel(lab1,lab2)
    
%% Comments
    % 
    
    
%% Author
    % MF Devaux
    % INRA - BIA - PVPP
    %
    
%% date
    % 17 decembre 2018

%% context variables 
orig=pwd;           % returns the current directory

message='OK';

%% start

if nargin >2
    error('use:  indice=comparerLabel(lab1,lab2)');
end



%% treatement
nl1=size(lab1,1);
nl2=size(lab2,1);


indice=zeros(size(nl2,1),1);

j=0;
for i=1:size(lab2,1)
    trouve=0;
    while ~trouve && j<=nl1
        j=j+1;
        trouve=strcmp(lab1(j,:),lab2(i,:));
    end
    
    if trouve
        indice(i)=j;
    else
        message=['label ' lab2(i,:) ' not found in first table'];
    end
    
    if j==nl1 && i~=nl2
        message='some labels in second output not found in that of the first output';
    end
end
        
        
    
    


% if nl1<nl2 
%     error('number of labels in second input ust be lower than in the first one');
% end
% 
% ci=find(strcmp(cellstr(lab1(1:nl2,:)),cellstr(lab2)));
% cis1=ci(end)+1;
% 
% if length(ci)==nl2
%     indice=1:nl2;
%     fini=1;
% else
%     if nl1==nl2
%         message='some labels in second output not found in that of the first output';
%     else
%         fini=0;
%         while ~fini
%             if ~exist('indice','var')
%                 indice=ci;
%             else
%                 indice=[indice;ci+cis1];
%                 lab2=lab2((ci+1):end,:);
%                 lab1=lab1((cis1+1):end,:);
%                 
%                 [ci,message]=comparerLabel(lab1(1:size(lab2,1),:),lab2);
%             
        
        
        



%% matlab function tracking  

% no tracking for this function

%% end

cd (orig)