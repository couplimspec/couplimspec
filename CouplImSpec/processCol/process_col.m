function process_col

 %% description
    % apply a function to all the files in a given folder
        
%% input

    %  no parameter
    
%% output
    
    % no output

%% principe
    % all the files in a folder are processed
    % 

%% use
    % process_col

    
%% Comments
    % written for F Allouche PhD thesis
    
    
%% Author
    % MF Devaux and F Allouche
    % BIA PVPP - INRA NAntes
    
%% date
    % 13 f�vrier 2014 : review for couplIMSpecToolbox
    % 4 mars 2011
    % 15 mars 2011 : ajout corriger bruit, selecvar, normalisation spatiale
                        %                   et spectrale
    % 8 aout 2011 pour images multispectrales de mani�re g�n�rales 
    % 1er mars 2013 : read_stif
    % 6 mars 2014 : couplImSPec

%% context variables 
orig=pwd;           % returns the current directory

warning ('off' ,'all');  % do not display warnings during collection processing

%% start

if nargin >0
    error('no parameter for this function');
end


%% input
% input function to apply

list_func={'import multivariate tif images';'read omnic map';'read spc map';'plot spectra from dataset object';...
    'compute grey level image from spectral image';'select a range of wavelength or wavenumbers';...
    'remove spikes';'remove noise by Fourier transform';...
    'joint spectral and spatial normalisation';...
    'calculate average spectra'};

funchandle={@stifRead;@readomnic;@readspc;@plotdso;@showspectrimage;@selecvar;...
    @removeSpike;@corriger_bruit_fft;@spespa_norm;@moyspectres};
codefunc={'STIFREAD';'READOMNIC';'READSPC';'PLOTDSO';'SHOWSPECTRIMAGE';'SELECVAR';...
    'CORRIGER_SPIKE';'CORRIGER_BRUIT_FFT';'SPESPANORM';'AVERAGESPECTRA'};
[numfunc,ok] = listdlg('ListString',list_func, 'SelectionMode','single','PromptString','choose process',...
    'name','process collections of data','listsize',[300 160]);
if ok 
    func=funchandle{numfunc};
    code=codefunc{numfunc};
else if isempty(numfunc)  % cancel of no method chosen
        disp('process choice cancelled in process_col');
        return;
    end;
end;

%% specific inputs 

% input file list
switch code
    case {'STIFREAD'}
        ext='.tif';
    case {'READOMNIC','READSPC'} % read *.tif or *.map or *.spc
        [fname,rfolder]=uigetfile({'*.tif;*.spc;*.map';},'name of first tif file, map or spc file to convert');
        [~,name,ext]=fileparts(fname);
    otherwise % process dso files
        % input image
        [fname,rfolder]=uigetfile({'*.dso.mat'},'name of first spectral image','*.dso.mat');
        ext='.dso.mat';
        name=strrep(fname,ext,'');
end;

% process parameters 
switch code
       case {'STIFREAD'}
            [~,rfolder,sfolder,codeseq,codebf,method,codevar]=stifReadIOParam;
       case {'PLOTDSO'}  % plot spectra
            sauve=1;
        case {'SHOWSPECTRIMAGE'}
            cd(rfolder)
            load(fname);
            [method,lo,exts]=choix_param_showspectrimage(dso);
            clear dso;
        case {'SELECVAR'}
            cd(rfolder)
            load(fname);
            [lo]=choix_param_selecvar(dso);
            nlo=lower(dso.title{2});
            unit=dso.userdata.acquisition.unit;
            clear dso;
        case {'CORRIGER_SPIKE'}      % remove spikes
        % parameter of spike correction
            [largeur,seuil]=choix_param_corriger_spike;
        case {'CORRIGER_BRUIT_FFT'}      % remove noise
        % parameter of spike correction
            [largeur]=choix_param_corriger_bruit_fft;
        case {'SPESPANORM'}
            cd(rfolder)
            load(fname);
            [method,lo]=choix_param_methodlo_spespanorm(dso);
            [taille]=choix_param_taille_spespanorm;
            [fMin]=choix_param_factMin_spespanorm;

            unit=dso.userdata.acquisition.unit;
            clear dso;
end;

    
% ouput folder
switch code
    case {'STIFREAD'}
        exts='.dso.mat';
    case {'READOMNIC','READSPC'} % read *.map or *.spc
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.dso.mat'));
        exts='.dso.mat';
    case {'PLOTDSO'}      % plot spectra in tif files
        cd(rfolder)
        sfolder=strcat(rfolder,'plotdso');      % impose save folder as a sub-folder of the input folder
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
        exts='.tif';
    case {'SHOWSPECTRIMAGE'}
        cd(rfolder)
        sfolder=strcat(rfolder,'showspectrim');      % impose save folder as a sub-folder of the input folder
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
    case {'SELECVAR'}      % select a range of wavelength or wavenumbers
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.sv.dso.mat'));
        exts='.sv.dso.mat';
    case {'CORRIGER_SPIKE'}      % remove spikes
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.spi.dso.mat'));
        exts='.spi.dso.mat';
    case {'CORRIGER_BRUIT_FFT'}      % remove noise
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.ltf.dso.mat'));
        exts='.ltf.dso.mat';
    case {'SPESPANORM'}      % remove noise
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.norm.dso.mat'));
        exts='.norm.dso.mat';
    case {'AVERAGESPECTRA'}      % caluclate average spectra
        cd(rfolder)
        sfolder=strcat(rfolder,'average');      % impose save folder as a sub-folder of the input folder
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
        exts='.average.dso.mat';
        
end;
    


%% treatement

% input file list to process
cd(rfolder);
switch code
    case {'STIFREAD'}
        liste=dir(['*' codeseq{1} ext]);
    otherwise
        liste=dir(['*' ext]);
end

nbf=length(liste);

% for each file, process
for i=1:nbf
    cd(rfolder)
    disp(liste(i).name);
    switch code
        case {'STIFREAD'}
            fnamegen=strrep(liste(i).name,ext,'');
            fnamegen=strrep(fnamegen,codeseq{1},'');
            func(fnamegen,rfolder,sfolder,codeseq,codebf,regbf,method,codevar,pixSize);
        case {'READOMNIC','READSPC'}
            func(liste(i).name,sfolder);
        case {'PLOTDSO'}
            func(liste(i).name,0,sauve,sfolder);
        case {'SHOWSPECTRIMAGE'}
            switch method
                case {'SUM','NORM'}
                    func(liste(i).name,method,sfolder);
                 case {'BAND','WAVELENGTH'} 
                    func(liste(i).name,method,lo,sfolder);
            end;
        case {'SELECVAR'}
            func(liste(i).name,lo,sfolder);
        case {'CORRIGER_SPIKE'}
            func(liste(i).name,largeur,seuil,sfolder);
            if i==1
                if ~exist(strcat(sfolder,'plotdso'),'dir')
                    mkdir(strcat(sfolder,'plotdso'))
                end;
            end;
            cd(sfolder)
            plotdso(strrep(liste(i).name,ext,exts),0,1,strcat(sfolder,'plotdso'))
         case {'CORRIGER_BRUIT_FFT'}
            func(liste(i).name,largeur,sfolder);
            if i==1
                if ~exist(strcat(sfolder,'plotdso'),'dir')
                    mkdir(strcat(sfolder,'plotdso'))
                end;
            end;
            cd(sfolder)
            plotdso(strrep(liste(i).name,ext,exts),0,1,strcat(sfolder,'plotdso'))
         case {'SPESPANORM'}
             switch method
                 case {'SUM','SUMPOS','NORM'}
                    func(liste(i).name,method,taille,fMin,sfolder);
                 case {'BAND','WAVELENGTH'}
                    func(liste(i).name,method,lo,taille,fMin,sfolder);
             end;
             if i==1
                if ~exist(strcat(sfolder,'plotdso'),'dir')
                    mkdir(strcat(sfolder,'plotdso'))
                end;
            end;
            cd(sfolder)
            plotdso(strrep(liste(i).name,ext,exts),0,1,strcat(sfolder,'plotdso'))
        case {'AVERAGESPECTRA'}
            func(liste(i).name,sfolder);
    end;
   
end;
            
            
%% matlab function tracking  
cd(sfolder)
tname=strcat(func2str(func),'.track.txt');
fid=fopen(tname,'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,' - Synchrotron project - process collection of files \r\n');

info=which(func2str(func));
[repfunc,funcname]=fileparts(info);
res=dir(info);

fprintf(fid,'\r\nFunction: %s: %s.m\r\n',list_func{numfunc},funcname);
fprintf(fid,'\r\n');
fprintf(fid,'created on %s \r\n',res.date);
fprintf(fid,'saved in folder: %s\r\n',repfunc);
fprintf(fid,'__________________________________________________________________________\r\n\r\n');

% processed files
fprintf(fid,'Input files folder: %s\r\n',rfolder);
fprintf(fid,'Files type: *%s\r\n\r\n',ext);

% process parameters 
switch code
    case {'STIFREAD'}
        fprintf(fid,'Process parameters:\r\n');
        fprintf(fid,'Imported images in dso files :\r\n');
        for i=1:length(codeseq)
            fprintf(fid,'\t - %s\r\n',codeseq{i});
        end
        fprintf(fid,'\r\n');
    case {'SELECVAR'}           % select a range of wavelenght or wavenumbers
        fprintf(fid,'Process parameters:\r\n');
        fprintf(fid,'selection of %s between %d and %d %s \r\n\r\n',nlo,round(lo(1)),round(lo(2)),unit);
    case {'CORRIGER_SPIKE'}      % remove spikes
        fprintf(fid,'Process parameters:\r\n');
        % parameter of spike correction
        fprintf(fid,'\tLargeur = %d\r\n\tSeuil d''intensit� = %d\r\n\r\n',largeur,seuil);
        fprintf(fid,'plot of corrected spectra are saved in folder %s\r\n\r\n',strcat(sfolder,'plotdso'));
    case {'CORRIGER_BRUIT_FFT'}      % remove noise by smoothing the Fourier transform
        fprintf(fid,'Process parameters:\r\n');
        % parameter of noise correction
        fprintf(fid,'\tLargeur du filtre gaussien = %d\r\n\r\n',largeur);
        fprintf(fid,'plot of corrected spectra are saved in folder %s\r\n\r\n',strcat(sfolder,'plotdso'));        
    case {'SPESPANORM'}      % remove noise by smoothing the Fourier transform
        fprintf(fid,'Process parameters:\r\n');
        % parameter of normalisation 
        switch method
            case 'SUM'
                fprintf(fid,'\tnormalisation base on sum of intensity \r\n');
            case 'SUMPOS'
                fprintf(fid,'\tnormalisation base on sum of positive intensities \r\n');
            case 'NORM'
                fprintf(fid,'\tnormalisation base on image os spectra norm\r\n');
            case 'BAND'
                fprintf(fid,'\tnormalisation base on sum of intensity between %d and %d %s \r\n',round(lo(1)),round(lo(2)),unit);
            case 'WAVELENGTH'
                fprintf(fid,'\tnormalisation base on intensity at %d %s \r\n',round(lo),unit);
        end;
        fprintf(fid,'\ttaille de lissage = %d\r\n',taille);
        fprintf(fid,'\tfacteur to assess the minima intensity f=%d \r\n',fMin);

        fprintf(fid,'plot of corrected spectra are saved in folder %s\r\n\r\n',strcat(sfolder,'plotdso'));        
end;
    
% ouput folder
fprintf(fid,'Output folder: %s\r\n',sfolder);
fprintf(fid,'Files type: *%s\r\n\r\n',exts);

% save function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    
end



%%
function [largeur,seuil]=choix_param_corriger_spike

% interactive choice of spike correcfion parameter

% defaulf values
largeur=21;
seuil=40;

% choose parameter
prompt = {'taille de la fen�tre (nombre impair >0 et <200) :','seuil d''intensit� (>0) :'};
dlg_title = 'param�tres du chapeau haut de forme';
num_lines = 1;
def = {num2str(largeur),num2str(seuil)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    ss=str2num(reponse{2}); %#ok<ST2NM>
    if ll <0 || ll > 200
        errordlg('largeur incorrecte')
        suitel=0;
    else
        largeur=ll;
        suitel=1;
    end;
    if ss <0
        errordlg('seuil incorrect')
        suites=0;
    else
        suites=1;
        seuil=ss;
    end;
    if suitel && suites
        suite=1;
    else 
        suite=0;
    end;
end;

end  
  
%% SHOWSPECTRIMAGE
function [method,lo,exts]=choix_param_showspectrimage(ims)
% interactive choice of parameter for showspectrimage
        
        
% choice of the method
choix={'one wavelength','sum between two wavelengths', 'sum of intensities', 'norm of intensities'};
codemethod={'WAVELENGTH','BAND','SUM','NORM'};
[nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
    'name','method to compute grey level image','listsize',[300 160]);
if ok 
    method=codemethod{nummethod};
else if isempty(nummethod)  % cancel of no method chosen
        disp('method choice cancelled showspectrimage');
        return;
    end;
end;
        
% according to the method wavelengths are required :
switch method
  case 'WAVELENGTH'
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(28)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,y]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x)) ' ' ims.userdata.unit])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;

        hold off
    end;
    lo=x;
    clear x
    clear y

    silo=lot-repmat(lo,1,length(lot));
    indicelo= abs(silo)==min(abs(silo));
    lo=lot(indicelo);



  case 'BAND'
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(1)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,y]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(1)=x;
    clear x
    clear y
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x,y]=ginput(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(2)=x;
    clear x
    clear y                
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));

    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));

    clear silo
    
    otherwise
        lo=0;
end;
                
            

    
%exts
switch method
    case 'WAVELENGTH'
        exts=strcat('.w',num2str(round(lo)),'.tif');
    case 'BAND'
        exts=strcat('.w',num2str(round(lo(1))),'-',num2str(round(lo(2))),'.tif');
    case 'SUM'
        exts='.sum.tif';
    case 'NORM'
        exts='.norm.tif';
end;

end  

%% sub- functions --------------------------------------------------

function [largeur]=choix_param_corriger_bruit_fft

% interactive choice of spike correcfion parameter

% defaulf values
largeur=21;

prompt = {'taille de la fen�tre (nombre impair >0) :'};
dlg_title = 'param�tres du lissage des spectres par transform�e de Fourier';
num_lines = 1;
def = {num2str(largeur)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if (ll-round(ll/2)*2)==0 || ll<0
        %ll <0 || ll > 200
        errordlg('largeur incorrecte : donner un nombre impair positif')
    else
        largeur=ll;
        suite=1;
    end;
end;

end

%% sub - functions
function [lo]=choix_param_selecvar(ims)

   % selection of the wavelengths 
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(1)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.acquisition.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,y]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=0;
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(round(x))});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(1)=x;
    clear x
    clear y
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.acquisition.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x,y]=ginput(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=0;
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(round(x))});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(2)=x;
    clear x
    clear y                
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));

    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));

    clear silo
    clear indicelo
end


function [method,lo]=choix_param_methodlo_spespanorm(ims)
% interactive choice of parameter for showspectrimage
        
        
% choice of the method
choix={'sum of intensities', 'sum of positive intensities', 'norm of intensities','one wavelength','sum between two wavelengths'};
codemethod={'SUM','SUMPOS','NORM','BAND','WAVELENGTH'};
[nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
    'name','normalisation image','listsize',[300 160]);
if ok 
    method=codemethod{nummethod};
else if isempty(nummethod)  % cancel of no method chosen
        disp('method choice cancelled showspectrimage');
        return;
    end;
end;
        
% according to the method wavelengths are required :
switch method
    case {'SUM','SUMPOS','NORM'}
        lo=0;
        indicelo=0;
    case 'WAVELENGTH'
        lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
        figure(28)                       % graphes de la s�rie des spectres
        ok=0;
        while ~ok
            nbs=size(ims.data,1);
            st=round(nbs/100);
            plot(lot,ims.data(1:st:nbs,:)); 
            if strcmp(ims.title{2},'Wavenumber')
                set(gca,'Xdir','reverse');
            end;
            xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
            ylabel('intensity', 'fontsize',16)
            [x,y]=ginput(1);
            hold on
            plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
            title(['selected ' ims.title{2} ': ' num2str(round(x)) ' ' ims.userdata.unit])
            ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
            if ~ok
                los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                if isempty(los)
                    disp('no wavelength chosen');
                    return;
                end;
                x=str2double(los);

                if x>min(lot) && x<max(lot)
                    ok=1;
                else
                    ok=ouinon('valeur erron�e, voulez vous continuer ?');
                    if ~ok
                        error('choice of %s cancelled in showspectrimage',ims.title{2});
                    else
                        ok=0;
                    end;
                end;
            end;

            hold off
        end;
        lo=x;
        clear x
        clear y

        silo=lot-repmat(lo,1,length(lot));
        indicelo= abs(silo)==min(abs(silo));
        lo=lot(indicelo);



  case 'BAND'
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(1)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,y]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(1)=x;
    clear x
    clear y
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x,y]=ginput(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(2)=x;
    clear x
    clear y                
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));

    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));

    clear silo
                
            
end;
    
end  

%% sub function
function [taille]=choix_param_taille_spespanorm


% defaulf values
taille=11;

prompt = {'taille du filtre moyen (nombre impair >0 et <200) :'};
dlg_title = 'lissage de l''image de normalisation';
num_lines = 1;
def = {num2str(taille)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if ll >0  && (ll-1)/2==round((ll-1)/2)
        taille=ll;
        suite=1;
    end;
end;

end

%% sub function
function [factMin]=choix_param_factMin_spespanorm

% defaulf values
factMin=20;
vMin=1;
vMax=99;

prompt = {'taille du facteur f � appliquer pour estimer l''intensit� des minimax relatifs (entre 1 et 99 %) :(Imax-Imin)/f'};
dlg_title = 'suppresion des minimas';
num_lines = 1;
def = {num2str(factMin)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if ll >=vMin && ll<=vMax
        factMin=ll;
        suite=1;
    end;
end;

end

