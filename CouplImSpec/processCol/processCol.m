function processCol

 %% description
    % apply a function to all the files in a given folder
        
%% input

    %  no parameter
    
%% output
    
    % no output

%% principe
    % all the files in a folder are processed
    % 

%% use
    % process_col

    
%% Comments
    % from process_col function
    % written for F Allouche PhD thesis
    
    
%% Author
    % MF Devaux and F Allouche
    % BIA PVPP - INRA NAntes
    
%% date
    % 30 juin 2017 : add spectral pyramids
    % 1 avril 2014 : imshowdso
    % 31 mars 2014 readStif
    % 13 f�vrier 2014 : review for couplIMSpecToolbox
    % 19 decembre 2019 : gestion small collection of spectra
    

%% context variables 
orig=pwd;           % returns the current directory

warning ('off' ,'all');  % do not display warnings during collection processing

%% start

if nargin >0
    error('no parameter for this function');
end


%% input
% input function to apply

list_func={'import multivariate tif images';'read omnic map';'read spc map';'plot spectra from dataset object';...
    'compute grey level image from spectral image';'select a range of wavelength or wavenumbers';...
    'remove spikes';'remove noise by Fourier transform';...
    'joint spectral and spatial normalisation';...
    'calculate average spectra';'compute spectral pyramids'};

funchandle={@readStif;@readomnic;@readspc;@plotdso;@imshowdso;@selecVar;...
    @corriger_spike;@corriger_bruit_fft;@spespa_norm;@meanSpectrum;@dsoSpectralRoiPyramid};
codefunc={'READSTIF';'READOMNIC';'READSPC';'PLOTDSO';'IMSHOWDSO';'SELECVAR';...
    'CORRIGER_SPIKE';'CORRIGER_BRUIT_FFT';'SPESPANORM';'AVERAGESPECTRA';'DSOSPECTRALROIPYRAMID'};
[numfunc,ok] = listdlg('ListString',list_func, 'SelectionMode','single','PromptString','choose process',...
    'name','process collections of data','listsize',[300 160]);
if ok 
    func=funchandle{numfunc};
    code=codefunc{numfunc};
else if isempty(numfunc)  % cancel of no method chosen
        disp('process choice cancelled in process_col');
        return;
    end;
end;

%% specific inputs 

% input file list
switch code
    case {'READSTIF'}
        ext='.tif';
    case {'READOMNIC','READSPC'} % read *.tif or *.map or *.spc
        [fname,rfolder]=uigetfile({'*.tif;*.spc;*.map';},'name of first tif file, map or spc file to convert');
        [~,name,ext]=fileparts(fname);
    otherwise % process dso files
        % input image
        [fname,rfolder]=uigetfile({'*.dso.mat'},'name of first dataset object','*.dso.mat');
        ext='.dso.mat';
        name=strrep(fname,ext,'');
end;

% process parameters 
switch code
       case {'READSTIF'}
            [method,rfolder,~,codeseq,codebf,regbf,pixSize,codevar,sfolder]=readStifIOParam;
       case {'PLOTDSO'}  % plot spectra
            sauve=1;
        case {'IMSHOWDSO'}
            cd(rfolder)
            dso=loaddso(fname);
            [method,lo]=imshowdsoIOParam(dso);
            clear dso;
        case {'SELECVAR'}
            cd(rfolder)
            load(fname);
            [lo]=choix_param_selecvar(dso);
            nlo=lower(dso.title{2});
            unit=dso.userdata.acquisition.unit;
            clear dso;
        case {'CORRIGER_SPIKE'}      % remove spikes
        % parameter of spike correction
            [largeur,seuil]=choix_param_corriger_spike;
        case {'CORRIGER_BRUIT_FFT'}      % remove noise
        % parameter of spike correction
            [largeur]=choix_param_corriger_bruit_fft;
        case {'SPESPANORM'}
            cd(rfolder)
            load(fname);
            [method,lo]=choix_param_methodlo_spespanorm(dso);
            [taille]=choix_param_taille_spespanorm;
            [fMin]=choix_param_factMin_spespanorm;

            unit=dso.userdata.acquisition.unit;
            clear dso;
end;

    
% ouput folder
switch code
    case {'READSTIF'}
        exts='.dso.mat';
    case {'READOMNIC','READSPC'} % read *.map or *.spc
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.dso.mat'));
        exts='.dso.mat';
    case {'PLOTDSO'}      % plot spectra in tif files
        cd(rfolder)
        sfolder=strcat(rfolder,'plotdso');      % impose save folder as a sub-folder of the input folder
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
        exts='.tif';
    case {'IMSHOWDSO'}
        cd(rfolder)
        sfolder=strcat(rfolder,'imshowdso');      % impose save folder as a sub-folder of the input folder
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
    case {'SELECVAR'}      % select a range of wavelength or wavenumbers
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.sv.dso.mat'));
        exts='.sv.dso.mat';
    case {'CORRIGER_SPIKE'}      % remove spikes
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.spi.dso.mat'));
        exts='.spi.dso.mat';
    case {'CORRIGER_BRUIT_FFT'}      % remove noise
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.ltf.dso.mat'));
        exts='.ltf.dso.mat';
    case {'SPESPANORM'}      % remove noise
        [~,sfolder] = uiputfile({'*.mat'},'save result in folder',strcat(name,'.norm.dso.mat'));
        exts='.norm.dso.mat';
    case {'AVERAGESPECTRA'}      % caluclate average spectra
        cd(rfolder)
        sfolder=strcat(rfolder,'average');      % impose save folder as a sub-folder of the input folder
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
        exts='.average.dso.mat';
    case {'DSOSPECTRALROIPYRAMID'}      % caluclate spectral pyramids
        cd(rfolder)
        cd ..
        sfolder='dsoSpectralRoiPyramid';      % impose save folder as a sub-folder at the same level than the input folder
        if ~exist(sfolder,'dir')
            mkdir(sfolder)
        end;
        cd(sfolder);
        sfolder=pwd;
        exts='.Pyr*.mat';
end;
    


%% treatement

% input file list to process
cd(rfolder);
switch code
    case {'READSTIF'}
        if ~isempty(codeseq{1})
            liste=dir(['*' codeseq{1} ext]);
            codefnamegen=codeseq{1};
        else
            if ~isempty(codebf)
                 liste=dir(['*' codebf ext]);
                 codefnamegen=codebf;
            else
                 liste=dir(['*' ext]);
                 codefnamegen='';
            end;
        end;
                
    otherwise
        liste=dir(['*' ext]);
end

nbf=length(liste);

% for each file, process
for i=1:nbf
    cd(rfolder)
    disp(liste(i).name);
    switch code
        case {'READSTIF'}
            fnamegen=strrep(liste(i).name,ext,'');
            fnamegen=strrep(fnamegen,codefnamegen,'');
            func(method,rfolder,fnamegen,codeseq,codebf,regbf,pixSize,codevar,sfolder);
        case {'READOMNIC','READSPC'}
            func(liste(i).name,sfolder);
        case {'PLOTDSO'}
            func(liste(i).name,0,sauve,sfolder);
        case {'IMSHOWDSO'}
            switch method
                case {'SUM','NORM'}
                    func(liste(i).name,method,sfolder);
                 case {'BAND','WAVELENGTH'} 
                    func(liste(i).name,method,lo,sfolder);
            end;
        case {'SELECVAR'}
            func(liste(i).name,lo,sfolder);
        case {'CORRIGER_SPIKE'}
            func(liste(i).name,largeur,seuil,sfolder);
            if i==1
                if ~exist(strcat(sfolder,'plotdso'),'dir')
                    mkdir(strcat(sfolder,'plotdso'))
                end;
            end;
            cd(sfolder)
            plotdso(strrep(liste(i).name,ext,exts),0,1,strcat(sfolder,'plotdso'))
         case {'CORRIGER_BRUIT_FFT'}
            func(liste(i).name,largeur,sfolder);
            if i==1
                if ~exist(strcat(sfolder,'plotdso'),'dir')
                    mkdir(strcat(sfolder,'plotdso'))
                end;
            end;
            cd(sfolder)
            plotdso(strrep(liste(i).name,ext,exts),0,1,strcat(sfolder,'plotdso'))
         case {'SPESPANORM'}
             switch method
                 case {'SUM','SUMPOS','NORM'}
                    func(liste(i).name,method,taille,fMin,sfolder);
                 case {'BAND','WAVELENGTH'}
                    func(liste(i).name,method,lo,taille,fMin,sfolder);
             end;
             if i==1
                if ~exist(strcat(sfolder,'plotdso'),'dir')
                    mkdir(strcat(sfolder,'plotdso'))
                end;
            end;
%             cd(sfolder)
%             plotdso(strrep(liste(i).name,ext,exts),0,1,strcat(sfolder,'plotdso'))
        case {'AVERAGESPECTRA'}
            func(liste(i).name,sfolder);
        case {'DSOSPECTRALROIPYRAMID'}
            func(liste(i).name,sfolder);
    end;
   
end;
            
            
%% matlab function tracking  
cd(sfolder)
tname=strcat(func2str(func),'.track.txt');
fid=fopen(tname,'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,' - Synchrotron project - process collection of files \r\n');

info=which(func2str(func));
[repfunc,funcname]=fileparts(info);
res=dir(info);

fprintf(fid,'\r\nFunction: %s: %s.m\r\n',list_func{numfunc},funcname);
fprintf(fid,'\r\n');
fprintf(fid,'created on %s \r\n',res.date);
fprintf(fid,'saved in folder: %s\r\n',repfunc);
fprintf(fid,'__________________________________________________________________________\r\n\r\n');

% processed files
fprintf(fid,'Input files folder: %s\r\n',rfolder);
fprintf(fid,'Files type: *%s\r\n\r\n',ext);

% process parameters 
switch code
    case {'READSTIF'}
        fprintf(fid,'Process parameters:\r\n');
        if isempty(codeseq{1})
            fprintf(fid,'\t - single multiple tiff images\r\n');
        else
            for i=1:length(codeseq)
                fprintf(fid,'\t - %s\r\n',codeseq{i});
            end
        end
        if ~isempty(codebf)
             fprintf(fid,'\t - code for brightfiled image : %s\r\n',codebf);
        end;
        fprintf(fid,'\r\n');
        fprintf(fid,'Imported images in dso files :\r\n');
        for i=1:nbf
            fnamegen=strrep(liste(i).name,ext,'');
            fnamegen=strrep(fnamegen,codefnamegen,'');
            fprintf(fid,'\t - %s\r\n',fnamegen);
        end;
        fprintf(fid,'\r\n');

    case {'SELECVAR'}           % select a range of wavelenght or wavenumbers
        fprintf(fid,'Process parameters:\r\n');
        fprintf(fid,'selection of %s between %d and %d %s \r\n\r\n',nlo,round(lo(1)),round(lo(2)),unit);
    case {'CORRIGER_SPIKE'}      % remove spikes
        fprintf(fid,'Process parameters:\r\n');
        % parameter of spike correction
        fprintf(fid,'\tLargeur = %d\r\n\tSeuil d''intensit� = %d\r\n\r\n',largeur,seuil);
        fprintf(fid,'plot of corrected spectra are saved in folder %s\r\n\r\n',strcat(sfolder,'plotdso'));
    case {'CORRIGER_BRUIT_FFT'}      % remove noise by smoothing the Fourier transform
        fprintf(fid,'Process parameters:\r\n');
        % parameter of noise correction
        fprintf(fid,'\tLargeur du filtre gaussien = %d\r\n\r\n',largeur);
        fprintf(fid,'plot of corrected spectra are saved in folder %s\r\n\r\n',strcat(sfolder,'plotdso'));        
    case {'SPESPANORM'}      % remove noise by smoothing the Fourier transform
        fprintf(fid,'Process parameters:\r\n');
        % parameter of normalisation
        switch method
            case 'SUM'
                fprintf(fid,'\tnormalisation base on sum of intensity \r\n');
            case 'SUMPOS'
                fprintf(fid,'\tnormalisation base on sum of positive intensities \r\n');
            case 'NORM'
                fprintf(fid,'\tnormalisation base on image os spectra norm\r\n');
            case 'BAND'
                fprintf(fid,'\tnormalisation base on sum of intensity between %d and %d %s \r\n',round(lo(1)),round(lo(2)),unit);
            case 'WAVELENGTH'
                fprintf(fid,'\tnormalisation base on intensity at %d %s \r\n',round(lo),unit);
        end;
        fprintf(fid,'\ttaille de lissage = %d\r\n',taille);
        fprintf(fid,'\tfacteur to assess the minima intensity f=%d \r\n',fMin);
        
        fprintf(fid,'plot of corrected spectra are saved in folder %s\r\n\r\n',strcat(sfolder,'plotdso'));
  
end;
    
% ouput folder
fprintf(fid,'Output folder: %s\r\n',sfolder);
fprintf(fid,'Files type: *%s\r\n\r\n',exts);

% save function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)
    
warning('on','all');

end


%% sub- functions --------------------------------------------------


%%
function [largeur,seuil]=choix_param_corriger_spike

% interactive choice of spike correcfion parameter

% defaulf values
largeur=21;
seuil=40;

% choose parameter
prompt = {'taille de la fen�tre (nombre impair >0 et <200) :','seuil d''intensit� (>0) :'};
dlg_title = 'param�tres du chapeau haut de forme';
num_lines = 1;
def = {num2str(largeur),num2str(seuil)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    ss=str2num(reponse{2}); %#ok<ST2NM>
    if ll <0 || ll > 200
        errordlg('largeur incorrecte')
        suitel=0;
    else
        largeur=ll;
        suitel=1;
    end;
    if ss <0
        errordlg('seuil incorrect')
        suites=0;
    else
        suites=1;
        seuil=ss;
    end;
    if suitel && suites
        suite=1;
    else 
        suite=0;
    end;
end;

end  
  

function [largeur]=choix_param_corriger_bruit_fft

% interactive choice of spike correcfion parameter

% defaulf values
largeur=21;

prompt = {'taille de la fen�tre (nombre impair >0) :'};
dlg_title = 'param�tres du lissage des spectres par transform�e de Fourier';
num_lines = 1;
def = {num2str(largeur)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if (ll-round(ll/2)*2)==0 || ll<0
        %ll <0 || ll > 200
        errordlg('largeur incorrecte : donner un nombre impair positif')
    else
        largeur=ll;
        suite=1;
    end;
end;

end

%% sub - functions
function [lo]=choix_param_selecvar(ims)

   % selection of the wavelengths 
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(1)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        if nbs>1000
            st=round(nbs/100);
        else
            st=1;
        end
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.acquisition.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,y]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=0;
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(round(x))});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(1)=x;
    clear x
    clear y
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        if nbs>1000
            st=round(nbs/100);
        else
            st=1;
        end
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.acquisition.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x,y]=ginput(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=0;
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(round(x))});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(2)=x;
    clear x
    clear y                
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));

    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));

    clear silo
    clear indicelo
end


function [method,lo]=choix_param_methodlo_spespanorm(ims)
% interactive choice of parameter for showspectrimage
        
        
% choice of the method
choix={'sum of intensities', 'sum of positive intensities', 'norm of intensities','one wavelength','sum between two wavelengths'};
codemethod={'SUM','SUMPOS','NORM','BAND','WAVELENGTH'};
[nummethod,ok] = listdlg('ListString',choix, 'SelectionMode','single','PromptString','choose method',...
    'name','normalisation image','listsize',[300 160]);
if ok 
    method=codemethod{nummethod};
else if isempty(nummethod)  % cancel of no method chosen
        disp('method choice cancelled showspectrimage');
        return;
    end;
end;
        
% according to the method wavelengths are required :
switch method
    case {'SUM','SUMPOS','NORM'}
        lo=0;
        indicelo=0;
    case 'WAVELENGTH'
        lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
        figure(28)                       % graphes de la s�rie des spectres
        ok=0;
        while ~ok
            nbs=size(ims.data,1);
            st=round(nbs/100);
            plot(lot,ims.data(1:st:nbs,:)); 
            if strcmp(ims.title{2},'Wavenumber')
                set(gca,'Xdir','reverse');
            end;
            xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
            ylabel('intensity', 'fontsize',16)
            [x,y]=ginput(1);
            hold on
            plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
            title(['selected ' ims.title{2} ': ' num2str(round(x)) ' ' ims.userdata.unit])
            ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
            if ~ok
                los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
                if isempty(los)
                    disp('no wavelength chosen');
                    return;
                end;
                x=str2double(los);

                if x>min(lot) && x<max(lot)
                    ok=1;
                else
                    ok=ouinon('valeur erron�e, voulez vous continuer ?');
                    if ~ok
                        error('choice of %s cancelled in showspectrimage',ims.title{2});
                    else
                        ok=0;
                    end;
                end;
            end;

            hold off
        end;
        lo=x;
        clear x
        clear y

        silo=lot-repmat(lo,1,length(lot));
        indicelo= abs(silo)==min(abs(silo));
        lo=lot(indicelo);



  case 'BAND'
    lot=ims.axisscale{2};           % liste des longueurs d'onde de l'image spectrale
    figure(1)                       % graphes de la s�rie des spectres
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        [x,y]=ginput(1);
        hold on
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(1)=x;
    clear x
    clear y
    ok=0;
    while ~ok
        nbs=size(ims.data,1);
        st=round(nbs/100);
        plot(lot,ims.data(1:st:nbs,:)); 
        if strcmp(ims.title{2},'Wavenumber')
            set(gca,'Xdir','reverse');
        end;
        xlabel([ims.title{2} ' (' ims.userdata.unit ')'], 'fontsize',16)
        ylabel('intensity', 'fontsize',16)
        hold on
        plot([lo(1) lo(1)],[min(ims.data(:)) max(ims.data(:))],':k')

        [x,y]=ginput(1);
        plot([x x],[min(ims.data(:)) max(ims.data(:))],':k')
        title(['selected ' ims.title{2} ': ' num2str(round(lo(1))) ' - ' num2str(round(x))])
        ok=ouinon(sprintf('selected %s : %d %s  Ok ?',ims.title{2},round(x),ims.userdata.unit));
        if ~ok
            los = inputdlg(sprintf('enter %s',ims.title{2}),sprintf('choose %s',ims.title{2}),1,{num2str(x)});
            if isempty(los)
                disp('no wavelength chosen');
                return;
            end;
            x=str2double(los);

            if x>min(lot) && x<max(lot)
                ok=1;
            else
                ok=ouinon('valeur erron�e, voulez vous continuer ?');
                if ~ok
                    error('choice of %s cancelled in showspectrimage',ims.title{2});
                else
                    ok=0;
                end;
            end;
        end;
        hold off
    end;
    lo(2)=x;
    clear x
    clear y                
    silo=lot-repmat(lo(1),1,length(lot));
    indicelo(1)=find(abs(silo)==min(abs(silo)));
    lo(1)=lot(indicelo(1));

    silo=lot-repmat(lo(2),1,length(lot));
    indicelo(2)=find(abs(silo)==min(abs(silo)));
    lo(2)=lot(indicelo(2));

    clear silo
                
            
end;
    
end  

%% sub function
function [taille]=choix_param_taille_spespanorm


% defaulf values
taille=11;

prompt = {'taille du filtre moyen (nombre impair >0 et <200) :'};
dlg_title = 'lissage de l''image de normalisation';
num_lines = 1;
def = {num2str(taille)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if ll >0  && (ll-1)/2==round((ll-1)/2)
        taille=ll;
        suite=1;
    end;
end;

end

%% sub function
function [factMin]=choix_param_factMin_spespanorm

% defaulf values
factMin=20;
vMin=1;
vMax=99;

prompt = {'taille du facteur f � appliquer pour estimer l''intensit� des minimax relatifs (entre 1 et 99 %) :(Imax-Imin)/f'};
dlg_title = 'suppresion des minimas';
num_lines = 1;
def = {num2str(factMin)};
options.Resize='on';
suite=0;
while ~suite
    reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
    ll=str2num(reponse{1}); %#ok<ST2NM>
    if ll >=vMin && ll<=vMax
        factMin=ll;
        suite=1;
    end;
end;

end

