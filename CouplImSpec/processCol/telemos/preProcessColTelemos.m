function preProcessColTelemos

%% description
% apply a function to all the files in a given folder

%% input

%  no parameter

%% output

% no output

%% principe
% all the files in a folder are processed
%

%% use
% process_col


%% Comments
% from process_col function
% written for F Allouche PhD thesis


%% Author
% MF Devaux and F Allouche
% BIA PVPP - INRA NAntes

%% date
% 29 octobre 2015 : only for pre Processing of spectral images
% 8 decembre 2015 : for telemos image only

%% context variables
orig=pwd;           % returns the current directory

warning ('off' ,'all');  % do not display warnings during collection processing

%% start

if nargin >0
    error('no parameter for this function');
end


%% input
% input function to apply

list_func={'Telemos image filtering by Fourier Transform'};

funchandle={@fftFilterTelemos};
codefunc={'FFTFILTERTELEMOS'};
[numfunc,ok] = listdlg('ListString',list_func, 'SelectionMode','single','PromptString','choose process',...
    'name','process collections of data','listsize',[300 160]);
if ok
    func=funchandle{numfunc};
    code=codefunc{numfunc};
else if isempty(numfunc)  % cancel of no method chosen
        disp('process choice cancelled in preProcessColTelemos');
        return;
    end;
end;

%% specific inputs

% input file list
[~,rfolder]=uigetfile({'*.tif'},'name of first image','*.tif');
ext='.tif';

% input file list to process
cd(rfolder);
liste=dir(['*' ext]);
nbf=length(liste);


% process parameters
switch code
    case {'FFTFILTERTELEMOS'}
        WsizeHF=inputParamFFTfilterTelemos;
        
end;


% ouput folder
switch code
    case {'FFTFILTERTELEMOS'}      % spectral image median filtering
        cd(rfolder)
        [~,f]=fileparts(fileparts(rfolder));
        
        cd ..
        
        if ~exist(strcat(f,'.fftTELEMOS'),'dir')
            mkdir(strcat(f,'.fftTELEMOS'))
        end;
        cd( strcat(f,'.fftTELEMOS'))
        sfolder=pwd;
        
        exts='.fftTELEMOS.tif';
        
end;



%% treatement


% for each file, process
for i=1:nbf
    cd(rfolder)
    disp(liste(i).name);
    switch code
        case {'FFTFILTERTELEMOS'}
            func(liste(i).name,WsizeHF,sfolder);
    end;
end;

%% save



%% matlab function tracking
cd(sfolder)
tname=strcat(func2str(func),'.track.txt');
fid=fopen(tname,'w');

if fid==0
    errordlg('enable to open track file');
end;

fprintf(fid,'\r\n%s\t',datestr(now,0));
fprintf(fid,' - Preprocess collection of files \r\n');

info=which(func2str(func));
[repfunc,funcname]=fileparts(info);
res=dir(info);

fprintf(fid,'\r\nFunction: %s: %s.m\r\n',list_func{numfunc},funcname);
fprintf(fid,'\r\n');
fprintf(fid,'created on %s \r\n',res.date);
fprintf(fid,'saved in folder: %s\r\n',repfunc);
fprintf(fid,'__________________________________________________________________________\r\n\r\n');

% processed files
fprintf(fid,'Input files folder: %s\r\n',rfolder);
fprintf(fid,'Files type: *%s\r\n\r\n',ext);

% process parameters
switch code
    case {'FFTFILTERTELEMOS'}           % spectral image median filtering
        if WsizeHF>0
            fprintf(fid,'Process parameters:\r\n');
            fprintf(fid,'size of high frequency filter  %d \r\n\r\n',WsizeHF);
        else
            fprintf(fid,'No high frequency filtering:\r\n');
        end;
end;

% ouput folder
fprintf(fid,'Output folder: %s\r\n',sfolder);
fprintf(fid,'Files type: *%s\r\n\r\n',exts);

% save function used
fprintf(fid,'__________________________________________________________________________\r\n');
info=which (mfilename);
repprog=fileparts(info);
fprintf(fid,'function name: %s ',mfilename);
res=dir(info);
fprintf(fid,'on %s \r\n',res.date);
fprintf(fid,'function folder: %s \r\n',repprog);
%fprintf(fid,'__________________________________________________________________________\r\n');

fclose(fid);

%% end

cd (orig)

warning('on','all');

end


%% sub- functions --------------------------------------------------


function [WsizeHF]=inputParamFFTfilterTelemos

% interactive choice of noise correcfion parameter
rNoise=yesno('remove high frequencey noise ?');

if rNoise
    
    % defaulf values
    WsizeHF=150;
    
    prompt = {'taille de la fen�tre (>0) :'};
    dlg_title = 'param�tres du lissage du bruit des image TELEMOS par transform�e de Fourier';
    num_lines = 1;
    def = {num2str(WsizeHF)};
    options.Resize='on';
    suite=0;
    while ~suite
        reponse = inputdlg(prompt,dlg_title,num_lines,def,options);
        ll=str2double(reponse{1});
        if ll<0
            errordlg('largeur incorrecte : donner un nombre  positif')
        else
            WsizeHF=ll;
            suite=1;
        end;
    end;
else
    WsizeHF=0;
end

end

